<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/flag.css',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
        'css/bootstrap.min.css',
        'font-awesome/css/font-awesome.css',
        'css/plugins/toastr/toastr.min.css',
        'js/plugins/gritter/jquery.gritter.css',
        'css/animate.css',
        'css/style.css',
        'css/aciTree.css',
        'css/demo.css',
        'css/menu.css',
        'css/form.css',
        'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css',

    ];
    public $js = [

        //<!-- Mainly scripts -->
//        "js/jquery-3.1.1.min.js",
//        "js/bootstrap.min.js",
        "js/plugins/metisMenu/jquery.metisMenu.js",
        "js/plugins/slimscroll/jquery.slimscroll.min.js",

        //<!-- Flot -->
        "js/plugins/flot/jquery.flot.js",
        "js/plugins/flot/jquery.flot.tooltip.min.js",
        "js/plugins/flot/jquery.flot.spline.js",
        "js/plugins/flot/jquery.flot.resize.js",
        "js/plugins/flot/jquery.flot.pie.js",

        //<!-- Peity -->
        "js/plugins/peity/jquery.peity.min.js",
        "js/demo/peity-demo.js",


        //<!-- Custom and plugin javascript -->
        "js/inspinia.js",
        "js/plugins/pace/pace.min.js",

        //<!-- jQuery UI -->
        "js/plugins/jquery-ui/jquery-ui.min.js",

        //<!-- GITTER -->
        "js/plugins/gritter/jquery.gritter.min.js",

        //<!-- Sparkline -->
        "js/plugins/sparkline/jquery.sparkline.min.js",

        //<!-- Sparkline demo data  -->
        "js/demo/sparkline-demo.js",

        //<!-- ChartJS-->
        "js/plugins//chartJs/Chart.min.js",

        //<!-- Toastr -->
        "js/plugins/toastr/toastr.min.js",

        "js/jquery.aciPlugin.min.js",
        "js/jquery.aciTree.dom.js",
        "js/jquery.aciTree.core.js",
        "js/jquery.aciTree.selectable.js",
        "js/jquery.aciTree.radio.js",
        "js/jquery.aciTree.checkbox.js",

        "js/jquery.scrollUp.min.js",
        "js/jquery.prettyPhoto.js",
        "js/jquery.dcjqaccordion.2.7.js",
        "js/jquery.cookie.js",
        "js/main.js",
        "js/validator.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js",


    ];
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset'
    ];
}
