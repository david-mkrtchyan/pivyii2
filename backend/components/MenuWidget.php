<?php


namespace backend\components;

use common\models\Category;
use yii;


class MenuWidget extends \yii\base\Widget

{
    public $tpl;
    public $data;
    public $tree;
    public $menuHtml;

    public function init(){
        parent::init();
        if($this->tpl===null){
            $this->tpl = 'manu' ;
        }
        $this->tpl .= '.php';
    }
    public function run() {

        //get cashe
      /*  $menu = Yii::$app->cache->get('manu1');
        if($menu) return $menu;*/
        $this->data = Category::find()->indexBy('id')->asArray()->all();
        $this->tree = $this->getTree();

        $this->menuHtml=$this->getMneuHtml($this->tree);
        //set cashe
       // $menu = Yii::$app->cache->set('manu1',$this->menuHtml ,60);

        //debug($this->tree);
        return $this->menuHtml;
    }

    protected function getTree()
    {
       return Category::buildTreeHierarchy(Category::find()->asArray()->all());
    }

    protected function getMneuHtml($tree){
        $str = '';
        foreach($tree as $category){
            $str .= $this->catToTemplate($category);
        }
        return $str;
    }

    protected function catToTemplate($category){
        include __DIR__.'/menu_tpl/'.$this->tpl;
    }

}