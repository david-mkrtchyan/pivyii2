<li>
    <a href="<?= \yii\helpers\Url::to(['product/create', 'id' => $category['id']]) ?>">
        <?php echo $category['name_' . Yii::$app->language]; ?>
        <?php if (isset($category['children'])): ?>
            <span class="badge pull-right"> <i class="fa fa-plus"></i> </span>
        <?php endif; ?>
    </a>
    <?php if (isset($category['children'])): ?>
        <ul>
            <?= $this->getMneuHtml($category['children']) ?>
        </ul>
    <?php endif; ?>
</li>
