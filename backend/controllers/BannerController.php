<?php

namespace backend\controllers;

use common\models\BannerPlacement;
use Yii;
use common\models\Banner;
use common\models\search\BannerSearch;
use common\models\BannerPages;
use common\models\BannerRules;
use common\models\BannerCountry;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * BannerController implements the CRUD actions for Banner model.
 */
class BannerController extends BaseController
{


    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->post('hasEditable')) {
            $bookId = Yii::$app->request->post('editableKey');
            $model = Banner::findOne($bookId);

            $out = Json::encode(['output'=>'', 'message'=>'']);

            $posted = current($_POST['Banner']);
            $post = ['Banner' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {
                // can save model or do something before saving model
                $model->save(false);
                $output = '';
                if (isset($posted['name'])) {
                    $output = $model->name;
                }

                $out = Json::encode(['output'=>$output, 'message'=>'']);
            }
            echo $out;
            return;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionMultipleDelete()
    {
        if(Yii::$app->request->isAjax == true) {
            $ids = Yii::$app->request->post('row_id');
            if(isset($ids) && $ids) {
                Banner::deleteAll(['id' => $ids]);
            }
        }
    }

    /**
     * Displays a single Banner model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Banner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $model = new Banner();
        $bannerRules = new BannerRules();
        $bannerCountry = new BannerCountry();
        $model->scenario = "create";

        if ($model->load(Yii::$app->request->post())) {
            $fileType = '';
            $img = ["jpg", "png", "gif", "jpeg"];
            $video  = ["mp4","m4v", "mov", "flv", "f4v", "ogg", "ogv", "wmv", "vp6", "vp5", "mpg", "avi", "mpeg", "webm"];
            $flash  = ["swf"];
            if($model->type == 0 ) {
                $files = UploadedFile::getInstances($model, 'file');

                if ($files && is_array($files)) {
                    foreach ($files as $file) {
                        $imageRandName = rand(1, 9999999);
                        $file->saveAs(Yii::getAlias('@frontend/web') . '/banners' . '/' . md5($imageRandName) . '.' . $file->extension);
                        $fileName = md5($imageRandName) . '.' . $file->extension;

                        if(in_array($file->extension ,$img)) {
                            $fileType  = 'image';
                        }elseif(in_array($file->extension ,$video)){
                            $fileType  = 'video';
                        }elseif(in_array($file->extension ,$flash)){
                            $fileType  = 'flash';
                        }
                    }
                    $model->file_extension_type = $files[0]->type;
                    $model->content = $fileName;
                    $model->type = 0;
                }
            }elseif ($model->type == 1) {
                if ($model->code) {
                    $model->content = $model->code;
                    $model->type = 1;
                    $fileType  = 'code';
                    $model->file_extension_type = '';
                }
            }
            $model->save();
            if($bannerRules->load(Yii::$app->request->post())){
                 $bannerRules->file_type = $fileType;
                 $bannerRules->banner_id = $model->id;
                $bannerRules->save();
            }

            if($bannerCountry->load(Yii::$app->request->post())){
                if($bannerCountry->country_include_id) {
                    foreach ($bannerCountry->country_include_id as $key) {
                        $bannerCountry = new BannerCountry();
                        $bannerCountry->banner_id = $model->id;
                        $bannerCountry->country_include_id = $key;
                        $bannerCountry->save();
                    }
                }

            }


          //  return $this->redirect(['view', 'id' => $model->id]);
               return $this->redirect(['index']);
        } else {
            if(yii::$app->request->isAjax) {
                return $this->renderAjax('create', [
                    'model' => $model,
                    'bannerRules' => $bannerRules,
                    'bannerCountry' => $bannerCountry,
                ]);
            }else{
                return $this->render('create', [
                    'model' => $model,
                    'bannerRules' => $bannerRules,
                    'bannerCountry' => $bannerCountry,
                ]);
            }
        }
    }

    /**
     * Updates an existing Banner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionImageDelete()
    {
        if (isset($_POST['key']) && $_POST['key']) {
            $key = intval(abs($_POST['key']));
            $banner = Banner::find()->where(['id' => $key])->one();
            if($banner !== null) {
                $banner->content = '';
                $banner->save();
            }
            echo 1;
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $bannerRules = BannerRules::find()->where(['banner_id' => $id])->one();
        $bannerCountryData = BannerCountry::find()->select('country_include_id')->where(['banner_id' => $id])->asArray()->all();
        $country = [];
        foreach ($bannerCountryData as $key ){
            $country[] = $key['country_include_id'];
        }
        $bannerCountry = new BannerCountry();

        $allimageid = [];$allimage = [];

        $fileTypePreview = '';

        if($model->type == 0) {

            if($bannerRules->file_type == 'video'){
                $fileTypePreview = 'video';
            }elseif ($bannerRules->file_type == 'image'){
                $fileTypePreview = 'image';
            }elseif ($bannerRules->file_type == 'flash'){
                $fileTypePreview = 'flash';
            }

            if($model->content) {
                $allimageid[] = array('url' => \yii\helpers\Url::toRoute([\Yii::$app->request->baseUrl . '/admins/banner/image-delete']), 'key' => $model->id,'filetype'=> $model->file_extension_type);
                $allimage[] =  '/banners/' . $model->content;
            }
        }



        if($bannerRules->limit_end_date == '01-01-1970'){
            $bannerRules->limit_end_date = null;
        }

        $fileType=$bannerRules->file_type;
        if ($model->load(Yii::$app->request->post())) {
            $img = ["jpg", "png", "gif", "jpeg"];
            $video  = ["m4v", "mov", "flv", "f4v", "ogg", "ogv", "wmv", "vp6", "vp5", "mpg", "avi", "mpeg", "webm","mp4"];
            $flash  = ["swf"];

            if($model->type == 0 ) {
                $files = UploadedFile::getInstances($model, 'file');

                if ($files && is_array($files)) {
                    foreach ($files as $file) {

                        $imageRandName = rand(1, 9999999);
                        $file->saveAs(Yii::getAlias('@frontend/web') . '/banners' . '/' . md5($imageRandName) . '.' . $file->extension);
                        $fileName = md5($imageRandName) . '.' . $file->extension;
                        if(in_array($file->extension ,$img)) {
                            $fileType  = 'image';
                        }elseif(in_array($file->extension ,$video)){
                            $fileType  = 'video';
                        }elseif(in_array($file->extension ,$flash)){
                            $fileType  = 'flash';
                        }

                    }
                    $model->file_extension_type = $files[0]->type;
                    $model->content = $fileName;
                    $model->type = 0;
                }
            }elseif ($model->type == 1) {
                if ($model->code) {
                    $model->content = $model->code;
                    $model->type = 1;
                    $fileType  = 'code';
                    $model->file_extension_type = '';
                }
            }
            $model->save();

            if($bannerRules->load(Yii::$app->request->post())){
                $bannerRules->file_type = $fileType;
                $bannerRules->banner_id = $model->id;
                $bannerRules->save();
            }


            if($bannerCountry->load(Yii::$app->request->post())){
                BannerCountry::deleteAll('banner_id = :id', [':id'=>$id]);
                if($bannerCountry->country_include_id) {
                    foreach ($bannerCountry->country_include_id as $key) {
                        $bannerCountry = new BannerCountry();
                        $bannerCountry->banner_id = $model->id;
                        $bannerCountry->country_include_id = $key;
                        $bannerCountry->save();
                    }
                }

            }
            return $this->redirect(['index']);
          //  return $this->redirect(['view', 'id' => $model->id]);
        } else {
            if(yii::$app->request->isAjax) {
                $this->layout = false;
                return $this->renderAjax('update', [
                    'model' => $model,
                    'allimageid' => $allimageid,
                    'allimage' => $allimage,
                    'fileTypePreview' => $fileTypePreview,
                    'bannerRules' => $bannerRules,
                    'bannerCountry' => $bannerCountry,
                    'country' => $country,
                ]);
            }else {
                return $this->render('update', [
                    'model' => $model,
                    'allimageid' => $allimageid,
                    'allimage' => $allimage,
                    'fileTypePreview' => $fileTypePreview,
                    'bannerRules' => $bannerRules,
                    'bannerCountry' => $bannerCountry,
                    'country' => $country,
                ]);
            }
        }
    }

    public function actionPlacementsAge(){
        if (isset($_POST['id']) && $_POST['id']) {
            $id = intval(abs($_POST['id']));
            $existsId = BannerPages::findOne($id);
            if(!$existsId): return false; endif;
            $countPlacements= BannerPlacement::find()
                ->where(['banner_pages_id' => $id])
                ->count();

            $modelPlacements = BannerPlacement::find()
                ->where(['banner_pages_id' => $id])->groupBy('is_mobile')
                ->all();


            $placemennt = [];
            if ($countPlacements > 0) {
                foreach ($modelPlacements as $modell) {
                    $modelPlacementsGroup = BannerPlacement::find()->where(['banner_pages_id' => $id,'is_mobile' => $modell->is_mobile])->all();
                    $placemennt[] = "<optgroup  label='".$modell->bannerMobile[$modell->is_mobile]."'>";
                         foreach ($modelPlacementsGroup as $key) {
                             $placemennt[] = "<option value='" . $key->id . "'>" . $key->width . ' - ' . $key->height . "</option>";
                         }
                     $placemennt[] = '</optgroup>';
                }
            }else{
                $placemennt[] ='<option value="0">Null</option>';
            }

            $allData = [
                'placement' => $placemennt,
            ];

            return json_encode($allData);
        }
    }

    /**
     * Deletes an existing Banner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Banner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Banner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Banner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
