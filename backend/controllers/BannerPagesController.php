<?php

namespace backend\controllers;

use common\models\Banner;
use Yii;
use common\models\BannerPages;
use common\models\search\BannerPagesSearch;
use common\models\search\BannerPlacementSerach;
use common\models\BannerPlacement;
use backend\models\Model;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

/**
 * BannerPagesController implements the CRUD actions for BannerPages model.
 */
class BannerPagesController extends BaseController
{


    /**
     * Lists all BannerPages models.
     * @return mixed
     */



    public function actionIndex()
    {
        $searchModel = new BannerPagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        $searchModelPlacement = new BannerPlacementSerach();
        $dataProviderPlacement = $searchModelPlacement->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModelPlacement' => $searchModelPlacement,
            'dataProviderPlacement' => $dataProviderPlacement,
        ]);
    }

    /**
     * Displays a single BannerPages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BannerPages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $model = new BannerPages;
        $modelsAddress = [new BannerPlacement];

        if ($model->load(Yii::$app->request->post())) {

            $modelsAddress = Model::createMultiple(BannerPlacement::classname());
            Model::loadMultiple($modelsAddress, Yii::$app->request->post());

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsAddress),
                    ActiveForm::validate($model)
                );
            }

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsAddress) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        foreach ($modelsAddress as $modelAddress) {
                            $modelAddress->banner_pages_id = $model->id;
                            if (! ($flag = $modelAddress->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['index']);
                     //   return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelsAddress' => (empty($modelsAddress)) ? [new BannerPlacement] : $modelsAddress
        ]);
    }

    public function actionAjaxBannerStatus(){
        if(Yii::$app->request->isAjax == true){

            $bannerId = yii::$app->request->post('bannerId');
            $bannerStatus= yii::$app->request->post('status');
            if($bannerId && ($bannerStatus  == 0 || $bannerStatus  == 1)) {
                $banner = Banner::find()->where(['id' => $bannerId])->one();
                if(!$banner)  return false;
                if($bannerStatus == 0) {
                    $banner->is_active = 0;
                    $banner->save(false);
                    return 'danger-status';
                }elseif ($bannerStatus == 1){
                    $banner->is_active = 1;
                    $banner->save(false);
                    return 'success-status';
                }else{
                    return false;
                }
                return true;
            }else{
                return false;
            }
        }
    }

    public function actionAjaxBannerStatusPlacement(){


            $bannerId = yii::$app->request->post('bannerId');
            $bannerStatus= yii::$app->request->post('status');
            if($bannerId && ($bannerStatus  == 0 || $bannerStatus  == 1)) {
                $banner = BannerPlacement::find()->where(['id' => $bannerId])->one();
                if(!$banner)  return false;
                if($bannerStatus == 0) {
                    $banner->status = 0;
                    $banner->save(false);
                    return 'danger-status';
                }elseif ($bannerStatus == 1){
                    $banner->status = 1;
                    $banner->save(false);
                    return 'success-status';
                }else{
                    return false;
                }
                return true;
            }else{
                return false;
            }

    }


    public function actionStatistics($id)
    {
        $model = $this->findModel($id);

        $bannerPlacement = BannerPlacement::find()->where(['banner_pages_id'=>$model->id])->all();
        return $this->render('statistic', [
            'pageName' => $model->name,
            'bannerPlacement' => $bannerPlacement,
        ]);



    }

    /**
     * Updates an existing BannerPages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelsAddress = BannerPlacement::find()->where(['banner_pages_id'=>$model->id])->all();

        if ($model->load(Yii::$app->request->post())) {

            $oldIDs = ArrayHelper::map($modelsAddress, 'id', 'id');
            $modelsAddress = Model::createMultiple(BannerPlacement::classname(), $modelsAddress);
            Model::loadMultiple($modelsAddress, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsAddress, 'id', 'id')));

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsAddress),
                    ActiveForm::validate($model)
                );
            }

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelsAddress) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (! empty($deletedIDs)) {
                            BannerPlacement::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($modelsAddress as $modelAddress) {
                            $modelAddress->banner_pages_id = $model->id;
                            if (! ($flag = $modelAddress->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelsAddress' => (empty($modelsAddress)) ? [new BannerPlacement] : $modelsAddress
        ]);
    }




    /**
     * Deletes an existing BannerPages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BannerPages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BannerPages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BannerPages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
