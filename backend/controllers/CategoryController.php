<?php

namespace backend\controllers;

use backend\models\CategoryFieldsWish;
use common\models\CategoryField;
use Yii;
use common\models\Category;
use common\models\search\CategorySearch;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends BaseController
{

    public function actions()
    {

        return [
            'browse-images' => [
                'class' => 'bajadev\ckeditor\actions\BrowseAction',
                'quality' => 80,
                'maxWidth' => 800,
                'maxHeight' => 800,
                'useHash' => true,
                'url' => '@web/contents/',
                'path' => '@backend/web/images/',
            ],
            'upload-images' => [
                'class' => 'bajadev\ckeditor\actions\UploadAction',
                'quality' => 80,
                'maxWidth' => 800,
                'maxHeight' => 800,
                'useHash' => true,
                'url' => '@web/contents/',
                'path' => '@backend/web/images/',
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $dataProvider = new ActiveDataProvider([
            'query' => CategoryFieldsWish::find()->where(['category_id' => $id])
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $model = new Category();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $model->icon = UploadedFile::getInstance($model, 'icon');
            if($model->icon) {
                $model->icon = Yii::$app->uploadAvatar->uploadImage($model->icon);
                $model->save(false);
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
/*
        $getId = 6;
        if ($getId && $category = Category::findOne($getId)) {
            $parentIds = array_flip(Category::findOne($getId)->getAllParentsIds());
        }*/

        $oldImage = $model->icon;

        if ($model->load(Yii::$app->request->post())) {

            if(!$model->icon){
                $model->icon = $oldImage;
            }

            if($model->save()){
                $model->icon = UploadedFile::getInstance($model, 'icon');

                if($model->icon) {
                    $model->icon = Yii::$app->uploadAvatar->uploadImage($model->icon);
                    $model->save(false);
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }



    public function actionCategoryTree()
    {
        $getId = Yii::$app->request->get('id');

        $parentIds = [];

        if ($getId && $category = Category::findOne($getId)) {
            $parentIds = array_flip(Category::findOne($getId)->getAllParentsIds());
        }

        $treeCategory = Category::buildTree(Category::find()->asArray()->all(), 0, $getId, $parentIds);

        return json_encode($treeCategory);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
