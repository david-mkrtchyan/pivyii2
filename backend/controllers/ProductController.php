<?php

namespace backend\controllers;

use common\models\Albom;
use common\models\Category;
use common\models\CategoryField;
use common\models\ProductField;
use frontend\models\ProductAddress;
use Yii;
use common\models\Product;
use common\models\search\ProductSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;


/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends BaseController
{

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ProductField::find()->where(['product_id' => $id])
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $categories = ArrayHelper::map(Category::find()->all(), 'id', 'name_' . Yii::$app->language);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categories' => $categories,
        ]);
    }


    public function actions()
    {
        $actions = parent::actions();

        return $actions;
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        if ($category = Category::findOne($id)) {
            if ($category->childs) {
                return $this->goHome();
            }
        }

        $fieldModel = new ProductField();
        $imageAlbom = new Albom();
        $productAddress = new ProductAddress();
        $model = new Product();
        $allErrors = [];
        $model->category_id = $id;

        if ($model->load(Yii::$app->request->post())) {
            if ($fieldModel->load(Yii::$app->request->post())) {

                /*validation category fields*/
                $validate = CategoryField::find()->where(['category_fields_wish_id' => $model->category_fields_wish_id])->all();

                $allErrors = $this->formValidate($validate, $fieldModel);
                $fieldModel->validate();

                if ($fieldModel->hasErrors() || $allErrors) {
                    return $this->render('create', [
                        'allErrors' => $allErrors,
                        'fieldModel' => $fieldModel,
                        'imageAlbom' => $imageAlbom,
                        'model' => $model,
                        'productAddress' => $productAddress,
                    ]);
                }
                /*end validation*/
                $model->user_id = Yii::$app->user->identity->id;
                $model->save();

                foreach ($fieldModel->allCategoryFields as $key => $value) {
                    $categoryField = CategoryField::findOne(['id' => $key]);
                    if ($categoryField && $value) {
                        $fieldModel->deleteAll(['category_field_id' => $categoryField->id, 'product_id' => $model->id]);
                        if ($categoryField->type == CategoryField::typeSelect ||
                            $categoryField->type == CategoryField::typeCheckbox ||
                            $categoryField->type == CategoryField::typeRadio ||
                            ($categoryField->type == CategoryField::typeInput && $categoryField->allow_multiple == CategoryField::multipleYes)
                        ) {
                            $value = (array)$value;

                            if (!$categoryField->allow_multiple) {
                                $value = [$value[0]];
                            }


                            foreach ($value as $fieldValue) {
                                $newModel = new ProductField();
                                $newModel->product_id = $model->id;
                                $newModel->category_field_id = $categoryField->id;
                                if ($categoryField->allow_range) {
                                    $newModel->value = $fieldValue;
                                } else {
                                    $newModel->category_field_value_id = $fieldValue;
                                }
                                $newModel->save();
                            }
                        } elseif (
                           ($categoryField->type == CategoryField::typeInput && $categoryField->allow_multiple == CategoryField::multipleNo) ||
                            $categoryField->type == CategoryField::typeTextArea
                        ) {

                            $newModel = new ProductField();
                            $newModel->product_id = $model->id;
                            $newModel->category_field_id = $key;
                            $newModel->value = $value;
                            if ($newModel->save()) {
                                if ($categoryField->name == 'Rate' || $categoryField->name == 'Price') {
                                    if ($categoryField->name == 'Rate') {
                                        $model->rate_id = $value;
                                    } elseif ($categoryField->name == 'Price') {
                                        $model->price = $value;
                                    }
                                    $model->save();
                                }
                            }
                        }
                    }
                }

                /* upload images */
                if ($imageAlbom->load(Yii::$app->request->post())) {
                    $model->uplodeImages($imageAlbom, $model);
                }

                /* product address */
                if ($productAddress->load(Yii::$app->request->post())) {
                    $productAddress->product_id = $model->id;
                    $productAddress->save();
                }

            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'allErrors' => $allErrors,
                'fieldModel' => $fieldModel,
                'imageAlbom' => $imageAlbom,
                'model' => $model,
                'productAddress' => $productAddress,
            ]);
        }
    }


    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $fieldModel = new ProductField();

        $productAddress = ProductAddress::findOne(['product_id' => $model->id]);
        if (!$productAddress) {
            $productAddress = new ProductAddress();
        }

        $imageAlbom = new Albom();
        $imageAlbomOld = Albom::find()->where(['product_id' => $id])->orderBy('image_rank asc')->all();
        $imageCount = Albom::find()->where(['product_id' => $id])->count();
        $allImage = array();
        $allImageId = array();
        $allErrors = [];

        if (is_array($imageAlbomOld)) {
            $date = date_create(date('Y-m-d', $model->created_at));
            foreach ($imageAlbomOld as $index => $eachimage) {
                $allImageId[] = array('url' => \yii\helpers\Url::toRoute(['product/image-delete']), 'key' => $eachimage->albom_id);
                $allImage[] = '/image/' . date_format($date, "Y-m-d") . '/' . $eachimage->image;
            }
        }


        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if ($fieldModel->load(Yii::$app->request->post())) {

                /*validation category fields*/
                $validate = CategoryField::find()->where(['category_fields_wish_id' => $model->category_fields_wish_id])->all();

                $allErrors = $this->formValidate($validate, $fieldModel);
                $fieldModel->validate();

                if ($fieldModel->hasErrors() || $allErrors) {
                    return $this->render('update', [
                        'allErrors' => $allErrors,
                        'model' => $model,
                        'fieldModel' => $fieldModel,
                        'imageAlbom' => $imageAlbom,
                        'imageCount' => $imageCount,
                        'allImage' => $allImage,
                        'allImageId' => $allImageId,
                        'productAddress' => $productAddress,
                    ]);
                }

                /*end validation*/


                foreach ($fieldModel->allCategoryFields as $key => $value) {
                    $categoryField = CategoryField::findOne(['id' => $key]);
                    if ($categoryField && $value) {
                        $fieldModel->deleteAll(['category_field_id' => $categoryField->id, 'product_id' => $model->id]);
                        if ($categoryField->type == CategoryField::typeSelect ||
                            $categoryField->type == CategoryField::typeCheckbox ||
                            $categoryField->type == CategoryField::typeRadio ||
                            ($categoryField->type == CategoryField::typeInput && $categoryField->allow_multiple == CategoryField::multipleYes)
                        ) {
                            $value = (array)$value;

                            if (!$categoryField->allow_multiple) {
                                $value = [$value[0]];
                            }

                            foreach ($value as $fieldValue) {
                                $newModel = new ProductField();
                                $newModel->product_id = $model->id;
                                $newModel->category_field_id = $categoryField->id;
                                if ($categoryField->allow_range) {
                                    $newModel->value = $fieldValue;
                                } else {
                                    $newModel->category_field_value_id = $fieldValue;
                                }
                                $newModel->save();
                            }

                        } elseif (
                            ($categoryField->type == CategoryField::typeInput && $categoryField->allow_multiple == CategoryField::multipleNo) ||
                            $categoryField->type == CategoryField::typeTextArea
                        ) {
                            $newModel = new ProductField();
                            $newModel->product_id = $model->id;
                            $newModel->category_field_id = $key;
                            $newModel->value = $value;
                            if ($newModel->save()) {
                                if ($categoryField->name == 'Rate' || $categoryField->name == 'Price') {
                                    if ($categoryField->name == 'Rate') {
                                        $model->rate_id = $value;
                                    } elseif ($categoryField->name == 'Price') {
                                        $model->price = $value;
                                    }
                                    $model->save();
                                }
                            }
                        }
                    }
                }


                /* upload images */
                if ($imageAlbom->load(Yii::$app->request->post())) {
                    $count = $imageCount ? $imageCount : 0;
                    $model->uplodeImages($imageAlbom, $model, $count);
                }

                /* product address update*/
                if ($productAddress->load(Yii::$app->request->post())) {
                    $productAddress->product_id = $model->id;
                    $productAddress->save();
                }

            }
            return $this->redirect(['view', 'id' => $model->id]);

        } else {
            return $this->render('update', [
                'allErrors' => $allErrors,
                'model' => $model,
                'fieldModel' => $fieldModel,
                'imageAlbom' => $imageAlbom,
                'imageCount' => $imageCount,
                'allImage' => $allImage,
                'allImageId' => $allImageId,
                'productAddress' => $productAddress,
            ]);
        }
    }

    public function formValidate($validate, $fieldModel)
    {

        if ($validate) {
            foreach ($validate as $validate) {
                if ($validate->required == CategoryField::requiredYes) {
                    if (isset($fieldModel->allCategoryFields[$validate->id]) &&
                        !$fieldModel->allCategoryFields[$validate->id]
                    ) {
                        $fieldModel->addError($validate->name, "$validate->name Can not be blank");
                    } elseif (!isset($fieldModel->allCategoryFields[$validate->id])) {
                        $fieldModel->addError($validate->name, "$validate->name Can not be blank");
                    }
                }

                if (isset($fieldModel->allCategoryFields[$validate->id]) && $fieldModel->allCategoryFields[$validate->id]) {
                    $valCheck = $fieldModel->allCategoryFields[$validate->id];
                    if ($validate->type == CategoryField::typeSelect ||
                        $validate->type == CategoryField::typeCheckbox ||
                        $validate->type == CategoryField::typeRadio ||
                        ($validate->type == CategoryField::typeInput && $validate->allow_multiple == CategoryField::multipleYes)
                    ) {

                        //validate when use db and multiple value
                        if($validate->type == CategoryField::typeInput){
                            if (is_array($valCheck)) {
                                foreach ($valCheck as $item) {
                                    $table = Yii::$app->db->schema->getTableSchema("{{%$validate->db_name}}");

                                    if(!is_null($table)){
                                        $post = Yii::$app->db->createCommand("SELECT id FROM $table->name WHERE id='$item'")
                                            ->queryOne();

                                        if(!$post){
                                            $fieldModel->addError($validate->name, "$validate->name is wrong ");
                                        }
                                    }
                                }
                            }
                        }

                        //validate when range
                        if ($validate->type == CategoryField::typeSelect &&
                            is_string($valCheck) && ($validate->allow_range == CategoryField::rangeYes) &&
                            $validate->range_min && $validate->range_max) {
                            if (!($validate->range_max >= $valCheck) || !($validate->range_min <= $valCheck)) {
                                $fieldModel->addError($validate->name, "$validate->name is wrong ");
                            }
                        }

                        //validate when type Select,Checkbox,Radio
                        if ($validate->categoryFieldValues) {
                            if (is_array($valCheck)) {
                                foreach ($valCheck as $item) {
                                    if (!in_array($item, ArrayHelper::map($validate->categoryFieldValues, 'id', 'id'))) {
                                        $fieldModel->addError($validate->name, "$validate->name is wrong ");
                                    }
                                }
                            } elseif (is_string($valCheck) && !in_array($valCheck, ArrayHelper::map($validate->categoryFieldValues, 'id', 'id'))) {
                                $fieldModel->addError($validate->name, "$validate->name is wrong ");
                            }
                        }
                    } elseif (
                        $validate->type == CategoryField::typeInput
                    ) {
                        //validate when used db
                        if ($validate->use_database == CategoryField::useDatabaseYes && $validate->db_name) {
                            $table = Yii::$app->db->schema->getTableSchema("{{%$validate->db_name}}");
                            if (!is_null($table) && is_string($valCheck)) {
                                $valCheck = intval($valCheck);

                                if ($validate->db_name_type && isset($table->columns['type'])) {
                                    $post = Yii::$app->db->createCommand("SELECT id
                                     FROM $table->name
                                     WHERE id = $valCheck and type = '$validate->db_name_type'
                                      ")->queryAll();
                                } else {
                                    $post = Yii::$app->db->createCommand("SELECT id
                                     FROM $table->name
                                     WHERE id = $valCheck
                                      ")->queryAll();
                                }

                                if (!$post) {
                                    $fieldModel->addError($validate->name, "$validate->name is wrong ");
                                }
                            }
                        }
                    }
                }
            }
        }
        return $fieldModel->getErrors();
    }


    public function actionCategoryUpdateField()
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            $catId = Yii::$app->request->post('catId');
            $model = New Product();

            $model->category_id = $catId;
            $fieldModel = new ProductField();

            if ($model && $fieldModel && $id) {
                return $this->renderAjax('category-update-field', [
                    'model' => $model,
                    'fieldModel' => $fieldModel,
                    'id' => $id,
                ]);
            }
        }
        return false;
    }

    public function actionImageDelete()
    {

        if (isset($_POST['key']) && $_POST['key']) {
            $key = intval(abs($_POST['key']));
            $albom = Albom::find()->where(['albom_id' => $key, 'user_id' => Yii::$app->user->identity->id])->one();
            if (!$albom) {
                return false;
            }
            $product = Product::find()->where(['id' => $albom->product_id, 'user_id' => Yii::$app->user->identity->id])->one();
            if (!$product) {
                return false;
            }
            if (!empty($product)) {
                $date = date_create(date('Y-m-d', $product->created_at));
                $path = dirname(Yii::$app->getBasePath()) . DIRECTORY_SEPARATOR . '/frontend/web/image/' . date_format($date, "Y-m-d") . '/' . DIRECTORY_SEPARATOR . $albom->image;
                $pathThumb = dirname(Yii::$app->getBasePath()) . DIRECTORY_SEPARATOR . '/frontend/web/image/' . date_format($date, "Y-m-d") . '/' . DIRECTORY_SEPARATOR . 'thumb/' . $albom->image;
                if (is_file($path)) {
                    if (Albom::deleteAll(['albom_id' => $key])) {
                        unlink($path);
                    }
                } else {
                    Albom::deleteAll(['albom_id' => $key]);
                }
                if (is_file($pathThumb)) {
                    unlink($pathThumb);
                }
            }
            echo 1;
        }
    }

    public function actionAdd()
    {
        return $this->render('add');
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
