<?php

namespace backend\controllers;

use backend\models\search\ProductFieldSearch;
use common\models\CategoryField;;
use common\models\Product;
use Yii;
use common\models\ProductField;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * ProductFieldController implements the CRUD actions for ProductField model.
 */
class ProductFieldController extends BaseController
{


    /**
     * Lists all ProductField models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductFieldSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductField model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductField model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new ProductField();
        $product = Product::findOne(['id' => $id]);
        if ($product) {
            $model->product_id = $product->id;
        }

        if ($model->load(Yii::$app->request->post())) {
            foreach ($model->allCategoryFields as $key => $value) {
                $categoryField = CategoryField::findOne(['id' => $key]);
                if ($categoryField && $value) {
                    $model->deleteAll(['category_field_id' => $categoryField->id, 'product_id' => $product->id]);
                    if ($categoryField->type == CategoryField::typeSelect
                    ||
                    ($categoryField->type == CategoryField::typeInput && $categoryField->allow_multiple == CategoryField::multipleYes)
                    ) {
                        $value = (array)$value;

                        if (!$categoryField->allow_multiple) {
                            $value = [$value[0]];
                        }


                        foreach ($value as $fieldValue) {
                            $newModel = new ProductField();
                            $newModel->product_id = $product->id;
                            $newModel->category_field_id = $categoryField->id;
                            if ($categoryField->allow_range) {
                                $newModel->value = $fieldValue;
                            } else {
                                $newModel->category_field_value_id = $fieldValue;
                            }
                            $newModel->save();
                        }

                    } elseif ($categoryField->type == CategoryField::typeInput) {
                        $newModel = new ProductField();
                        $newModel->product_id = $model->product_id;
                        $newModel->category_field_id = $key;
                        $newModel->value = $value;
                        $newModel->save();
                    }
                }
            }
            return $this->redirect(Url::toRoute(['product/view', 'id' => $model->product_id]));
        }
        return $this->render('create', compact('model', 'product'));

    }

    /**
     * Updates an existing ProductField model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        /*$categoryFields = ArrayHelper::map(CategoryField::find()->where(['category_id' => $productfield->product->category_id])->asArray()->all(), 'id', 'name');*/
        $product = $model->product;

        if ($product) {
            $categoriesIds = [];
            $categoriesIds = $product->category->allParentsIds;
            $categoriesIds [] = $product->category_id;
            $categoryFields = ArrayHelper::map(CategoryField::find()->where(['IN', 'category_id', $categoriesIds])->asArray()->all(), 'id', 'name');
            $model->product_id = $product->id;
        }

//       if ($model->load(Yii::$app->request->post())) {
//            if (isset(Yii::$app->request->post('ProductField')['values']))
//                $model->rangeProductFieldsValues = Yii::$app->request->post('ProductField')['values'];
//
//            if (empty($model->value)) {
//                if (!empty($model->category_field_value_id)) {
//                    if ($model->categoryField->allow_multiple) {
//                        ProductField::deleteAll(['product_id' => $model->product_id, 'category_field_id' => $model->category_field_id]);
//                    }
//                    foreach ((array)$model->category_field_value_id as $fieldvalue) {
//                        $newmodel = new ProductField();
//                        $newmodel->product_id = $model->product_id;
//                        $newmodel->category_field_id = $model->category_field_id;
//                        $newmodel->category_field_value_id = $fieldvalue;
//                        $newmodel->save();
//                    }
//                    return $this->redirect(Url::toRoute(['product/view', 'id' => $model->product_id]));
//                }
//
//
//            } else {
//
//                if (empty($model->rangeProductFieldsValues)) {
//                    $model->save();
//                    return $this->redirect(Url::toRoute(['product/view', 'id' => $model->product_id]));
//                } else {
//                    if ($model->categoryField->allow_multiple) {
//                        ProductField::deleteAll(['product_id' => $model->product_id, 'category_field_id' => $model->category_field_id]);
//                    }
//                    foreach ($model->rangeProductFieldsValues as $fieldvalue) {
//                        $newmodel = new ProductField();
//                        $newmodel->product_id = $model->product_id;
//                        $newmodel->category_field_id = $model->category_field_id;
//                        $newmodel->value = $fieldvalue;
//                        $newmodel->save();
//                    }
//                    return $this->redirect(Url::toRoute(['product/view', 'id' => $model->product_id]));
//                }
//
//
//            }
//        }
//
        if ($model->load(Yii::$app->request->post())) {
            foreach ($model->allCategoryFields as $key => $value) {
                $categoryField = CategoryField::findOne(['id' => $key]);
                echo '<pre>';
                if ($value) {
                    if (is_array($value)) {
                        if ($categoryField->allow_multiple) {
                            foreach ($value as $fieldValue) {
                                $newModel = new ProductField();
                                $newModel->product_id = $model->product_id;
                                $newModel->category_field_id = $key;
                                if ($categoryField->allow_range) {
                                    if (is_numeric($fieldValue)) {
                                        $newModel->value = $fieldValue;
                                    }
                                } else {
                                    $newModel->category_field_value_id = $fieldValue;
                                }
                                $newModel->save();
                            }
                        }
                    } else {
                        $newModel = new ProductField();
                        $newModel->product_id = $model->product_id;
                        $newModel->category_field_id = $key;
                        $newModel->value = $value;
                        $newModel->save();
                    }
                }

            }

            return $this->redirect(Url::toRoute(['product/view', 'id' => $model->product_id]));
        } else {
            return $this->render('update', [
                'model' => $model,
                'categoryFields' => $categoryFields,
            ]);
        }
    }

    /**
     * Deletes an existing ProductField model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductField model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductField the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductField::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

