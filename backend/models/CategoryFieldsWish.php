<?php

namespace backend\models;

use common\models\Category;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "piv_category_fields_wish".
 *
 * @property int $id
 * @property int $category_id
 * @property string $value_lt
 * @property string $value_ru
 * @property string $value_en
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Category $category
 */
class CategoryFieldsWish extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'piv_category_fields_wish';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'created_at', 'updated_at', 'rank'], 'integer'],
            [['value_lt','value_en','value_ru'], 'required'],
            [['value_lt','value_en','value_ru'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'value_lt' => 'Value Lt',
            'value_en' => 'Value En',
            'value_ru' => 'Value Ru',
            'rank' => Yii::t('app', 'Rank'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
