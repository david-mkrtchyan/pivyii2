<?php
namespace backend\models;

use common\models\Brand;
use common\models\BrandModels;
use common\models\opcia\Opcia;
use common\models\Rate;
use Yii;
use yii\base\Model;

class Config extends Model
{

    public static function getAllDbTable(){
        return Yii::$app->db->schema->getTableNames();
    }

    public static  function getAllowTableList(){
        
        $opcia = Opcia::tableName();
        $brand = Brand::tableName();
        $model = BrandModels::tableName();
        $rate = Rate::tableName();


        $arrayList = [
            $opcia => $opcia,
            $brand => $brand,
            $model => $model,
            $rate => $rate,
        ];

        $schema = self::getAllDbTable();


        $lastResult = [];
        foreach ($schema as $value){
            if(in_array($value,$arrayList)){
                $value = str_replace('piv_','',$value);
                $lastResult[$value] = $value;
            }
        }

        return $lastResult;
    }
}