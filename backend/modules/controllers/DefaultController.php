<?php

namespace backend\modules\controllers;

use backend\controllers\BaseController;
/**
 * Default controller for the `users` module
 */
class DefaultController extends BaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
