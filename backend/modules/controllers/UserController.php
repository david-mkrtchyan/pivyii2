<?php

namespace backend\modules\controllers;

use Yii;
use common\models\User;
use common\models\search\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BaseController;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends BaseController
{
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {
            $model->birth_day = date('Y-m-d',strtotime($model->birth_day));
            $model->setPassword($model->password_hash);
            $model->generateAuthKey();
            $model->generatePasswordResetToken();
            unset($model->social_users_favourite);
            if($model->validate()  && $model->save()){
                $model->image = UploadedFile::getInstance($model, 'image');
                if($model->image) {
                    $model->image = Yii::$app->uploadAvatar->uploadImageAvatar($model->image);
                    $model->save(false);
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $oldPassword = $model->password_hash;
        $oldImage = $model->image;
        $model->password_hash = null;

        if ($model->load(Yii::$app->request->post())) {

            unset($model->social_users_favourite);
            $model->birth_day = date('Y-m-d',strtotime($model->birth_day));

            if(!$model->password_hash){
                $model->password_hash = $oldPassword;
            }else{
                $model->setPassword($model->password_hash);
            }

            if(!$model->image){
                $model->image = $oldImage;
            }

            if($model->validate()  && $model->save()){
                $model->image = UploadedFile::getInstance($model, 'image');
                if($model->image) {
                    $model->image = Yii::$app->uploadAvatar->uploadImageAvatar($model->image);
                    $model->save(false);
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
