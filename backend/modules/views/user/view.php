<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
            'birth_day',
            'adresse',
            'city_id',
            'phone',
            'email:email',

            [
                'attribute' => 'status',
                'value' =>function($data){
                    return ArrayHelper::getValue($data::getStatusList(),$data->status);
                },
            ],
            [
                'format' => 'raw',
                'attribute' => 'image',
                'value' =>function($data){
                    return \common\models\Config::getAvatarPhoto($data->image,64);
                },
            ],
            [
                    'attribute' => 'created_at',
                    'value' =>function($data){
                            return date('Y-m-d',$data->created_at);
                    }
            ],
            [
                    'attribute' => 'updated_at',
                    'value' =>function($data){
                            return date('Y-m-d',$data->updated_at);
                    }
            ],
        ],
    ]) ?>

</div>
