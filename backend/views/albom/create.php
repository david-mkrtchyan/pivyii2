<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Albom */

$this->title = Yii::t('app', 'Create Albom');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Alboms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="albom-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
