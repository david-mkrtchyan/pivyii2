<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Albom */

$this->title = Yii::t('app', 'Update Albom: ' . $model->albom_id, [
    'nameAttribute' => '' . $model->albom_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Alboms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->albom_id, 'url' => ['view', 'id' => $model->albom_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="albom-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
