<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Albom */

$this->title = $model->albom_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Alboms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="albom-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->albom_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->albom_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'albom_id',
            'image',
            'image_rank',
            'user_id',
            'product_id',
        ],
    ]) ?>

</div>
