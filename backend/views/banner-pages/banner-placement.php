<?php
use kartik\grid\GridView;
use kartik\switchinput\SwitchInput;
use yii\widgets\Pjax;
?>

<?php Pjax::begin(); ?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', ],
            'name',
            [
                'attribute' => 'position',
                'value' => function($data){
                    return $data->positionName[$data->position];
                },
                'filter' => $searchModel->positionName,
            ],

            'width',
            'height',
            [
                'attribute' => 'is_mobile',
                'value' => function($data){
                    return $data->bannerMobile[$data->is_mobile];
                },
                'filter' => $searchModel->is_mobile,
            ],

            [
                'attribute' => 'status',
                'value' => function($data){
                    //return  isset($data->bannerStatus[$data->type]) ? $data->bannerStatus[$data->is_active] : '';
                    return    SwitchInput::widget([
                        'name' => 'status_banner',
                        'options' => ['class' => 'switch-id-status','data-id'=>$data->id],
                        'items' => [
                            ['label' => 'On', 'value' => 1],
                            ['label' => 'Off', 'value' => 0]
                        ],
                        'value' => $data->status,
                        'type' => SwitchInput::CHECKBOX
                    ]);
                },
                'format' => 'raw',
                'vAlign' => 'middle',
            ],

            [

                'class' => 'kartik\grid\ActionColumn',
                // 'contentOptions' => ['style' => 'width:260px;'],
                'header' => '',
                'template' => '',


            ],
        ],
    ]); ?>
<?php Pjax::end(); ?>




