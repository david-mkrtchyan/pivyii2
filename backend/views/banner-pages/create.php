<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BannerPages */

$this->title = 'Create Banner Placements';
$this->params['breadcrumbs'][] = ['label' => 'Banner Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-pages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsAddress' => $modelsAddress,
    ]) ?>

</div>
