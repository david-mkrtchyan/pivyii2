<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use app\models\BannerPages;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BannerPagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Banner Placements';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
?>
<div class="banner-pages-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Banner Placements', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                 'value' => function ($model, $key, $index, $column) {
                     return GridView::ROW_COLLAPSED;
                 },
                'detail' => function ($model, $key, $index, $column) {
                    $searchModel = new \common\models\search\BannerPlacementSerach();
                    $searchModel->banner_pages_id = $model->id;
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                    return Yii::$app->controller->renderPartial('banner-placement', [
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                    ]);
                },
            ],

            [

                'attribute' => 'name',
                'value' => function($data){
                    return  $data->name;
                },
                'vAlign' => 'middle',
            ],

            [
                'attribute' => 'created_at',
                'value' => function($data){
                    return \Yii::$app->formatter->asDate($data->created_at, 'dd-MM-yyyy');
                },

                'vAlign' => 'middle',
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($data){
                    return \Yii::$app->formatter->asDate($data->updated_at, 'dd-MM-yyyy');
                },
                'vAlign' => 'middle',
            ],



            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => " {statistics} {view} {update} {delete}",
                    'buttons' => [
                        'statistics' => function ($url, $model) {
                            return Html::a('<i class="fa fa-wrench" aria-hidden="true"></i>', $url, [
                                'title' => Yii::t('app', 'Statistics'),
                            ]);
                        },
                    ],
            ],
        ],
    ]); ?>

<?php Pjax::end(); ?></div>


<?php
$this->registerJs("  
               jQuery('.bootstrap-switch-handle-on,.bootstrap-switch-label,.bootstrap-switch-handle-off').each(function(){ 
                      $(this).click(function () { 
                              var bannerClass = $(this).closest('.bootstrap-switch.bootstrap-switch-wrapper');
                               
                              if(bannerClass.is( '.bootstrap-switch-off')){
                                   var status = 0;
                              } else if(bannerClass.is( '.bootstrap-switch-on')){
                                   var status = 1;
                              }
                              
                              var bannerId = $(this).closest('.bootstrap-switch-container').find('.switch-id-status').attr('data-id');

                              $.ajax({
                                   url: '/admins/banner-pages/ajax-banner-status-placement',
                                   type: 'post',
                                   data: {bannerId: bannerId,status:status},
                                   success: function (data) {
                                      console.log(data)
                                   }
                               });
                });
                });
              ");


?>
