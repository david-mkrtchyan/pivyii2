<?php
/**
 * Created by PhpStorm.
 * User: Zakar
 * Date: 7/20/2017
 * Time: 11:28 AM
 */


use app\models\Banner;
use kartik\widgets\SwitchInput;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use app\models\Mobile_Detect;


/* @var $this yii\web\View */
/* @var $searchModel app\models\BannerPagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $pageName . ' page   ';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('@web/css/statistic.css');

?>
<div class="banner-pages-index">

    <h1 style="text-align: center"><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php  foreach($bannerPlacement as $model):?>
    <div class="col-md-12 header-section statistic">
        <div class="content-header-section clearfix">
            <div class="col-md-4 col-sm-4 col-xs-4">
                <div class="statistic-name">
                    <?php echo  $model->name ;  ?>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <div class="statistic-is_device">
                    <?php echo $model->bannerMobile[$model->is_mobile]; ?>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <div class="statistic-size">
                    <?php
                    echo 'Placement '.$model->width . '*' . $model->height;
                    ?>
                </div>
            </div>
        </div>

        <?php if(!$model->banners):
            echo '<div class="not-placement">This placement have not a banner</div>';
        else: ?>
        <div class="all-banner-name-ctr-limit">
            <div class="col-md-12 table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Banner Description</th>
                        <th>Status</th>
                        <th>Ctr</th>
                        <th>Statistic</th>
                        <th>Targeting</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                         <?php

                         $searchModel1 = new \app\models\BannerSearch();
                         $searchModel1->banner_placement_id = $model->id;
                         $searchModel1->banner_page_id = $model->banner_pages_id;
                         $dataProvider1 = $searchModel1->search(Yii::$app->request->queryParams);
                         $dataProvider1->pagination->pageSize = 10;

                         $banner  = Banner::find()->select('id')->where(['banner_page_id' => $model->banner_pages_id,'banner_placement_id' => $model->id])->asArray()->all();
                         $bannerId = [];
                         foreach ($banner as $value){
                             $bannerId[] = $value['id'];
                         }

                         echo ListView::widget([
                             'dataProvider' => $dataProvider1,
                             'itemView' => 'statistic_show',
                             'layout' => "{items}\n<tr><td>{pager}</td></tr>",
                             'summary' => "",
                             'viewParams' => ['bannerId' => $bannerId]
                         ]);

                         ?>
                    </tbody>
                </table>
            </div>
            <div class="col-md-5">
            </div>
        </div>
        <?php endif; ?>
    </div>
<?php endforeach;?>


</div>
<?php
$this->registerJs(" 
               jQuery('.bootstrap-switch-handle-on,.bootstrap-switch-label,.bootstrap-switch-handle-off').click(function () { 
                              var bannerClass = $(this).closest('.bootstrap-switch.bootstrap-switch-wrapper');
                              var bannerStatusColor = $(this).closest('tr').find('.status-type-style').find('.status-type');
                              if(bannerClass.is( '.bootstrap-switch-off')){
                                   var status = 0;
                              } else if(bannerClass.is( '.bootstrap-switch-on')){
                                   var status = 1;
                              }
                              
                              var bannerId = $(this).closest('.status-name-banner').attr('data-id');
                              $.ajax({
                                   url: '/admins/banner-pages/ajax-banner-status',
                                   type: 'post',
                                   data: {bannerId: bannerId,status:status},
                                   success: function (data) {
                                    
                                        if(data == 'danger-status'){
                                              bannerStatusColor.removeClass('bg-success');
                                              bannerStatusColor.addClass('bg-danger');
                                        } else if(data == 'success-status'){
                                              bannerStatusColor.addClass('bg-success');
                                              bannerStatusColor.removeClass('bg-danger');  
                                        }
                                   }
                               });
                });
              ");


?>
