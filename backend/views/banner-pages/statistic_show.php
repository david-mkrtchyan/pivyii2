<?php
/**
 * Created by PhpStorm.
 * User: Zakar
 * Date: 7/20/2017
 * Time: 11:42 AM
 */
use kartik\switchinput\SwitchInput;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use miloschuman\highcharts\Highcharts;
use miloschuman\highcharts\SeriesDataHelper;
use miloschuman\highcharts\Highstock;
use yii\web\JsExpression;

?>

<?php
$stat = \app\models\BannerStat::find()->select(' sum(clicks_count)/sum(shows_count)*100 as ctr,sum(shows_count) as shows_count,sum(clicks_count) as clicks_count')->where(['banner_id' => $model->id])->asArray()->one(); ?>
<?php
$rules = \app\models\BannerRules::find()->where(['banner_id' => $model->id])->one(); ?>
<tr>
    <td>
        <div class="group-banner group">
            <div class="status-name status-id-<?= $model->id ?> status-name-banner" data-id="<?= $model->id ?>">
                <?php
                echo SwitchInput::widget([
                    'name' => 'status_banner',
                    'options' => ['class' => 'switch-id-status'],
                    'items' => [
                        ['label' => 'Yes', 'value' => 1],
                        ['label' => 'No', 'value' => 0]
                    ],
                    'value' => $model->is_active,
                    'type' => SwitchInput::CHECKBOX
                ]);
                ?>


            </div>


            <div class="banner-description">
                <div class="status-banner">
                    <p><span class="banner-name">Name-</span><a target="_blank"
                                                                href="<?= yii::$app->request->baseUrl ?>/admins/banner/update?id=<?= $model->id ?>"> <?= $model->name ?></a>
                    </p>
                    <p class="type"><span class="banner-type">Type-</span> <?= $model->bannerType[$model->type] ?></p>
                </div>
            </div>
        </div>
    </td>

    <td>
        <div class="status-type-style">
            <p class="status-type <?php if ($model->is_active == 1): echo 'bg-success';
            else: echo 'bg-danger'; endif; ?>" style="border-radius: 50%;width: 20px;height: 20px;"></p>
        </div>
    </td>
    <td>
        <?php if ($stat['ctr']): ?>
            <?= (double)$stat['ctr'] . '%'; ?>
        <?php else: ?>
            <?php echo '0%' ?>
        <?php endif; ?>
    </td>

    <td>

        <?php
        Modal::begin([
            'header' => '<h2 class="modal-header-name">Statistics</h2> <h3 class="modal-header-size-placement">' . $model->bannerPlacement->width . '*' . $model->bannerPlacement->height . '</h3>',
            'class' => 'statistic-button btn modal-open-targ',
            'id' => 'statistic' . $model->id,
            'size' => 'modal-lg',
            'toggleButton' => ['label' => 'Statistic', 'class' => 'statistic-button btn '],
        ]);
        ?>
        <?php

        $stat = \app\models\BannerStat::find()->select('created_at,sum(clicks_count)/sum(shows_count)*100 as ctr,sum(clicks_count) as clicks_count,sum(shows_count) as shows_count')->where(['banner_id' => $model->id])->andWhere(['>', 'clicks_count', 0])->andWhere(['>', 'shows_count', 0])->groupBy('created_at')->asArray()->all();
        $arrayChartsPlac = [];
        $i = 0;

        if ($stat) {
            foreach ($stat as $key => $value) {
                $arrayChartsPlac[$i][] = (int)$value['created_at'];
                $arrayChartsPlac[$i][] = (double)$value['ctr'];
                $arrayChartsPlac[$i][] = (int)$value['clicks_count'];
                $arrayChartsPlac[$i][] = (int)$value['shows_count'];
                $i++;
            }
        }
        $detect = new Mobile_Detect;
        if(!$detect->isMobile()) {
            $widthGlobal  = 870;
        }else{
            $widthGlobal  = 500;
        }
        if ($arrayChartsPlac) {
                echo Highstock::widget([
                'options' => [
                    'title' => ['text' => 'Banner(' . $model->name . ')'],
                    'yAxis' => [
                        ['title' => ['text' => $model->name], 'height' => '60%'],
                        ['title' => ['text' => 'Ctr'], 'top' => '65%', 'height' => '35%', 'offset' => 0],
                    ],
                    'chart' => ['width' => $widthGlobal ],

                    'series' => [

                       /* ["colorByPoint" => true],
                        ['rangeSelector' => [
                            'selected' => 2
                        ]],*/
                        [
                            'type' => 'area',
                            'threshold' => null,
                            'name' => $model->name,
                            // just like before, only now the columns are referenced by array offset
                            'data' => new SeriesDataHelper($arrayChartsPlac, ['0:timestamp', 1]),
                            'fillColor' => [
                                'linearGradient' => [
                                    'x1' => 0,
                                    'y1' => 0,
                                    'x2' => 0,
                                    'y2' => 1
                                ],
                                'stops' => [
                                    [0, new JsExpression('Highcharts.getOptions().colors[0]')],
                                    [1, new JsExpression('Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get("rgba")')]
                                ]
                            ],
                            'marker' => [
                                'lineWidth' => 1,
                                'lineColor' => new JsExpression('Highcharts.getOptions().colors[3]'),
                                'fillColor' => 'white',
                            ],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Ctr',
                            'data' => new SeriesDataHelper($arrayChartsPlac, ['0:timestamp', '1:int']),
                            'yAxis' => 1,
                        ],

                        [
                            'type' => 'column',
                            'name' => 'Clicks Count',
                            'data' => new SeriesDataHelper($arrayChartsPlac, ['0:timestamp', '2:int']),
                            'yAxis' => 1,
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Show Count',
                            'data' => new SeriesDataHelper($arrayChartsPlac, ['0:timestamp', '3:int']),
                            'yAxis' => 1,
                        ],


                    ]
                ]
            ]);
        }

        ?>
        <hr/>
        <?php

        // $stat = \app\models\BannerStat::find()->select('banner_id,sum(clicks_count)/sum(shows_count)*100 as ctr')->where(['banner_id' => $bannerId])->groupBy('banner_id')->asArray()->all();
        $stat = \app\models\BannerStat::find()->select('banner_id, sum(shows_count) as shows_count')->where(['banner_id' => $bannerId])->groupBy('banner_id')->asArray()->all();

        $statClick = \app\models\BannerStat::find()->select('banner_id, sum(clicks_count) as clicks_count')->where(['banner_id' => $bannerId])->groupBy('banner_id')->asArray()->all();

        $arrayChartsPlacBanners = [];
        $i = 0;
        if ($stat) {
            foreach ($stat as $key => $value) {
                $banner = \app\models\Banner::find()->where(['id' => $value['banner_id']])->one();

                $arrayChartsPlacBanners[$i][] = (string)$banner->name;
                $arrayChartsPlacBanners[$i][] = floatval($value['shows_count']);
                $i++;
            }
        }

        $arrayChartsPlacBannersClicksCount = [];
        $s = 0;
        if ($statClick) {
            foreach ($statClick as $key => $value) {
                $banner = \app\models\Banner::find()->where(['id' => $value['banner_id']])->one();

                $arrayChartsPlacBannersClicksCount[$s][] = (string)$banner->name;
                $arrayChartsPlacBannersClicksCount[$s][] = floatval($value['clicks_count']);
                $s++;
            }
        }
        if(!$detect->isMobile()) {
            $widthCountShow= 350;
        }else{
            $widthCountShow = 150;
        }
        ?>

        <div class="row">
            <?php if ($arrayChartsPlacBanners): ?>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <?php echo Highcharts::widget([
                        'options' => [
                            'chart' => ['width' => $widthCountShow],
                            'title' => ['text' => 'All ' . $model->bannerPlacement->width . '*' . $model->bannerPlacement->height . ' banners statistic with Show Count'],
                            'xAxis' => [
                                //'categories' => ['Apples', 'Bananas', 'Oranges']
                            ],
                            'yAxis' => [
                                'title' => ['text' => 'Fruit eaten']
                            ],

                            'series' => [
                                [ // new opening bracket
                                    'type' => 'pie',
                                    'name' => 'Shows Count',
                                    'data' => $arrayChartsPlacBanners,
                                ]
                            ]
                        ]
                    ]);
                    ?>
                </div>
            <?php endif; ?>
            <?php if ($arrayChartsPlacBanners): ?>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <?php echo Highcharts::widget([
                        'options' => [
                            'chart' => ['width' => $widthCountShow],
                            'title' => ['text' => 'All ' . $model->bannerPlacement->width . '*' . $model->bannerPlacement->height . ' banners statistic with Click  Count'],
                            'xAxis' => [
                                //'categories' => ['Apples', 'Bananas', 'Oranges']
                            ],
                            'yAxis' => [
                                'title' => ['text' => 'Fruit eaten']
                            ],

                            'series' => [
                                [ // new opening bracket
                                    'type' => 'pie',
                                    'name' => 'Click Count',
                                    'data' => $arrayChartsPlacBannersClicksCount,
                                ]
                            ]
                        ]
                    ]);

                    ?>
                </div>
            <?php endif; ?>
        </div>
        <?php Modal::end(); ?>

        <?php
                                 $this->registerJs("
                                     jQuery('#statistic{$model->id}').click(function () {
                                        jQuery('#statistic{$model->id}').show();
                                       });
                                 ");
        ?>
    </td>
    <td>
        <?php
        Modal::begin([
            'header' => '<h2>Targeting Description</h2>',
            'class' => 'statistic-button btn modal-open-targ',
            'id' => 'jobPop' . $model->id,
            'toggleButton' => ['label' => 'Targeting', 'class' => 'statistic-button btn '],
        ]);
        ?>

        <div class="row">
            <div class="col-md-6">
                <p class="type"><span class="banner-type">Name-</span> <?= $model->name ?></p>
            </div>
            <div class="col-md-6">
                <p class="type"><span class="banner-type">Title-</span> <?= $model->title ?></p>
            </div>
            <div class="col-md-6">
                <p class="type"><span
                            class="banner-type">Type-</span> <?= $model->bannerType[$model->type] . '(' . $rules->file_type . ')' ?>
                </p>
            </div>
            <div class="col-md-6">
                <p class="type"><span class="banner-type">Status-</span> <?= $model->bannerStatus[$model->is_active] ?>
                </p>
            </div>


            <div class="col-md-6">
                <p class="type"><span class="banner-type">Limit End Date -</span> <?= $rules->limit_end_date ?></p>
            </div>
            <div class="col-md-6">
                <p class="type"><span
                            class="banner-type">Limit Show -</span> <?php echo $stat['shows_count'] ? $rules->limit_count . ' (Has been shown ' . $stat['shows_count'] . ' times)' : $rules->limit_count . ' (Has been shown ' . '0' . ' times)' ?>
                </p>
            </div>
            <div class="col-md-6">
                <p class="type"><span class="banner-type">Limit Days -</span> <?= $rules->limit_days ?></p>
            </div>

            <div class="col-md-6">
                <p class="type"><span class="banner-type">Created -</span> <?= \Yii::$app->formatter->asDate($model->created_at, 'dd-MM-yyyy H:m:s'); ?></p>
            </div>

            <div class="col-md-6">
                <p class="type"><span class="banner-type">Updated -</span> <?= \Yii::$app->formatter->asDate($model->updated_at , 'dd-MM-yyyy H:m:s'); ?></p>
            </div>

        </div>
        <?php Modal::end();
        ?>
        <?php
        $this->registerJs("
                                jQuery('#jobPop{$model->id}').click(function () {
                                   jQuery('#jobPop{$model->id}').show();
                                  });
                                 ");
        ?>
    </td>
    <td>
        <?php
        $url = Url::to(['/admins/banner/update', 'id' => $model->id]);
        echo Html::a('<span class="glyphicon glyphicon-wrench">', $url, [
            'title' => 'update',
            'target' => '_blank',
            'data-method' => 'post',
        ]); ?>
    </td>

    <td>
        <?php
        $url = Url::to(['/admins/banner/delete', 'id' => $model->id]);
        echo Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, [
            'title' => 'Delete',
            'target' => '_blank',
            'aria-label' => Yii::t('yii', 'Delete'),
            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'data-method' => 'post',
            'data-pjax' => '0',
        ]);

        ?>
    </td>

</tr>






