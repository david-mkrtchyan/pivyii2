<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\BannerPlacement;

/* @var $this yii\web\View */
/* @var $model frontend\models\BannerPlacement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banner-placement-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'banner_pages_id')->dropDownList($model->bannerPages) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php  echo $form->field($model, 'position')
        ->dropDownList(
            $model->getPositionName(),
            ['prompt'=>'']
        );
    ?>

    <?= $form->field($model, 'width')->textInput() ?>

    <?= $form->field($model, 'height')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
