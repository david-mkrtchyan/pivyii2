<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BannerPlacement */

$this->title = 'Create Banner Placement';

?>
<div class="banner-placement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
