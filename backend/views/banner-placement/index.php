<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\BannerPages;
use yii\Helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\BannerPlacementSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Banner Placements';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-placement-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Banner Placement', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
           /* 'name',
            'page_name',*/
            [
                    'attribute' => 'position',
                    'value' => function($data){
                            return $data->positionName[$data->position];
                    },
                    'filter' => $searchModel->positionName,
            ],

            [
                'attribute' => 'banner_pages_id',
                'value' => function($data){
                    $model =  BannerPages::find()->where(['id' => $data->banner_pages_id])->one();
                    if($model){
                        return $model->name;
                    }
                },

                'filter' => $searchModel->bannerPages,
            ],
            //'stub',
             'width',
             'height',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
