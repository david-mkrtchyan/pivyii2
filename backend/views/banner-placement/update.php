<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BannerPlacement */

$this->title = 'Update Banner Placement: ' . $model->name;

?>
<div class="banner-placement-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
