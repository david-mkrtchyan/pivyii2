<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\BannerPlacement;
use app\models\BannerPages;

/* @var $this yii\web\View */
/* @var $model app\models\BannerPlacement */

$this->title = $model->name;

?>
<div class="banner-placement-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'banner_pages_id',
                'value' => function($data){
                    $model =  BannerPages::find()->where(['id' => $data->banner_pages_id])->one();

                    return $model->name;
                },
            ],
            /*'name',
            'page_name',*/
            [
                'attribute' => 'position',
                'value' => $model->positionName[$model->position] ? $model->positionName[$model->position] : '',
            ],


            //'stub',
            'width',
            'height',
        ],
    ]) ?>

</div>
