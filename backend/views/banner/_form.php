<?php

use kartik\field\FieldRange;
use kartik\file\FileInput;
use kartik\growl\Growl;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\Banner */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
if (!$model->isNewRecord) {

    if($model->type == 0 ) {
        $this->registerCss("
            .field-banner-code{display:none}"
        );
    }elseif ($model->type == 1){
        $this->registerCss("
            .field-banner-file{display:none}"
        );
    }
}?>

<div class="banner-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>


    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?= $form->field($model, 'type')->dropDownList($model->bannerType) ?>

    <?php
    if ($model->isNewRecord) {
        echo $form->field($model, 'file[]',['template' => '<label>File <span style="font-size: 12px;">(jpg,png,gif,jpeg,mp4,m4v,mov,flv,f4v,ogg,ogv,wmv,vp6,vp5,mpg,avi,mpeg,webm,swf)</label>  {input}{error}'])
            ->widget(FileInput::classname(), [
            'options' => [
                'multiple' => false,
            ],
            'pluginOptions' => [
                'allowedFileExtensions' => ["jpg", "png", "gif", "jpeg","mp4", "m4v", "mov", "flv", "f4v", "ogg", "ogv", "wmv", "vp6", "vp5", "mpg", "avi", "mpeg", "webm", "swf"],
                'showPreview' => true,
                'showCaption' => true,
                'showRemove' => true,
                'message' => "hello",
                'showUpload' => false,
                'removeLabel' => Yii::t('app', 'Удалять'),
                'browseLabel' => Yii::t('app', 'Добавить'),
                'maxFileCount' => 1,
                'initialCaption' => Yii::t('app', 'Добавить фотографии'),
                'uploadUrl' => Url::to(['#']),
                'uploadLabel' => false,
                'uploadAsync' => false,

                //previewFileType'previewFileType' => 'image'
            ]
        ]);

    }else{
           // echo  $form->field($model, 'file')->fileInput();
            echo $form->field($model, 'file[]',['template' => '<label>File <span style="font-size: 12px;">(jpg,png,gif,jpeg,mp4,m4v,mov,flv,f4v,ogg,ogv,wmv,vp6,vp5,mpg,avi,mpeg,webm,swf)</label>  {input}{error}'])
                ->widget(FileInput::classname(), [
                'options' => [
                    'multiple' => false,
                ],
                'pluginOptions' => [
                    'showPreview' => true,
                    'showRemove' => true,
                    'showUpload' => false,
                    'removeLabel' => Yii::t('app', 'Удалять'),
                    'browseLabel' => Yii::t('app', 'Добавить'),
                    'allowedFileExtensions' => ["jpg", "png", "gif", "jpeg","mp4", "m4v", "mov", "flv", "f4v", "ogg", "ogv", "wmv", "vp6", "vp5", "mpg", "avi", "mpeg", "webm", "swf"],
                    'maxFileCount' => 1,
                    'initialPreview' => $allimage,
                    'overwriteInitial' => false,
                    'initialPreviewAsData' => true,
                    'initialCaption' => Yii::t('app', 'Добавить фотографии'),
                    'initialPreviewConfig' => $allimageid,
                    "initialPreviewFileType" => $fileTypePreview,

                ]
            ]);
    }
    ?>
    <?php
    if (!$model->isNewRecord && $model->type == 1) {
        echo $form->field($model, 'code',['template' => '<label>File <span style="font-size: 12px;">(HTML,Javascript)</label>  {input}{error}'])->textarea(['rows' => 6,'value' => $model->content ]);
    }else {
        echo $form->field($model, 'code',['template' => '<label>Code <span style="font-size: 12px;">(Html,Javascript)</label>  {input}{error}'])->textarea(['rows' => 6,'value' => $model->content ]);
    }
    ?>


    <?=  $form->field($model, 'destination_url')->textInput(['maxlength' => true]);?>



    <div class="row">
        <div class="col-md-6">
            <?php // $form->field($model, 'width')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?php // $form->field($model, 'height')->textInput() ?>
        </div>
    </div>


    <?php //$form->field($model, 'is_limit_ended')->dropDownList([ '1' => 'Active', '0' => 'Passive', ], ['prompt' => '']) ?>

    <div class="row">
        <div class="col-md-6">
             <?=
             $form->field($model, 'banner_page_id', ['template' => '{label}{input}{error}{hint}']
             )->widget(Select2::classname(), [
                 'data' => $model->bannerPages,
                 'language' => 'ru',
                 'options' => ['placeholder' => Yii::t('app', 'Пожалуйста, выбери Cтраница'),
                     'onchange' => '
               $.post("' . Yii::$app->request->baseUrl . '/banner/placements-age",{ id: $(this).val() },
                           function(data) {
                             var json_obj = $.parseJSON(data);
                                    $("select#banner-banner_placement_id").html(json_obj.placement); 
                           }
                                   );'],
                 'pluginOptions' => ['allowClear' => true],
             ]);
             ?>
        </div>

        <?php
        if (!$model->isNewRecord ) { ?>
            <div class="col-md-6">
                <?php  echo $form->field($model, 'banner_placement_id')->widget(Select2::classname(), [
                    'data' => $model->bannerPlacementChecked,
                    'language' => 'ru',
                    'options' => ['placeholder' => Yii::t('app', 'Пожалуйста, выбери Pазмещение'),
                    ],
                    'pluginOptions' => ['allowClear' => true],
                ]);


                ?>
            </div>
       <?php
         }else { ?>
            <div class="col-md-6">
                <?php echo $form->field($model, 'banner_placement_id')->widget(Select2::classname(), [
                    'language' => 'ru',
                    'options' => ['placeholder' => Yii::t('app', 'Пожалуйста, выбери Pазмещение'),
                    ],
                    'pluginOptions' => ['allowClear' => true],
                ]);
                ?>
            </div>
         <?php } ?>

    </div>
    <?php if($model->isNewRecord):    ?>
    <?=   $form->field($bannerCountry, 'country_include_id')->widget(Select2::classname(), [
        'data' => $model->bannerCountry,
        'options' => ['placeholder' => 'Select a Country ...', 'multiple' => true],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [',', ' '],
            'maximumInputLength' => 10
        ],
    ]);

    ?>
    <?php else: ?>
    <?=   $form->field($bannerCountry, 'country_include_id')->widget(Select2::classname(), [
        'data' => $model->bannerCountry,
        'options' => [
            'placeholder' => 'Select a Country ...',
            'multiple' => true,
            'value' => $country],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [',', ' '],
            'maximumInputLength' => 10
        ],
    ]);
    ?>
    <?php endif; ?>

    <div class="row">
        <div class="col-md-6">
            <?php $model->is_active = true;?>
            <?=   $form->field($model, "is_active")->widget(SwitchInput::classname(), [
                'type' => SwitchInput::CHECKBOX,
                'items' => [
                    ['label' => 'On', 'value' => 1],
                    ['label' => 'Off', 'value' => 0]
                ],
            ]); ?>
         </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($bannerRules, 'limit_count')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($bannerRules, 'limit_days')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($bannerRules, 'limit_end_date')->widget(\yii\jui\DatePicker::classname(), [
                'language' => 'en',
                'options' => [ 'class' => 'form-control' ],
            ]) ?>
         </div>
    </div>



<?php if(!yii::$app->request->isAjax) {
    if($model->isNewRecord): ?>
    <?=  Growl::widget([
        'type' => Growl::TYPE_SUCCESS,
        'title' => 'Add Banners!',
        'icon' => 'glyphicon glyphicon-ok-sign',
        'body' => 'From here you can add your Banner',
        'showSeparator' => true,
        'delay' => 0,
        'pluginOptions' => [
            'showProgressbar' => true,
            'placement' => [
                'from' => 'top',
                'align' => 'right',
            ]
        ]
    ]);?>
<?php else: ?>
    <?=  Growl::widget([
        'type' => Growl::TYPE_SUCCESS,
        'title' => 'Update Banners!',
        'icon' => 'glyphicon glyphicon-ok-sign',
        'body' => 'From here you can update  "'.$model->name.'" Banner',
        'showSeparator' => true,
        'delay' => 0,
        'pluginOptions' => [
            'showProgressbar' => true,
            'placement' => [
                'from' => 'top',
                'align' => 'right',
            ]
        ]
    ]);?>
<?php
    endif;
} ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
 if ($model->isNewRecord) {
    $this->registerCss("
        .field-banner-code{display:none}
      
        ");
 }
?>

<?php
if (!$model->isNewRecord) {
    $this->registerJs("  
    $('#banner-type').change(function(){
        $('#banner-code').html('');
    })
    ");
}

$this->registerCss(" 
 .file-drop-zone-title{
     padding: 10px;
    }
    .file-input.file-input-ajax-new{
    padding-bottom:20px
    }
   .kv-file-upload {
    display: none !important;
    }
");
$this->registerJs("
    $('#banner-type').change(function(){
        if($(this).val() == 0){
         $('.field-banner-file').show();
         $('.field-banner-code').hide();
        } else if($(this).val() == 1){
         $('.field-banner-file').hide();
         $('.field-banner-code').show();
        }
    })
    
", \yii\web\View::POS_READY);
?>