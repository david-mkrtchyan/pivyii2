<?php
use kartik\grid\GridView;
use yii\widgets\Pjax;
?>

<?php Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn', ],

        [
            'attribute' => 'shows_count',
            'value' => function($data){
                return  $data->shows_count;
            },
            'vAlign' => 'middle',
        ],
        [
            'attribute' => 'clicks_count',
            'value' => function($data){
                return  $data->clicks_count;
            },
            'vAlign' => 'middle',
        ],
        [
            'attribute' => 'ctr',
            'value' => function($data){
                return  $data->ctr.'%';
            },
            'vAlign' => 'middle',
        ],
        [
            'attribute' => 'created_at',
            'value' => function($data){
                return \Yii::$app->formatter->asDate($data->created_at, 'dd-MM-yyyy');
            },
            'vAlign' => 'middle',
        ],
    ],
    'summary' => "",

]); ?>
<?php Pjax::end(); ?>




