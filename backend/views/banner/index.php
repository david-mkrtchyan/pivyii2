<?php

use kartik\switchinput\SwitchInput;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use kartik\export\ExportMenu;
use kartik\mpdf\Pdf;
use kartik\editable\Editable;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\BannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Banners';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->registerCss('.Allow-Country{
    color: #337ab7;
    text-decoration: none;}
    .big-color.kv-align-middle{
        color: #337ab7 !important;
}
 
 @media (min-width: 991px){ 
    .for-banner-page-big-layout{
               min-width: 1700px;
    }
 }
           #modal .modal-dialog.modal-lg{ padding-top: 30px !important;}
.dropdown-banner{  display: flex;    padding-bottom: 10px;}  
.multiple-delete{        margin-left: 3px;margin-right: 3px;}
    ')?>
<div class="banner-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <div class="dropdown-banner">
            <?= Html::a('Create Banner', ['create'], ['class' => 'btn btn-success create-banner']) ?>





           <?= Html::button('Multiple Delete', ['value'=>'Multiple Delete', 'class' => 'btn btn-danger multiple-delete','id'=>'MyButton']); ?>


            <div class="dropdown dropdown-modal">
                <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">Open Modal
                    <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><?= Html::button('Create Banner', ['value'=>Url::to('/pivadmins/banner/create'), 'class' => 'btn-link','id'=>'modalButton']); ?></li>
                    <li> <?= Html::button('Update Banner', ['value'=>Url::to('/pivadmins/banner/update'), 'class' => 'btn-link','id'=>'updateModalButton']); ?></li>
                </ul>
            </div>

        </div>

        <?php
        Modal::begin([
            'header'=>'<h4>Banner</h4>',
            'id'=>'modal',
            'size'=>'modal-lg',
        ]);
        echo '<div id="modalContent"></div>';
        Modal::end();
        ?>


            <?php

            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'detail' => function ($model, $key, $index, $column) {
                        $searchModel = new \common\models\search\BannerStatSerach();
                        $searchModel->banner_id = $model->id;
                        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                        return Yii::$app->controller->renderPartial('banner-stat', [
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                        ]);
                    },
                ],

                [
                    'attribute' => 'is_active',
                    'value' => function($data){
                        //return  isset($data->bannerStatus[$data->type]) ? $data->bannerStatus[$data->is_active] : '';
                        return    SwitchInput::widget([
                            'name' => 'status_banner',
                            'options' => ['class' => 'switch-id-status','data-id'=>$data->id],
                            'items' => [
                                ['label' => 'On', 'value' => 1],
                                ['label' => 'Off', 'value' => 0]
                            ],
                            'value' => $data->is_active,
                            'type' => SwitchInput::CHECKBOX
                        ]);
                    },
                    'filter' => $searchModel->bannerStatus,
                    'format' => 'raw',
                    'vAlign' => 'middle',
                ],

                [
                    'attribute' => 'name',
                    'value' => function($data){
                        if($data){
                            return  $data->name;
                        }

                    },
                    'vAlign' => 'middle',
                ],
                [
                    'attribute' => 'banner_placement_id',
                    'value' => function($data){
                        $model = \common\models\BannerPlacement::find()->where(['id' => $data->banner_placement_id])->one();
                        if($data){
                            return  $data->name;
                        }
                    },
                    // 'filter' => \common\models\BannerPlacement::find()->asArray()->all(),
                    'vAlign' => 'middle',
                ],
                /*[
                    'attribute' => 'title',
                    'value' => function($data){
                        return  $data->title;
                    },
                    'vAlign' => 'middle',
                ],*/


                [
                    'attribute' => 'type',
                    'value' => function($data){
                        return  isset($data->bannerType[$data->type]) ? $data->bannerType[$data->type] : '';
                    },
                    'filter' => $searchModel->bannerType,
                    'vAlign' => 'middle',
                ],

 

                [
                    'label' => 'Limit Count',
                    'value' => function($data){
                        $model = \common\models\BannerRules::find()->where(['banner_id' => $data->id])->one();
                        return   $model->limit_count;
                    },
                    'headerOptions' => ['class'=>'big-color'],
                    'vAlign' => 'middle',
                ],[
                    'label' => 'Limit Days',
                    'value' => function($data){
                        $model = \common\models\BannerRules::find()->where(['banner_id' => $data->id])->one();
                        return   $model->limit_days;
                    },
                    'headerOptions' => ['class'=>'big-color'],
                    'vAlign' => 'middle',
                ],[
                    'label' => 'Limit End Date',
                    'value' => function($data){
                        $model = \common\models\BannerRules::find()->where(['banner_id' => $data->id])->one();
                        return   $model->limit_end_date;
                    },
                    'headerOptions' => ['class'=>'big-color'],
                    'vAlign' => 'middle',
                ],



                [
                    'label' => 'Device',
                    'value' => function($data){
                        $model = \common\models\BannerPlacement::find()->where(['id' => $data->banner_placement_id])->one();
                        return $data->bannerMobile[$model->is_mobile];
                    },
                    'headerOptions' => ['class'=>'big-color'],
                    'vAlign' => 'middle',
                ],
                [
                    'attribute' => 'banner_page_id',
                    'value' => function($data){
                        $model = \common\models\BannerPages::find()->where(['id' => $data->banner_page_id])->one();
                        if($model){
                            return $model->name;
                        }
                    },
                    'filter' =>  ArrayHelper::map(\common\models\BannerPages::find()->asArray()->all(), 'id', 'name'),
                    'vAlign' => 'middle',
                ],

                 [
                       'label' => 'Allow Country',
                       'value' => function($data){
                           return $data->getbannerCountryIndex($data->id);
                       },
                       'headerOptions' => ['class' => 'Allow-Country'],
                       'vAlign' => 'middle',
                   ],


                [
                    'attribute' => 'created_at',
                    'value' => function($data){
                        return \Yii::$app->formatter->asDate($data->created_at, 'dd-MM-yyyy H:m:s');
                    },
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'name' => 'created_at',
                        'value' =>  $searchModel->created_at ? \Yii::$app->formatter->asDate($searchModel->created_at, 'dd-MM-yyyy') : '',
                        'pluginOptions' => [
                            'format' => 'mm-dd-yyyy',
                            'autoclose' => true,
                        ],

                    ]),
                    'vAlign' => 'middle',
                ],
                [
                    'attribute' => 'updated_at',
                    'value' => function($data){
                        return \Yii::$app->formatter->asDate($data->updated_at, 'dd-MM-yyyy H:m:s');
                    },
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'name' => 'updated_at',
                        'value' =>  $searchModel->created_at ? \Yii::$app->formatter->asDate($searchModel->created_at, 'dd-MM-yyyy') : '',
                        'pluginOptions' => [
                            'locale' => [
                                'format' => 'Y-m-d'
                            ],
                            'format' => 'mm-dd-yyyy',
                            'autoclose' => true,
                        ]
                    ]),
                    'vAlign' => 'middle',
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ];

            // Renders a export dropdown menu
            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns
            ]);
            ?>
    </p>

<?php Pjax::begin(); ?>    <?= \kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'checkboxOptions' => function($data) {
                    return ['value' => $data->id];
                }
            ],

            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'value' => function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                'detail' => function ($model, $key, $index, $column) {
                    $searchModel = new \common\models\search\BannerStatSerach();
                    $searchModel->banner_id = $model->id;
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                    return Yii::$app->controller->renderPartial('banner-stat', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]);
                },
            ],

            // 'destination_url:url',
            // 'width',
            // 'height',
            /*[
                'attribute' => 'id',
                'value' => function($data){
                    return  $data->id;
                },
                'vAlign' => 'middle',
            ],*/
            [
                'attribute' => 'is_active',
                'value' => function($data){
                    //return  isset($data->bannerStatus[$data->type]) ? $data->bannerStatus[$data->is_active] : '';
                    return    SwitchInput::widget([
                        'name' => 'status_banner',
                        'options' => ['class' => 'switch-id-status','data-id'=>$data->id],
                        'items' => [
                            ['label' => 'On', 'value' => 1],
                            ['label' => 'Off', 'value' => 0]
                        ],
                        'value' => $data->is_active,
                        'type' => SwitchInput::CHECKBOX
                    ]);
                },
                'filter' => $searchModel->bannerStatus,
                'format' => 'raw',
                'vAlign' => 'middle',
            ],
            [
                'class'=>'kartik\grid\EditableColumn',
                'attribute'=>'name',
                'value' => function($data){
                    return  $data->name;
                },
                'vAlign'=>'middle'
            ],
            [
                'attribute' => 'banner_placement_id',
                'value' => function($data){
                    $model = \common\models\BannerPlacement::find()->where(['id' => $data->banner_placement_id])->one();
                    if($model){
                        return $model->name;
                    }
                },
                'vAlign' => 'middle',
            ],

          /*  [
                'class'=>'kartik\grid\EditableColumn',
                'attribute' => 'banner_placement_id',
                'value' => function($data){
                    $model = \common\models\BannerPlacement::find()->where(['id' => $data->banner_placement_id])->one();
                    return $model->name;
                },
                'vAlign' => 'middle',
                'editableOptions' => [
                    'data'=>ArrayHelper::map(\common\models\BannerPlacement::find()->where(['id'=>$searchModel->banner_placement_id])->one(),'id','name')  ,
                    'inputType' => Editable::INPUT_LIST,
                ],
                'filter' => $searchModel->bannerType,
            ],*/

            [
                'label' => 'Width-Height',
                'value' => function($data){
                    $model = \common\models\BannerPlacement::find()->where(['id' => $data->banner_placement_id])->one();
                    return $model->width.'-'.$model->height;
                },
                //'filter' => $searchModel->bannerType,
                'headerOptions' => ['class'=>'big-color'],
                'vAlign' => 'middle',
            ],

            [
                'attribute' => 'type',
                'value' => function($data){
                    return  isset($data->bannerType[$data->type]) ? $data->bannerType[$data->type] : '';
                },
                'filter' => $searchModel->bannerType,
                'vAlign' => 'middle',
            ],
 

            [
                'label' => 'Limit Count',
                'value' => function($data){
                    $model = \common\models\BannerRules::find()->where(['banner_id' => $data->id])->one();
                    if($model){
                        return   $model->limit_count;
                    }
                },
                'headerOptions' => ['class'=>'big-color'],
                'vAlign' => 'middle',
            ],



            [
                'label' => 'Limit Days',
                'value' => function($data){
                    $model = \common\models\BannerRules::find()->where(['banner_id' => $data->id])->one();
                    if($model){
                        return   $model->limit_days;
                    }
                },
                'headerOptions' => ['class'=>'big-color'],
                'vAlign' => 'middle',
            ],
            [
                'label' => 'Limit End Date',
                'value' => function($data){
                    $model = \common\models\BannerRules::find()->where(['banner_id' => $data->id])->one();
                    if($model){
                        return   $model->limit_end_date !== '01-01-1970' ? $model->limit_end_date : '';
                    }

                },
                'headerOptions' => ['class'=>'big-color'],
                'vAlign' => 'middle',
            ],


            [
                'label' => 'Device',
                'value' => function($data){
                    $model = \common\models\BannerPlacement::find()->where(['id' => $data->banner_placement_id])->one();
                    if($model){
                        return $data->bannerMobile[$model->is_mobile];
                    }

                },
                'headerOptions' => ['class'=>'big-color'],
                'vAlign' => 'middle',
            ],
            [
                'attribute' => 'banner_page_id',
                'value' => function($data){
                      $model = \common\models\BannerPages::find()->where(['id' => $data->banner_page_id])->one();
                    if($model){
                        return $model->name;
                    }

                },
                'filter' =>  ArrayHelper::map(\common\models\BannerPages::find()->asArray()->all(), 'id', 'name'),
                'vAlign' => 'middle',
            ],






            /*   [
                   'label' => 'Allow Country',
                   'value' => function($data){
                       return $data->getbannerCountryIndex($data->id);
                   },
                   'headerOptions' => ['class' => 'Allow-Country'],
                   'vAlign' => 'middle',
               ],*/


            [
                'attribute' => 'created_at',
                'value' => function($data){
                    return \Yii::$app->formatter->asDate($data->created_at, 'dd-MM-yyyy H:m:s');
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'name' => 'created_at',
                    'value' =>  $searchModel->created_at ? \Yii::$app->formatter->asDate($searchModel->created_at, 'dd-MM-yyyy') : '',
                    'pluginOptions' => [
                        'format' => 'mm-dd-yyyy',
                        'autoclose' => true,
                    ],

                ]),
                'vAlign' => 'middle',
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($data){
                    return \Yii::$app->formatter->asDate($data->updated_at, 'dd-MM-yyyy H:m:s');
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'name' => 'updated_at',
                    'value' =>  $searchModel->created_at ? \Yii::$app->formatter->asDate($searchModel->created_at, 'dd-MM-yyyy') : '',
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'Y-m-d'
                        ],
                        'format' => 'mm-dd-yyyy',
                        'autoclose' => true,
                    ]
                ]),
                'vAlign' => 'middle',
            ],


            // 'is_limit_ended',
            // 'banner_placement_id',
            // 'banner_page_id',
            // 'banner_age_id',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>


<?php
$this->registerJs("  
               jQuery('.bootstrap-switch-handle-on,.bootstrap-switch-label,.bootstrap-switch-handle-off').each(function(){ 
                      $(this).click(function () { 
                              var bannerClass = $(this).closest('.bootstrap-switch.bootstrap-switch-wrapper');
                               
                              if(bannerClass.is( '.bootstrap-switch-off')){
                                   var status = 0;
                              } else if(bannerClass.is( '.bootstrap-switch-on')){
                                   var status = 1;
                              }
                              
                              var bannerId = $(this).closest('.bootstrap-switch-container').find('.switch-id-status').attr('data-id');

                              $.ajax({
                                   url: '/pivadmins/banner-pages/ajax-banner-status',
                                   type: 'post',
                                   data: {bannerId: bannerId,status:status},
                                   success: function (data) {
                                      console.log(data)
                                   }
                               });
                });
                });
            
             $(document).ready(function(){
               $('#MyButton').click(function(){
            
                 var HotId = $('#w8').yiiGridView('getSelectedRows');
                   var SelectedRows = $('.kv-grid-table.table.table-bordered.table-striped.kv-table-wrap tr.danger');
                   $.ajax({
                     type: 'POST',
                     url : '/pivadmins/banner/multiple-delete',
                     data : {row_id: HotId},
                     success : function(data) { 
                      SelectedRows.remove();  
                     }
                 });
            
             });
             });
    
    
    
    
            $('#modalButton').click(function(){
               $('#modal').modal('show')
                     .find('#modalContent')
                     .load($(this).attr('value'));
            });
   
            $('#updateModalButton').click(function(){
               var HotId = $('#w8').yiiGridView('getSelectedRows');
               if(HotId.length >1){
                 alert('Choose only one section')
               }else{    
                    
                    if(HotId[0]){
                     $('#modal').modal('show')
                     .find('#modalContent')
                     .load('/pivadmins/banner/update?id='+HotId[0]+'');
                    }else{
                         alert('Choose one section')
                    }
               }


            });
                
                
                
                
  ");


?>