<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BrandModels */

$this->title = Yii::t('app', 'Create Brend Models');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Brend Models'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brend-models-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
