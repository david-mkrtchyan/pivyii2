<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\BrendModelsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Brend Models');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brend-models-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Brend Models'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
               'attribute' => 'brend_id',
                'value' => function($data){
                    return $data->brand['name_'.Yii::$app->language];
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'name' => 'BrandModelsSearch[brend_id]',
                    'value' => isset($searchModel['brend_id']) ? $searchModel['brend_id']: null,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => 'Select a brand ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                    'data' => ArrayHelper::map(\common\models\Brand::find()->asArray()->all(),'id','name_'.Yii::$app->language)
                ]),
            ],


            'name_ru',
            'name_en',
            'name_lt',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
