<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BrandModels */
$lang = Yii::$app->language;
$this->title = Yii::t('app', 'Update Brend Models: ' . $model["name_$lang"], [
    'nameAttribute' => '' .  $model["name_$lang"],
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Brend Models'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' =>  $model["name_$lang"], 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="brend-models-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
