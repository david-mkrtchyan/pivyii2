<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CategoryFieldValue */
/* @var $fieldId integer */

$this->title = 'Create Field Value';
$this->params['breadcrumbs'][] = ['label' => 'Category Field Values', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-field-value-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'fieldId' => $fieldId
    ]) ?>

</div>