<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategoryFieldValueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Category Field Values';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-field-value-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Field Value', ['create'], ['class' => 'open_modal btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'category_field_id',
            'value_lt',
            'value_ru',
            'value_en',
            'created_at',
            'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
