<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CategoryFieldValue */
/* @var $fieldId integer */

$this->title = 'Update Category Field Value: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Category Field Values', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="category-field-value-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'fieldId' => $fieldId
    ]) ?>

</div>
