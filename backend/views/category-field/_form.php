<?php

use backend\models\Config;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use unclead\multipleinput\MultipleInput;
use yii\web\View;
use common\models\CategoryFieldValue;
use yii\helpers\ArrayHelper;
use common\models\Category;

/* @var $this yii\web\View */
/* @var $model common\models\CategoryField */
/* @var $form yii\widgets\ActiveForm */


?>

<?php $this->registerJs("
             $('#categoryfield-type').change(function() {
             if( $(this).val() == 2 )
             {
                $('#categoryfield-use_database,#categoryfield-db_name','#categoryfield-db_name_type').attr('disabled','disabled');
               $( '#categoryfield-allow_multiple,#categoryfield-allow_range,#categoryfield-range_max,#categoryfield-range_max,#categoryfield-range_min' ).removeAttr('disabled');
               $('#multiple-fieldValue').removeAttr('hidden');
               disabledDiv();
             }
             else if($(this).val() == 0)
             {
               $('#categoryfield-use_database').removeAttr('disabled');
               $('#allow_multiple').removeAttr('disabled');
               $( '#categoryfield-db_name,#categoryfield-db_name_type,#categoryfield-allow_range,#categoryfield-range_max,#categoryfield-range_max,#categoryfield-range_min' ).attr('disabled','disabled');
               $('#multiple-fieldValue').attr('hidden','hidden');
               disabledDiv();
                 $('#categoryfield-use_database').closest('.form-group').show();
             }  else if($(this).val() == 1){
                  $('#categoryfield-use_database').removeAttr('disabled');
                 $( '#categoryfield-db_name,#categoryfield-db_name_type,#categoryfield-allow_multiple,#categoryfield-allow_range,#categoryfield-range_max,#categoryfield-range_max,#categoryfield-range_min' ).attr('disabled','disabled');
                 $('#multiple-fieldValue').attr('hidden','hidden');
                 disabledDiv();
                 $('#categoryfield-use_database').closest('.form-group').show();
             } else if($(this).val() == 3 || $(this).val() == 4)
             {
               $( '#categoryfield-allow_multiple,#categoryfield-allow_range,#categoryfield-range_max,#categoryfield-range_max,#categoryfield-range_min' ).removeAttr('disabled');
               $( '#categoryfield-db_name,#categoryfield-db_name_type,#categoryfield-allow_multiple,#categoryfield-allow_range,#categoryfield-range_min,#categoryfield-range_max,#categoryfield-use_database').attr('disabled','disabled');
               $('#multiple-fieldValue').removeAttr('hidden');
                 disabledDiv();
             }
            });
             $('#categoryfield-type').trigger('change');
            
             "
    , View::POS_END) ?>


<div class="category-fields-form">

    <?php $form = ActiveForm::begin(); ?>
    <!--beforeSave() is getting category model with url['id']-->
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropdownList(
        $model::getType()
    // ['prompt' => 'Select Type']
    ) ?>
    <?php if ($model->isNewRecord) : ?>
        <!--When type is select, show input fields -->
        <div id="multiple-fieldValue" hidden>

            <?= $form->field(new CategoryFieldValue(), 'value')->widget(MultipleInput::className(), [
                'max' => 80,
                'columns' => [
                    [
                        'name' => 'value_lt',
                        'title' => 'Value Lt',
                        'enableError' => true,
                        // 'defaultValue' => 1,

                    ],
                    [
                        'name' => 'value_ru',
                        'title' => 'Value Ru',
                        'enableError' => true,
                        'options' => [
                            'class' => 'input-priority'
                        ]
                    ],
                    [
                        'name' => 'value_en',
                        'title' => 'Value En',
                        'enableError' => true,
                        'options' => [
                            'class' => 'input-priority'
                        ]
                    ]
                ]
            ])->label(false);
            ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model, 'allow_multiple')->dropdownList($model::getAllowMultiple()) ?>

    <?= $form->field($model, 'allow_range')->dropdownList($model::getRange()) ?>

    <?= $form->field($model, 'range_min')->input('number') ?>

    <?= $form->field($model, 'range_max')->input('number') ?>

    <?= $form->field($model, 'value_source')->input('number') ?>

    <?= $form->field($model, 'rank')->input('number') ?>

    <?= $form->field($model, 'required')->checkbox() ?>

    <?= $form->field($model, 'use_database')->checkbox() ?>

    <?= $form->field($model, 'allow_filter')->checkbox() ?>

    <?= $form->field($model, 'db_name')->dropdownList(Config::getAllowTableList()) ?>

    <?= $form->field($model, 'db_name_type')->dropdownList(\common\models\opcia\Opcia::getTypeList()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJs(" 
    
        $('#categoryfield-db_name').change(function(){
              if($(this).val() == 'opcia'){
                  $( \"#categoryfield-db_name_type\" ).removeAttr('disabled');
                  $('#categoryfield-db_name_type').closest('.form-group').show();
              }else{
                  $( \"#categoryfield-db_name_type\" ).attr( \"disabled\",'disabled' );
                  $('#categoryfield-db_name_type').closest('.form-group').hide();
               }
        });
        
        $('#categoryfield-use_database').change(function(){
            if ($(this).is(':checked')) {
              $( \"#categoryfield-db_name\" ).removeAttr('disabled');
              $('#categoryfield-db_name').closest('.form-group').show(); 
            }else{
              $( \"#categoryfield-db_name\" ).attr( \"disabled\",'disabled' );
              $( \"#categoryfield-db_name_type\" ).attr( \"disabled\",'disabled' );
              $('#categoryfield-db_name_type').closest('.form-group').hide();
              $('#categoryfield-db_name').closest('.form-group').hide();
            } 
        }) 
    
    
             $('#categoryfield-type').trigger('change');
             function disabledDiv(){
       
                 if($('select:disabled').length){
                        $('select:disabled').each(function(){
                              $(this).closest('.form-group').hide();
                     })
                 } 
                 
                 if($('input:disabled').length){
                     $('input:disabled').each(function(){
                              $(this).closest('.form-group').hide();
                     })
                 }
                 
                     if($( \"input:enabled\" ))
                {
                     $( \"input:enabled\" ).each(function(){
                         $(this).closest('.form-group').show();
                     })
                }
                
                  if($( \"select:enabled\" ))
                {
                     $( \"select:enabled\" ).each(function(){
                         $(this).closest('.form-group').show();
                     })
                }
                
                
                
                   if ($('#categoryfield-use_database').is(':checked')) {
                      $( \"#categoryfield-db_name\" ).removeAttr('disabled');
            
                      if($('#categoryfield-use_database').val() == 'opcia'){
                           $( \"#categoryfield-db_name_type\" ).removeAttr('disabled');
                           $('#categoryfield-db_name_type').closest('.form-group').show(); 
                      }else{
                             $( \"#categoryfield-db_name_type\" ).attr( \"disabled\",'disabled' );
                             $('#categoryfield-db_name_type').closest('.form-group').hide();
                      } 
                      $('#categoryfield-db_name').closest('.form-group').show(); 
                      $('#categoryfield-db_name').change()
                   }
             }
             disabledDiv();
             "
    , View::POS_END) ?>


