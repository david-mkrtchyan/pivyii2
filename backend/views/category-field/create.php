<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CategoryField */
/* @var $categoryFieldValueModel common\models\CategoryFieldValue */

$this->title = 'Create Fields';
$this->params['breadcrumbs'][] = ['label' => 'Category Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-fields-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categoryFieldValueModel' => $categoryFieldValueModel
    ]) ?>

</div>

