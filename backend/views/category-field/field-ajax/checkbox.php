<?php

use common\models\ProductField;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$productFields = ProductField::findAll(['product_id' => $model->id, 'category_field_id' => $categoryField->id]);
$fieldModel->allCategoryFields[$categoryField->id] = ArrayHelper::getColumn($productFields, 'category_field_value_id');
$fieldValues = ArrayHelper::map($categoryField->categoryFieldValues, 'id', 'value_' . Yii::$app->language);
$values = ArrayHelper::getValue($fieldModel['allCategoryFields'], $categoryField->id);

$class = $categoryField->required ? "required" : false;
$for = str_replace(' ', '_', $categoryField->name);
echo '<div class="form-group ' . $class . '">
        <label class="control-label" for="form-'.$for. '" >' . $categoryField->name . '</label>';
echo Html::checkboxList("ProductField[allCategoryFields][$categoryField->id]", $values, $fieldValues
    , [
        'class' => "form-control",
        'required' => $class,
        'id' => "form-{$for}",
    ]);

echo "<span class=\"glyphicon form-control-feedback\" aria-hidden=\"true\"></span>
                <div class=\"help-block with-errors\"></div>";
echo '</div>';
