<?php

use common\models\ProductField;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$productField = ProductField::findOne(['product_id' => $model->id, 'category_field_id' => $categoryField->id]);
if ($productField) {
    $fieldModel->allCategoryFields[$categoryField->id] = $productField->value;
}


$class = $categoryField->required ? "required" : false;
$for = str_replace(' ', '_', $categoryField->name);
echo '<div class="form-group ' . $class . '">
        <label class="control-label" for="form-' . $for . '" >' . $categoryField->name . '</label>';
$values = ArrayHelper::getValue($fieldModel['allCategoryFields'], $categoryField->id);

echo CKEditor::widget([
    'name' => "ProductField[allCategoryFields][$categoryField->id]",
    'value' => $values,
    'id' => "form-{$for}",
    'editorOptions' => [
        'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
        'inline' => false, //по умолчанию false
    ],
]);
if($class == "required"){
    $this->registerJs('
                $("#form-'.$for.'").attr("required","required")
           ');
}


echo "<span class=\"glyphicon form-control-feedback\" aria-hidden=\"true\"></span>
                <div class=\"help-block with-errors\"></div>";
echo '</div>';