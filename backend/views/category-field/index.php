<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategoryFieldearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Category Fields';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-fields-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'allow_multiple',
                'value' => function($data){
                    return  ArrayHelper::getValue($data::getAllowMultiple(),$data->allow_multiple);
                }
            ],

            [
                'attribute' => 'type',
                'value' => function($data){
                    return  ArrayHelper::getValue($data::getType(),$data->type);
                }
            ],
            [
                'attribute' => 'allow_range',
                'value' => function($data){
                    return  ArrayHelper::getValue($data::getRange(),$data->allow_range);
                }
            ],
            // 'allow_range',
            // 'range_min',
            // 'range_max',
            // 'value_source',
            // 'created_at',
            // 'updated_at',
            // 'rank',

            ['class' => 'yii\grid\ActionColumn',
                'controller' => 'category-field'
            ]
        ],
    ]); ?>
</div>
