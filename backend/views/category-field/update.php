<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CategoryField */
/* @var $categoryFieldValueModel common\models\CategoryFieldValue */

$this->title = 'Update Fields: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Category Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="category-fields-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categoryFieldValueModel' => $categoryFieldValueModel
    ]) ?>

</div>
