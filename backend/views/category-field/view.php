<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use common\models\CategoryFieldValue;
use yii\data\ArrayDataProvider;

/* @var $this yii\web\View */
/* @var $model common\models\CategoryField */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Category Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-fields-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'allow_multiple',
                'value' => function($data){
                    return  ArrayHelper::getValue($data::getAllowMultiple(),$data->allow_multiple);
                }
            ],

            [
                'attribute' => 'type',
                'value' => function($data){
                    return  ArrayHelper::getValue($data::getType(),$data->type);
                }
            ],
            [
                'attribute' => 'allow_range',
                'value' => function($data){
                    return  ArrayHelper::getValue($data::getRange(),$data->allow_range);
                }
            ],
            'range_min',
            'range_max',
            'value_source',
            [
                'attribute' => 'created_at',
                'value' => function($data){
                    return date('Y-m-d',$data->created_at);
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($data){
                    return date('Y-m-d',$data->updated_at);
                }
            ],
            'rank',
        ],
    ]) ?>

    <?php

    $dataProvider = new ArrayDataProvider([
        'allModels' => CategoryFieldValue::find()->where(['category_field_id' => $model->id])->all(),
    ]);
    ?>

    <?php if ($model->type == $model::typeSelect || $model->type ==  $model::typeCheckbox
        || $model->type ==  $model::typeRadio) {

        echo Html::a('Create Field Value', ['category-field-value/create', 'id' => $model->id], ['title'=>'Create','class' => 'open_modal btn btn-primary']);

        echo GridView::widget([
            'dataProvider' => $dataProvider,

            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'value_lt',
                'value_ru',
                'value_en',
                [
                    'attribute' => 'actions',
                    'label' => '',
                    'format' => 'html',
                    'value' => function($model) {
                        return \yii\helpers\Html::a(
                            '<span class="glyphicon glyphicon-pencil green"></span>',
                            ['category-field-value/update', 'id' => $model->id],
                            [
                                'title' => 'Status',
                                'data-pjax' => '0',
                            ]
                        );
                    }
                ],
            ],
        ]);


    } ?>

</div>
