<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CategoryFieldsWish */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-fields-wish-form">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => true,
    ]); ?>

    <ul class="nav nav-tabs">
        <li  class="active">
            <a data-toggle="tab" href="#name_lt">
                <i class="flag flag-18 flag-18-lt"></i> LT
            </a>
        </li>

        <li>
            <a data-toggle="tab" href="#name_en">
                <i class="flag flag-18 flag-18-en"></i> English
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#name_ru">
                <i class="flag flag-18 flag-18-ru"></i> Русский
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="name_lt" class="tab-pane fade in active">
            <?= $form->field($model, 'value_lt')->textInput(['maxlength' => true]) ?>
        </div>
        <div id="name_en" class="tab-pane fade">
            <?= $form->field($model, 'value_en')->textInput(['maxlength' => true]) ?>
        </div>
        <div id="name_ru" class="tab-pane fade">
            <?= $form->field($model, 'value_ru')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?= $form->field($model, 'rank')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
