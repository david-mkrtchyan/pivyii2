<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CategoryFieldsWish */

$this->title = Yii::t('app', 'Create Category Fields Wish');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Category Fields Wishes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-fields-wish-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
