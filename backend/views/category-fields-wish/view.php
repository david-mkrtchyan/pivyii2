<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\CategoryFieldsWish */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Category Fields Wishes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-fields-wish-view">


    <?php /*DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'category_id',
            'value',
            'created_at',
            'updated_at',
        ],
    ]) */?>


    <h1><?= Html::encode($this->title) ?> Category Wish  Fields </h1>
    <p><?= Html::a('Create Fields', ['category-field/create', 'id' => $model->id], ['title'=>'Create','class' => 'open_modal btn btn-primary']) ?></p>
    <?php Pjax::begin() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id',
            'name',
            'type',
            [
                'attribute'=>'created_at',
                'format'=>['date','php:d.m.Y H:i']
            ],
            [
                'attribute'=>'updated_at',
                'format'=>['date','php:d.m.Y H:i']
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'category-field'
            ]

        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>
