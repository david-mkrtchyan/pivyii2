<?php

use bajadev\ckeditor\CKEditor;
use common\models\Category;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>
    <ul class="nav nav-tabs">
        <li class="active">
            <a data-toggle="tab" href="#name_lt">
                <i class="flag flag-18 flag-18-lt"></i> LT
            </a>
        </li>

        <li>
            <a data-toggle="tab" href="#name_en">
                <i class="flag flag-18 flag-18-en"></i> English
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#name_ru">
                <i class="flag flag-18 flag-18-ru"></i> Русский
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="name_lt" class="tab-pane fade in active">
            <?= $form->field($model, 'name_lt')->textInput(['maxlength' => true]) ?>
        </div>
        <div id="name_en" class="tab-pane fade">
            <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>
        </div>
        <div id="name_ru" class="tab-pane fade">
            <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group field-parent-category">
        <label>Parent Category</label>
        <div id="tree" class="aciTree"></div>
    </div>

    <?= $form->field($model, 'parent_id')->hiddenInput(['maxlength' => true])->label(false) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?php //= $form->field($model, 'parent_id')->textInput() ?>

    <?= $form->field($model, 'meta_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_desc')->widget(CKEditor::className(), [

        'editorOptions' => [
            'preset' => 'full', // basic, standard, full
            'inline' => false,
            'filebrowserBrowseUrl' => 'browse-images',
            'filebrowserUploadUrl' => 'upload-images',
            'extraPlugins' => 'imageuploader',
        ],
    ]); ?>

    <?= $form->field($model, 'show')->dropDownList(Category::$show_hide) ?>

    <?php if (!$model->isNewRecord) { ?>
        <?= \common\models\Config::getIcon($model->icon); ?>
    <?php } ?>
    <?= $form->field($model, 'icon')->fileInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rank')->textInput(['type' => 'number']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>


    <?php
    $this->registerJs("  $(function(){ 
            
            // init the tree
               var api = $('#tree').aciTree({
                ajax: {
                    url: '/pivadmins/category/category-tree?id=" . $model->id . "'
                },
                        radio: true,
                        unique: true
            });
            
             
            var log = $('.log div');

            // write to log
   
            $('#tree,#tree-combined').on('acitree', function(event, api, item, eventName, options){
                if (api.isItem(item)){ 
                    log.prepend('<p>' + eventName + ' [' + api.getId(item) + ']</p>');
                } else {
                   
                    log.prepend('<p>' + eventName + ' [tree]</p>');
                }
                
                if(eventName == 'checked'){
                        $('#category-parent_id').val(api.getId(item));
                 }
            });


 
        });", View::POS_END, 'my-options'); ?>


</div>
