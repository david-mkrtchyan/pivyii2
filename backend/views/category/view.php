<?php

use common\models\CategoryField;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="category-view">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name_en',
                'name_ru',
                'name_lt',
                'slug',
                'parent_id',
                'meta_key',
                'meta_desc',
                'show',
                'icon',
                'rank',
                'created_at:dateTime',
                'updated_at:dateTime',
            ],
        ]) ?>


        <h1><?= Html::encode($this->title) ?> Category Wish </h1>
        <p><?= Html::a('Create Wish', ['category-fields-wish/create', 'id' => $model->id], ['class' => 'open_modal create_wish btn btn-primary', 'title' => 'Create Wish']) ?></p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'id' => 'applyFilter2',
            'columns' => [

                ['class' => 'kartik\grid\ExpandRowColumn',
                    'allowBatchToggle' => true,
                    'expandOneOnly' => false,
                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'detail' => function ($model, $key, $index, $column) {
                        $searchModel = new \common\models\search\CategoryFieldSearch();
                        $searchModel->category_fields_wish_id = $model->id;
                        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                        return Yii::$app->controller->renderPartial('//category-field/index', [
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                        ]);

                    },

                    'detailOptions' => [
                        'class' => 'kv-state-enable1',
                    ],

                ],
                'value_lt',
                'value_ru',
                'value_en',
                'rank',
                ['class' => 'yii\grid\ActionColumn',
                    'controller' => 'category-fields-wish'
                ]
            ],
        ]); ?>
    </div>

<?php
yii\bootstrap\Modal::begin([
    'header' => '<span id="modalHeaderTitle"></span>',
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    //'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo '<div class="modalContent"><div style="text-align:center"><img src="' . Url::to('/pivadmins/img/ajax-loader_2.gif') . '"></div></div>';
yii\bootstrap\Modal::end();
$load = '<div style="text-align:center"><img src="' . Url::to('/pivadmins/img/ajax-loader_2.gif') . '"></div>';
$this->registerJs("(function($){

    $(document).on('click', '.table.table-striped.table-bordered a,.open_modal', function (e) { 
           e.preventDefault();
           var class_new = 'modal_'+$('.modal-lg').length;
            if ($('#modal').hasClass('in')) {
            $('body').append('<div class=\"'+class_new+' fade modal\" >'+$('#modal').html()+'</div>')
           
        }
        var modalHtml =  $('#modal');
        if($('.'+class_new).length) {
            modalHtml = $('.'+class_new);
            modalHtml.find('.modalContent').html('$load');
        }
           modalHtml.modal('show')
                    .find('.modalContent')
                    .load($(this).attr('href'));
            document.getElementById('modalHeaderTitle').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
       
    });
    
    
   $(document).on('submit', 'form', function(e) {
    e.preventDefault();
    var form = $(this);
    var formData = form.serialize();
        $.ajax({
            url: form.attr(\"action\"),
            type: form.attr(\"method\"),
            data: formData,
            success: function (data) {
                <!--$.pjax({container: '#applyFilter2'})-->
                form.closest('.fade.modal').modal('hide');
            },
            error: function () {
               
            }
        });
    });


 })(jQuery);");
