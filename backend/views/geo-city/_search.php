<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\GeoCitySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="geo-city-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'geonameid') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'asciiname') ?>

    <?= $form->field($model, 'latitude') ?>

    <?php // echo $form->field($model, 'longitude') ?>

    <?php // echo $form->field($model, 'fclass') ?>

    <?php // echo $form->field($model, 'fcode') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'cc2') ?>

    <?php // echo $form->field($model, 'admin1') ?>

    <?php // echo $form->field($model, 'admin1_id') ?>

    <?php // echo $form->field($model, 'population') ?>

    <?php // echo $form->field($model, 'elevation') ?>

    <?php // echo $form->field($model, 'gtopo30') ?>

    <?php // echo $form->field($model, 'moddate') ?>

    <?php // echo $form->field($model, 'deleted') ?>

    <?php // echo $form->field($model, 'publish') ?>

    <?php // echo $form->field($model, 'sort') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
