<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use bajadev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Help */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="help-form">

    <?php $form = ActiveForm::begin(); ?>

    <ul class="nav nav-tabs">
        <li class="active">
            <a data-toggle="tab" href="#name_lt">
                <i class="flag flag-18 flag-18-lt"></i> LT
            </a>
        </li>

        <li>
            <a data-toggle="tab" href="#name_en">
                <i class="flag flag-18 flag-18-en"></i> English
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#name_ru">
                <i class="flag flag-18 flag-18-ru"></i> Русский
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="name_lt" class="tab-pane fade in active">
            <?= $form->field($model, 'page_name_en')->textInput(['maxlength' => true]) ?>
        </div>
        <div id="name_en" class="tab-pane fade">
            <?= $form->field($model, 'page_name_ru')->textInput(['maxlength' => true]) ?>
        </div>
        <div id="name_ru" class="tab-pane fade">
            <?= $form->field($model, 'page_name_lt')->textInput(['maxlength' => true]) ?>
        </div>
    </div>


    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->widget(CKEditor::className(), [

        'editorOptions' => [
            'preset' => 'full', // basic, standard, full
            'inline' => false,
            'filebrowserBrowseUrl' => 'browse-images',
            'filebrowserUploadUrl' => 'upload-images',
            'extraPlugins' => 'imageuploader',
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
