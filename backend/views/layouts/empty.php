<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use backend\assets\EmptyAsset;
use yii\bootstrap\Alert;
use yii\helpers\Html;
EmptyAsset::register($this);

$app = Yii::$app;
$request = $app->request;
$baseUrl = $request->baseUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= $app->language ?>">
<head>
    <meta charset="<?= $app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <link href="<?= $baseUrl ?>/css/bootstrap.min.css" rel="stylesheet">

    <?php $this->head() ?>
</head>
<body class="gray-bg">
<?php $this->beginBody() ?>

<?= $content ?>

<!-- Mainly scripts -->
<script src="<?= $baseUrl ?>/js/jquery-3.1.1.min.js"></script>
<script src="<?= $baseUrl ?>/js/bootstrap.min.js"></script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
