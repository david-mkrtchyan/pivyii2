<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use common\models\Config;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);

$app = Yii::$app;
$request = $app->request;
$baseUrl = $request->baseUrl;
$route = Config::getRouteFirstPath($this->context->route);
$user = $app->user->identity;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= $app->language ?>">
<head>
    <meta charset="<?= $app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <link href="<?= $baseUrl ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $baseUrl ?>/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?= $baseUrl ?>/css/animate.css" rel="stylesheet">
    <link href="<?= $baseUrl ?>/css/style.css" rel="stylesheet">

    <?php $this->head() ?>
</head>
<body class="gray-bg">
<?php $this->beginBody() ?>


<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <?= Alert::widget() ?>
        <?= $content ?>

    </div>
</div>

<!-- Mainly scripts -->
<script src="<?= $baseUrl ?>/js/jquery-3.1.1.min.js"></script>
<script src="<?= $baseUrl ?>/js/bootstrap.min.js"></script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
