<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use common\models\Config;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);

$app = Yii::$app;
$request = $app->request;
$baseUrl = $request->baseUrl;

$route = $this->context->id;
$user = $app->user->identity;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= $app->language ?>">
<head>
    <meta charset="<?= $app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>/css/aciTree.css" media="all">
    <link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>/css/demo.css" media="all">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                                <?php if ($user->image): ?>
                                    <?=  Config::getAvatarPhoto($user->image,48,'img-circle') ?>
                                <?php else: ?>
                                    <img alt="image" class="img-circle" src="<?= $baseUrl ?>/img/profile_small.jpg"/>
                                <?php endif; ?>
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong
                                            class="font-bold"><?= $user->first_name . ' ' . $user->last_name ?></strong>
                             </span></span> </a>
                    </div>
                    <div class="logo-element">
                        IN+
                    </div>
                </li>

                <li class="<?= ($route == 'user' || $route == 'admin') ? 'active' : 'users-tab' ?>"> <!--active-->
                    <a href="index.html"><i class="fa fa-th-large"></i> <span class="nav-label">
                            <?= Yii::t('app', 'Members') ?>
                        </span> <span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li  class="<?= ($route == 'admin') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/users/admin/index']) ?>"> <?= Yii::t('app', 'Admins') ?>
                            </a>
                        </li>
                        <li  class="<?= ($route == 'user') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/users/user/index']) ?>">
                                <?= Yii::t('app', 'Users') ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="<?= ($route == 'category') ? 'active' : 'users-tab' ?>">
                    <a href="<?= Url::to(['/category/index']) ?>"><i class="fa fa-diamond"></i>
                        <span class="nav-label"> <?= Yii::t('app', 'Categories') ?></span>
                    </a>
                </li>


                <li class="<?= ($route == 'product') ? 'active' : 'product' ?>"> <!--active-->
                    <a href="<?= Url::to(['/product/index']) ?>"><i class="fa fa-th-large"></i> <span class="nav-label">
                            <?= Yii::t('app', 'Products') ?>
                        </span> <span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li class="<?= ($route == 'product') ? 'active' : null ?>" >
                            <a href="<?= Url::to(['/product/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Product') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'add') ? 'active' : null ?> ">
                            <a href="<?= Url::to(['/product/add']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Add Product') ?></span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="<?= ($route == 'contact') ? 'active' : 'contact' ?>">
                    <a href="<?= Url::to(['/contact']) ?>"><i class="fa fa-envelope-o"></i>
                        <span class="nav-label"> <?= Yii::t('app', 'Contact') ?></span>
                    </a>
                </li>

                <li class="<?= ($route == 'brand' || $route == 'brand-models') ? 'active' : 'brand' ?>"> <!--active-->
                    <a href="<?= Url::to(['/brand/index']) ?>"><i class="fa fa-th-large"></i> <span class="nav-label">
                            <?= Yii::t('app', 'Cars') ?>
                        </span> <span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">

                        <li class="add <?= ($route == 'brand') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/brand/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Brands') ?></span>
                            </a>
                        </li>

                        <li class="<?= ($route == 'brand-models') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/brand-models/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Models') ?></span>
                            </a>
                        </li>

                    </ul>
                </li>


                <li class="<?= ($route == 'opcia-fuel' ||
                    $route == 'opcia-rooms' ||
                    $route == 'opcia-floor' ||
                    $route == 'opcia-condition' ||
                    $route == 'opcia-daily-monthly' ||
                    $route == 'opcia-engine-type' ||
                    $route == 'opcia-make' ||
                    $route == 'opcia-parts-type' ||
                    $route == 'opcia-shoe-size' ||
                    $route == 'opcia-size' ||
                    $route == 'opcia-transmission' ||
                    $route == 'opcia-tv-type' ||
                    $route == 'opcia-type' ||
                    $route == 'opcia-color' ||
                    $route == 'opcia-owner-dealer-name' ||
                    $route == 'opcia' ||
                    $route == 'rate' ||
                    $route == 'opcia-construction-type') ? 'active' : 'opcia' ?>"> <!--active-->
                    <a href="<?= Url::to(['/brand/index']) ?>"><i class="fa fa-th-large"></i> <span class="nav-label">
                            <?= Yii::t('app', 'Opcia') ?>
                        </span> <span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">

                        <li class="<?= ($route == 'opcia') ? 'active' : 'opcia' ?>">
                            <a href="<?= Url::to(['/opcia/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Opcia') ?></span>
                            </a>
                        </li>


                        <li class="<?= ($route == 'rate') ? 'active' : 'rate' ?>">
                            <a href="<?= Url::to(['/rate/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Rate') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'opcia-fuel') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/opcia-fuel/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Fuel') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'opcia-color') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/opcia-color/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Color') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'opcia-construction-type') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/opcia-construction-type/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Construction Types') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'opcia-rooms') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/opcia-rooms/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Rooms') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'opcia-floor') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/opcia-floor/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Floor') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'opcia-condition') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/opcia-condition/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Conditions') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'opcia-daily-monthly') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/opcia-daily-monthly/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Daily Monthly') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'opcia-engine-type') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/opcia-engine-type/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Engine Type') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'opcia-make') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/opcia-make/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Make') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'opcia-parts-type') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/opcia-parts-type/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Parts Type') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'opcia-shoe-size') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/opcia-shoe-size/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Shoe Size') ?></span>
                            </a>
                        </li>
                        <li class="add <?= ($route == 'opcia-size') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/opcia-size/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Size') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'opcia-transmission') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/opcia-transmission/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Transmission') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'opcia-tv-type') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/opcia-tv-type/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Tv Type') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'opcia-type') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/opcia-type/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Type') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'opcia-owner-dealer-name') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/opcia-owner-dealer-name/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Owner Dealer') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'opcia-equipment') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/opcia-equipment/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Equipment') ?></span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="<?= ($route == 'banner' || $route == 'banner-pages') ? 'active' : 'banner-pages' ?>"> <!--active-->
                    <a href="<?= Url::to(['/banner-pages/index']) ?>"><i class="fa fa-th-large"></i> <span class="nav-label">
                            <?= Yii::t('app', 'Banners') ?>
                        </span> <span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">

                        <li class="add <?= ($route == 'banner') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/banner/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Banners') ?></span>
                            </a>
                        </li>

                        <li class="add <?= ($route == 'banner-pages') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/banner-pages/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Banner Pages') ?></span>
                            </a>
                        </li>

                    </ul>
                </li>


                <li class="<?= ($route == 'report-category' || $route == 'report') ? 'active' : 'banner-pages' ?>"> <!--active-->
                    <a href="<?= Url::to(['/report-category/index']) ?>"><i class="fa fa-th-large"></i> <span class="nav-label">
                            <?= Yii::t('app', 'Reports') ?>
                        </span> <span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">

                        <li class="add <?= ($route == 'report') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/report/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Reports') ?></span>
                            </a>
                        </li>


                        <li class="add <?= ($route == 'report-category') ? 'active' : null ?>">
                            <a href="<?= Url::to(['/report-category/index']) ?>"><i class="fa fa-diamond"></i>
                                <span class="nav-label"> <?= Yii::t('app', 'Report Categories') ?></span>
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="<?= ($route == 'settings') ? 'active' : 'settings' ?>">
                    <a href="<?= Url::to(['/settings/update']) ?>"><i class="fa fa-cog fa-fw"></i>
                        <span class="nav-label"> <?= Yii::t('app', 'Settings') ?></span>
                    </a>
                </li>

                  <li class="<?= ($route == 'geo-city') ? 'active' : 'geo-city' ?>">
                    <a href="<?= Url::to(['/geo-city/index']) ?>"><i class="fa fa-diamond"></i>
                        <span class="nav-label"> <?= Yii::t('app', 'City') ?></span>
                    </a>
                </li>

                <li class="<?= ($route == 'help') ? 'active' : 'help' ?>">
                    <a href="<?= Url::to(['/help/index']) ?>"><i class="fa fa-info"></i>
                        <span class="nav-label"> <?= Yii::t('app', 'Footer Info') ?></span>
                    </a>
                </li>

            </ul>

        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
                    </a>
                    <form role="search" class="navbar-form-custom" action="search_results.html">
                        <div class="form-group">
                            <input type="text" placeholder="Search for something..." class="form-control"
                                   name="top-search" id="top-search">
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">


                        <li class="<?php // Config::getLangActive('lt') ?>">
                            <a href="<?= Url::to(['/', 'language' => 'lt']) ?>"><i
                                        class="flag flag-18 flag-18-lt"></i>
                            </a>
                        </li>

                        <li class="<?php //  Config::getLangActive('en') ?>">
                            <a href="<?= Url::to(['/', 'language' => 'en']) ?>"><i
                                        class="flag flag-18 flag-18-en"></i>
                            </a>
                        </li>
                        <li class="<?php // Config::getLangActive('ru') ?>">
                            <a href="<?= Url::to(['/', 'language' => 'ru']) ?>"><i
                                        class="flag flag-18 flag-18-ru"></i>
                            </a>
                        </li>



                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell"></i> <span class="label label-primary">8</span>
                        </a>


                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="mailbox.html">
                                    <div>
                                        <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>

                        </ul>
                    </li>


                    <li>
                        <?= Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            '  <i class="fa fa-sign-out"></i> '.Yii::t('app', 'Logout').' (' . $user->first_name . ' ' . $user->last_name . ')',
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm(); ?>
                    </li>
                    <li>
                        <a class="right-sidebar-toggle">
                            <i class="fa fa-tasks"></i>
                        </a>
                    </li>
                </ul>

            </nav>
        </div>

        <div class="">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Breadcrumbs</h2>
                    <?= Breadcrumbs::widget([
                        'homeLink' => ['label' => Yii::t('app','Home'), 'url' => '/'],
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
            <div>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>
    </div>
    <div class="small-chat-box fadeInRight animated">

        <div class="heading" draggable="true">
            <small class="chat-date pull-right">
                02.19.2015
            </small>
            Small chat
        </div>

        <div class="content">

            <div class="left">
                <div class="author-name">
                    Monica Jackson
                    <small class="chat-date">
                        10:02 am
                    </small>
                </div>
                <div class="chat-message active">
                    Lorem Ipsum is simply dummy text input.
                </div>

            </div>
            <div class="right">
                <div class="author-name">
                    Mick Smith
                    <small class="chat-date">
                        11:24 am
                    </small>
                </div>
                <div class="chat-message">
                    Lorem Ipsum is simpl.
                </div>
            </div>
            <div class="left">
                <div class="author-name">
                    Alice Novak
                    <small class="chat-date">
                        08:45 pm
                    </small>
                </div>
                <div class="chat-message active">
                    Check this stock char.
                </div>
            </div>
            <div class="right">
                <div class="author-name">
                    Anna Lamson
                    <small class="chat-date">
                        11:24 am
                    </small>
                </div>
                <div class="chat-message">
                    The standard chunk of Lorem Ipsum
                </div>
            </div>
            <div class="left">
                <div class="author-name">
                    Mick Lane
                    <small class="chat-date">
                        08:45 pm
                    </small>
                </div>
                <div class="chat-message active">
                    I belive that. Lorem Ipsum is simply dummy text.
                </div>
            </div>


        </div>
        <div class="form-chat">
            <div class="input-group input-group-sm">
                <input type="text" class="form-control">
                <span class="input-group-btn"> <button
                            class="btn btn-primary" type="button">Send
                </button> </span></div>
        </div>

    </div>
<!--    <div id="small-chat">

        <span class="badge badge-warning pull-right">5</span>
        <a class="open-small-chat">
            <i class="fa fa-comments"></i>

        </a>
    </div>-->
    <div id="right-sidebar" class="animated">
        <div class="sidebar-container">

            <ul class="nav nav-tabs navs-3">

                <li class="active"><a data-toggle="tab" href="#tab-1">
                        Notes
                    </a></li>
                <li><a data-toggle="tab" href="#tab-2">
                        Projects
                    </a></li>
                <li class=""><a data-toggle="tab" href="#tab-3">
                        <i class="fa fa-gear"></i>
                    </a></li>
            </ul>

            <div class="tab-content">


                <div id="tab-1" class="tab-pane active">

                    <div class="sidebar-title">
                        <h3><i class="fa fa-comments-o"></i> Latest Notes</h3>
                        <small><i class="fa fa-tim"></i> You have 10 new message.</small>
                    </div>

                    <div>

                        <div class="sidebar-message">
                            <a href="#">
                                <div class="pull-left text-center">
                                    <img alt="image" class="img-circle message-avatar" src="<?= $baseUrl ?>/img/a1.jpg">

                                    <div class="m-t-xs">
                                        <i class="fa fa-star text-warning"></i>
                                        <i class="fa fa-star text-warning"></i>
                                    </div>
                                </div>
                                <div class="media-body">

                                    There are many variations of passages of Lorem Ipsum available.
                                    <br>
                                    <small class="text-muted">Today 4:21 pm</small>
                                </div>
                            </a>
                        </div>
                        <div class="sidebar-message">
                            <a href="#">
                                <div class="pull-left text-center">
                                    <img alt="image" class="img-circle message-avatar" src="<?= $baseUrl ?>/img/a2.jpg">
                                </div>
                                <div class="media-body">
                                    The point of using Lorem Ipsum is that it has a more-or-less normal.
                                    <br>
                                    <small class="text-muted">Yesterday 2:45 pm</small>
                                </div>
                            </a>
                        </div>
                        <div class="sidebar-message">
                            <a href="#">
                                <div class="pull-left text-center">
                                    <img alt="image" class="img-circle message-avatar" src="<?= $baseUrl ?>/img/a3.jpg">

                                    <div class="m-t-xs">
                                        <i class="fa fa-star text-warning"></i>
                                        <i class="fa fa-star text-warning"></i>
                                        <i class="fa fa-star text-warning"></i>
                                    </div>
                                </div>
                                <div class="media-body">
                                    Mevolved over the years, sometimes by accident, sometimes on purpose (injected
                                    humour and the like).
                                    <br>
                                    <small class="text-muted">Yesterday 1:10 pm</small>
                                </div>
                            </a>
                        </div>
                        <div class="sidebar-message">
                            <a href="#">
                                <div class="pull-left text-center">
                                    <img alt="image" class="img-circle message-avatar" src="<?= $baseUrl ?>/img/a4.jpg">
                                </div>

                                <div class="media-body">
                                    Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the
                                    <br>
                                    <small class="text-muted">Monday 8:37 pm</small>
                                </div>
                            </a>
                        </div>
                        <div class="sidebar-message">
                            <a href="#">
                                <div class="pull-left text-center">
                                    <img alt="image" class="img-circle message-avatar" src="<?= $baseUrl ?>/img/a8.jpg">
                                </div>
                                <div class="media-body">

                                    All the Lorem Ipsum generators on the Internet tend to repeat.
                                    <br>
                                    <small class="text-muted">Today 4:21 pm</small>
                                </div>
                            </a>
                        </div>
                        <div class="sidebar-message">
                            <a href="#">
                                <div class="pull-left text-center">
                                    <img alt="image" class="img-circle message-avatar" src="<?= $baseUrl ?>/img/a7.jpg">
                                </div>
                                <div class="media-body">
                                    Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes
                                    from a line in section 1.10.32.
                                    <br>
                                    <small class="text-muted">Yesterday 2:45 pm</small>
                                </div>
                            </a>
                        </div>
                        <div class="sidebar-message">
                            <a href="#">
                                <div class="pull-left text-center">
                                    <img alt="image" class="img-circle message-avatar" src="<?= $baseUrl ?>/img/a3.jpg">

                                    <div class="m-t-xs">
                                        <i class="fa fa-star text-warning"></i>
                                        <i class="fa fa-star text-warning"></i>
                                        <i class="fa fa-star text-warning"></i>
                                    </div>
                                </div>
                                <div class="media-body">
                                    The standard chunk of Lorem Ipsum used since the 1500s is reproduced below.
                                    <br>
                                    <small class="text-muted">Yesterday 1:10 pm</small>
                                </div>
                            </a>
                        </div>
                        <div class="sidebar-message">
                            <a href="#">
                                <div class="pull-left text-center">
                                    <img alt="image" class="img-circle message-avatar" src="<?= $baseUrl ?>/img/a4.jpg">
                                </div>
                                <div class="media-body">
                                    Uncover many web sites still in their infancy. Various versions have.
                                    <br>
                                    <small class="text-muted">Monday 8:37 pm</small>
                                </div>
                            </a>
                        </div>
                    </div>

                </div>

                <div id="tab-2" class="tab-pane">

                    <div class="sidebar-title">
                        <h3><i class="fa fa-cube"></i> Latest projects</h3>
                        <small><i class="fa fa-tim"></i> You have 14 projects. 10 not completed.</small>
                    </div>

                    <ul class="sidebar-list">
                        <li>
                            <a href="#">
                                <div class="small pull-right m-t-xs">9 hours ago</div>
                                <h4>Business valuation</h4>
                                It is a long established fact that a reader will be distracted.

                                <div class="small">Completion with: 22%</div>
                                <div class="progress progress-mini">
                                    <div style="width: 22%;" class="progress-bar progress-bar-warning"></div>
                                </div>
                                <div class="small text-muted m-t-xs">Project end: 4:00 pm - 12.06.2014</div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="small pull-right m-t-xs">9 hours ago</div>
                                <h4>Contract with Company </h4>
                                Many desktop publishing packages and web page editors.

                                <div class="small">Completion with: 48%</div>
                                <div class="progress progress-mini">
                                    <div style="width: 48%;" class="progress-bar"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="small pull-right m-t-xs">9 hours ago</div>
                                <h4>Meeting</h4>
                                By the readable content of a page when looking at its layout.

                                <div class="small">Completion with: 14%</div>
                                <div class="progress progress-mini">
                                    <div style="width: 14%;" class="progress-bar progress-bar-info"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="label label-primary pull-right">NEW</span>
                                <h4>The generated</h4>
                                There are many variations of passages of Lorem Ipsum available.
                                <div class="small">Completion with: 22%</div>
                                <div class="small text-muted m-t-xs">Project end: 4:00 pm - 12.06.2014</div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="small pull-right m-t-xs">9 hours ago</div>
                                <h4>Business valuation</h4>
                                It is a long established fact that a reader will be distracted.

                                <div class="small">Completion with: 22%</div>
                                <div class="progress progress-mini">
                                    <div style="width: 22%;" class="progress-bar progress-bar-warning"></div>
                                </div>
                                <div class="small text-muted m-t-xs">Project end: 4:00 pm - 12.06.2014</div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="small pull-right m-t-xs">9 hours ago</div>
                                <h4>Contract with Company </h4>
                                Many desktop publishing packages and web page editors.

                                <div class="small">Completion with: 48%</div>
                                <div class="progress progress-mini">
                                    <div style="width: 48%;" class="progress-bar"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="small pull-right m-t-xs">9 hours ago</div>
                                <h4>Meeting</h4>
                                By the readable content of a page when looking at its layout.

                                <div class="small">Completion with: 14%</div>
                                <div class="progress progress-mini">
                                    <div style="width: 14%;" class="progress-bar progress-bar-info"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="label label-primary pull-right">NEW</span>
                                <h4>The generated</h4>
                                <!--<div class="small pull-right m-t-xs">9 hours ago</div>-->
                                There are many variations of passages of Lorem Ipsum available.
                                <div class="small">Completion with: 22%</div>
                                <div class="small text-muted m-t-xs">Project end: 4:00 pm - 12.06.2014</div>
                            </a>
                        </li>

                    </ul>

                </div>

                <div id="tab-3" class="tab-pane">

                    <div class="sidebar-title">
                        <h3><i class="fa fa-gears"></i> Settings</h3>
                        <small><i class="fa fa-tim"></i> You have 14 projects. 10 not completed.</small>
                    </div>

                    <div class="setings-item">
                    <span>
                        Show notifications
                    </span>
                        <div class="switch">
                            <div class="onoffswitch">
                                <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example">
                                <label class="onoffswitch-label" for="example">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="setings-item">
                    <span>
                        Disable Chat
                    </span>
                        <div class="switch">
                            <div class="onoffswitch">
                                <input type="checkbox" name="collapsemenu" checked class="onoffswitch-checkbox"
                                       id="example2">
                                <label class="onoffswitch-label" for="example2">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="setings-item">
                    <span>
                        Enable history
                    </span>
                        <div class="switch">
                            <div class="onoffswitch">
                                <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example3">
                                <label class="onoffswitch-label" for="example3">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="setings-item">
                    <span>
                        Show charts
                    </span>
                        <div class="switch">
                            <div class="onoffswitch">
                                <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example4">
                                <label class="onoffswitch-label" for="example4">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="setings-item">
                    <span>
                        Offline users
                    </span>
                        <div class="switch">
                            <div class="onoffswitch">
                                <input type="checkbox" checked name="collapsemenu" class="onoffswitch-checkbox"
                                       id="example5">
                                <label class="onoffswitch-label" for="example5">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="setings-item">
                    <span>
                        Global search
                    </span>
                        <div class="switch">
                            <div class="onoffswitch">
                                <input type="checkbox" checked name="collapsemenu" class="onoffswitch-checkbox"
                                       id="example6">
                                <label class="onoffswitch-label" for="example6">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="setings-item">
                    <span>
                        Update everyday
                    </span>
                        <div class="switch">
                            <div class="onoffswitch">
                                <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example7">
                                <label class="onoffswitch-label" for="example7">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="sidebar-content">
                        <h4>Settings</h4>
                        <div class="small">
                            I belive that. Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            And typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since
                            the 1500s.
                            Over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>
</div>

<!-- Mainly scripts -->
<script src="<?= $baseUrl ?>/js/jquery-3.1.1.min.js"></script>
<script src="<?= $baseUrl ?>/js/bootstrap.min.js"></script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
