<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\opcia\OpciaColor */

$this->title = Yii::t('app', 'Create Opcia Color');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Opcia Colors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcia-color-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
