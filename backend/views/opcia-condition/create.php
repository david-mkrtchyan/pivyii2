<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\opcia\OpciaCondition */

$this->title = Yii::t('app', 'Create Opcia Condition');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Opcia Conditions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcia-condition-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
