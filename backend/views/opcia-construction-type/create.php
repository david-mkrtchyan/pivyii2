<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\opcia\OpciaConstructionType */

$this->title = Yii::t('app', 'Create Opcia Construction Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Opcia Construction Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcia-construction-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
