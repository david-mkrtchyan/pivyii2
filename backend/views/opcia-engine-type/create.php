<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\opcia\OpciaEngineType */

$this->title = Yii::t('app', 'Create Opcia Engine Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Opcia Engine Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcia-engine-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
