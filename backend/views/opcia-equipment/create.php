<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\opcia\OpciaEquipment */

$this->title = Yii::t('app', 'Create Opcia Equipment');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Opcia Equipments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcia-equipment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
