<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\opcia\OpciaMake */

$this->title = Yii::t('app', 'Create Opcia Make');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Opcia Makes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcia-make-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
