<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OpciaOwnerAgency */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="opcia-owner-agency-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'owner_agency_name_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'owner_agency_name_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'owner_agency_name_lt')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
