<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\OpciaOwnerAgency */

$this->title = Yii::t('app', 'Create Opcia Owner Agency');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Opcia Owner Agencies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcia-owner-agency-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
