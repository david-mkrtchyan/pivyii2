<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\opcia\OpciaPartsType */

$this->title = Yii::t('app', 'Create Opcia Parts Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Opcia Parts Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcia-parts-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
