<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\opcia\OpciaShoeSize */

$this->title = Yii::t('app', 'Create Opcia Shoe Size');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Opcia Shoe Sizes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcia-shoe-size-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
