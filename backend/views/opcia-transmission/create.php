<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\opcia\OpciaTransmission */

$this->title = Yii::t('app', 'Create Opcia Transmission');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Opcia Transmissions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcia-transmission-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
