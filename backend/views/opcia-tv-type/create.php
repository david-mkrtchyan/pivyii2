<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\opcia\OpciaTvType */

$this->title = Yii::t('app', 'Create Opcia Tv Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Opcia Tv Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opcia-tv-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
