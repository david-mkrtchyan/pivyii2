<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\opcia\Opcia */

$this->title = Yii::t('app', 'Create Opcia');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Opcias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="opcia-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
