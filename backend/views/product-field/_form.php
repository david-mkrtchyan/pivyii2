<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use \common\models\ProductField;
/* @var $this yii\web\View */
/* @var $model common\models\ProductField */
/* @var $form yii\widgets\ActiveForm */
/* @var $product \common\models\Product */
?>

<span id="hiddspan"><?= $model->product_id ?></span>
<div class="product-field-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php foreach ($product->categoryFields as $categoryField) : ?>

        <?php
        if ($categoryField->type) {

            $productFields = ProductField::findAll(['product_id' => $product->id, 'category_field_id' => $categoryField->id]);

            if ($categoryField->allow_range) {
                $rangeInterval = [];
                $range = range($categoryField->range_min, $categoryField->range_max);
                $fieldValues = array_combine($range, $range);
                $model->allCategoryFields[$categoryField->id] = ArrayHelper::getColumn($productFields, 'value');
                echo $form->field($model, "allCategoryFields[$categoryField->id]")->dropDownList($fieldValues, ['prompt'=>'Select value ...', 'options' => [null => ['disabled' => true]],
                    'multiple' => $categoryField->allow_multiple ? true : false
                ])->label($categoryField->name);
            } else {
                $model->allCategoryFields[$categoryField->id] = ArrayHelper::getColumn($productFields, 'category_field_value_id');
                $fieldValues = ArrayHelper::map($categoryField->categoryFieldValues, 'id', 'value_'.Yii::$app->language);
                echo $form->field($model, "allCategoryFields[$categoryField->id]")->dropDownList($fieldValues, [
                    'multiple' => $categoryField->allow_multiple ? true : false
                ])->label($categoryField->name);
            }


        } else {
            $productField = ProductField::findOne(['product_id' => $product->id, 'category_field_id' => $categoryField->id]);
            if($productField) {
                $model->allCategoryFields[$categoryField->id] = $productField->value;
            }
            echo $form->field($model, "allCategoryFields[$categoryField->id]")->textInput(['maxlength' => true])->label($categoryField->name);
        }
        ?>


    <?php endforeach; ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


    <script src="https://select2.github.io/vendor/js/jquery.min.js"></script>
    <script src="https://select2.github.io/dist/js/select2.full.js"></script>

    <script>
        $("select").select2({
            language: "es"
        });
    </script>
<?php
//
//$this->registerJs("
//    $(document).ready(function() {
//        $('select').select2();
//    });
//", \yii\web\View::POS_READY);
//
//?>