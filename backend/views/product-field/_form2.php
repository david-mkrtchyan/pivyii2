<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model common\models\ProductField */
/* @var $form yii\widgets\ActiveForm */
/* @var $products array */
/* @var $categoryFields array */
/* @var $allowmultiple number */
/* @var $type number */
?>

<link rel="stylesheet" href="https://select2.github.io/dist/css/select2.min.css">
<span id="hiddspan"><?= $model->product_id ?></span>

<div class="product-field-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_id')->hiddenInput()->label(false); ?>

    <?php foreach ($categoryFields as $key => $value) : ?>

        <?php if ($model->categoryField->type) { ?>
            <?php if (!$model->categoryField->allow_range) { ?>
                <?php $categoryFields2 = \yii\helpers\ArrayHelper::map(\common\models\CategoryFieldValue::find()->where(['category_field_id' => $model->category_field_id])->all(), 'id', 'value'); ?>
                <?= $form->field($model, 'category_field_value_id')->dropDownList($categoryFields2, ['multiple' => $model->categoryField->allow_multiple ? true : false]) ?>
            <?php } else { ?>
                <?php $rangeInterval = array();
                for ($i = $model->categoryField->range_min; $i <= $model->categoryField->range_max; $i++) {
                    $rangeInterval[$i] = $i;
                }
                $query = \common\models\ProductField::find()->where(['product_id' => $model->product_id, 'category_field_id' => $model->category_field_id])->all();
                $model->values = \yii\helpers\ArrayHelper::map($query, 'value', 'value');
                ?>
                <?= $form->field($model, 'values')->dropDownList($rangeInterval, ['multiple' => $model->categoryField->allow_multiple ? true : false]); ?>
            <?php } ?>

        <?php } else { ?>
            <?php ?>
            <?= $form->field($model, 'value')->textInput() ?>
        <?php } ?>

    <?php endforeach; ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script src="https://select2.github.io/vendor/js/jquery.min.js"></script>
<script src="https://select2.github.io/dist/js/select2.full.js"></script>
<script>
    $("select").select2({
        language: "es"
    });
</script>