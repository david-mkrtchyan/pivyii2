<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProductField */

$this->title = 'Create Product Field';
$this->params['breadcrumbs'][] = ['label' => 'Product Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-field-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model',  'product')) ?>

</div>
