<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProductField */
/* @var $categoryFields array */
/* @var $type  */

$this->title = 'Update Product Field: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-field-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categoryFields' => $categoryFields,
    ]) ?>

</div>
