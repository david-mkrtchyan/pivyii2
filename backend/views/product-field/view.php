<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ProductField */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-field-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'product_id',
                'label' =>'Product Name',
                'value' => $model->product ? $model->product->name : 'Product Name'
            ],
            [
                'attribute' => 'category_field_id',
                'label' =>'Category Field',
                'value' => $model->categoryField ? $model->categoryField->name : 'Category Field'
            ],
            [
                'attribute' => 'value',
                'value' =>   $model->categoryField->type ? $model->categoryFieldValue ? $model->categoryFieldValue->value : $model->value : $model->value
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
