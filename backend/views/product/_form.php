<?php

use bajadev\ckeditor\CKEditor;
use borales\extensions\phoneInput\PhoneInput;
use common\models\Config;
use common\models\Geo;
use common\models\ProductField;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
/* @var $shops array */
/* @var $categories array */
/* @var $brands array */

?>

<div class="product-form">
    <?php if ($fieldModel->hasErrors()): ?>
        <?php if ($fieldModel->getErrors()): ?>
            <?php foreach ($fieldModel->getErrors() as $errors): ?>
                <div class="alert alert-danger">
                    <?php foreach ($errors as $error): ?>
                        <?= $error ?><br>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($allErrors): ?>
        <?php if ($allErrors): ?>
            <div class="alert alert-danger">
                <?php foreach ($allErrors as $errors): ?>
                    <?php foreach ($errors as $error): ?>
                        <?= $error ?><br>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($model->category && $categoryWish = $model->category->categoryFieldsWish): ?>
        <?php $form = ActiveForm::begin([
            'enableClientValidation' => true,
            'enableClientValidation' => true,
            'options' => [
                'enctype' => 'multipart/form-data',
                'data-toggle' => 'validator',
                'role' => 'form',
            ]]);
        ?>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= Html::encode($this->title) ?></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                </div>
            </div>
            <div class="ibox-content">

                <?php // $form->field($model, 'imageFile')->fileInput(); ?>

                <div class="form-radio form-group ">
                    <?php
                    foreach ($categoryWish as $key => $value): ?>
                        <?php
                        $checked = null;

                        if ((!$model->category_fields_wish_id && $key == 0) || ($value['id'] == $model->category_fields_wish_id)) {
                            $checked = true;
                        }

                        echo Html::radio('Product[category_fields_wish_id]', $checked, ['value' => $value['id'], 'label' => $value['value_' . Yii::$app->language], 'class' => 'form-radio-checkbox']);
                        ?>
                    <?php endforeach; ?>
                </div>


                <?= $form->field($model, 'title')->textInput() ?>


                <div class="form-category-filed">
                    <?= backend\widgets\CategoryFields::widget(['model' => $model, 'fieldModel' => $fieldModel, 'id' => $model->category_fields_wish_id]); ?>
                </div>

                <?= $form->field($model, 'status')->dropDownList($model->statusList) ?>

                <?= $form->field($model, 'description')->textarea(); ?>

                <?php if ($model->isNewRecord) {
                    echo FileInput::widget([
                        //'name' => 'attachment',
                        'options' => [
                            'multiple' => true,
                            'accept' => 'image/*',
                            'paramName' => 'image',
                        ],
                        'model' => $imageAlbom,
                        'attribute' => 'image[]',
                        'pluginOptions' => [
                            'allowedFileExtensions' => ["jpg", "png", "gif", "jpeg"],
                            'showPreview' => true,
                            'showCaption' => true,
                            'showRemove' => true,
                            'message' => "hello",
                            'showUpload' => false,
                            'removeLabel' => Yii::t('app', 'Remove'),
                            'browseLabel' => Yii::t('app', 'Add'),
                            'maxFileCount' => 7,
                            'initialCaption' => Yii::t('app', 'Add Photos'),
                            'uploadUrl' => Url::to(['#']),
                            'uploadLabel' => false,
                            'uploadAsync' => false,
                        ]
                    ]);
                } else {
                    echo FileInput::widget([
                        //'name' => 'attachment',
                        'options' => [
                            'multiple' => true,
                            'accept' => 'image/*'
                        ],
                        'model' => $imageAlbom,
                        'attribute' => 'image[]',
                        'pluginOptions' => [
                            'showPreview' => true,
                            'showCaption' => true,
                            'showRemove' => true,
                            'showUpload' => false,
                            'removeLabel' => Yii::t('app', 'Remove'),
                            'browseLabel' => Yii::t('app', 'Add'),
                            'allowedFileExtensions' => ["jpg", "png", "gif", "jpeg"],
                            'maxFileCount' => 7 - $imageCount,
                            'initialPreview' => $allImage,
                            'overwriteInitial' => false,
                            'initialPreviewAsData' => true,
                            'initialCaption' => Yii::t('app', 'Add Photos'),
                            'initialPreviewConfig' => $allImageId,
                            'maxFileSize' => 2800,
                            'uploadUrl' => Url::to(['#']),
                            'uploadAsync' => true,
                        ],
                        'pluginEvents' => [
                            'filesorted' => 'function(event, params) { $.ajax({
                                     url: "' . Url::to(['/product/sort-image']) . '",
                                     type: "post",
                                     data: { previewId:params.stack[params.newIndex].key, oldIndex:params.oldIndex, newIndex:params.newIndex, stack:params.stack},
                                    }).done(function( msg ) { 
                                                      });
                                                                        ;

                                    }',
                        ],

                    ]);
                }
                ?>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    <?= Yii::t('app', 'Contact Information How would you like to be contacted?') ?>
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                </div>
            </div>
            <div class="ibox-content">
                <?= $form->field($productAddress, 'address')->textInput() ?>


                <?= $form->field($productAddress, 'city_id')->widget(Select2::classname(), [
                    'data' => Config::getRegionList(),
                    'options' => ['placeholder' => 'Select a state ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>

                <?= $form->field($productAddress, 'number_one')
                    ->widget(PhoneInput::className(), [
                        'options' => ['class' => 'form-control'],
                        'jsOptions' => [
                            'preferredCountries' => ['LT'],
                            'nationalMode' => false
                        ]
                    ]);
                ?>

                <?= $form->field($productAddress, 'number_two')
                    ->widget(PhoneInput::className(), [
                        'options' => ['class' => 'form-control'],
                        'jsOptions' => [
                            'preferredCountries' => ['LT'],
                            'nationalMode' => false
                        ]
                    ]);
                ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn  btn-primary' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    <?php endif; ?>

</div>


<?php

$script = <<< JS
 $(document).ready(function(){ 
    $('.form-radio-checkbox').click(function(){
        var id = $(this).val();
        var catId = {$model->category_id};
        $.ajax({
              url: '/pivadmins/product/category-update-field',
              type: 'POST',
              data: {
                  'id' : id,
                  'catId' : catId,
              },
              success: function(data) { 
                      $('.form-category-filed').html(data);
                      $('#w0').validator('destroy');
                      $('#w0').validator(); 
              },
              error: function(e) { 
              }
         }); 
    });
    
    
     $(document).on('change',"#form-Brand",function() {
        var id = $(this).val();
        $.ajax({
             type: "post",
             url:  "/pivadmins/category-field/get-model-list",
             data: {
                id: id
             },
             dataType: "html",
             success: function(data) {
                $("#form-Model").html(data);
             },
         });
     }) 
        
}); 
JS;
$this->registerJs($script);
?>

