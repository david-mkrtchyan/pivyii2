<section style="    margin-top: 26px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="left-sidebar ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Categories</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <ul class="catalog category-products ibox-content">
                        <?= \backend\components\MenuWidget::widget(['tpl'=>'menu']); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>