<?php

use common\models\Rate;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $categories array */
/* @var $shops array */
/* @var $brands array */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'value' => function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                'detail' => function ($model, $key, $index, $column) {
                    $searchModel = new \common\models\search\ProductMessageSearch();
                    $searchModel->product_id = $model->id;
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                    return Yii::$app->controller->renderPartial('product-message', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]);
                },
            ],

            [
                'attribute' => 'category_id',
                'format' => 'raw',
                'value' => function($model){
                    /* @var $model common\models\Product */
                    return Html::a($model->category['name_'.Yii::$app->language],\yii\helpers\Url::toRoute(['category/view', 'id' => $model->category_id]), ['target' => '_blank']);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'value' => isset($searchModel['category_id']) ? $searchModel['category_id']: null,
                    'name' => 'ProductSearch[category_id]',
                    'options' => [
                        'placeholder' => 'Select a category field ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                    'data' => $categories
                ]),
            ],
             [
                'attribute' => 'title',
                'value' => function($model){
                    return  $model->title;
                },
            ],
            'price',
            [
                'attribute' => 'rate_id',
                'value' => function($model){
                    return  ArrayHelper::getValue($model,'rate.name_'.Yii::$app->language);
                },
                'filter' => ArrayHelper::map( Rate::find()->all(), 'id' ,'name_'.Yii::$app->language)
            ],
            'counter',
            // 'created_at',
            // 'updated_at',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
