<?php

use common\models\ProductMessage;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

?>
<?php Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn', ],

        [
            'attribute' => 'message',
            'value' => function($data){
                return  $data->message;
            },
        ],
        [
            'attribute' => 'type',
            'value' => function($data){
                return  $data->type;
            },
            'filter' => $searchModel::getTypes(),
        ],
        [
            'attribute' => 'deleted',
            'value' => function($data){
                return  ArrayHelper::getValue($data::getDeleted(),$data->deleted);
            },
            'filter' => $searchModel::getDeleted(),
        ],

        [
            'header' => 'Like',
            'value' => function($data){
                return  \common\models\ProductMessageLike::find()->where(['product_message_id' => $data->id,'like' => 1])->count();
            },
        ],

        [
            'header' => 'DisLike',
            'value' => function($data){
                return  \common\models\ProductMessageLike::find()->where(['product_message_id' => $data->id,'like' => 0])->count();
            },
        ],
    ],
    'summary' => "",

]); ?>
<?php Pjax::end(); ?>



