<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $shops array */
/* @var $categories array */
/* @var $brands array */

$this->title = 'Update Product: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'allErrors' => $allErrors,
        'model' => $model,
        'fieldModel' => $fieldModel,
        'imageAlbom' => $imageAlbom,
        'imageCount' => $imageCount,
        'allImage' => $allImage,
        'allImageId' => $allImageId,
        'productAddress' => $productAddress,
    ]) ?>

</div>
