<?php

use common\models\Config;
use common\models\ReportCategory;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Reports');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--    <p>
        <? /*= Html::a(Yii::t('app', 'Create Report'), ['create'], ['class' => 'btn btn-success']) */ ?>
    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'report_category_id',
                'format' => 'raw',
                'value' => function($model){
                    /* @var $model common\models\Product */
                    return $model->reportCategory['name_'.Yii::$app->language];
                 },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'value' => isset($searchModel['report_category_id']) ? $searchModel['report_category_id']: null,
                    'name' => 'ReportSearch[report_category_id]',
                    'options' => [
                        'placeholder' => 'Select a report ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                    'data' => ArrayHelper::map(ReportCategory::find()->all(),'id','name_'.Yii::$app->language)
                ]),
            ],

            [
                'attribute' => 'product_id',
                'format' => 'raw',
                'value' => function($model){
                    /* @var $model common\models\Report */
                    return Html::a(Config::replaceTitle(ArrayHelper::getValue($model,'product.title'),30),Url::toRoute(['product/view', 'id' => $model->product_id]), ['target' => '_blank']);
                },
            ],

            'email:email',
            [
                'attribute' => 'created_at',
                'value' => function($data){
                    return \Yii::$app->formatter->asDate($data->created_at, 'dd-MM-yyyy H:m:s');
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'name' => 'created_at',
                    'value' =>  $searchModel->created_at ? \Yii::$app->formatter->asDate($searchModel->created_at, 'dd-MM-yyyy') : '',
                    'pluginOptions' => [
                        'format' => 'mm-dd-yyyy',
                        'autoclose' => true,
                    ],

                ]),
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($data){
                    return \Yii::$app->formatter->asDate($data->updated_at, 'dd-MM-yyyy H:m:s');
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'name' => 'updated_at',
                    'value' =>  $searchModel->created_at ? \Yii::$app->formatter->asDate($searchModel->created_at, 'dd-MM-yyyy') : '',
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'Y-m-d'
                        ],
                        'format' => 'mm-dd-yyyy',
                        'autoclose' => true,
                    ]
                ]),
            ],

            //'updated_at',
            [
                'class' => 'yii\grid\ActionColumn',

                'template' => '{view} {delete}',
                'header' => 'Actions'
            ]
        ],
    ]); ?>
</div>
