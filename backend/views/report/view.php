<?php

use common\models\Config;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Report */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'report_category_id',
                'format' => 'raw',
                'value' => function($model){
                    /* @var $model common\models\Product */
                    return $model->reportCategory['name_'.Yii::$app->language];
                },
            ],

            [
                'attribute' => 'product_id',
                'format' => 'raw',
                'value' => function($model){
                    /* @var $model common\models\Report */
                    return Html::a(ArrayHelper::getValue($model,'product.title'),Url::toRoute(['product/view', 'id' => $model->product_id]), ['target' => '_blank']);
                },
            ],

            'email:email',
            'review:ntext',
            [
                'attribute' => 'created_at',
                'value' => function($data){
                    return \Yii::$app->formatter->asDate($data->created_at, 'dd-MM-yyyy H:m:s');
                },

            ],
            [
                'attribute' => 'updated_at',
                'value' => function($data){
                    return \Yii::$app->formatter->asDate($data->updated_at, 'dd-MM-yyyy H:m:s');
                },
            ],
        ],
    ]) ?>

</div>
