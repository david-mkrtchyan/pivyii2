<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Login( Admin Panel)');
$this->params['breadcrumbs'][] = $this->title;
?>

<div>
    <h1 class="logo-name"><?= Html::img('/images/logo.svg') ?></h1>
</div>


<h3><?= Html::encode($this->title) ?></h3>
<p><?= Yii::t('app', 'Please fill out the following fields to login:') ?></p>

<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'class' => 'm-t',
]); ?>

<?= $form->field($model, 'email')->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Email')])->label(false) ?>

<?= $form->field($model, 'password')->passwordInput(['placeholder' => Yii::t('app', 'Password')])->label(false) ?>

<?= $form->field($model, 'rememberMe')->checkbox() ?>

<?= Html::submitButton('Login', ['class' => 'btn btn-primary block full-width m-b', 'name' => 'login-button']) ?>


<?php ActiveForm::end(); ?>

