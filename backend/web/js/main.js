
$(document).ready(function(){
    $('.catalog').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: false,
        disableLink: true,
        showCount: true,
        speed: 300
    });


    if($('#productaddress-city_id').length){
        getGeoSelect2('#productaddress-city_id');
    }


    function getGeoSelect2($id) {
        $($id).select2({
            ajax: {
                url: function () {
                    return '/geo/ajax-city/';
                },
                dataType: 'json',
                delay: 250,
                placeholder: "Please, enter Location...",
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 100) < data.total_count
                        }
                    };
                },
                cache: true
            },
            width: '',
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 2
        });
    }




    $(function () {
        $.scrollUp({
            scrollName: 'scrollUp', // Element ID
            scrollDistance: 300, // Distance from top/bottom before showing element (px)
            scrollFrom: 'top', // 'top' or 'bottom'
            scrollSpeed: 300, // Speed back to top (ms)
            easingType: 'linear', // Scroll to top easing (see http://easings.net/)
            animation: 'fade', // Fade, slide, none
            animationSpeed: 200, // Animation in speed (ms)
            scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
            //scrollTarget: false, // Set a custom target element for scrolling to the top
            scrollText: '<i class="fa fa-angle-up"></i>', // Text for element, can contain HTML
            scrollTitle: false, // Set a custom <a> title if required.
            scrollImg: false, // Set true to use image
            activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
            zIndex: 2147483647 // Z-Index for the overlay
        });
    });
});
