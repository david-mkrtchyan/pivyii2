<?php

namespace backend\widgets;

class CategoryFields extends \yii\bootstrap\Widget
{
    public $model;
    public $fieldModel;
    public $id;

    public function run(){
        return $this->render('category-fields',[
            'model' => $this->model,
            'fieldModel' => $this->fieldModel,
            'id' => $this->id,
        ]);
    }
}