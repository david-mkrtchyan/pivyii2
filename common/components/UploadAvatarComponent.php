<?php

namespace common\components;


use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;

class UploadAvatarComponent extends Component
{
    public static $userImageSize = [48, 64, 128];

    public function uploadImage($file)
    {
        $rand = uniqid(time()) . '_' . rand(1, 999999);
        $uploads_dir = Yii::getAlias('@frontend') . '/web/icon';

        $tmp_name = ArrayHelper::getValue($file, 'tempName');
        $name = basename(ArrayHelper::getValue($file, 'name'));
        $ext = pathinfo($name, PATHINFO_EXTENSION);
        $name = $rand . '.' . $ext;


        $allowed = ['gif', 'png', 'jpg', 'jpeg'];

        if (!in_array($ext, $allowed)) {
            if (!$tmp_name) {
                Yii::$app->session->setFlash('errorUploadAvatar', Yii::t('app', "You did not select an image"));
            } else {
                Yii::$app->session->setFlash('errorUploadAvatar', Yii::t('app', "The file must be gif, png, jpg, jpeg"));
            }
            return false;
        }

        if (!is_dir(Yii::getAlias('@frontend') . '/web/icon')) {
            mkdir(Yii::getAlias('@frontend') . "/web/icon/", true);
        }

        if (move_uploaded_file($tmp_name, "$uploads_dir/$name")) {
            Image::thumbnail("$uploads_dir/$name", 25, 25)
                ->save(Yii::getAlias('@frontend/web/icon/' . $name), ['quality' => 60]);
            return $name;
        }
    }

    public function uploadImageAvatar($file)
    {
        if ($file) {
            $rand = uniqid(time()) . '_' . rand(1, 999999);
            $uploads_dir = Yii::getAlias('@frontend') . '/web/user/large';

            $tmp_name = ArrayHelper::getValue($file, 'tempName');
            $name = basename(ArrayHelper::getValue($file, 'name'));
            $ext = pathinfo($name, PATHINFO_EXTENSION);
            $name = $rand . '.' . $ext;


            $allowed = ['gif', 'png', 'jpg', 'jpeg'];

            if (!in_array($ext, $allowed)) {
                if (!$tmp_name) {
                    Yii::$app->session->setFlash('errorUploadAvatar', Yii::t('app', "You did not select an image"));
                } else {
                    Yii::$app->session->setFlash('errorUploadAvatar', Yii::t('app', "The file must be gif, png, jpg, jpeg"));
                }
                return false;
            }


            $this->createUserFolders();


            if (move_uploaded_file($tmp_name, "$uploads_dir/$name")) {
                $this->removeUserOldPictures();
                if ($size = self::$userImageSize) {
                    if ($size && is_array($size)) {
                        foreach ($size as $key => $value) {
                            Image::thumbnail("$uploads_dir/$name", intval($value), intval($value))
                                ->save(Yii::getAlias('@frontend/web/user/' . $value . '/' . $name), ['quality' => 60]);
                        }
                        return $name;
                    }
                }
            }
        }
    }


    public function removeUserOldPictures()
    {
        if ($size = self::$userImageSize) {
            if ($size && is_array($size)) {
                if (is_dir(Yii::getAlias('@frontend') . '/web/user')) {
                    foreach ($size as $key => $value) {
                        if (is_dir(Yii::getAlias('@frontend') . '/web/user/' . $value))
                            //Yii::getAlias('@frontend')."/web/user/" . $key . "/".Yii::$app->user->identity->image
                            $path = Yii::getAlias('@frontend') . '/web/user/' . $value . "/" . Yii::$app->user->identity->image;
                        if (Yii::$app->user->identity->image) {
                            if (file_exists(trim('user/' . $value . "/" . Yii::$app->user->identity->image))) {
                                unlink($path);
                            }
                        }
                    }
                    if (is_dir(Yii::getAlias('@frontend') . '/web/user/large')) {
                        if (Yii::$app->user->identity->image) {
                            if (file_exists(trim('user/large/' . Yii::$app->user->identity->image))) {
                                $path = Yii::getAlias('@frontend') . '/web/user/large/' . Yii::$app->user->identity->image;
                                unlink($path);
                            }
                        }
                    }
                }

            }
        }
    }

    public function createUserFolders()
    {
        if ($size = self::$userImageSize) {
            if ($size && is_array($size)) {

                if (!is_dir(Yii::getAlias('@frontend') . '/web/user')) {
                    mkdir(Yii::getAlias('@frontend') . "/web/user/", true);
                }

                foreach ($size as $key => $value) {
                    if (!is_dir(Yii::getAlias('@frontend') . '/web/user/' . $value)) {
                        mkdir(Yii::getAlias('@frontend') . "/web/user/" . $value . "/", true);
                    }
                }
                if (!is_dir(Yii::getAlias('@frontend') . '/web/user/large')) {
                    mkdir(Yii::getAlias('@frontend') . "/web/user/large/", true);
                }
            }
        }
    }


}