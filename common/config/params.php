<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'languages'=> ['lt'=>'lt', 'en' => 'en','ru' => 'ru'],
    'languages_long'=> ['lt'=>'Литовский', 'en' => 'English','ru' => 'Русский'],
    'languages_short'=> ['lt'=>'Лит', 'en' => 'Eng', 'ru' => 'Рус'],
    'default_language'=> 'lt',
    'default_currency'=> 'USD',
];
