<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/confirm', 'username' => $user->username,'key' =>$user->auth_key]);
?>
<div class="password-reset">

    <p>Բարի գալուստ <span style="font-weight: bold;"><?= $user->username   ?></span></p>

    Սեղմեք այստեղ Ձեր հաշիվն ակտիվացնելու համար
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    <p>Ձեր գրանցման տվյալներն են </p>
    <p>Մուտքի Էլեկտրոնային հասցեն: <span style="font-weight: bold;"><?= $user->email ?></span></p>
    <p>Անուն: <span style="font-weight: bold;"><?= $user->username   ?></span></p>
    <p>Գախտնաբառ: <span style="font-weight: bold;"><?= $password  ?></span></p>

</div>