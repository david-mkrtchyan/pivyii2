<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "piv_albom".
 *
 * @property int $albom_id
 * @property string $image
 * @property int $image_rank
 * @property int $user_id
 * @property int $product_id
 */
class Albom extends \yii\db\ActiveRecord
{


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'piv_albom';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image_rank', 'user_id', 'product_id'], 'integer'],
            [['product_id'], 'required'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'albom_id' => Yii::t('app', 'Albom ID'),
            'image' => Yii::t('app', 'Image'),
            'image_rank' => Yii::t('app', 'Image Rank'),
            'user_id' => Yii::t('app', 'User ID'),
            'product_id' => Yii::t('app', 'Product'),
        ];
    }


    public static function getImagePath($create_date, $images)
    {
        if ($images !== null):
            $date_baze = date_format(date_create(date('Y-m-d', $create_date)), 'Y-m-d');
            $img = Yii::$app->request->baseUrl . '/image/' . $date_baze . '/' . $images;
        else:
            $img = Yii::$app->request->baseUrl . '/image_user/default/nopicture.png';
        endif;
        return $img;
    }

    public static function getImageFolderPath($create_date, $images)
    {
        $path = null;
        if ($images !== null):
            $date_baze = date_format(date_create(date('Y-m-d', $create_date)), 'Y-m-d');
            $path = '/image/' . $date_baze . '/';
        endif;
        return $path;
    }

    public static function getImageThumbFirst($create_at,$image)
    {
        $date_baze = date_format(date_create(date('Y-m-d', $create_at)), 'Y-m-d');
        $img = 'image/' . $date_baze . '/thumb/'.$image;

        if(file_exists($img)){
            return "/$img";
        };

        return false;
    }

    public static function getImageFrontBack($create_date, $images)
    {
        if ($images !== null):
            $date_baze = date_format(date_create(date('Y-m-d', $create_date)), 'Y-m-d');
            $img = '/image/' . $date_baze . '/' . $images;
        endif;
        return $img;
    }
}
