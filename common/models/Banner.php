<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use common\models\BannerPages;
use common\models\GeoCountry;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $type
 * @property string $content
 * @property string $destination_url
 * @property integer $width
 * @property integer $height
 * @property string $is_active
 * @property string $is_limit_ended
 * @property integer $banner_placement_id
 * @property integer $banner_page_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property BannerAge $bannerAge
 * @property BannerPages $bannerPage
 * @property BannerPlacement $bannerPlacement
 * @property BannerRules[] $bannerRules
 * @property BannerStat[] $bannerStats
 */
class Banner extends \yii\db\ActiveRecord
{
    public $file;
    public $code;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%banner}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'title', 'type', 'banner_placement_id', 'banner_page_id','is_active'], 'required'],
            [['content', 'is_active', 'is_limit_ended','file_extension_type'], 'string'],
            [['file'], 'file'],
            [['code'],'string'],
            [['type','is_active','is_limit_ended'], 'in', 'range' => [0, 1]],

            [['file'], 'required', 'on' =>'create','when' => function ($model) {
                if ($model->type == '0' ) {
                    return true;
                } elseif ($model->type == '1') {
                    return false;
                }
            }, 'whenClient' => "function (attribute, value) {
                   var name = $('#banner-type').val();
                   if(name=='0'){return name;} 
              }"
            ],

            [['code'], 'required', 'when' => function ($model) {
                if ($model->type == '1' ) {
                    return true;
                } elseif ($model->type == '0') {
                    return false;
                }
            }, 'whenClient' => "function (attribute, value) {
                   var name = $('#banner-type').val();
                   if(name=='1'){return name;} 
                }"
            ],


            [['width', 'height', 'banner_placement_id', 'banner_page_id', 'created_at', 'updated_at',], 'integer'],
            [['name', 'title', 'destination_url'], 'string', 'max' => 255],
            [['banner_page_id'], 'exist', 'skipOnError' => true, 'targetClass' => BannerPages::className(), 'targetAttribute' => ['banner_page_id' => 'id']],
            [['banner_placement_id'], 'exist', 'skipOnError' => true, 'targetClass' => BannerPlacement::className(), 'targetAttribute' => ['banner_placement_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Banner Name'),
            'title' => Yii::t('app', 'Title'),
            'type' => Yii::t('app', 'Type'),
            'content' => Yii::t('app', 'Content'),
            'destination_url' => Yii::t('app', 'Destination Url'),
            'width' => Yii::t('app', 'Width'),
            'height' => Yii::t('app', 'Height'),
            'is_active' => Yii::t('app', 'Status'),
            'is_limit_ended' => Yii::t('app', 'Limit Ended'),
            'banner_placement_id' => Yii::t('app', 'Placement Name'),
            'banner_page_id' => Yii::t('app', 'Banner Page'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerPage()
    {
        return $this->hasOne(BannerPages::className(), ['id' => 'banner_page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerPlacement()
    {
        return $this->hasOne(BannerPlacement::className(), ['id' => 'banner_placement_id']);
    }

   /* public function getBannerPlacementChecked()
    {
        $pageId = $this->banner_page_id;
        $BannerPlacements = BannerPlacement::find()->where(['banner_pages_id' => $this->banner_page_id])->all();
        if(!$BannerPlacements) return false;
        $getData = [];
        foreach ($BannerPlacements as $BannerPlacement){
            $getData[$BannerPlacement->id] = $BannerPlacement->width . '-' . $BannerPlacement->height;
        };
        return $getData;
    }*/


    public  function getBannerPlacementChecked() {
        $options = [];

        $pageId = $this->banner_page_id;
        $BannerPlacements = BannerPlacement::find()->where(['banner_pages_id' => $this->banner_page_id])->all();
        if(!$BannerPlacements) return false;


        foreach($BannerPlacements as   $p) {
            $modelPlacementsGroup = BannerPlacement::find()->where(['banner_pages_id' => $p->banner_pages_id,'is_mobile' => $p->is_mobile])->all();
            //$placemennt[] = "<optgroup  label='".$modell->bannerMobile[$modell->is_mobile]."'>";

            $child_options = [];
            foreach($modelPlacementsGroup as $child) {
                $child_options[$child->id] = $child->width . '-' . $child->height;
            }
            $options[$this->bannerMobile[$p->is_mobile] ] = $child_options;
        }
        return $options;
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerRules()
    {
        return $this->hasMany(BannerRules::className(), ['banner_id' => 'id']);
    }

    public function getBannerCountryBanner()
    {
        return $this->hasMany(BannerCountry::className(), ['banner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getBannerStats()
    {
        return $this->hasMany(BannerStat::className(), ['banner_id' => 'id']);
    }



    public function getBannerCountryIndex($dataId){
        if(!$dataId) return false;
        $countryBnnerID = BannerCountry::find()->select('country_include_id')->where(['banner_id' => $dataId])->asArray()->all();
        if(!$countryBnnerID) return false;
        $countryId = [];
        foreach ($countryBnnerID as $country){
            $countryId[] = $country['country_include_id'];
        }
        $countrys = GeoCountry::find()->select('name')->where(['geonameId' => $countryId])->all();
        if(!$countrys) return false;
        $countrysString = '';
        foreach ($countrys as $key => $country){
            $countrysString.= $country->name.', ';
        }
        $countrysString = substr($countrysString,0,-2);
        return $countrysString;
    }

    public function getBannerPages()
    {
        return ArrayHelper::map(BannerPages::find()->asArray()->all(), 'id', 'name');
    }

    public function getBannerStatus(){
        return [
            0 => 'Off',
            1 => 'On',
        ];
    }

    public function getBannerType(){
        return [
            0 => 'Medium',
            1 => 'Cod',
        ];
    }

    public function getBannerMobile(){
        return [
            0 => 'Desktop',
            1 => 'Tablet',
            2 => 'Mobile',
        ];
    }

    public function getBannerSex(){
        return [
            2 => 'All',
            0 => 'Male',
            1 => 'Female',
        ];
    }



    public function getBannerCountry()
    {
        return ArrayHelper::map(GeoCountry::find()->asArray()->all(), 'geonameId', 'name');
    }

}
