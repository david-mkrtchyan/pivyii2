<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "banner_country".
 *
 * @property integer $id
 * @property string $country_include_id
 * @property integer $banner_id
 *
 * @property Banner $banner
 */
class BannerCountry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%banner_country}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['banner_id'], 'required'],
            [['banner_id'], 'integer'],
            [['country_include_id'],'safe'],
            [['banner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Banner::className(), 'targetAttribute' => ['banner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'country_include_id' => Yii::t('app', 'Country Include ID'),
            'banner_id' => Yii::t('app', 'Banner ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanner()
    {
        return $this->hasOne(Banner::className(), ['id' => 'banner_id']);
    }
}
