<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\models\BannerPlacement;
/**
 * This is the model class for table "banner_pages".
 *
 * @property integer $id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 */
class BannerPages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%banner_pages}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'name'], 'required'],
            [['name'],'unique'],
            [[ 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],

        ];
    }

    /**
     * @inheritdoc
     */

    public function getPositionName(){
        return [
            0 => 'Left',
            1 => 'Right',
            2 => 'Bottom',
            3 => 'Top',
            4 => 'Middle',
        ];
    }

    public function getBannerMobile(){
        return [
            0 => 'Desktop',
            1 => 'Tablet',
            2 => 'Mobile',
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Page Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
