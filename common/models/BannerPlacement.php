<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use common\models\BannerPages;

/**
 * This is the model class for table "banner_placement".
 *
 * @property integer $id
 * @property string $name
 * @property string $position
 * @property string $stub
 * @property string $is_mobile
 * @property integer $banner_pages_id
 * @property integer $width
 * @property integer $height
 *
 * @property Banner[] $banners
 */
class BannerPlacement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%banner_placement}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'position', 'width', 'height','is_mobile'], 'required'],
            [['position'], 'in', 'range' => [0, 1, 2 , 3, 4]],
            [['banner_pages_id', 'width', 'height'], 'integer'],
            [['status'], 'in', 'range' => [0, 1]],
            [['is_mobile'], 'in', 'range' => [0, 1, 2]],
            [['is_mobile','name', 'position'], 'string', 'max' => 255],
            [['stub'], 'string', 'max' => 256],

        ];
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'position' => 'Position',
            'stub' => 'Stub',
            'banner_pages_id' => 'Banner Pages',
            'width' => 'Width',
            'height' => 'Height',
            'is_mobile' => 'Device',
        ];
    }


    public function getBannerMobile(){
        return [
            0 => 'Desktop',
            1 => 'Tablet',
            2 => 'Mobile',
        ];
    }

    public function getPositionName(){
        return [
            0 => 'Left',
            1 => 'Right',
            2 => 'Bottom',
            3 => 'Top',
            4 => 'Middle',
        ];
    }

    public function getBannerPages()
    {
        return ArrayHelper::map(BannerPages::find()->asArray()->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanners()
    {
        return $this->hasMany(Banner::className(), ['banner_placement_id' => 'id']);
    }
}
