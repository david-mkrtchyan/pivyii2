<?php

namespace common\models;

use Yii;
use common\models\Settings;

/**
 * This is the model class for table "banner_rules".
 *
 * @property integer $id
 * @property string $is_mobile 
 * @property string $is_avatar_exists
 * @property string $is_vip
 * @property integer $data_complete_percent_from
 * @property integer $data_complete_percent_to
 * @property string $is_trip_exists
 * @property integer $limit_count
 * @property integer $limit_days
 * @property integer $limit_end_date
 * @property integer $banner_id
 *
 * @property Banner $banner
 */
class BannerRules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%banner_rules}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {

        return [ 
            [[  'file_type', 'is_vip', 'is_trip_exists'], 'string'],
            [['data_complete_percent_from', 'data_complete_percent_to', 'limit_count', 'limit_days', 'banner_id'], 'integer'],
 
            ['limit_end_date', 'date', 'timestampAttribute' => 'limit_end_date'],

            [['banner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Banner::className(), 'targetAttribute' => ['banner_id' => 'id']],
        ];
    }




    public function beforeSave($insert)
    {
        $this->limit_end_date = Yii::$app->formatter->asDate($this->limit_end_date, 'yyyy-MM-dd');
        parent::beforeSave($insert);
        return true;
    }

    public function afterFind()
    {
        $this->limit_end_date = Yii::$app->formatter->asDate($this->limit_end_date, 'dd-MM-yyyy');
        parent::afterFind();
        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID', 
            'file_type' => 'File Type',
            'is_vip' => 'Is Vip',
            'data_complete_percent_from' => 'Data Complete Percent From',
            'data_complete_percent_to' => 'Data Complete Percent To',
            'is_trip_exists' => 'Is Trip Exists',
            'limit_count' => 'Limit Show',
            'limit_days' => 'Limit Days',
            'limit_end_date' => 'Limit End Date',
            'banner_id' => 'Banner ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanner()
    {
        return $this->hasOne(Banner::className(), ['id' => 'banner_id']);
    }
}
