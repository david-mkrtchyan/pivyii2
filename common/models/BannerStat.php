<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "banner_stat".
 *
 * @property integer $id
 * @property integer $shows_count
 * @property integer $clicks_count
 * @property integer $ctr
 * @property integer $banner_id
 *
 * @property Banner $banner
 */
class BannerStat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%banner_stat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shows_count', 'clicks_count', 'ctr', 'banner_id'], 'integer'],
            [['banner_id'], 'required'],
            [['banner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Banner::className(), 'targetAttribute' => ['banner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shows_count' => 'Shows Count',
            'clicks_count' => 'Clicks Count',
            'ctr' => 'Ctr',
            'banner_id' => 'Banner ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanner()
    {
        return $this->hasOne(Banner::className(), ['id' => 'banner_id']);
    }
}
