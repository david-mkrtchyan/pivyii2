<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "piv_brend".
 *
 * @property int $id
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_lt
 */
class Brand extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'piv_brends';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_ru'], 'string', 'max' => 255],
            [['name_en'], 'string', 'max' => 255],
            [['name_lt'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_ru' => Yii::t('app', 'Name'),
            'name_en' => Yii::t('app', 'Name'),
            'name_lt' => Yii::t('app', 'Name'),
        ];
    }
}
