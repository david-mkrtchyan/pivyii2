<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "piv_models".
 *
 * @property int $id
 * @property int $brend_id
 * @property string $name_lt
 * @property string $name_ru
 * @property string $name_en
 */
class BrandModels extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'piv_models';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brend_id'], 'integer'],
            [['name_en'], 'string', 'max' => 255],
            [['name_ru'], 'string', 'max' => 255],
            [['name_lt'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'brend_id' => Yii::t('app', 'Brend'),
            'name_en' => Yii::t('app', 'Name En'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_lt' => Yii::t('app', 'Name Lt'),
        ];
    }

    public function getBrand()
    {
        return $this->hasOne(Brand::classname(), ['id' => 'brend_id']);
    }

}
