<?php

namespace common\models;

use backend\models\CategoryFieldsWish;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%categories}}".
 *
 * @property int $id
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_lt
 * @property string $slug
 * @property int $parent_id
 * @property string $meta_key
 * @property string $meta_desc
 * @property int $show
 * @property string $icon
 * @property int $rank
 * @property int $created_at
 * @property int $updated_at
 *
 *
 * @property Product[] $Products
 * @property Category[] $childs
 * @property Category $parent
 * @property CategoryFieldsWish $categoryFieldsWish
 * @property CategoryField[] $categoryFields
 * @property Category[] $allParents
 * @property array $allParentsIds
 * @property array $allChildsIds
 * @property Category[] $allChilds
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%categories}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'show', 'rank', 'created_at', 'updated_at'], 'integer'],
            [['name_lt' ], 'required'],
            [['name_en', 'name_ru', 'name_lt', 'slug', 'meta_key', 'icon'], 'string', 'max' => 255],
            [['meta_desc'], 'safe'],
            [['slug'], 'unique'],
            [['parent_id'], 'default', 'value'=> 0],
            [['show'], 'boolean', 'trueValue' => '1', 'falseValue' => '0', 'strict' => '1']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_en' => 'Name En',
            'name_ru' => 'Name Ru',
            'name_lt' => 'Name Lt',
            'slug' => 'Slug',
            'parent_id' => 'Parent ID',
            'meta_key' => 'Meta Key',
            'meta_desc' => 'Meta Desc',
            'show' => 'Show',
            'icon' => 'Icon',
            'rank' => 'Rank',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static $show_hide = [
      1 => 'Show',
      0 => 'Hide'
    ];

    public static function  buildTree(array $elements, $parentId = 0, $getId = null, $parentIds = []) {
        $branch  = array();

        foreach ($elements as $element) {

            $data = [
                'id' => $element['id'],
                'label' => $element['name_'.Yii::$app->language],
                'inode' => false,
                'open' => false,
                "checkbox" => false,
                "radio" => true
            ];



            if($getId && $parentIds){
                if(!is_null(ArrayHelper::getValue($parentIds,$element['id'])) ||
                    $element['id'] == $getId
                ){
                    $data['open'] = true;
                    $data['checked'] = true;
                }

            }

            if ($element['parent_id'] == $parentId) {

                $children = self::buildTree($elements, $element['id'],$getId, $parentIds);

                if ($children) {
                    $data['branch'] =  $children;
                }
                $branch[] = $data;
            }
        }

        return $branch;
    }


    public static function  buildTreeHierarchy(array $elements, $parentId = 0) {
        $branch  = array();

        foreach ($elements as $element) {


            if ($element['parent_id'] == $parentId) {

                $children = self::buildTreeHierarchy($elements, $element['id']);

                if ($children) {
                    $element['children'] =  $children;
                }
                
                $branch[] = $element;
            }
        }

        return $branch;
    }

    public static function getHierarchy()
    {
        $options = [];
        $parents = self::findAll(['parent_id' => 0]);
        foreach ($parents as $parent) {
            $children = [$parent];
            $children = array_merge($children, $parent->childs);
            $options[$parent->name_en] = ArrayHelper::map($children, 'id', 'name_en');
        }
        return $options;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryField()
    {
        return $this->hasMany(CategoryField::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }

    public function getChilds()
    {
        return $this->hasMany(self::classname(), ['parent_id' => 'id']);
    }

    public function getParent()
    {
        return $this->hasOne(self::classname(), ['id' => 'parent_id']);
    }

    public function getAllParents($allParents = [])
    {
        if ($this->parent) {
            $allParents[] = $this->parent;
            return $this->parent->getAllParents($allParents);
        }
        return $allParents;
    }


     public function getCategoryFieldsWish()
    {
        return $this->hasMany(CategoryFieldsWish::classname(), ['category_id' => 'id'])->orderBy('rank desc');
    }

    public function getAllParentsIds()
    {
        return ArrayHelper::getColumn($this->allParents, 'id');
    }



    public function getAllChilds(&$allChilds = [])
    {
        if ($this->childs) {
            foreach ($this->childs as $child) {
                $allChilds[] = $child;
                $child->getAllChilds($allChilds);
            }
        }
        return $allChilds;

    }

    public function getAllChildsIds()
    {
        return ArrayHelper::getColumn($this->allChilds, 'id');
    }

    public function getProductsFields()
    {
        return $this->hasMany(ProductField::className(), ['product_id' => 'id']);
    }

  /*  public function getCategoryFields()
    {
        $categoriesIds = $this->allParentsIds;
        $categoriesIds[] = $this->id;
        if ($categoriesIds) {
            $categoryFieldsWishIds = CategoryFieldsWish::find()->select('id')->where(['IN', 'category_id', $categoriesIds])->indexBy('id')->column();

            if($categoryFieldsWishIds){
                return CategoryField::find()->where(['IN', 'category_fields_wish_id', $categoryFieldsWishIds])->all();
            }
        }
        return [];
    }*/
}
