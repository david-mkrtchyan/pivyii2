<?php

namespace common\models;

use backend\models\CategoryFieldsWish;
use common\models\opcia\Opcia;
use Yii;
use common\models\Category;
use common\models\CategoryFieldValue;
use common\models\ProductField;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "byshpo_category_fields".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property integer $allow_multiple
 * @property integer $allow_range
 * @property string $range_min
 * @property string $range_max
 * @property string $db_name_type
 * @property string $required
 * @property integer $value_source
 * @property integer $created_at
 * @property array $catFieldValues
 * @property integer $updated_at
 * @property integer $category_fields_wish_id
 * @property integer $rank
 * @property integer $allow_filter
 * @property integer $use_database
 * @property string $db_name
 * @property array $value
 *
 * @property CategoryFieldValue[] $categoryFieldValues
 * @property CategoryFieldsWish[] $categoryFieldsWish
 * @property Category $category
 * @property ProductField[] $productFields
 */
class CategoryField extends \yii\db\ActiveRecord
{

    const  typeInput = 0;
    const  typeTextArea = 1;
    const  typeSelect = 2;
    const  typeCheckbox = 3;
    const  typeRadio = 4;
    const  multipleNo = 0;
    const  multipleYes = 1;
    const  rangeNo = 0;
    const  rangeYes = 1;
    const  useDatabaseNo = 0;
    const  useDatabaseYes = 1;
    const  allowFilterYes = 1;
    const  allowFilterNo = 0;
    const  requiredNo = 0;
    const  requiredYes = 1;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    public static function allowMultipleList()
    {
       return [
           self::multipleNo => self::multipleNo,
           self::multipleYes => self::multipleYes,
       ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category_fields}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['category_fields_wish_id','required'],
            [['type', 'allow_multiple', 'allow_range', 'range_min', 'range_max', 'value_source', 'created_at', 'updated_at', 'rank'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['catFieldValues','db_name_type'], 'safe'],
            [['category_fields_wish_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoryFieldsWish::className(), 'targetAttribute' => ['category_fields_wish_id' => 'id']],
            ['type', 'in', 'range' => [self::typeInput, self::typeTextArea, self::typeSelect, self::typeCheckbox, self::typeRadio]],
            ['allow_multiple', 'in', 'range' => [self::multipleNo, self::multipleYes]],
            ['allow_range', 'in', 'range' => [self::rangeNo, self::rangeYes]],
            ['required', 'in', 'range' => [self::requiredNo, self::requiredYes]],
            ['use_database', 'in', 'range' => [self::useDatabaseNo, self::useDatabaseYes]],
            ['db_name_type', 'in', 'range' => Opcia::$typeListAttr],
            ['allow_filter', 'in', 'range' => [self::allowFilterYes, self::allowFilterNo]],
            [['db_name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'allow_multiple' => 'Allow Multiple',
            'allow_range' => 'Allow Range',
            'range_min' => 'Range Min',
            'range_max' => 'Range Max',
            'value_source' => 'Value Source',
            'catFieldValues' => 'dda',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'rank' => 'Rank',
            'use_database' => 'Use Database Table',
            'db_name' => 'Database Name',
            'db_name_type' => 'Database Name Type',
            'allow_filter' => 'Allow Filter',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryFieldValues()
    {
        return $this->hasMany(CategoryFieldValue::className(), ['category_field_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryFieldsWish()
    {
        return $this->hasOne(CategoryFieldsWish::className(), ['id' => 'category_fields_wish_id']);
    }


    public static function getAllowMultiple()
    {
        return [
            self::multipleNo => 'No',
            self::multipleYes => 'Yes'
        ];
    }


    public static function getType()
    {
        return [
            self::typeInput => 'Input',
            self::typeTextArea => 'TextArea',
            self::typeSelect => 'Select',
            self::typeCheckbox => 'Checkbox',
            self::typeRadio => 'Radio'
        ];
    }

    public static function getRange()
    {
        return [
            self::rangeNo => 'No',
            self::rangeYes => 'Yes'
        ];
    }

}
