<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%category_field_values}}".
 *
 * @property integer $id
 * @property integer $category_field_id
 * @property string $value_lt
 * @property string $value_ru
 * @property string $value_en
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CategoryField $categoryField
 */
class CategoryFieldValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category_field_values}}';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_field_id', 'created_at', 'updated_at'], 'integer'],
            [['value_lt','value_en','value_ru'], 'required'],
            [['value_lt','value_en','value_ru'], 'string', 'max' => 255],
            [['category_field_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoryField::className(), 'targetAttribute' => ['category_field_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_field_id' => 'Category Field ID',
            'value_lt' => 'Value Lt',
            'value_en' => 'Value En',
            'value_ru' => 'Value Ru',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryField()
    {
        return $this->hasOne(CategoryField::className(), ['id' => 'category_field_id'])->orderBy('rank desc');
    }
}
