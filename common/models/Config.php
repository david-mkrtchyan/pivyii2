<?php

namespace common\models;

use common\models\MobileDetect;
use Symfony\Component\CssSelector\Parser\Reader;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Cookie;

/**
 * Signup form
 */
class Config extends Model
{

    public static function getLangActive($lang)
    {
        if ($lang) {
            if (isset(Yii::$app->params['languages'][$lang])) {
                return (Yii::$app->params['languages'][$lang] == Yii::$app->language) ? 'active' : '';
            }
            return false;
        }

    }

    public static function getLanguageDefault()
    {
        if (!Yii::$app->session['language']) {
            Yii::$app->language = Yii::$app->params['default_language'];
            if (isset(Yii::$app->request->userIP) && Yii::$app->request->userIP) {
                if (Yii::$app->request->userIP == '127.0.0.1') {
                    Yii::$app->language = Yii::$app->params['default_language'];
                } else {
                    if ($isoCode = self::getUserContinental()) {
                        if ($isoCode === 'US') {
                            Yii::$app->language = Yii::$app->params['default_language'];
                        } elseif ($isoCode === 'RU') {
                            Yii::$app->language = 'ru';
                        }
                    }
                }
            }
        }
    }

    public static function getUserContinental()
    {
        $reader = new Reader(yii::$app->basePath . '/GeoIP2/GeoIP2-Country.mmdb');
        if (isset(Yii::$app->request->userIP) && Yii::$app->request->userIP && Yii::$app->request->userIP !== '127.0.0.1') {
            return;
            $record = $reader->country($_SERVER['REMOTE_ADDR']);
            if ($record) {
                //return ;
                return (isset($record->country->isoCode) && $record->country->isoCode) ? $record->country->isoCode : '';
            }
        }
        return false;
    }

    public static function getRouteFirstPath($route)
    {
        return ArrayHelper::getValue(explode('/', $route), '0');
    }

    public function getAvatarPhoto($file, $size, $class = null)
    {
        $path = "user/{$size}/{$file}";
        if ($file && file_exists(Yii::getAlias("@frontend") . "/web/$path")) {
            return Html::img("/$path", ['class' => $class]);
        }
        return Html::img("/images/default_avatar.png", ['class' => $class]);
    }

    public function getIcon($file, $class = null)
    {
        $path = "icon/{$file}";
        if ($file && file_exists(Yii::getAlias("@frontend") . "/web/$path")) {
            return Html::img("/$path", ['class' => $class]);
        }
    }

    public static function getAllCurrency($currencyGet)
    {
        $currencys = Rate::find()->all();
        $currencysArrray = [];
        $lang = Yii::$app->language;
        foreach ($currencys as $currency) {

            if ($currency["name_$lang"]) {
                if ($currencyGet == $currency["name_$lang"]) {
                    $activeClass = 'active';
                } else {
                    $activeClass = '';
                }
                $currencysArrray[] = [
                    'label' => '<b>' . $currency["name_$lang"] . '</b> ' . $currency->country_rate,
                    'url' => ['/site/change-currency', 'currentCurrency' => $currency["name_$lang"]],
                    'options' => ['class' => $activeClass],
                ];
            }
        }
        return $currencysArrray;
    }


    public static function getCurrencyDefault()
    {
        $currency = " ";
        if (!Yii::$app->session->get('currency')) {
            $currency = Yii::$app->params['default_currency'];
            if (isset(Yii::$app->request->userIP) && Yii::$app->request->userIP) {
                if (Yii::$app->request->userIP == '127.0.0.1') {
                    $currency = Yii::$app->params['default_currency'];
                } else {
                    if ($isoCode = self::getUserContinental()) {
                        if ($isoCode === 'US') {
                            $currency = Yii::$app->params['default_currency'];
                        } elseif ($isoCode === 'RU') {
                            $currency = 'RUB';
                        } elseif ($isoCode === 'EU') {
                            $currency = 'EUR';
                        }
                    }
                }
            }
        }
        return $currency;
    }


    public static function getRegionList($lang = 'lt')
    {


        $admin1 = Yii::$app->db->createCommand("SELECT * FROM geo_city WHERE country='$lang' and publish = 1 order by sort desc")->queryAll();
        $regionList = ArrayHelper::map($admin1, 'geonameid', 'name');

        return $regionList;

        /*$regionList = [];
        $getCountryRegionList = Geo::getCountryRegionList();

        if ($getCountryRegionList && is_array($getCountryRegionList)) {
            $keys = array_keys($getCountryRegionList);
            $admin1 = Yii::$app->db->createCommand("SELECT * FROM geo_admin1codesascii WHERE geonameid in (" . implode(',', $keys) . ')')->queryAll();
            $regionList = ArrayHelper::map($admin1, 'geonameid', 'name');
        }
        return $regionList;*/
    }


    public static function getFavorites()
    {
        $cookiesFavourite = Yii::$app->request->cookies->getValue('favourite', []);
        if (!Yii::$app->user->isGuest) {
            $cookiesFavourite = '';
            $user = yii::$app->user->identity;
            if ($user->social_users_favourite) {
                $cookiesFavourite = unserialize($user->social_users_favourite);
            }
        }
        return $cookiesFavourite;
    }

    public static function getUserFavorites()
    {
        $cookiesFavourite = [];
        if (!Yii::$app->user->isGuest) {
            $cookiesFavourite = '';
            $user = yii::$app->user->identity;
            if ($user->social_users_favourite) {
                $cookiesFavourite = unserialize($user->social_users_favourite);
            }
        }
        return $cookiesFavourite;
    }

    public static function replaceTitle($string, $lenght = 100)
    {
        if ($string && strlen($string) >= $lenght) {
            return substr($string, 0, $lenght) . '...';
        }

        return $string;
    }

    public static function getGeoCity()
    {
        $_SERVER['REMOTE_ADDR'] = '37.157.208.0';
        if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1') {
            $geonameId = Geo::geoIp($_SERVER['REMOTE_ADDR']);
            $list = [];
            if ($geonameId) {
                $result = Geo::getCity($geonameId);
                if ($result) {
                    $city_text = Geo::getFullName($geonameId, 'en');
                    $list = [$geonameId => $city_text];
                }
            }

            return $list;
        }
        return false;
    }


    public static function checkUserFollow($userId)
    {
        $user = Yii::$app->user;
        if ($userId && !$user->isGuest) {
            $model = UserFollow::find()->where(['sender_id' => $user->identity->id, 'receiver_id' => $userId])->one();
            if ($model && $model->follow == 1) {
                return true;
            }
        }
        return false;
    }

    public static function getSiteFullUrl()
    {
        return Yii::$app->request->hostInfo . Yii::$app->request->url;
    }

    public static function getShowType()
    {
        return [
            'list' => 'list',
            'grid' => 'grid',
        ];
    }

    public static function getTypeResponse()
    {
        return [
            'desktop' => 'desktop',
            'mobile' => 'mobile',
        ];
    }


    public static function timeElapsedString($whatever)
    {
        if (!$whatever) return;
        $commentTime = $whatever;
        $today = strtotime('today');
        $yesterday = strtotime('yesterday');
        $todaysHours = strtotime('now') - strtotime('today');

        $tokens = array(
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );
        $time = time() - $commentTime;
        $time = ($time < 1) ? 1 : $time;
        if ($commentTime >= $today || $commentTime < $yesterday) {
            foreach ($tokens as $unit => $text) {
                if ($time < $unit) {
                    continue;
                }
                if ($text == 'day') {
                    $numberOfUnits = floor(($time - $todaysHours) / $unit) + 1;
                } else {
                    $numberOfUnits = floor(($time) / $unit);
                }
                return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '') . ' ago';
            }
        } else {
            return 'Yesterday';
        }
    }

    public function getSettingValue($column)
    {
        $table = Yii::$app->db->schema->getTableSchema('piv_settings');
        if (isset($table->columns[$column])) {
            $settings = Settings::find()->one();
            return $settings->$column;
        }

        return false;
    }

    public static function getGridList()
    {
        $showType = Yii::$app->session->get('showType');

        if ($showType && $showType == ArrayHelper::getValue(Config::getShowType(), 'list')) {
            $template = 'category_list';
        } else {
            $template = 'category_grid';
        }
        return $template;
    }

    public static function getMobileType()
    {
        $typeResponse = Yii::$app->session->get('typeResponse');
        $response = '';
        if ($typeResponse) {
            if ($typeResponse == ArrayHelper::getValue(Config::getTypeResponse(), 'desktop')) {
                $response = 'desktop';
            } else {
                $response = 'mobile';
            }
        }
        return $response;
    }

    public static function getMobileTypeText()
    {
        if (Config::getMobileType() == 'desktop') {
            return '<meta name="viewport" content="width=1024">';
        } else {
            return '<meta name="viewport" content="width=device-width, initial-scale=1">';
        }


    }

    public static function getAllRelation($table_name, $values)
    {
        if (!$values || !$table_name) return false;

        $relations = Yii::$app->db->createCommand("
        SELECT 
  `TABLE_SCHEMA`,                          -- Foreign key schema
  `TABLE_NAME`,                            -- Foreign key table
  `COLUMN_NAME`,                           -- Foreign key column
  `REFERENCED_TABLE_SCHEMA`,               -- Origin key schema
  `REFERENCED_TABLE_NAME`,                 -- Origin key table
  `REFERENCED_COLUMN_NAME`                 -- Origin key column
FROM
  `INFORMATION_SCHEMA`.`KEY_COLUMN_USAGE`  -- Will fail if user don't have privilege
WHERE
  `REFERENCED_TABLE_NAME` = '" . $table_name . "' AND
  `TABLE_SCHEMA` = SCHEMA()                -- Detect current schema in USE 
  AND `REFERENCED_TABLE_NAME` IS NOT NULL; -- Only tables with foreign keys
        ")->queryAll();

        $array = [];

        if ($relations) {
            foreach ($relations as $key => $relation) {
                $data = array_merge($relations[$key], ['value' => $values]);
                $array[$relation['TABLE_NAME']] = $data;
            }
        }

        return $array;
    }

    public static function addCategoryViewedCount($id, $model)
    {
        if (!$id || !$model) {
            return false;
        }
        $cookies = Yii::$app->response->cookies;
        $cookiesBlog = \Yii::$app->getRequest()->getCookies()->getValue('pageView', []);
        $array = [];
        if ($cookiesBlog) {
            if (is_array($cookiesBlog)) {
                if (!in_array($id, $cookiesBlog)) {
                    $array[$id] = $id;
                    $margePostId = array_merge($cookiesBlog, $array);
                    $cookies->add(new Cookie([
                        'name' => 'pageView',
                        'value' => $margePostId,
                        'expire' => time() + 2 * 24 * 60 * 60
                    ]));
                    $model->counter += 1;
                    $model->update(false);
                }
            }
        } else {
            $array[$id] = $id;
            $cookies->add(new Cookie([
                'name' => 'pageView',
                'value' => $array,
                'expire' => time() + 2 * 24 * 60 * 60
            ]));
            $model->counter += 1;
            $model->update(false);
        }
        return;
    }

    public static function addLastViewed($id = null, $model = null)
    {
        $cookies = Yii::$app->response->cookies;
        $cookiesViewed = \Yii::$app->getRequest()->getCookies()->getValue('lastViewedItem', []);

        if ($id && $model) {
            $array = [];
            if ($cookiesViewed) {
                if (is_array($cookiesViewed)) {
                    krsort($cookiesViewed);
                    $id = intval($id);
                    if (!in_array($id, $cookiesViewed)) {
                        $array[$id] = $id;

                        if (count($cookiesViewed) > 14) {
                            $count = 1;
                            foreach ($cookiesViewed as $key => $value) {
                                if ($count > 14) {
                                    if (isset($cookiesViewed[$key])) {
                                        unset($cookiesViewed[$key]);
                                    }
                                }
                                $count++;
                            }
                        }
                        $margePostId = array_merge($cookiesViewed, $array);
                        $cookies->add(new Cookie([
                            'name' => 'lastViewedItem',
                            'value' => $margePostId,
                            'expire' => time() + 2 * 24 * 60 * 60
                        ]));
                    }
                }
            } else {
                $array[$id] = $id;
                $cookies->add(new Cookie([
                    'name' => 'lastViewedItem',
                    'value' => $array,
                    'expire' => time() + 2 * 24 * 60 * 60
                ]));
            }
        }

        return $cookiesViewed;
    }

    public static function addRecentSearch($category_wish = null, $key = null, $valueCookies = null)
    {
        $cookies = Yii::$app->response->cookies;
        $cookiesSearch = \Yii::$app->getRequest()->getCookies()->getValue('lastSearch', []);

        if ($category_wish) {
            $array = [];
            if ($cookiesSearch) {
                if (is_array($cookiesSearch)) {
                    $category_wish = intval($category_wish);
                    if (!in_array($category_wish, $cookiesSearch)) {
                        $cookiesSearch[$category_wish][$key][$valueCookies] = $valueCookies;
                        /*ksort($cookiesSearch);
                       if(count($cookiesSearch,COUNT_RECURSIVE) > 3){
                           $count = 1;
                           foreach ($cookiesSearch as $key => $value){
                               if($count > 3){
                                   if(isset($cookiesSearch[$key])){
                                       unset($cookiesSearch[$key]);
                                   }
                               }
                               $count++;
                           }
                       }*/

                        $cookies->add(new Cookie([
                            'name' => 'lastSearch',
                            'value' => $cookiesSearch,
                            'expire' => time() + 2 * 24 * 60 * 60
                        ]));
                    }
                }
            } else {
                $array[$category_wish][$key][$valueCookies] = $valueCookies;
                $cookies->add(new Cookie([
                    'name' => 'lastSearch',
                    'value' => $array,
                    'expire' => time() + 2 * 24 * 60 * 60
                ]));
            }
        }

        return $cookiesSearch;
    }


}