<?php

namespace common\models;


use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\ActiveRecordInterface;
use imanilchaudhari\CurrencyConverter\CurrencyConverter;
use yii\helpers\ArrayHelper;

/**
 * Currency is a model
 */
class Currency extends Model
{

    /*public static function showCurrency($price,$rate)
     {
      if(Yii::$app->session->get('currency')):
         $converter = new CurrencyConverter();
         $rate =  $converter->convert( $rate,Yii::$app->session->get('currency')->currency);
      endif;

      return  Yii::$app->session->get('currency') ? round($price*$rate, 0) .' '.Yii::$app->session->get('currency')->currencySymbol : $price;
     }*/

    public static function showCurrency($price, $rate)
    {
        $currency = Yii::$app->session->get('currency');
        $currencySession = Yii::$app->session->get('currencySession');
       
        if ($currencySession && $currency) {
            if ($currency->currency) {
                if (isset($currencySession[$rate][$currency->currency]) && $currencySession[$rate][$currency->currency]) {
                    $rate = $currencySession[$rate][$currency->currency];
                } else {
                    $rate = 1;
                }
                return round($price * $rate, 0) . ' ' . $currency->currencySymbol;
            }
        }
        $lang = Yii::$app->language;
        $rate = Rate::find()->where(["name_$lang" => $rate])->one();

        return $price.' '.ArrayHelper::getValue($rate,'symbols');
    }


}

