<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

class Geo extends \yii\db\ActiveRecord
{
    private static $sphinx = null;
    private static $language = 'en';
    private static $geo = null;

    public static function getCity($geoname_id, $language = null)
    {
        if (!$geoname_id) {
            return array();
        }
        if ($language) {
            self::$language = $language;
        }
        $cache_key = 'geoname_' . $geoname_id;
        $geoname = Yii::$app->cache->get($cache_key);
        if ($geoname === false) {
            $sql = "SELECT * FROM geo_city WHERE geonameid=" . Yii::$app->db->quoteValue($geoname_id);
            $geoname = Yii::$app->db->createCommand($sql)->queryOne();

            $geoname_lang = self::getGeonameLang($geoname_id);
            if (!is_array($geoname_lang)) $geoname_lang = [];
            if (!is_array($geoname)) $geoname = [];
            $geoname = array_merge($geoname_lang, $geoname);

            Yii::$app->cache->set($cache_key, $geoname);
        }
        if (!is_array($geoname)) $geoname = [];
        return $geoname;
    }


    public static function getCountryRegionList($country = 'LT')
    {
        $results = GeoCity::find()->where(['country' => $country])->all();


        if ($results) {

            $array = [];
            foreach ($results as $key => $result) {

                $sql = "SELECT * FROM geo_admin1codesascii WHERE geonameid=" . $result['admin1_id'];
                $admin1 = Yii::$app->db->createCommand($sql)->queryOne();

                if($admin1){
                    $data = [
                        'region' => $admin1['name'],
                        'admin1' => $result['geonameid'],
                    ];
                    $array[$result['admin1_id']][] =  $data;
                }
            }

           return $array;
        }
    }



    public static function getRegionName($admin1_id)
    {
        $admin1 = self::getAdmin1($admin1_id);
        $nameAscii = ArrayHelper::getValue($admin1, 'nameAscii');
        $admin1_name = isset($admin1['alternateName']) ? $admin1['alternateName'] : $nameAscii;

        $geoname_country = self::getCountry(ArrayHelper::getValue($admin1, 'code'), true);
        $country_name = ArrayHelper::getValue($geoname_country, '_name');

        $result = [];

        if ($admin1) {
            $result[] = $admin1_name;
        }
        if ($country_name) {
            $result[] = $country_name;
        }

        return implode(', ', $result);
    }

    public static function getGeonameLang($geoname_id)
    {
        if (!$geoname_id) {
            return array();
        }
        $cache_key = 'geoname_lang_' . $geoname_id . '_lang_' . self::$language;
        $geoname_lang = Yii::$app->cache->get($cache_key);
        if ($geoname_lang === false) {
            $sql = "SELECT * FROM geo_lang WHERE geonameid=" . Yii::$app->db->quoteValue($geoname_id) . " AND isoLanguage=" . Yii::$app->db->quoteValue(self::$language);
            $geoname_lang = Yii::$app->db->createCommand($sql)->queryOne();
            Yii::$app->cache->set($cache_key, $geoname_lang);
        }
        if (!is_array($geoname_lang)) $geoname_lang = [];
        return $geoname_lang;
    }

    public static function getCountryById($geonameid)
    {
        if (!$geonameid) {
            return array();
        }

        $cache_key = 'country_' . $geonameid;
        $country = Yii::$app->cache->get($cache_key);
        if ($country === false) {
            $sql = "SELECT * FROM geo_countryinfo WHERE geonameid=" . Yii::$app->db->quoteValue($geonameid);
            $country = Yii::$app->db->createCommand($sql)->queryOne();

            if (isset($country['geonameid'])) {
                $lang = self::getGeonameLang($country['geonameid']);
                if (!is_array($lang)) $lang = [];
                if (!is_array($country)) $country = [];
                $country = array_merge($lang, $country);
            }
            $country['_name'] = isset($country['alternateName']) ? $country['alternateName'] : $country['name'];
            Yii::$app->cache->set($cache_key, $country);
        }

        return $country;
    }

    public static function getCountry($country_code, $from_region_code = false)
    {
        if (!$country_code) {
            return array();
        }

        if ($from_region_code) {
            list($country_code, $regionCode) = explode('.', $country_code);
        }

        $cache_key = 'country_' . $country_code;
        $country = Yii::$app->cache->get($cache_key);
        if ($country === false) {
            $sql = "SELECT * FROM geo_countryinfo WHERE iso_alpha2=" . Yii::$app->db->quoteValue($country_code);
            $country = Yii::$app->db->createCommand($sql)->queryOne();
            if (isset($country['geonameid'])) {
                $lang = self::getGeonameLang($country['geonameid']);
                if (!is_array($lang)) $lang = [];
                if (!is_array($country)) $country = [];
                $country = array_merge($lang, $country);
            }
            $country['_name'] = isset($country['alternateName']) ? $country['alternateName'] : $country['name'];
            Yii::$app->cache->set($cache_key, $country);
        }

        return $country;
    }

    public static function getAdmin1($code)
    {
        if (!$code) {
            return array();
        }

        $cache_key = 'geo_admin1_' . $code;
        $admin1 = Yii::$app->cache->get($cache_key);
        if ($admin1 === false) {
            $sql = "SELECT * FROM geo_admin1codesascii WHERE geonameid=" . Yii::$app->db->quoteValue($code);
            $admin1 = Yii::$app->db->createCommand($sql)->queryOne();
            Yii::$app->cache->set($cache_key, $admin1);
        }

        if ($admin1['geonameid']) {
            $lang = self::getGeonameLang($admin1['geonameid']);
            if (!is_array($lang)) $lang = [];
            if (!is_array($admin1)) $admin1 = [];
            return array_merge($lang, $admin1);
        }

        return $admin1;
    }

    public static function searchFull($text)
    {
        self::getSphinxConnection();
        $text = str_replace("'", '', $text);
        $text = str_replace('"', '', $text);
        $text = str_replace('~', '', $text);
        $parts = explode(',', $text);
        $parts_sql = [];
        foreach ($parts as $part) {
            $parts_sql[] = '*' . trim($part) . '*';
        }

        $parts_sql = implode('|', $parts_sql);
        if ($parts_sql == '') {
            return array();
        }
        $parts_sql = Yii::$app->db->quoteValue($parts_sql);

        $sql = '
          SELECT
            *, WEIGHT() AS myweight
          FROM
            geo_full
          WHERE
            MATCH(' . $parts_sql . ')
          ORDER BY
            myweight DESC
          LIMIT
            100
          OPTION
            ranker=sph04,
            field_weights=(name=100, asciiname=100, full_name=1, full_name_ascii=1)
          ';

        $all = self::$sphinx->createCommand($sql)->queryAll();
        return $all;
    }

    public static function getSphinxConnection()
    {
        if (!self::$sphinx) {
            self::$sphinx = new \yii\db\Connection();
            self::$sphinx->dsn = 'mysql:host=127.0.0.1;port=9306';
        }
    }

    public static function searchCountry($text)
    {
        self::getSphinxConnection();
        $text = strip_tags($text);

        $parts = explode(',', $text);
        $parts_sql = [];
        foreach ($parts as $part) {
            $parts_sql[] = '*' . trim($part) . '*';
        }

        $parts_sql = implode('|', $parts_sql);
        $parts_sql = Yii::$app->db->quoteValue($parts_sql);

        $sql = '
          SELECT
            *, WEIGHT() AS myweight
          FROM
            geo_country
          WHERE
            MATCH(' . $parts_sql . ')
          ORDER BY
            myweight DESC
          LIMIT
            100
          OPTION
                ranker=proximity
          ';

        $all = self::$sphinx->createCommand($sql)->queryAll();
        return $all;
    }

    public static function searchRegion($text)
    {
        self::getSphinxConnection();
        $text = strip_tags($text);

        $parts = explode(',', $text);
        $parts_sql = [];
        foreach ($parts as $part) {
            $parts_sql[] = '*' . trim($part) . '*';
        }

        $parts_sql = implode('|', $parts_sql);
        $parts_sql = Yii::$app->db->quoteValue($parts_sql);

        $sql = '
          SELECT
            *, WEIGHT() AS myweight
          FROM
            geo_region
          WHERE
            MATCH(' . $parts_sql . ')
          ORDER BY
            myweight DESC
          LIMIT
            100
          OPTION
                ranker=proximity
          ';

        $all = self::$sphinx->createCommand($sql)->queryAll();
        return $all;
    }

    public static function searchCity($text)
    {
        $text = strip_tags($text);

        $parts = explode(',', $text);
        $parts_sql = [];
        foreach ($parts as $part) {
            $parts_sql[] = '%' . trim($part) . '%';
        }

        $parts_sql = implode('|', $parts_sql);
        $parts_sql = Yii::$app->db->quoteValue($parts_sql);

        $sql = '
          SELECT
            *FROM
            geo_city
          WHERE
          MATCH (name) AGAINST (' . $parts_sql . ' IN BOOLEAN MODE)
          ';

        $all = Yii::$app->db->createCommand($sql)->queryAll();

        return $all;
    }


    public static function getFullNameRegion($geoname_id)
    {
        $admin1 = self::getAdmin1($geoname_id);
        $nameAscii = ArrayHelper::getValue($admin1, 'nameAscii');
        $admin1_name = isset($admin1['alternateName']) ? $admin1['alternateName'] : $nameAscii;

        $geoname_country = self::getCountry(ArrayHelper::getValue($admin1, 'code'), true);
        $country_name = ArrayHelper::getValue($geoname_country, '_name');

        $result = [];

        if ($admin1) {
            $result[] = $admin1_name;
        }
        if ($country_name) {
            $result[] = $country_name;
        }

        return implode(', ', $result);
    }

    public static function getFullNameCountry($iso_alpha2)
    {
        $country = self::getCountry($iso_alpha2);
        return $country['_name'];
    }

    public static function getFullName($geoname_id)
    {
        $geoname_city = self::getCity($geoname_id);
        $admin1_id = isset($geoname_city['admin1_id']) ? $geoname_city['admin1_id'] : null;
        $region_country = self::getFullNameRegion($admin1_id);

        $result = [];
        $result[] = isset($geoname_city['alternateName']) ? $geoname_city['alternateName'] : ArrayHelper::getValue($geoname_city, 'asciiname');

        if ($region_country) {
            $result[] = $region_country;
        }
        return implode(', ', $result);
    }

    public static function setLanguage($language)
    {
        if ($language) {
            self::$language = $language;
        }
    }

    public static function getFullNameByTypeId($location)
    {
        list($location_type, $location_id) = explode('_', $location);
        $location_id = intval($location_id);
        if ($location_id > 0) {
            switch ($location_type) {
                case 'c':
                    $country = self::getCountryById($location_id);
                    return $country['_name'];
                    break;

                case 'r':
                    return self::getFullNameRegion($location_id);
                    break;

                case 't':
                    return self::getFullName($location_id);
                    break;
            }
        }
        return '';
    }

    public static function getCityText($city_id)
    {
        if ($city_id < 1) {
            return '';
        }
        if (!self::$geo) {
            self::$geo = new self();
        }
        $city = self::$geo->getCity($city_id);
        return $city['alternateName'] ? $city['alternateName'] : $city['asciiname'];
    }


    public static function getUserText($city_id)
    {
        if ($city_id < 1) {
            return '';
        }

        if (!self::$geo) {
            self::$geo = new self();
        }
        $city = self::$geo->getCity($city_id);
        $geoname = [];
        $geoname[] = $city['alternateName'] ? $city['alternateName'] : $city['asciiname'];

        $country = self::$geo->getCountry($city['country']);
        $geoname[] = $country['_name'];

        return implode(', ', $geoname);
    }

    public static function geoIp($ip)
    {
        $geonameId = 0;
        $latitude = 0;
        $longitude = 0;

        require_once dirname(__DIR__) . '/components/SxGeo/SxGeo.php';
        $SxGeo = new \SxGeo(dirname(__DIR__) . '/components/SxGeo/SxGeoCity.dat');
        $city = $SxGeo->getCity($ip);
        if ($city) {
            $geonameId = $city['city']['id'];
            $latitude = $city['city']['lat'];
            $longitude = $city['city']['lon'];
        }
        if (!$geonameId) {
            try {
                $reader = new \GeoIp2\Database\Reader(dirname(__DIR__) . '/components/GeoIP/GeoLite2-City.mmdb');
                $record = $reader->city($ip);
                $geonameId = $record->city->geonameId;
                $latitude = $record->location->latitude;
                $longitude = $record->location->longitude;
            } catch (\GeoIp2\Exception\AddressNotFoundException $e) {

            }
        }

        $geoname_city = self::getCity($geonameId);
        if (!$geoname_city && $latitude && $longitude) {
            $max_distance = 25;
            // find nearest City
            $R = 6371;  // earth's radius, km
            // first-cut bounding box (in degrees)
            $max_latitude = $latitude + rad2deg($max_distance / $R);
            $min_latitude = $latitude - rad2deg($max_distance / $R);
            // compensate for degrees longitude getting smaller with increasing latitude
            $max_longitude = $longitude + rad2deg($max_distance / $R / cos(deg2rad($latitude)));
            $min_longitude = $longitude - rad2deg($max_distance / $R / cos(deg2rad($latitude)));

            $sql = "SELECT * FROM geo_city WHERE 
                      latitude > " . Yii::$app->db->quoteValue($min_latitude) .
                " AND latitude < " . Yii::$app->db->quoteValue($max_latitude) .
                " AND longitude > " . Yii::$app->db->quoteValue($min_longitude) .
                " AND longitude < " . Yii::$app->db->quoteValue($max_longitude) .
                " ORDER BY population DESC";
            $geoname = Yii::$app->db->createCommand($sql)->queryOne();
            if ($geoname) {
                return $geoname['geonameid'];
            }
        }

        return $geonameId;
    }

    public static function getLocation($location)
    {
        list($location_type, $location_id) = explode('_', $location);
        $location_id = intval($location_id);
        if (in_array($location_type, ['c', 't', 'r'])) {
            return ['type' => $location_type, 'id' => $location_id];
        }
        return [];
    }

    public static function getCountryId($location)
    {
        $location_parts = self::getLocation($location);
        switch ($location_parts['type']) {
            case 'c':
                return $location_parts['id'];
                break;
            case 't':
                $geo = new Geo;
                $city = $geo->getCity($location_parts['id']);
                if ($city) {
                    $country = $geo->getCountry($city['country']);
                    if ($country['geonameId']) {
                        return $country['geonameId'];
                    }
                }
                break;
        }
    }

    public static function getSelectedListTitles($want_to_travel_selected)
    {
        $want_to_travel = [];
        if (!is_array($want_to_travel_selected)) return $want_to_travel;
        foreach ($want_to_travel_selected as $item) {
            $item_text = Geo::getFullNameByTypeId($item);
            if ($item_text) {
                $want_to_travel[$item] = $item_text;
            }
        }
        return $want_to_travel;
    }
}