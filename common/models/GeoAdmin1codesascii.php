<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "geo_admin1codesascii".
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $nameAscii
 * @property int $geonameid
 */
class GeoAdmin1codesascii extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'geo_admin1codesascii';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'geonameid'], 'integer'],
            [['name', 'nameAscii'], 'string'],
            [['code'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'nameAscii' => Yii::t('app', 'Name Ascii'),
            'geonameid' => Yii::t('app', 'Geonameid'),
        ];
    }
}
