<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "geo_city".
 *
 * @property int $geonameid
 * @property string $name
 * @property string $asciiname
 * @property string $latitude
 * @property string $longitude
 * @property string $fclass
 * @property string $fcode
 * @property string $country
 * @property string $cc2
 * @property string $admin1
 * @property int $admin1_id
 * @property int $population
 * @property int $elevation
 * @property int $gtopo30
 * @property string $moddate
 * @property string $deleted
 */
class GeoCity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'geo_city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['geonameid', 'admin1_id'], 'required'],
            [['geonameid', 'admin1_id', 'population', 'elevation', 'gtopo30', 'publish', 'sort'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['moddate'], 'safe'],
            [['deleted'], 'string'],
            [['name', 'asciiname'], 'string', 'max' => 200],
            [['fclass'], 'string', 'max' => 1],
            [['fcode'], 'string', 'max' => 10],
            [['country'], 'string', 'max' => 2],
            [['cc2'], 'string', 'max' => 60],
            [['admin1'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'geonameid' => 'Geonameid',
            'name' => 'Name',
            'asciiname' => 'Asciiname',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'fclass' => 'Fclass',
            'fcode' => 'Fcode',
            'country' => 'Country',
            'cc2' => 'Cc2',
            'admin1' => 'Admin1',
            'admin1_id' => 'Admin1 ID',
            'population' => 'Population',
            'elevation' => 'Elevation',
            'gtopo30' => 'Gtopo30',
            'moddate' => 'Moddate',
            'deleted' => 'Deleted',
        ];
    }

    public function getPublish()
    {
        return [
           1 => 'Show', 0 => 'Hide',
        ];
    }
}
