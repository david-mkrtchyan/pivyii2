<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

class GeoCountry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_countryinfo';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    public static function getList($language)
    {
        return \Yii::$app->db->createCommand("
        
SELECT 
            c.geonameId, 
            c.iso_alpha2, 
            c.name, 
            
(select alternateName from geo_lang AS lng WHERE lng.geonameid = c.geonameId AND isoLanguage=:isoLanguage limit 1) as alternateName
        FROM 
        geo_countryinfo AS c")
            ->bindValue(':isoLanguage', $language)
            ->queryAll();
    }
}
