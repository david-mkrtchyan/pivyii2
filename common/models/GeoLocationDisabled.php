<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "geo_location_disabled".
 *
 * @property integer $id
 * @property integer $country_id
 *
 * @property GeoCountryinfo $country
 */
class GeoLocationDisabled extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_location_disabled';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'country_id'], 'required'],
            [['country_id'], 'integer'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => GeoCountryinfo::className(), 'targetAttribute' => ['country_id' => 'geonameId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(GeoCountryinfo::className(), ['geonameId' => 'country_id']);
    }
}
