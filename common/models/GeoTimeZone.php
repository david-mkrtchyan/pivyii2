<?php

namespace common\models;

use Yii;

class GeoTimeZone extends \yii\db\ActiveRecord
{
    private static $list_id = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'geo_time_zone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['timeZoneId', 'name'], 'required'],
            [['timeZoneId', 'name'], 'string'],
            [['GMT_offset', 'DST_offset'], 'number'],
        ];
    }

    public static function getTime($time_zone_id, $timestamp)
    {
        if($time_zone_id < 1) {
            return $timestamp;
        }
        if (self::$list_id === null) {
            $cache_key = self::tableName() . '_list_id';
            self::$list_id = \Yii::$app->cache->get($cache_key);
            if (self::$list_id === false) {
                $list = self::find()->asArray()->all();
                $field = (date('I', time())) ? 'GMT_offset' : 'DST_offset';
                foreach ($list as $item) {
                    self::$list_id[$item['id']] = $item[$field];
                }
                \Yii::$app->cache->set($cache_key, self::$list_id, 60);
            }
        }
        return $timestamp + (self::$list_id[$time_zone_id] * 3600);
    }

    public static function findOne($condition)
    {
        if (is_array($condition) && count($condition) == 1 && !empty($condition['name'])) {
            $cache_key = self::tableName() . $condition['name'];
            $value = \Yii::$app->cache->get($cache_key);
            if ($value === false) {
                $value = parent::findOne($condition);
                \Yii::$app->cache->set($cache_key, $value, 86400);
            }
            return $value;
        } else {
            return parent::findOne($condition);
        }
    }
}