<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%help}}".
 *
 * @property int $id
 * @property string $page_name_en
 * @property string $page_name_lt
 * @property string $page_name_ru
 * @property string $slug
 * @property string $text
 * @property int $created_at
 * @property int $updated_at
 */
class Help extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%help}}';
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_name_en', 'page_name_ru','page_name_lt','slug', 'text'], 'required'],
            [['text'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['page_name_en', 'page_name_ru','page_name_lt', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_name_en' => 'Page Name En',
            'page_name_ru' => 'Page Name Ru',
            'page_name_lt' => 'Page Name Lt',
            'slug' => 'Slug',
            'text' => 'Text',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
