<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%opcia_owner_agency}}".
 *
 * @property int $id
 * @property string $owner_agency_name_en
 * @property string $owner_agency_name_ru
 * @property string $owner_agency_name_lt
 * @property int $created_at
 * @property int $updated_at
 */
class OpciaOwnerAgency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'piv_opcia_owner_agency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['owner_agency_name_en', 'owner_agency_name_ru', 'owner_agency_name_lt'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['owner_agency_name_en', 'owner_agency_name_ru', 'owner_agency_name_lt'], 'string', 'max' => 100],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'owner_agency_name_en' => Yii::t('app', 'Owner Agency Name En'),
            'owner_agency_name_ru' => Yii::t('app', 'Owner Agency Name Ru'),
            'owner_agency_name_lt' => Yii::t('app', 'Owner Agency Name Lt'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
