<?php

namespace common\models;

use backend\models\CategoryFieldsWish;
use frontend\models\ProductAddress;
use Imagine\Image\ImageInterface;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\Box;

/**
 * This is the model class for table "{{%products}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $category_fields_wish_id
 * @property integer $status
 * @property integer $counter
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $rate_id
 * @property integer $price
 * @property string $title
 * @property string $description
 * @property integer $updated_at
 *
 * @property ProductField[] $productFields
 * @property CategoryField[] $categoryFields
 * @property StatusList[] $statusList
 * @property Albom[] $albom
 * @property CategoryFieldsWish[] $categoryFieldsWish
 * @property ProductAddress[] $productAddress
 * @property ProductFirstImage[] $productFirstImage
 * @property User[] $user->
 * @property CategoryFieldsRel[] $categoryFieldsRel
 * @property Category $category
 */
class Product extends \yii\db\ActiveRecord
{
    const statusActive = 1;
    const statusPassive = 0;
    /**
     * @var UploadedFile
     */
    /*
        public function upload()
        {
            if ($this->validate()) {
                if (!is_dir(Yii::getAlias('@frontend') . '/web/images/product/main/' . $this->shop_id)) {
                    mkdir(Yii::getAlias('@frontend') . '/web/images/product/main/' . $this->shop_id);
                }
                $this->image = 'images/product/main/' . $this->shop_id . '/' . md5(time() . $this->id) . '.jpg';
                if ($this->lastImage) {
                    $this->image = $this->lastImage;
                }
    //            echo Yii::getAlias('@frontend') . '/web/' . $this->image;die;
                if($this->imageFile)
                $this->imageFile->saveAs(Yii::getAlias('@frontend') . '/web/' . $this->image);

                return true;
            } else {
                return false;
            }


        }*/


    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['counter', 'user_id', 'category_id', 'status', 'category_fields_wish_id', 'created_at', 'updated_at'], 'integer', 'message' => Yii::t('app', '{attribute} must be an integer.')],
            [['category_id', 'title', 'category_fields_wish_id'], 'required', 'message' => Yii::t('app', '{attribute} cannot be blank.')],
            [['title', 'description'], 'string'],
            //[['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            ['status', 'in', 'range' => [self::statusActive, self::statusPassive]],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => Yii::t('app', 'Category'),
            'counter' => Yii::t('app', 'View Count'),
            'rate_id' => Yii::t('app', 'Rate'),
            'description' => Yii::t('app', 'Description'),
            'title' => Yii::t('app', 'Title'),
            'category_fields_wish_id' => 'Category Wish',
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductFields()
    {
        return $this->hasMany(ProductField::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRate()
    {
        return $this->hasOne(Rate::className(), ['id' => 'rate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryFieldsWish()
    {
        return $this->hasOne(CategoryFieldsWish::className(), ['id' => 'category_fields_wish_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryFieldsValue(int $id, $tableName)
    {
        $lang = Yii::$app->language;
        $data = $this->hasMany(ProductField::className(), ['product_id' => 'id'])->andWhere(['category_field_id' => $id])->asArray()->all();
        $data = ArrayHelper::map($data, 'category_field_value_id', 'category_field_value_id');
        if ($data) {
            $ids = implode(',', $data);
            return ArrayHelper::map(Yii::$app->db->createCommand("SELECT id,name_$lang FROM $tableName WHERE id in ($ids)")->queryAll(), 'id', 'id');

        }
        return false;
    }

    public function getCategoryFieldsValueName(int $id, $tableName)
    {
        $lang = Yii::$app->language;
        $data = $this->hasMany(ProductField::className(), ['product_id' => 'id'])->andWhere(['category_field_id' => $id])->asArray()->all();
        $data = ArrayHelper::map($data, 'category_field_value_id', 'category_field_value_id');
        if ($data) {
            $ids = implode(',', $data);
            return ArrayHelper::map(Yii::$app->db->createCommand("SELECT id,name_$lang FROM $tableName WHERE id in ($ids)")->queryAll(), 'name_'.$lang, 'name_'.$lang);

        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbom()
    {
        return $this->hasMany(Albom::className(), ['product_id' => 'id'])->orderBy('image_rank asc');
    }


    public function getProductFirstImage()
    {
        if ($this->albom) {
            return Albom::getImageThumbFirst($this->created_at, $this->albom[0]['image']);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryFieldsRel()
    {
        return $this->hasMany(CategoryField::className(), ['category_fields_wish_id' => 'category_fields_wish_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAddress()
    {
        return $this->hasOne(ProductAddress::className(), ['product_id' => 'id']);
    }


    public function getCategoryFields($id = null)
    {
        // $categoriesIds = $this->category->allParentsIds;

        $categoriesIds[] = $this->category_id;

        if ($categoriesIds) {
            $categoryFieldsWishIds = CategoryFieldsWish::find()->select('id')->where(['IN', 'category_id', $categoriesIds])->orderBy('rank asc')->indexBy('id')->column();

            if ($categoryFieldsWishIds) {
                $field = $id ? ArrayHelper::getValue($categoryFieldsWishIds, $id) : key($categoryFieldsWishIds);
                if ($field) {
                    return CategoryField::find()->where(['IN', 'category_fields_wish_id', $field])->orderBy('rank desc')->all();
                }
            }
        }
        return [];
    }

    public function getStatusList()
    {
        return [
            self::statusActive => 'Active',
            self::statusPassive => 'Passive',
        ];
    }


    /* upload images*/
    public function uplodeImages($imageAlbom, $model, $count = 0)
    {
        $files = UploadedFile::getInstances($imageAlbom, 'image');
        if (is_array($files)) {
            foreach ($files as $file) {
                $count += 1;
                $imageAlbom = new Albom();
                $date = date_create(date('Y-m-d', $model->created_at));
                $imagekesh = uniqid(time() . '_' . rand(1, 9999999));
                if (is_dir(Yii::getAlias('@frontend/web') . '/image/' . date_format($date, "Y-m-d"))) {
                    $file->saveAs(Yii::getAlias('@frontend/web') . '/image/' . date_format($date, "Y-m-d") . '/' . md5($imagekesh) . '.' . $file->extension);
                    $imageAlbom->image_rank = $count;
                    $imageAlbom->image = md5($imagekesh) . '.' . $file->extension;
                    $this->resizeImage($model, $imageAlbom, $date);
                } else {
                    mkdir(Yii::getAlias('@frontend/web') . "/image/" . date_format($date, "Y-m-d") . "/", 0777, true);
                    $file->saveAs(Yii::getAlias('@frontend/web') . '/image/' . date_format($date, "Y-m-d") . '/' . md5($imagekesh) . '.' . $file->extension);
                    $imageAlbom->image_rank = $count;
                    $imageAlbom->image = md5($imagekesh) . '.' . $file->extension;
                    $this->resizeImage($model, $imageAlbom, $date);
                }

                $imageAlbom->user_id = Yii::$app->user->identity->id;
                $imageAlbom->product_id = $model->id;
                $imageAlbom->save(false);
            }
        }
    }

    /* resize images*/
    public function resizeImage($model, $imagealbom, $date)
    {
        $imagine = Image::getImagine();
        $fullUrl = Albom::getImageFrontBack($model->created_at, $imagealbom->image);
        $alias = yii::getAlias('@frontend' . '/web/' . $fullUrl);
        $folderPath = Albom::getImageFolderPath($model->created_at, $imagealbom->image);


        if (is_dir(Yii::getAlias('@frontend') . "/web/image/" . date_format($date, "Y-m-d") . '/thumb')) {
            $newAlias = yii::getAlias('@frontend' . '/web/' . $folderPath . '/thumb/');
            /*    width: 580px;
        height: 430px;*/
            if (file_exists($alias)) {
                $image = $imagine->open($alias);
                if ($image) {
                    $image->resize(new Box(240, 180))->save(Yii::getAlias($newAlias . $imagealbom->image, ['quality' => 70]));
                    $image->resize(new Box(580, 430))->save(Yii::getAlias($newAlias . $imagealbom->image, ['quality' => 70]));
                }
            }
        } else {
            mkdir(Yii::getAlias('@frontend') . "/web/image/" . date_format($date, "Y-m-d") . "/thumb", 0777, true);

            $newAlias = yii::getAlias('@frontend' . '/web/' . $folderPath . '/thumb/');
            if (file_exists($alias)) {
                $image = $imagine->open($alias);
                if ($image) {
                    $image->resize(new Box(240, 180))->save(Yii::getAlias($newAlias . $imagealbom->image, ['quality' => 70]));
                }
            }
        }
    }
}
