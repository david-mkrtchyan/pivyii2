<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%product_message}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $owner_chosen_user_id
 * @property string $message
 * @property string $type
 * @property int $product_id
 * @property array $types
 * @property int $deleted
 * @property int $created_at
 * @property int $updated_at
 * @property array $productMessageLike
 * @property array $productMessageLikeDislike
 *
 * @property User $user
 * @property Products $product
 */
class  ProductMessage extends \yii\db\ActiveRecord
{
    const TYPE_PRIVATE = 'private';
    const TYPE_PUBLIC = 'public';
    const DELETED_YES = 1;
    const DELETED_NO = 0;

    public $checkedPrivate;
    public $checkedPublic;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%product_message}}';
    }

    public static function getDeleted()
    {
        return [
              self::DELETED_YES => Yii::t('app','Deleted'),
              self::DELETED_NO =>  Yii::t('app','Not Deleted'),
        ];
    }
    public static function getTypes()
    {
        return [
              self::TYPE_PRIVATE => self::TYPE_PRIVATE,
              self::TYPE_PUBLIC =>  self::TYPE_PUBLIC,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'message', 'product_id'], 'required','message' => Yii::t('app','{attribute} cannot be blank.')],
            [['user_id', 'product_id', 'owner_chosen_user_id','created_at', 'updated_at', 'deleted'], 'integer','message' => Yii::t('app','{attribute} must be an integer.')],
            [['message', 'type'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['owner_chosen_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['owner_chosen_user_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            ['type', 'in', 'range' => [self::TYPE_PRIVATE, self::TYPE_PUBLIC]],
            ['deleted', 'in', 'range' => [self::DELETED_NO, self::DELETED_YES]],
            ['owner_chosen_user_id', 'required', 'when' => function($model) {
                if ($this->type == self::TYPE_PRIVATE && Yii::$app->user->identity->id == ArrayHelper::getValue( $this,'product.user_id')){
                    return true;
                };
                return false;
            },'message' => Yii::t('app','You must choose user,which this message is related')],
            [['checkedPrivate','checkedPublic'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => Yii::t('app','User ID'),
            'message' => Yii::t('app','Message'),
            'type' => Yii::t('app','Type'),
            'product_id' => Yii::t('app','Product ID'),
            'owner_chosen_user_id' => Yii::t('app','User'),
            'checkedPrivate' => Yii::t('app','Enter send the message'),
            'checkedPublic' => Yii::t('app','Enter send the message'),
            'created_at' => Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwnerChosenUser()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_chosen_user_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductMessageLike()
    {
        return $this->hasMany(ProductMessageLike::className(), ['product_message_id' => 'id'])->andWhere(['like' => 1]);
    }

     public function getProductMessageLikeDislike()
    {
        return $this->hasMany(ProductMessageLike::className(), ['product_message_id' => 'id'])->andWhere(['like' => 0]);
    }

    public function getProductMessageLikeOne()
    {
        return $this->hasOne(ProductMessageLike::className(), ['product_message_id' => 'id'])->andWhere(['user_id' => Yii::$app->user->identity->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
