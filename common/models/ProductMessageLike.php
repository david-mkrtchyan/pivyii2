<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%product_message_like}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $product_message_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $like
 *
 * @property User $user
 * @property ProductMessage $productMessage
 */
class ProductMessageLike extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%product_message_like}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'product_message_id'], 'required'],
            [['user_id', 'product_message_id', 'created_at', 'updated_at', 'like'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['product_message_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductMessage::className(), 'targetAttribute' => ['product_message_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'product_message_id' => 'Product Message ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductMessage()
    {
        return $this->hasOne(ProductMessage::className(), ['id' => 'product_message_id']);
    }
}
