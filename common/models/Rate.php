<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "piv_rate".
 *
 * @property int $id
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_lt
 * @property string $symbols
 * @property string $country_rate
 * @property int $created_at
 * @property int $updated_at
 */
class Rate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'piv_rate';
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_en', 'name_ru','name_lt','symbols', 'country_rate'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name_en', 'name_ru','name_lt', 'symbols', 'country_rate'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_en' => Yii::t('app', 'Rate Name En'),
            'name_ru' => Yii::t('app', 'Rate Name Ru'),
            'name_lt' => Yii::t('app', 'Rate Name Lt'),
            'symbols' => Yii::t('app', 'Symbols'),
            'country_rate' => Yii::t('app', 'Country Rate'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
