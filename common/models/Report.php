<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "piv_report".
 *
 * @property int $id
 * @property int $report_category_id
 * @property string $email
 * @property string $review
 * @property int $user_id
 * @property int $created_at
 * @property int $product_id
 * @property int $updated_at
 *
 * @property Report $reportCategory
 * @property Product[] $product
 */
class Report extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'piv_report';
    }


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['report_category_id', 'email','review'], 'required'],
            [['report_category_id', 'product_id','created_at', 'updated_at','user_id'], 'integer'],
            [['review'], 'string'],
            [['email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['report_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ReportCategory::className(), 'targetAttribute' => ['report_category_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'report_category_id' => Yii::t('app', 'Report'),
            'email' => Yii::t('app', 'Email'),
            'review' => Yii::t('app', 'Review'),
            'user_id' => Yii::t('app', 'User'),
            'product_id' => Yii::t('app', 'Product'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportCategory()
    {
        return $this->hasOne(ReportCategory::className(), ['id' => 'report_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
