<?php

namespace common\models\opcia;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%opcia}}".
 *
 * @property int $id
 * @property Opcia typeList[]
 * @property Opcia typeListAttr[]
 * @property string $type
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_lt
 * @property int $created_at
 * @property int $updated_at
 */
class Opcia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'piv_opcia';
    }

    public static $typeListAttr = [
        'car_type',
        'color',
        'condition',
        'construction_type',
        'daily_monthly',
        'engine_type',
        'equipment',
        'floor',
        'fuel',
        'make',
        'dealer_name',
        'parts_type',
        'rooms',
        'shoe_size',
        'size',
        'transmission',
        'tv_type',
        'type',
        'area','country','engine_capacity','power','drive_wheels','stearing_position','predestination','trailer_equipment','manufacturer','quantity','diameter'
    ];

    public static function getTypeList()
    {
        return [
            'car_type' => 'Car type',
            'color' => 'Color',
            'condition' => 'Condition',
            'construction_type' => 'Construction type',
            'daily_monthly' => 'Daily monthly',
            'engine_type' => 'Engine type',
            'equipment' => 'Equipment',
            'floor' => 'Floor',
            'fuel' => 'Fuel',
            'make' => 'Make',
            'dealer_name' => 'Dealer name',
            'parts_type' => 'Parts type',
            'rooms' => 'Rooms',
            'shoe_size' => 'Shoe size',
            'size' => 'Size',
            'transmission' => 'Transmission',
            'tv_type' => 'Tv type',
            'type' => 'Type',
            'area' => 'Area',
            'country' => 'Country',
            'engine_capacity' => 'Engine_capacity',
            'power' => 'Power',
            'drive_wheels' => 'Drive Wheels',
            'stearing_position' => 'Stearing Position',
            'predestination' => 'Predestination',
            'trailer_equipment'  => 'Trailer Equipment',
            'manufacturer' => 'Manufacturer',
            'quantity' => 'Quantity',
            'diameter'=> 'Diameter',

        ];
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'name_en', 'name_ru', 'name_lt'], 'required'],
            [['type'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['name_en', 'name_ru', 'name_lt'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'name_en' => Yii::t('app', 'Name En'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_lt' => Yii::t('app', 'Name Lt'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }


}
