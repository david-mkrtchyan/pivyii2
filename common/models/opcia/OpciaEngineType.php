<?php

namespace common\models\opcia;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%opcia_engine_type}}".
 *
 * @property int $id
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_lt
 * @property int $created_at
 * @property int $updated_at
 */
class OpciaEngineType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'piv_opcia_engine_type';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_en', 'name_ru', 'name_lt'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name_en', 'name_ru', 'name_lt'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_en' => 'Name En',
            'name_ru' => 'Name Ru',
            'name_lt' => 'Name Lt',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
