<?php

namespace common\models\opcia;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%opcia_equipment}}".
 *
 * @property int $id
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_lt
 * @property int $created_at
 * @property int $updated_at
 */
class OpciaEquipment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'piv_opcia_equipment';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_en', 'name_ru', 'name_lt'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name_en', 'name_ru', 'name_lt'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_en' => Yii::t('app', 'Value En'),
            'name_ru' => Yii::t('app', 'Value Ru'),
            'name_lt' => Yii::t('app', 'Value Lt'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
