<?php

namespace common\models\opcia;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "piv_opcia_floor".
 *
 * @property int $id
 * @property string $name_en
 * @property string $name_lt
 * @property string $name_ru
 * @property int $created_at
 * @property int $updated_at
 */
class OpciaFloor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'piv_opcia_floor';
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_en', 'name_lt', 'name_ru'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name_en', 'name_lt', 'name_ru'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_en' => Yii::t('app', 'Name En'),
            'name_lt' => Yii::t('app', 'Name Lt'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
