<?php

namespace common\models\opcia;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%opcia_name_dealer_name}}".
 *
 * @property int $id
 * @property string $name_en
 * @property string $name_ru
 * @property int $name_lt
 * @property int $created_at
 * @property int $updated_at
 */
class OpciaOwnerDealerName extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'piv_opcia_owner_dealer_name';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'name_en', 'name_ru', 'name_lt'], 'required'],
            [['name_lt', 'created_at', 'updated_at'], 'integer'],
            [['name_en', 'name_ru'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_en' => Yii::t('app', 'Name En'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_lt' => Yii::t('app', 'Name Lt'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
