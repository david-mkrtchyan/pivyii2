<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BannerStat;

/**
 * BannerPlacementSerach represents the model behind the search form about `app\models\BannerPlacement`.
 */
class BannerStatSerach extends BannerStat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shows_count', 'clicks_count', 'ctr', 'banner_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BannerStat::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'shows_count' => $this->shows_count,
            'clicks_count' => $this->clicks_count,
            'ctr' => $this->ctr,
            'banner_id' => $this->banner_id,
        ]);


        return $dataProvider;
    }
}
