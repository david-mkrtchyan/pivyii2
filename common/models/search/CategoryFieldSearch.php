<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CategoryField;

/**
 * CategoryFieldearch represents the model behind the search form about `backend\models\CategoryField`.
 */
class CategoryFieldSearch extends CategoryField
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_fields_wish_id', 'type', 'allow_multiple', 'allow_range', 'range_min', 'range_max', 'value_source', 'created_at', 'updated_at', 'rank'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CategoryField::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_fields_wish_id' => $this->category_fields_wish_id,
            'type' => $this->type,
            'allow_multiple' => $this->allow_multiple,
            'allow_range' => $this->allow_range,
            'range_min' => $this->range_min,
            'range_max' => $this->range_max,
            'value_source' => $this->value_source,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'rank' => $this->rank,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
