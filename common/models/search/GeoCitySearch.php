<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\GeoCity;

/**
 * GeoCitySearch represents the model behind the search form of `common\models\GeoCity`.
 */
class GeoCitySearch extends GeoCity
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'geonameid', 'admin1_id', 'population', 'elevation', 'gtopo30', 'publish', 'sort'], 'integer'],
            [['name', 'asciiname', 'fclass', 'fcode', 'country', 'cc2', 'admin1', 'moddate', 'deleted'], 'safe'],
            [['latitude', 'longitude'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GeoCity::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'geonameid' => $this->geonameid,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'admin1_id' => $this->admin1_id,
            'population' => $this->population,
            'elevation' => $this->elevation,
            'gtopo30' => $this->gtopo30,
            'moddate' => $this->moddate,
            'publish' => $this->publish,
            'sort' => $this->sort,
        ]);

        $query->andFilterWhere(['like','country','lt' ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'asciiname', $this->asciiname])
            ->andFilterWhere(['like', 'fclass', $this->fclass])
            ->andFilterWhere(['like', 'fcode', $this->fcode])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'cc2', $this->cc2])
            ->andFilterWhere(['like', 'admin1', $this->admin1])
            ->andFilterWhere(['like', 'deleted', $this->deleted]);

        return $dataProvider;
    }
}
