<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OpciaOwnerAgency;

/**
 * OpciaOwnerAgencySearch represents the model behind the search form of `common\models\OpciaOwnerAgency`.
 */
class OpciaOwnerAgencySearch extends OpciaOwnerAgency
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['owner_agency_name_en', 'owner_agency_name_ru', 'owner_agency_name_lt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OpciaOwnerAgency::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'owner_agency_name_en', $this->owner_agency_name_en])
            ->andFilterWhere(['like', 'owner_agency_name_ru', $this->owner_agency_name_ru])
            ->andFilterWhere(['like', 'owner_agency_name_lt', $this->owner_agency_name_lt]);

        return $dataProvider;
    }
}
