<?php

namespace common\models\search;

use common\models\Albom;
use common\models\CategoryField;
use common\models\Config;
use common\models\ProductField;
use frontend\models\ProductAddress;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Product;
use yii\helpers\ArrayHelper;

/**
 * ProductSearch represents the model behind the search form about `common\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','user_id', 'category_id', 'status', 'created_at', 'updated_at','category_fields_wish_id'], 'integer'],
            [['title'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        if(isset($params['cookiesFavourite'])){
            $query->andWhere(['id'=>$params['cookiesFavourite']]);
        }

        $query->orderBy('id desc');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>array(
                'pageSize'=>21,
            ),
        ]);


        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }



        // grid filtering conditions
        $query->andFilterWhere([
            'category_fields_wish_id' => $this->category_fields_wish_id,
            'category_id' => $this->category_id,
            'user_id' => $this->user_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);



        if(isset($params['categoryIds'])){
            $query->andFilterWhere(['in', 'category_id', $params['categoryIds']]);
        }

        if(isset($params['category_fields_wish_id'])){
            $query->andFilterWhere(['category_fields_wish_id' => $params['category_fields_wish_id']]);
        }

        if(isset($params['searchId']) && intval($params['searchId'])){
            $query->andFilterWhere(['category_id' =>  intval($params['searchId'])]);
        }

        if(isset($params['cityIds']) && $params['cityIds']){
            $query->andFilterWhere(['in', 'id', $params['cityIds'] ]);
        }

        if(isset($params['imageChecked']) && $params['imageChecked']){
            $ids= Albom::find()->select('product_id')->groupBy('product_id')->indexBy('product_id')->column();
            $query->andFilterWhere(['in', 'id', $ids ]);
        }

        if(isset($params['cityCode_id']) && $params['cityCode_id']){
            $product = ProductAddress::find()->select('product_id')->where(['city_id' =>$params['cityCode_id'] ])->indexBy('product_id')->column();
            if($product){
                $query->andFilterWhere(['in', 'id', $product ]);
            }
        }





        if(isset($params['city_id'])){
           $ids =  ArrayHelper::map(ProductAddress::find()->select('product_id')->where(['city_id' => $params['city_id']])->groupBy('city_id')->all(),'product_id','product_id');
           $query->andFilterWhere(['in', 'id', $ids]);
        }


        if (isset($params['category_wish']) && isset($params['ProductField']['allCategoryFields'])) {


            $params['ProductField']['allCategoryFields'];
            $key = key($params['ProductField']['allCategoryFields']);
            $valueCookies = NULL;
            if(isset($params['ProductField']['allCategoryFields'][$key]) &&
                !is_array($params['ProductField']['allCategoryFields'][$key])){
                $valueCookies = $params['ProductField']['allCategoryFields'][$key];
            }

            Config::addRecentSearch($params['category_wish'],$key,$valueCookies);


            $productFieldTable = ProductField::tableName();
            $productTable = Product::tableName();
            $statusActive = Product::statusActive;

            $str = '';
            $count = 0;
            $query->andFilterWhere([ 'category_id' => $params['category_wish'] ]);

            foreach ($params['ProductField']['allCategoryFields'] as $key => $value) {
                if ($key && $value) {
                    $categoryField = CategoryField::findOne(['id' => $key]);
                    if ($categoryField) {
                        if ($categoryField->type == CategoryField::typeSelect ||
                            $categoryField->type == CategoryField::typeCheckbox ||
                            $categoryField->type == CategoryField::typeRadio ||
                            ($categoryField->type == CategoryField::typeInput && $categoryField->allow_multiple == CategoryField::multipleYes)
                        ) {
                            if ($categoryField->type == CategoryField::typeSelect &&
                                $categoryField->allow_range
                            ) {
                                if (is_array($value) && isset($value['min']) && isset($value['max'])) {
                                    $min = $value['min'];
                                    $max = $value['max'];
                                    $minText = $min ? "and $productFieldTable.value >= $min" : null;
                                    $maxText = $max ? "and $productFieldTable.value <= $max" : null;
                                    if ($min || $max) {
                                        if ($count == 0) {
                                            $str .= "$productTable.id IN (SELECT $productFieldTable.product_id FROM $productFieldTable WHERE $productFieldTable.category_field_id = $key $minText $maxText and $productFieldTable.product_id = $productTable.id)";
                                        } else {
                                            $str .= " and $productTable.id IN (SELECT $productFieldTable.product_id FROM $productFieldTable WHERE $productFieldTable.category_field_id = $key  $minText $maxText   and $productFieldTable.product_id = $productTable.id )";
                                        }
                                        $count++;
                                    }
                                }
                            } else {
                                $value = (array)$value;
                                $value = implode(',', $value);
                                if ($count == 0) {
                                    $str .= "$productTable.id IN (SELECT $productFieldTable.product_id FROM $productFieldTable WHERE $productFieldTable.category_field_id = $key and $productFieldTable.category_field_value_id IN ($value) and $productFieldTable.product_id = $productTable.id)";
                                } else {
                                    $str .= " and $productTable.id IN (SELECT $productFieldTable.product_id FROM $productFieldTable WHERE $productFieldTable.category_field_id = $key and $productFieldTable.category_field_value_id IN ($value) and $productFieldTable.product_id = $productTable.id )";
                                }
                                $count++;
                            }

                        } elseif ($categoryField->type == CategoryField::typeInput) {
                            if (is_array($value) && isset($value['min']) && isset($value['max'])) {
                                $min = $value['min'];
                                $max = $value['max'];
                                $minText = $min ? "and $productFieldTable.value >= $min" : null;
                                $maxText = $max ? "and $productFieldTable.value <= $max" : null;
                                if ($min || $max) {
                                    if ($count == 0) {
                                        $str .= "$productTable.id IN (SELECT $productFieldTable.product_id FROM $productFieldTable WHERE $productFieldTable.category_field_id = $key $minText $maxText and $productFieldTable.product_id = $productTable.id)";
                                    } else {
                                        $str .= " and $productTable.id IN (SELECT $productFieldTable.product_id FROM $productFieldTable WHERE $productFieldTable.category_field_id = $key  $minText $maxText   and $productFieldTable.product_id = $productTable.id )";
                                    }
                                    $count++;
                                }
                            } elseif (is_string($value) && $value) {
                                if ($count == 0) {
                                    $str .= "$productTable.id IN (SELECT $productFieldTable.product_id FROM $productFieldTable WHERE $productFieldTable.category_field_id = $key and $productFieldTable.value = $value and $productFieldTable.product_id = $productTable.id)";
                                } else {
                                    $str .= " and $productTable.id IN (SELECT $productFieldTable.product_id FROM $productFieldTable WHERE $productFieldTable.category_field_id = $key and  $productFieldTable.value = $value  and $productFieldTable.product_id = $productTable.id )";
                                }
                                $count++;
                            }
                        }
                    }
                }
            }
            if ($str) {
                $products = Yii::$app->db->createCommand("
               SELECT  *FROM $productTable  
               WHERE $productTable.status = $statusActive and $str
            ")->queryAll();

                if ($products) {
                    $ids = ArrayHelper::map($products, 'id', 'id');
                    $query->andFilterWhere(['in', 'id', $ids]);
                } else {
                    $query->andFilterWhere(['in', 'id', -1]);

                }
            }


        }

        $query->andFilterWhere(['like', 'title', $this->title]);

        if(isset($params['searchTerm'])){
            $query->andFilterWhere(['like', 'title', $params['searchTerm']]);
        }



        return $dataProvider;
    }

}
