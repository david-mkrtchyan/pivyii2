<?php

use yii\db\Migration;

/**
 * Class m180604_185959_create_table_categories
 */
class m161213_125213_create_table_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function Up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%categories}}', [
            'id' => $this->primaryKey(),
            'name_en' => $this->string(),
            'name_ru' => $this->string(),
            'name_lt' => $this->string(),
            'slug' => $this->string(),
            'parent_id' => $this->integer()->defaultValue(0),
            'meta_key' => $this->string(),
            'meta_desc' => $this->string(),
            'show' => $this->integer()->defaultValue(1),
            'icon' => $this->string(),
            'rank' =>$this->integer()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $tableOptions);

    }

    /*
     * {@inheritdoc}
    */
    public function Down()
    {
        echo "m180604_185959_create_table_categories cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180604_185959_create_table_categories cannot be reverted.\n";

        return false;
    }
    */
}
