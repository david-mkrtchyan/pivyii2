<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category_fields`.
 */
class m161213_125214_create_category_fields_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%category_fields}}', [
            'id' => $this->primaryKey(),
            'category_fields_wish_id' => $this->integer()->notNull(),
            'name' => $this->string(45),
            'type' => $this->smallInteger(2)->defaultValue(0),
            'allow_multiple' => $this->boolean()->defaultValue(0),
            'allow_range' => $this->boolean()->defaultValue(0),
            'range_min' => $this->integer()->unsigned(),
            'range_max' => $this->integer()->unsigned(),
            'value_source' => $this->boolean(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'rank' => $this->integer()->defaultValue(0),
            'required' => $this->integer()->defaultValue(0),
            'use_database' => $this->integer()->defaultValue(0),
            'db_name' => $this->string()
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{category_fields}}');
    }
}
