<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category_field_values`.
 */
class m161213_135944_create_category_field_values_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%category_field_values}}', [
            'id' => $this->primaryKey(),
            'category_field_id' => $this->integer(),
            'value_lt' => $this->string(255)->notNull(),
            'value_ru' => $this->string(255)->notNull(),
            'value_en' => $this->string(255)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $tableOptions);
        $this->addForeignKey('category_field_values_field', '{{%category_field_values}}', 'category_field_id', '{{%category_fields}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%category_field_values}}');
    }
}
