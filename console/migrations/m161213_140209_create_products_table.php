<?php

use yii\db\Migration;

/**
 * Handles the creation of table `products`.
 */
class m161213_140209_create_products_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'category_fields_wish_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->text(),
            'status' => $this->smallInteger()->unsigned()->defaultValue(1),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
        $this->addForeignKey('product_category', '{{%products}}', 'category_id', '{{%categories}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('product_category_fields_wish', '{{%products}}', 'category_fields_wish_id', '{{%category_fields_wish}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%products}}');
    }
}
