<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_fields`.
 */
class m161213_144024_create_product_fields_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%product_fields}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'category_field_id' => $this->integer(),
            'category_field_value_id' => $this->integer(),
            'value' => $this->string(255),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey('product_fields_product', '{{%product_fields}}', 'product_id', '{{%products}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('product_fields_category_field', '{{%product_fields}}', 'category_field_id', '{{%category_fields}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%product_fields}}');
    }
}
