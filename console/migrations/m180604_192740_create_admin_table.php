<?php

use yii\db\Migration;

/**
 * Class m180604_192740_create
 */
class m180604_192740_create_admin_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%admins}}', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'birth_day' => $this->date()->notNull(),
            'adresse' => $this->string()->notNull(),
            'city_id' => $this->integer()->notNull(),
            'phone' => $this->string()->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'image' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $user = new \backend\models\PivAdmins();
        $user->email = 'pviadmin@mail.ru';
        $user->first_name = 'Admin';
        $user->last_name =  'Admin';
        $user->birth_day = date("1992-12-12");
        $user->adresse = 'my address';
        $user->city_id = 123;
        $user->phone = '+37477850709';
        $user-> password_hash =   '$2y$13$ZrlYCDmiaRPIqYpTJnUjrOrtFAzYJpTNnB3ieh7o28gE2PCPjmlbO'; //pvi_admin
        $user->auth_key = Yii::$app->security->generateRandomString();
        $user->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
        $user->created_at = time();
        $user->updated_at = time();
        $user->save();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180604_192740_create cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180604_192740_create cannot be reverted.\n";

        return false;
    }
    */
}
