<?php

use yii\db\Migration;

/**
 * Class m180606_202335_create_table_category_fields_wish
 */
class m180606_202335_create_table_category_fields_wish extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%category_fields_wish}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'value' => $this->string(255)->notNull(),
            'rank' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $tableOptions);
        $this->addForeignKey('category_fields_wish_field', '{{%category_fields_wish}}', 'category_id',  '{{%categories}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('category_fields_wish_id_category', '{{%category_fields}}', 'category_fields_wish_id', '{{%category_fields_wish}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180606_202335_create_table_category_fields_wish cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180606_202335_create_table_category_fields_wish cannot be reverted.\n";

        return false;
    }
    */
}
