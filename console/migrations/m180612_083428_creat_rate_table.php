<?php

use yii\db\Migration;

/**
 * Class m180612_083428_creat_rate_table
 */
class m180612_083428_creat_rate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%rate}}', [
            'id' => $this->primaryKey(),
            'rate_name' => $this->string()->notNull(),
            'symbols' => $this->string()->notNull(),
            'country_rate' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $tableOptions);
   }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180612_083428_creat_rate_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180612_083428_creat_rate_table cannot be reverted.\n";

        return false;
    }
    */
}
