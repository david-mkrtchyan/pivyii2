<?php

use yii\db\Migration;

/**
 * Class m180612_174917_create_table_contacts
 */
class m180612_174917_create_table_contacts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function Up()
    {
        $this->createTable('{{%contacts}}',[
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'title' => $this->string(),
            'email' => $this->string(),
            'message' => $this->text(),
            'status' => $this->integer()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function Down()
    {
        echo "m180612_174917_create_table_contacts cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180612_174917_create_table_contacts cannot be reverted.\n";

        return false;
    }
    */
}
