<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/lightslider.css',
        'css/site.css',
        'css/style.css',
        'css/flag.css',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
        'css/bootstrap-social.css',
        'css/category.css',
        'css/nav.css',
        'js/lightGallery/dist/css/lightgallery.css',
        'https://fonts.googleapis.com/icon?family=Material+Icons',
        'css/responsive.css',
    ];
    public $js = [
        'js/main.js',
        'js/nav.js',
        'js/menu.js',
        'https://code.jquery.com/jquery-2.2.0.min.js',
        'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
        'slick/slick.js',
        'js/jquery.dcjqaccordion.2.7.js',
        'js/lightslider.js',
        'https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js',
        'js/lightGallery/dist/js/lightgallery-all.min.js',
        'js/lightGallery/lib/jquery.mousewheel.min.js',
        "js/favourite.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
