<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/lightslider.css',
        'css/site.css',
        'css/style.css',
        'css/flag.css',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
        'css/bootstrap-social.css',
        'css/category.css',
        'css/nav.css',
        'js/lightGallery/dist/css/lightgallery.css',
        'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css',
        'https://fonts.googleapis.com/icon?family=Material+Icons',
        'css/responsive.css',
    ];
    public $js = [
        'js/main.js',
        'js/nav.js',
        'js/menu.js',
        'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
        'slick/slick.js',
        'js/jquery.dcjqaccordion.2.7.js',
        'js/lightslider.js',
        'https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js',
        'js/lightGallery/dist/js/lightgallery-all.min.js',
        'js/lightGallery/lib/jquery.mousewheel.min.js',
        "js/favourite.js",
        "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js",
        "js/jquery-ias.min.js",
        "//static.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js",
        "https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.js",
        "https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
