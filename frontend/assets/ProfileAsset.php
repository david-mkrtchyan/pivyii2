<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ProfileAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/style.css',
        'css/flag.css',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
        'css/aciTree.css',
        'css/demo.css',
        'css/menu.css',
        'css/profile.css',
        'css/bootstrap-social.css',
        'css/category.css',
        'css/nav.css',
        'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css',
        'https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css',
        'css/responsive.css',
    ];
    public $js = [
        'js/nav.js',
        'js/menu.js',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
        'slick/slick.js',
        'js/jquery.scrollUp.min.js',
        'js/jquery.prettyPhoto.js',
        'js/jquery.dcjqaccordion.2.7.js',
        'js/jquery.cookie.js',
        'js/main.js',
        'js/inspinia.js',
        "js/validator.min.js",
        "js/profile.js",
        "js/favourite.js",
        "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js",
        "https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
