<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'profile' => [
            'class' => 'frontend\modules\profile\profile',
        ],
    ],
    'components' => [
        'request' => [
            'enableCsrfValidation' => false,
            'csrfParam' => '_csrf',
            'baseUrl' => ''
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<action>' => 'site/<action>',
                '' => 'site/index',
                'about' => 'site/about',
                'contact' => 'site/contact',
                'signup' => 'site/signup',
                'login' => 'site/login',
                'category/<slug>' => 'category/index',
                'help/<slug>' => 'help/index',
                'category/view/<id:\d+>' => 'category/view',
                'map/<id:\d+>/<type>' => 'category/get-map',
                'profile/my-ads' => 'profile/product/index',
                'user/profile/<id:\d+>/' => 'user/profile',
                'profile/preferred' => 'profile/product/preferred',
                'profile/my-ads/view/<id:\d+>' => 'profile/product/view',
                'profile/my-ads/update/<id:\d+>' => 'profile/product/update',
                'profile/my-ads/delete/<id:\d+>' => 'profile/product/delete',
                'profile/my-ads/category-update-field' => 'profile/product/category-update-field/',
                'category/search/<term>' => '/category/search',
            ],
        ],
    ],
    'params' => $params,
];
