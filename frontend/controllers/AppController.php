<?php
namespace frontend\controllers;

use common\controllers\EntityController;
use common\models\Config;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
 
/**
 * Site controller
 */
class AppController extends EntityController
{
    public function init()
    {
        parent::init();
    }
}