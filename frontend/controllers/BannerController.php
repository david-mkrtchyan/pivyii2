<?php
/**
 * Created by PhpStorm.
 * User: Zakar
 * Date: 7/14/2017
 * Time: 4:15 PM
 */

namespace frontend\controllers;

use common\models\Banner;
use common\models\BannerStat;
use common\models\User;
use Yii;
use yii\web\Controller;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class BannerController extends Controller
{
    public function actionBannerCount()
    {
        if(Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('bannerId');
            $banner = Banner::find()->where(['id' => $id])->one();
            if(!$banner){return false;}
            $bannerStat = BannerStat::find()->where(['banner_id' => $banner->id])->orderBy('id desc')->one();
            if($bannerStat) {

                $bannerLastWeek = strtotime('-2 day');
                $bannerDayNow = strtotime('now');
                $bannerDayCreatedAt = $bannerStat->created_at;
                if($bannerLastWeek <= $bannerDayCreatedAt && $bannerDayCreatedAt <= $bannerDayNow) {
                    $bannerStat->clicks_count += 1;
                    if ($bannerStat->shows_count !== 0) {
                        $ctr = ($bannerStat->clicks_count / $bannerStat->shows_count) * 100;
                        $bannerStat->ctr = $ctr;
                    }
                    $bannerStat->update(false);
                }else{
                    $bannerStat = new BannerStat;
                    $bannerStat->shows_count = 0;
                    $bannerStat->ctr = 0;
                    $bannerStat->banner_id = $banner->id;
                    $bannerStat->clicks_count += 1;
                    $bannerStat->created_at = time();
                    $bannerStat->save(false);
                }
            }else{
                $bannerStat = new BannerStat;
                $bannerStat->shows_count = 0;
                $bannerStat->ctr = 0;
                $bannerStat->banner_id = $banner->id;
                $bannerStat->clicks_count += 1;
                $bannerStat->created_at = time();
                $bannerStat->save(false);
            }
            //клики / показы * 100 %
        }
    }

    public function actionSessionBanner(){
        if(Yii::$app->request->isAjax == true) {
             $user_id = Yii::$app->session->get('user_id');
            if($user_id){
                $user = Users::findOne($user_id);
                if($user->is_premium == 'true'){
                    return false;
                }
            }

            $session = Yii::$app->session;
            if ($session->isActive)
            $session->open();
            $sessionBanner = $session['bannerId-' . Yii::$app->session->get('user_id')];
            $session['bannerId-' . Yii::$app->session->get('user_id')] += 1;
            $session->close();
            return $sessionBanner;

        }
    }
    public function actionBannerShown()
    {
        if(Yii::$app->request->isAjax == true) {
            $id = Yii::$app->request->post('bannerId');
            $banner = Banner::find()->where(['id' => $id])->one();
            if(!$banner){return false;}
            $bannerStat = BannerStat::find()->where(['banner_id' => $banner->id])->orderBy('id desc')->one();
            if($bannerStat) {

                $bannerLastWeek = strtotime('-2 day');
                $bannerDayNow = strtotime('now');
                $bannerDayCreatedAt = $bannerStat->created_at;

                if($bannerLastWeek <= $bannerDayCreatedAt && $bannerDayCreatedAt <= $bannerDayNow){
                    $bannerStat->shows_count += 1;
                    if($bannerStat->shows_count !== 0 ){
                        $ctr =  ($bannerStat->clicks_count /$bannerStat->shows_count) * 100;
                        $bannerStat->ctr = $ctr;
                    }
                    $bannerStat->update(false);
                }else{
                    $bannerStat = new BannerStat;
                    $bannerStat->shows_count = 1;
                    $bannerStat->ctr = 0;
                    $bannerStat->banner_id = $banner->id;
                    $bannerStat->clicks_count = 0;
                    $bannerStat->created_at = time();
                    $bannerStat->save(false);
                }

            }else{
                $bannerStat = new BannerStat;
                $bannerStat->shows_count = 1;
                $bannerStat->ctr = 0;
                $bannerStat->banner_id = $banner->id;
                $bannerStat->clicks_count = 0;
                $bannerStat->created_at = time();
                $bannerStat->save(false);
            }
        }
    }

    public function actionIframe($image)
    {
        $this->layout = 'empty';
        return $this->render('iframe',[
            'image' => $image
        ]);
    }
}
