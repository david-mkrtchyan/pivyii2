<?php
namespace frontend\controllers;

use common\models\Rate;
use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;

/**
 * Currency controller
 */
class CurrencyController extends Controller
{

    public function actionGetData($arrays){
        $newArray = [];
        foreach ($arrays as $key =>$array){
            if(array_search($array,$arrays)) {
                $newArray[$array] = $arrays;
                if(ArrayHelper::getValue($newArray,$array.'.'.$array)) {
                    unset($newArray[$array][$array]);
                }
            }
        }
        return $newArray;
    }


    public function actionIndex()
    {
        $lang = Yii::$app->language;
        $currencys = Rate::find()->select("name_$lang")->asArray()->all();

        $arrays = [];
        foreach ($currencys as $currency) {
            $name = ArrayHelper::getValue($currency, "name_$lang");
            $arrays[$name] = $name;
        }
        $getCurrency  =  $this->actionGetData($arrays);
        $complateArraay = [];
        if($getCurrency){
            foreach ($getCurrency as $key=>$value){
                foreach ($value as $val){
                    $complateArraay[$key][$val] = $this->currencyConverter($key, $val,1);
                }
            }
        }

        $session = Yii::$app->session;
        if ($session->isActive)
            $session->open();
        $session->set('currencySession', $complateArraay);
        $session->close();
    }

    public function currencyConverter($from_Currency,$to_Currency,$amount) {

        $converted_currency = null;

        $from_Currency = urlencode(strtoupper($from_Currency));
        $to_Currency = urlencode(strtoupper($to_Currency));
        $url = file_get_contents('http://free.currencyconverterapi.com/api/v3/convert?q=' . $from_Currency . '_' . $to_Currency . '&compact=ultra');
        $json = json_decode($url, true);

        if($json){
            $converted_currency  = ArrayHelper::getValue($json,$from_Currency.'_'.$to_Currency);
        }

   /*     $from_Currency = urlencode($from_Currency);
        $to_Currency = urlencode($to_Currency);
        $get = file_get_contents("https://finance.google.com/finance/converter?a=1&from=$from_Currency&to=$to_Currency");
        $get = explode("<span class=bld>",$get);
        $get = explode("</span>",ArrayHelper::getValue($get,'1'));
        $converted_currency = preg_replace("/[^0-9\.]/", null, ArrayHelper::getValue($get,'0'));*/

        return $converted_currency;
    }
}
