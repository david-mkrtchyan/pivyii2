<?php

namespace frontend\controllers;

use common\fixtures\UserFixture;
use common\models\Contact;
use common\models\Geo;
use common\models\Product;
use common\models\Rate;
use common\models\UserFollow;
use Yii;
use yii\base\InvalidParamException;
use yii\bootstrap\ActiveForm;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\Cookie;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends AppController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'products'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'products' => ['get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        $products = Product::find()->where(['status' => Product::statusActive])->orderBy('id desc')->indexBy('id')->limit(18)->all();
        $mostViewProduct = Product::find()->where(['status' => Product::statusActive])->orderBy('counter desc')->indexBy('id')->limit(18)->all();

        return $this->render('index', compact('products', 'mostViewProduct'));
    }

    public function actionProducts($id)
    {
        $products = Product::findAll(['id' => $id]);
        return $this->render('products', compact('products'));
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = 'custom';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';
            return $this->render('login', [
                'model' => $model,
            ]);
        }


        /* $model = new LoginForm();

         if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
             Yii::$app->response->format = Response::FORMAT_JSON;
             return ActiveForm::validate($model); \Yii::$app->end();
         }*/

    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        die;
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $modelContact = new Contact();
                $data = Yii::$app->request->post()['ContactForm'];
                $data['title'] = $data['subject'];
                $data['message'] = htmlspecialchars($data['body']);
                if(!Yii::$app->user->isGuest){
                    $data['user_id'] = Yii::$app->user->identity->id;
                }
                if ($modelContact->load(['Contact' => $data])) {
                    $modelContact->save();
                }
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $this->layout = 'custom';

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionAddUserFavourite()
    {
        if (Yii::$app->request->isAjax) {
            $id = intval(Yii::$app->request->post('id'));

            $user = Yii::$app->user->identity;
            if($user->id !== $id) {
                $userFollow = UserFollow::findOne(['sender_id' => $user->id, 'receiver_id' => $id]);
                if (!$userFollow) {
                    $userFollow = new UserFollow();
                    $userFollow->sender_id = $user->id;
                    $userFollow->receiver_id = $id;
                    $userFollow->follow = $userFollow::followYes;
                    if ($userFollow->save()) {
                        return json_encode(['added' => $userFollow->follow]);
                    }
                } else {
                    if ($userFollow->follow == $userFollow::followYes) {
                        $userFollow->follow = $userFollow::followNo;
                    } elseif ($userFollow->follow == $userFollow::followNo) {
                        $userFollow->follow = $userFollow::followYes;
                    }
                    if ($userFollow->save()) {
                        return json_encode(['added' => $userFollow->follow]);
                    }
                }
            }
        }
        return false;
    }

    public function actionAddfavourite()
    {
        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('id');
            $isAdded = intval(Yii::$app->request->post('isAdded'));
            $cookies = Yii::$app->response->cookies;
            $cookiesFavourite = Yii::$app->request->cookies->getValue('favourite', []);

            if (Yii::$app->user->isGuest) {
                if ($isAdded == 1) {
                    if (!in_array($id, $cookiesFavourite)) {
                        $cookiesFavourite[] = $id;
                    }
                } elseif (($key = array_search($id, $cookiesFavourite)) !== false) {
                    unset($cookiesFavourite[$key]);
                }
                $cookies->add(new Cookie([
                    'name' => 'favourite',
                    'value' => $cookiesFavourite,
                    'expire' => time() + 365 * 24 * 60 * 60
                ]));

            } else {
                $user = Yii::$app->user->identity;
                if (!$user->social_users_favourite) {
                    $user->social_users_favourite = serialize($cookiesFavourite);
                } else {
                    $newArray = unserialize($user->social_users_favourite);
                    if ($cookiesFavourite) {
                        foreach ($cookiesFavourite as $ids) {
                            $newArray[] = $ids;
                        }
                    }

                    $newArray = array_unique($newArray);

                    if ($isAdded == 1) {

                        if (!in_array($id, $newArray)) {
                            $newArray[] = $id;
                            $newArray = array_unique($newArray);
                            $user->social_users_favourite = serialize($newArray);
                        } else {
                            $user->social_users_favourite = serialize($newArray);
                        }
                    } elseif (($key = array_search($id, unserialize($user->social_users_favourite))) !== false) {
                        unset($newArray[$key]);
                        $user->social_users_favourite = serialize($newArray);
                    }
                    if ($cookies) {
                        $cookies->remove('favourite');
                    }
                }
                $user->save(false);
            }


            return json_encode(['isAdded' => $isAdded]);

        }
        return false;
    }

    public function actionChangeCurrency($currentCurrency)
    {
        if(!$currentCurrency)     throw new NotFoundHttpException;
        $lang = Yii::$app->language;
        $currencys = Rate::find()->where(["name_$lang" => strip_tags($currentCurrency)])->asArray()->one();
        if(!$currencys)  throw new NotFoundHttpException;

        $session = Yii::$app->session;
        $object = new \stdClass();
        $object->currency = $currencys["name_$lang"];
        $object->currencySymbol =  $currencys['symbols'];
        $session->set('currency', $object);

        return $this->redirect(Yii::$app->request->referrer);
    }



}
