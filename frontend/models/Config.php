<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "piv_albom".
 *
 * @property int $albom_id
 * @property string $image
 * @property int $image_rank
 * @property int $user_id
 * @property int $product_id
 */
class Config extends \yii\base\Model
{
    public static function tableNameFromCategoryFieldOne($db_name, $values)
    {
        if ($db_name && $values) {
            $table = Yii::$app->db->schema->getTableSchema("{{%$db_name}}");

            if (!is_null($table)) {
                $lang = Yii::$app->language;
                $values  = intval($values);
                $post = Yii::$app->db->createCommand("SELECT id,name_$lang
                              FROM $table->name
                              WHERE id = $values
                               ")
                    ->queryOne();

                return $post;
            }
        }

        return;
    }
}