<?php

namespace frontend\models;

use borales\extensions\phoneInput\PhoneInputValidator;
use common\models\GeoAdmin1codesascii;
use common\models\GeoCity;
use common\models\Product;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%product_address}}".
 *
 * @property int $id
 * @property int $product_id
 * @property string $address
 * @property int $city_id
 * @property string $number_one
 * @property string $number_two
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Products $product
 */
class ProductAddress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%product_address}}';
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'city_id'], 'required'],
            [['product_id', 'city_id', 'created_at', 'updated_at'], 'integer'],
            [['address', 'number_one', 'number_two'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['number_one'], PhoneInputValidator::className()],
            [['number_two'], PhoneInputValidator::className()],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => GeoCity::className(), 'targetAttribute' => ['city_id' => 'geonameid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => Yii::t('app','Product ID'),
            'address' =>  Yii::t('app','Address'),
            'city_id' => Yii::t('app','City Name'),
            'number_one' => Yii::t('app','Number One'),
            'number_two' => Yii::t('app','Number Two'),
            'created_at' =>  Yii::t('app','Created At'),
            'updated_at' => Yii::t('app','Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
