<?php
namespace frontend\models;

use common\models\Geo;
use common\models\GeoAdmin1codesascii;
use common\models\GeoCity;
use common\models\GeoCountry;
use yii\base\Model;
use common\models\User;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;
    public $password;
    public $first_name;
    public $last_name;
    public $birth_day;
    public $adresse;
    public $city_id;
    public $phone;
    public $repeat_password;
    public $social_users_favourite;


    const SEX_MALE =  0;
    const SEX_FEMALE = 1;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name','last_name','birth_day','adresse','city_id','phone'], 'required','message' => Yii::t('app','{attribute} cannot be blank.')],
            [['social_users_favourite'],'string'],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app','This email address has already been taken.')],

            [['first_name', 'last_name', 'adresse'], 'string', 'max' => 255,'min' => 5, "tooShort" => yii::t('app', '{attribute} should contain at least {min} characters.'), "tooLong" => yii::t('app', '{attribute} should contain at most {max} characters.')],


            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['repeat_password', 'required'],
            ['repeat_password', 'string', 'min' => 6],
            ['repeat_password', 'compare', 'compareAttribute' => 'password','message' => Yii::t('app','Repeat Password must be equal to "New Password".')],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => GeoCity::className(), 'targetAttribute' => ['city_id' => 'geonameid']],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->email = $this->email;
        $user->social_users_favourite = '';
        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->birth_day = date('Y-m-d',strtotime($this->birth_day));
        $user->adresse = $this->adresse;
        $user->city_id = $this->city_id;
        $user->phone = $this->phone;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        return $user->save() ? $user : null;
    }

    public static function getSexList()
    {
        return [
            self::SEX_MALE  => Yii::t('app','Male'),
            self::SEX_FEMALE =>  Yii::t('app','Female')
        ];
    }

}
