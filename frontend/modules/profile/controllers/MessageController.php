<?php

namespace frontend\modules\profile\controllers;
use common\models\Contact;
use common\models\Report;
use Yii;

/**
 * MessageController implements the CRUD actions for Product model.
 */
class MessageController extends BaseController
{
    public function actionIndex()
    {
        $adminContact = Contact::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
        $reportContact = Report::find()->where(['user_id' => Yii::$app->user->identity->id])->all();

        return $this->render('index',[
            'adminContact' => $adminContact,
            'reportContact' => $reportContact,
        ]);
    }

}
