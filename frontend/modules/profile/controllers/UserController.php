<?php

namespace frontend\modules\profile\controllers;

use common\models\User;
use common\models\UserFollow;
use frontend\modules\profile\models\PasswordForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use Exception;

/**
 * UserController implements the CRUD actions for User model.
 */

class UserController  extends BaseController
{
    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate()
    {
        $id = \Yii::$app->user->identity->id;
        $model = $this->findModel($id);

        $oldPassword = $model->password_hash;
        $oldImage = $model->image;
        $model->password_hash = null;

        if ($model->load(Yii::$app->request->post())) {

            $model->birth_day = date('Y-m-d',strtotime($model->birth_day));

            if(!$model->password_hash){
                $model->password_hash = $oldPassword;
            }else{
                $model->setPassword($model->password_hash);
            }

            if(!$model->image){
                $model->image = $oldImage;
            }

            unset($model->social_users_favourite);

            if($model->validate()  && $model->save()){
                $model->image = UploadedFile::getInstance($model, 'image');
                if($model->image) {
                    $model->image = Yii::$app->uploadAvatar->uploadImageAvatar($model->image);
                    $model->save(false);
                }

                Yii::$app->session->setFlash('success',Yii::t('app', 'My user information updated successfully'));

                return $this->redirect(['update']);
            }

            return $this->redirect(['update']);
        }

        return $this->render('_form',[
            'model' => $model
        ]);
    }


    public function actionChangePassword()
    {

        $model = new PasswordForm;

        $modeluser = \Yii::$app->user->identity;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                try {
                    $modeluser->password_hash = Yii::$app->security->generatePasswordHash($_POST['PasswordForm']['newpass']);
                    unset($modeluser->social_users_favourite);
                    if ($modeluser->save()) {
                        Yii::$app->session->setFlash('success',Yii::t('app', 'Password changed successfully'));
                        return $this->redirect(['change-password']);
                    } else {
                        Yii::$app->getSession()->setFlash(
                            'error', 'Password not changed'
                        );
                        return $this->redirect(['change-password']);
                    }
                } catch (Exception $e) {
                    Yii::$app->getSession()->setFlash(
                        'error', "{$e->getMessage()}"
                    );
                    return $this->render('change-password', [
                        'model' => $model
                    ]);
                }
            } else {
                return $this->render('change-password', [
                    'model' => $model
                ]);
            }
        } else {
            return $this->render('change-password', [
                'model' => $model
            ]);
        }
    }

    public function actionFollow()
    {
        $query = UserFollow::find()->where(['sender_id'=> Yii::$app->user->identity->id,'follow' => UserFollow::followYes ]);

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize'=>10]);
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('follow', [
            'models' => $models,
            'pages' => $pages,
        ]);
    }

    public function actionFollowDelete()
    {
        $receiver_id = Yii::$app->request->post('id');
        if(!$receiver_id) return false;

        $query = UserFollow::find()->where(['sender_id'=> Yii::$app->user->identity->id,'receiver_id' => $receiver_id])->one();
        if($query && $query->delete()){
            return json_encode(['success' => 'deleted']);
        }
        return false;
    }


    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
