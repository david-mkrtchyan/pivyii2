<?php
namespace frontend\modules\profile\models;

use Yii;
use yii\base\Model;
use common\models\User;

class PasswordForm extends Model
{
    public $oldpass;
    public $newpass;
    public $repeatnewpass;

    public function rules()
    {
        return [
            [['oldpass', 'newpass', 'repeatnewpass'], 'required','message' => Yii::t('app','{attribute} cannot be blank.')],
            ['oldpass', 'findPasswords'],
            [['newpass'],'string','min' => 6, ],
            ['repeatnewpass', 'compare', 'compareAttribute' => 'newpass','message' => Yii::t('app','Repeat Password must be equal to "New Password".')],

        ];
    }

    public function findPasswords($attribute, $params)
    {
        $user = User::find()->where([
            'id' => Yii::$app->user->identity->id
        ])->one();
        $password = $user->password_hash;
        if (!Yii::$app->security->validatePassword($this->oldpass, $password))
            $this->addError($attribute, yii::t('app','Old password is wrong'));
    }

    public function attributeLabels()
    {
        return [
            'oldpass' => yii::t('app','Old Password'),
            'newpass' => yii::t('app','New Password'),
            'repeatnewpass' => yii::t('app','Repeat Password'),
        ];
    }
}