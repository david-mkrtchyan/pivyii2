<?php

use common\models\CategoryField;
use common\models\ProductField;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


$productField = ProductField::findOne(['product_id' => $model->id, 'category_field_id' => $categoryField->id]);
if ($productField) {
    $fieldModel->allCategoryFields[$categoryField->id] = $productField->value;
}


$class = $categoryField->required ? "required" : false;
$for = str_replace(' ', '_', $categoryField->name);
echo '<div class="form-group ' . $class . '">
        <label class="control-label" for="form-'.$for. '" >' . Yii::t('app',$categoryField->name) . '</label>';
$values = ArrayHelper::getValue($fieldModel['allCategoryFields'], $categoryField->id);
$lang = Yii::$app->language;

if($categoryField->use_database && $categoryField->db_name){
    $table = Yii::$app->db->schema->getTableSchema("{{%$categoryField->db_name}}");

    if(!is_null($table)){

        if($categoryField->db_name_type && isset($table->columns['type'])){
            $post = Yii::$app->db->createCommand("SELECT id,name_$lang FROM $table->name WHERE type='$categoryField->db_name_type'")
                ->queryAll();
        }else{
            $post = Yii::$app->db->createCommand("SELECT id,name_$lang FROM $table->name")
                ->queryAll();
        }


        if($categoryField->db_name == 'models'){
            $post = [];
            $values  = intval($values);
            if($values){
                $post = Yii::$app->db->createCommand("SELECT id,name_$lang
                              FROM $table->name
                              WHERE brend_id IN (SELECT bs.brend_id FROM $table->name bs WHERE bs.id = $values )
                               ")
                  ->queryAll();
                $post = $post ? $post : [];
            }
        }

        if($categoryField->allow_multiple == CategoryField::multipleYes){
            $categoryFieldValueUseDb = $model->getCategoryFieldsValue($categoryField->id,$table->name);
            echo Html::checkboxList("ProductField[allCategoryFields][$categoryField->id]", $categoryFieldValueUseDb,  ArrayHelper::map($post,'id','name_'.$lang)
                , [
                    'class' => "form-control",
                    'required' => $class,
                    'id' => "form-{$for}",
                ]);
        }else{
            echo Html::dropDownList("ProductField[allCategoryFields][$categoryField->id]", $values, ArrayHelper::map($post,'id','name_'.$lang)
                , [
                    'multiple' => false,
                    'class' => 'form-control',
                    'required' => $class,
                    'id' => "form-{$for}",
                ]);
        }


    }

}else{
    echo Html::input('text', "ProductField[allCategoryFields][$categoryField->id]", $values, [
        'maxlength' => true,
        'class' => "form-control",
        'required' => $class,
        'id' => "form-{$for}",
    ]);
}


echo "<span class=\"glyphicon form-control-feedback\" aria-hidden=\"true\"></span>
                <div class=\"help-block with-errors\"></div>";
echo '</div>';