<div class=" user-add-product">

    <div class="left-sidebar ibox float-e-margins">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-title">
                    <h5><?= Yii::t('app', 'Create ad') ?></h5>
                </div>
                <ul class="catalog category-products ibox-content">
                    <?= \backend\components\MenuWidget::widget(['tpl' => 'menu']); ?>
                </ul>
            </div>
        </div>
    </div>
</div>