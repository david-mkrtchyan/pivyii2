<?php

/* @var $this \yii\web\View */

/* @var $content string */

use frontend\widgets\AlertWidget;
use frontend\widgets\AsideWidget;
use frontend\widgets\HeaderWidget;
use frontend\widgets\FooterWidget;
use frontend\widgets\ProfileAsideWidget;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\ProfileAsset;

ProfileAsset::register($this);
$app = Yii::$app;
$request = $app->request;
$baseUrl = $request->baseUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="container parent-container">
    <!-- Header -->
    <?= HeaderWidget::widget(); ?>
    <div id="content" class="clearfix">
        <?= Breadcrumbs::widget([
            'homeLink' => ['label' => Yii::t('app','Home'), 'url' => '/'],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= AlertWidget::widget() ?>

        <div class="col-md-3 sidenav">
            <?= ProfileAsideWidget::widget() ?>
        </div>

        <div class="col-md-9 text-left">
            <div class="page-profile">
                <?= $content ?>
            </div>
        </div>

    </div>

    <!-- Footer -->
    <?= FooterWidget::widget(); ?>
</div>

<!-- Login modal -->
<?php //frontend\widgets\LoginModalWidget::widget(); ?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
