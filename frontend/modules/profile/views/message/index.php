<?php

use common\models\Config;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $reportContact common\models\Report */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Messages');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="messages-content">
    <h1><?= Yii::t('app','My Messages')?></h1>
    <ul class="nav nav-tabs">
        <li class="active">
            <a data-toggle="tab" href="#report_mess">
                <?= Yii::t('app', 'Report Messages') ?>
            </a>
        </li>

        <li>
            <a data-toggle="tab" href="#admin_mess">
                <?= Yii::t('app', 'Admin Messages') ?>
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="report_mess" class="tab-pane fade in active">
            <table id="table_report" class="display">
                <thead>
                <tr>
                    <th><?= Yii::t('app', 'Product') ?></th>
                    <th><?= Yii::t('app', 'Report Type') ?></th>
                    <th><?= Yii::t('app', 'Email') ?></th>
                    <th><?= Yii::t('app', 'Review') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php if ($reportContact): ?>
                    <?php foreach ($reportContact as $value): ?>
                        <tr>
                            <td><a target="_blank"
                                   href="<?= Url::to(['/category/view/', 'id' => $value->product->id]) ?>"><?= Config::replaceTitle($value->product->title, 70) ?></a>
                            </td>
                            <td><?= ArrayHelper::getValue($value, 'reportCategory.name_' . Yii::$app->language) ?></td>
                            <td><?= $value->email ?></td>
                            <td><?= $value->review ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>

        <div id="admin_mess" class="tab-pane">
            <table id="table_admin" class="display">
                <thead>
                <tr>

                    <th><?= Yii::t('app', 'Email') ?></th>
                    <th><?= Yii::t('app', 'Name') ?></th>
                    <th><?= Yii::t('app', 'Title') ?></th>
                    <th><?= Yii::t('app', 'Message') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php if ($adminContact): ?>
                    <?php foreach ($adminContact as $value): ?>
                        <tr>

                            <td><?= $value->email ?></td>
                            <td><?= $value->name ?></td>
                            <td><?= $value->title ?></td>
                            <td><?= $value->message ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
$this->registerJs('
$(document).ready(function() {
    if( $("#table_report").length){
        $("#table_report").dataTable({

            "oLanguage": {
                "oPaginate": {
                    "sFirst": "'.Yii::t('app','First').'", // This is the link to the first page
                    "sPrevious": "'.Yii::t('app','Previous').'", // This is the link to the previous page
                    "sNext": "'.Yii::t('app','Next').'", // This is the link to the next page
                    "sLast": "'.Yii::t('app','Last').'" // This is the link to the last page
                },
                "sLengthMenu": "'.Yii::t('app','Show _MENU_ entries').'",
                "sSearch": "'.Yii::t('app','Search').'",
                "sInfo": "'.Yii::t('app','Displaying _START_ to _END_ of _TOTAL_ records').'",
                "sEmptyTable": "'.Yii::t('app','No data available in table').'",
                "sInfoEmpty": "'.Yii::t('app','Showing 0 to 0 of 0 records').'",
            }
        });
    }

    if( $("#table_admin").length){
        $("#table_admin").dataTable({
        "oLanguage": {
                "oPaginate": {
                    "sFirst": "'.Yii::t('app','First').'", // This is the link to the first page
                    "sPrevious": "'.Yii::t('app','Previous').'", // This is the link to the previous page
                    "sNext": "'.Yii::t('app','Next').'", // This is the link to the next page
                    "sLast": "'.Yii::t('app','Last').'" // This is the link to the last page
                },
                "sLengthMenu": "'.Yii::t('app','Show _MENU_ entries').'",
                "sSearch": "'.Yii::t('app','Search').'",
                "sInfo": "'.Yii::t('app','Displaying _START_ to _END_ of _TOTAL_ records').'",
                "sEmptyTable": "'.Yii::t('app','No data available in table').'",
                "sInfoEmpty": "'.Yii::t('app','Showing 0 to 0 of 0 records').'",
            }
        });
    }
})
')
?>




