    <?php

    use yii\helpers\Html;



    /* @var $this yii\web\View */
    /* @var $model common\models\Product */
    /* @var $shops array */
    /* @var $categories array */
    /* @var $brands array */

    $this->title = Yii::t('app','Create Ads');
    $this->params['breadcrumbs'][] = ['label' =>  Yii::t('app','My Ads'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
    ?>
    <div class="product-create">

        <?= $this->render('_form', [
            'allErrors' => $allErrors,
            'model' => $model,
            'fieldModel' => $fieldModel,
            'imageAlbom' => $imageAlbom,
            'productAddress' => $productAddress,
        ]) ?>

    </div>
