<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $categories array */
/* @var $shops array */
/* @var $brands array */

$this->title =  Yii::t('app','My Ads');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <ul class="nav nav-tabs">
        <li class="active">
            <a data-toggle="tab" href="#name_lt">
               <?= Yii::t('app','My Ads')." (".$dataProvider->getCount().")"?>
            </a>
        </li>

        <li>
            <a data-toggle="tab" href="#name_en">
                <?= Yii::t('app','Only active ads')." (".$dataProviderActive->getCount().")"?>
            </a>
        </li>
        <li>
            <a data-toggle="tab" href="#name_ru">
                <?= Yii::t('app','Only inactive ads')." (".$dataProviderPassive->getCount().")"?>
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="name_lt" class="tab-pane fade in active">
            <div id="catlist">
                <?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => 'product_list',
                    'viewParams' => [
                        'fullView' => true,
                        'context' => 'main-page',
                    ],
                    'layout' => "<div class='item-all'>{items}</div>\n<div class='link-page'>{pager}</div>",
                    'emptyText' => Yii::t('app','No results found.')
                ]);
                ?>
            </div>
        </div>
        <div id="name_en" class="tab-pane fade">
            <div id="catlist">
                <?= ListView::widget([
                    'dataProvider' => $dataProviderActive,
                    'itemView' => 'product_list',
                    'viewParams' => [
                        'fullView' => true,
                        'context' => 'main-page',
                    ],
                    'layout' => "<div class='item-all'>{items}</div>\n<div class='link-page'>{pager}</div>",
                    'emptyText' => Yii::t('app','No results found.')
                ]);
                ?>
            </div>
        </div>
        <div id="name_ru" class="tab-pane fade">
            <div id="catlist">
                <?= ListView::widget([
                    'dataProvider' => $dataProviderPassive,
                    'itemView' => 'product_list',
                    'viewParams' => [
                        'fullView' => true,
                        'context' => 'main-page',
                    ],
                    'layout' => "<div class='item-all'>{items}</div>\n<div class='link-page'>{pager}</div>",
                    'emptyText' => Yii::t('app','No results found.')
                ]);
                ?>
            </div>
        </div>
    </div>



</div>
