<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $categories array */
/* @var $shops array */
/* @var $brands array */

$this->title = yii::t('app', 'Favorites');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <div>
        <div id="catlist">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => 'product_list',
                'viewParams' => [
                    'fullView' => true,
                    'context' => 'main-page',
                ],
                'layout' => "<div class='item-all'>{items}</div>\n<div class='link-page'>{pager}</div>",
                'emptyText' => Yii::t('app','No results found.')
            ]);
            ?>
        </div>
    </div>


</div>
