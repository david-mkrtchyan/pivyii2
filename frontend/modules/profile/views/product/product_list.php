<?php

use common\models\Config;
use kartik\field\FieldRange;
use kartik\file\FileInput;
use kartik\growl\Growl;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */

?>


<dl>
    <dt>
        <img src="<?= $model->productFirstImage ? $model->productFirstImage : '/images/nopicture.png'; ?>">
        <strong>
            <?= ArrayHelper::getValue($model, 'price') . ' ' . ArrayHelper::getValue($model, 'rate.name_'.Yii::$app->language) ?>
        </strong>
        <span class="title"> <a target="_blank" href="<?= Url::to(['/category/view','id' =>$model->id ]) ?>"> <?= Config::replaceTitle($model->title, '70') ?></a></span>
    </dt>
    <dd>
        <p class="profile-description">
            <?= Config::replaceTitle($model->description, '200') ?>
        </p>
    </dd>

    <?php if (Yii::$app->controller->action->id !== "preferred"): ?>
        <dd class="action">

            <a href="<?= Url::to(['product/view', 'id' => $model->id]) ?> " class="btn btn-info" title="View"
               aria-label="View">
                <?= Yii::t('app','View') ?>
            </a>
            <a href="<?= Url::to(['product/update', 'id' => $model->id]) ?>" class="btn btn-primary" title="Update"
               aria-label="Update">
                <?= Yii::t('app', 'Update') ?>
            </a>
            <a href="<?= Url::to(['product/delete', 'id' => $model->id]) ?>" class="btn btn-danger" title="Delete"
               aria-label="Delete" data-confirm=" <?= Yii::t('app', 'Are you sure you want to delete this item?') ?>"
               data-method="post">
                <?= Yii::t('app','Delete') ?>
            </a>
        </dd>
    <?php endif; ?>


    <?php if (Yii::$app->controller->action->id == "preferred"):
        $cookiesFavourite = Yii::$app->request->cookies->getValue('favourite', []);
        $favorite = intval(in_array($model->id, $cookiesFavourite));
        $currency = Yii::$app->session->get('currency');

        if(!Yii::$app->user->isGuest) {
            $user = yii::$app->user->identity;
            if ($user->social_users_favourite) {
                $favorite = intval(in_array($model->id, unserialize($user->social_users_favourite)));
            }
        }

        ?>
        <dd class="action">
            <a href="#" class="btn btn-danger favourite favourite-profile" title="Delete"  data-id="<?= $model->id; ?>" data-favourite="<?= $favorite ?>">
                <?= Yii::t('app', 'Delete') ?>
            </a>
        </dd>

        <?php
        if(yii::$app->controller->action->id == 'preferred'){
            $this->registerJs('
                $(".favourite-profile").each(function(){
                    $(this).click(function(){
                            $(this).closest("div").remove(); 
                    });
  
                })
            ');
        }
        ?>

    <?php endif; ?>
</dl>
