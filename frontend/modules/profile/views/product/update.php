<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $shops array */
/* @var $categories array */
/* @var $brands array */

$this->title = Yii::t('app','Update My Ads').': ' . $model->id;
$this->params['breadcrumbs'][] = ['label' =>  Yii::t('app','My Ads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-update">


    <?= $this->render('_form', [
        'allErrors' => $allErrors,
        'model' => $model,
        'fieldModel' => $fieldModel,
        'imageAlbom' => $imageAlbom,
        'imageCount' => $imageCount,
        'allImage' => $allImage,
        'allImageId' => $allImageId,
        'productAddress' => $productAddress,
    ]) ?>

</div>
