<?php

use common\models\Geo;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $dataProvider common\models\ProductField */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','My Ads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            [
                'label' => 'Title',
                'value' => $model->title
            ],

            [
                'label' => 'Description',
                'value' => $model->description
            ],
            [
                'label' => 'Price',
                'value' => ArrayHelper::getValue($model,'price'). ' '.ArrayHelper::getValue($model,'rate.name_'.Yii::$app->language)
            ],
            [
                'label' => 'Category',
                'value' => ArrayHelper::getValue($model->category,'name_'.Yii::$app->language),
            ],

            [
                'label' => 'Address',
                'value' => ArrayHelper::getValue($model,'productAddress.address')
            ],

            [
                'label' => 'Phones',
                'value' => ArrayHelper::getValue($model,'productAddress.number_one').' '.ArrayHelper::getValue($model,'productAddress.number_two') ,
            ],

            [
                'label' => 'City Name',
                'value' => Geo::getFullName( ArrayHelper::getValue($model,'productAddress.city_id'), 'en'),
            ],

            [
                'label' => 'created_at',
                'value' => date('Y-m-d',$model->created_at),
            ],

            [
                'label' => 'updated_at',
                'value' =>date('Y-m-d',$model->updated_at),
            ],
        ],
    ]) ?>


</div>