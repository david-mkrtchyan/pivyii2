<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = yii::t('app','Change Password');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pass'] = yii::t('app','Change Password');
?>
<h1 class="panel-title-user-change"><?php echo Html::encode($this->title) ?></h1>
<hr/>
<div class="row">
    <div class="col-md-5">


        <?php $form = ActiveForm::begin([
            'id' => 'change-password-form',
            'options' => ['class' => 'form-horizontal'],
            /* 'fieldConfig'=>[
                  'template'=>"{label}\n<div class=\"col-lg-12\">
                              {input}</div>\n<div class=\"col-lg-12\">
                              {error}</div>",
                  'labelOptions'=>['class'=>'col-lg-12 control-label'],
              ], */
        ]); ?>
        <?= $form->field($model, 'oldpass', ['inputOptions' => [
            'placeholder' =>  yii::t('app','Old Password')
        ]])->passwordInput() ?>

        <?= $form->field($model, 'newpass', ['inputOptions' => [
            'placeholder' => yii::t('app', 'New Password')
        ]])->passwordInput() ?>

        <?= $form->field($model, 'repeatnewpass', ['inputOptions' => [
            'placeholder' =>  yii::t('app','Repeat Password')
        ]])->passwordInput() ?>

        <div class="form-group">

            <?= Html::submitButton( yii::t('app','Change Password'), [
                'class' => 'btn btn-primary'
            ]) ?>

        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
