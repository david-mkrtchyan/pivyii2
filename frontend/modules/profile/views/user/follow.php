<?php


use common\models\Config;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = Yii::t('app','Sellers I follow');
$this->params['breadcrumbs'][] = $this->title;
?>

    <h1><?= Html::encode($this->title) ?></h1>
<table class="table table-hover">
    <tbody>
    <?php foreach ($models as $value): $user =  $value->receiver?>
        <tr>
            <td width="45px">
                <div style="overflow: hidden;" class="img-circle svg-icon-40 svg-dbaplus-avatar-male">
                    <?php if ($user->image): ?>
                        <?php
                        echo Config::getAvatarPhoto($user->image, 48, 'img-circle');
                    else: ?>
                        <img alt="image" class="img-circle"
                             src="<?= Yii::$app->request->baseUrl ?>/images/default_avatar.png"/>
                    <?php endif; ?>
                </div>
            </td>
            <td>
                <a href="<?= Url::toRoute(['/user/profile','id' => $value->receiver_id ])?>"
                   class="trackClicks"><?= $user->first_name.' '.ucfirst(substr($user->last_name, 0, 1)) ?> </a> <br>
                    <?= $user->adresse  ?>
            </td>
            <td width="20px">
                <a href="<?= Url::toRoute(['/profile/user/follow-delete/'])?>" data-id="<?= $value->receiver_id ?>"
                   title="<?= Yii::t('app','Delete Follower')?>" class="unfollow">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
    <?php
    endforeach; ?>
    </tbody>
    <tfoot></tfoot>
</table>

<?= LinkPager::widget([
    'pagination' => $pages,
]); ?>

<?php $this->registerJs(
        ' $(\'.unfollow\').click(function (e) {
        event.preventDefault();
        var e = $(this);
        var id = e.attr(\'data-id\');
   
        var href = e.attr(\'href\');
    
        $.ajax({
            url:  href,
            type: \'post\',
            data: {
                id: id
            },
            dataType: \'json\',
            success: function (data) {
                if (data.success == "deleted") {
                    e.closest("tr").remove();
                }
            }
        });
    });'
);?>