<?php

use common\models\CategoryField;
use common\models\ProductField;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

if(!$categoryField->allow_filter) return;

$productField = ProductField::findOne(['product_id' => $model->id, 'category_field_id' => $categoryField->id]);

$values = null;
$queryParams = json_decode($queryParams);
$valID = $categoryField->id;
if (isset( $queryParams->ProductField) &&
    isset($queryParams->ProductField->allCategoryFields) &&
    property_exists($queryParams->ProductField->allCategoryFields, $valID)) {
    $values = $queryParams->ProductField->allCategoryFields->$valID;
}

$dataRelationDb = Yii::$app->session->get('dataRelationDb');


$for = str_replace(' ', '_', $categoryField->name);
$lang = Yii::$app->language;

if($categoryField->use_database && $categoryField->db_name){

    $table = Yii::$app->db->schema->getTableSchema("{{%$categoryField->db_name}}");


    if(!is_null($table)){

        if($categoryField->db_name_type && isset($table->columns['type'])){
            $post = Yii::$app->db->createCommand("SELECT id,name_$lang FROM $table->name WHERE type='$categoryField->db_name_type'")
                ->queryAll();
        }else{
            $post = Yii::$app->db->createCommand("SELECT id,name_$lang FROM $table->name")
                ->queryAll();
        }

        if($dataRelationDb){
            if(isset($dataRelationDb[$table->name])){
                $relationData = $dataRelationDb[$table->name];
                $TABLE_NAME  = ArrayHelper::getValue($relationData,'TABLE_NAME');
                $COLUMN_NAME  = ArrayHelper::getValue($relationData,'COLUMN_NAME');
                $REFERENCED_TABLE_NAME  = ArrayHelper::getValue($relationData,'REFERENCED_TABLE_NAME');
                $VALUE  = ArrayHelper::getValue($relationData,'value');

                $VALUE  = intval($VALUE);
                if($VALUE){
                    $post = Yii::$app->db->createCommand("SELECT id,name_$lang
                              FROM $TABLE_NAME
                              WHERE $COLUMN_NAME IN (SELECT bs.id FROM $REFERENCED_TABLE_NAME bs WHERE bs.id = $VALUE )
                               ")
                        ->queryAll();
                }

                $post = $post ? $post : [];
            }
        }

        $relation = \common\models\Config::getAllRelation($table->name,$values);


        if($relation){
            if($dataRelationDb){
                $dataRelationDb =   array_merge($dataRelationDb,$relation);
                Yii::$app->session['dataRelationDb'] = $dataRelationDb;
            }else{
                $dataRelationDb = $relation;
                Yii::$app->session['dataRelationDb'] = $relation;
            }
        }

        if($categoryField->allow_multiple == CategoryField::multipleYes){

            $values = null;
            $valID = $categoryField->id;
            if (isset( $queryParams->ProductField) &&
                isset($queryParams->ProductField->allCategoryFields) &&
                property_exists($queryParams->ProductField->allCategoryFields, $valID)) {
                $values = $queryParams->ProductField->allCategoryFields->$valID;
            }

            echo '<div class="form-group ">
        <label class="control-label" for="form-'.$for. '" >' . Yii::t('app',$categoryField->name). '</label>';
            echo Html::checkboxList("ProductField[allCategoryFields][$categoryField->id]", $values,  ArrayHelper::map($post,'id','name_'.$lang)
                , [
                    'class' => "form-control",
                    'id' => "form-{$for}",
                ]);
            echo "<span class=\"glyphicon form-control-feedback\" aria-hidden=\"true\"></span>
                <div class=\"help-block with-errors\"></div>";
            echo '</div>';

        }else{
          echo '<div class="form-group">
          <label class="control-label" for="form-'.$for. '" >' . Yii::t('app',$categoryField->name) . '</label>';
            echo Html::dropDownList("ProductField[allCategoryFields][$categoryField->id]", $values, ArrayHelper::map($post,'id','name_'.$lang)
                , [
                    'multiple' => false,
                    'prompt' => Yii::t('app',"Select $categoryField->name"),
                    'class' => 'form-control',
                    'id' => "form-{$for}",
                ]);
            echo '</div>';
        }


    }

}else{

    $values1 = $values2 = null;

    if ( isset( $queryParams->ProductField) &&
        isset($queryParams->ProductField->allCategoryFields) &&
        property_exists($queryParams->ProductField->allCategoryFields, $valID)) {
        $allCategoryFields = $queryParams->ProductField->allCategoryFields;

        if ( property_exists($allCategoryFields->$valID, 'min') &&
            property_exists($allCategoryFields->$valID, 'max')
        ) {
            $values1 = $allCategoryFields->$valID->min;
            $values2 = $allCategoryFields->$valID->max;
        }
    }

    $for = str_replace(' ', '_', $categoryField->name);
    echo '<div class="form-group">
        <label class="control-label" for="form-'.$for. '" >' . Yii::t('app',$categoryField->name) . '</label>';
    echo '<div class="row"><div class="col-md-6">';
    echo Html::input('text', "ProductField[allCategoryFields][$categoryField->id][min]", $values1, [
        'maxlength' => true,
        'placeholder' => Yii::t('app','From'),
        'class' => "form-control",
        'id' => "form-{$for}",
    ]);
    echo '</div><div class="col-md-6">';

    echo Html::input('text', "ProductField[allCategoryFields][$categoryField->id][max]", $values2, [
        'maxlength' => true,
        'placeholder' => Yii::t('app','To'),
        'class' => "form-control",
        'id' => "form-{$for}",
    ]);

    echo '</div></div></div>';

}

