<?php

use common\models\ProductField;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
if(!$categoryField->allow_filter) return;

$productFields = ProductField::findAll(['product_id' => $model->id, 'category_field_id' => $categoryField->id]);
$fieldValues = ArrayHelper::map($categoryField->categoryFieldValues, 'id', 'value_' . Yii::$app->language);

$values = null;
$queryParams = json_decode($queryParams);
$valID = $categoryField->id;
if (isset( $queryParams->ProductField) &&
    isset($queryParams->ProductField->allCategoryFields) &&
    property_exists($queryParams->ProductField->allCategoryFields, $valID)) {
    $values = $queryParams->ProductField->allCategoryFields->$valID;
}

$for = str_replace(' ', '_', $categoryField->name);
echo '<div class="form-group">
        <label class="control-label" for="form-'.$for. '" >' . Yii::t('app',$categoryField->name) . '</label>';


echo Html::radioList("ProductField[allCategoryFields][$categoryField->id]", $values, $fieldValues
    , [
        'class' => "form-control",
        'id' => "form-{$for}",
    ]);

echo "<span class=\"glyphicon form-control-feedback\" aria-hidden=\"true\"></span>
                <div class=\"help-block with-errors\"></div>";
echo '</div>';