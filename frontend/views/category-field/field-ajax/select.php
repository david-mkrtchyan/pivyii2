<?php

use common\models\ProductField;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
if(!$categoryField->allow_filter) return;
$productFields = ProductField::findAll(['product_id' => $model->id, 'category_field_id' => $categoryField->id]);

if ($categoryField->allow_range) {
    $rangeInterval = [];
    $range = range($categoryField->range_min, $categoryField->range_max);
    $fieldValues = array_combine($range, $range);


    $values1 = $values2 = null;
    $queryParams = json_decode($queryParams);
    $valID = $categoryField->id;

    if (isset( $queryParams->ProductField) &&
        isset($queryParams->ProductField->allCategoryFields) &&
        property_exists($queryParams->ProductField->allCategoryFields, $valID)) {
        $allCategoryFields = $queryParams->ProductField->allCategoryFields;

        if ( property_exists($allCategoryFields->$valID, 'min') &&
             property_exists($allCategoryFields->$valID, 'max')
        ) {
            $values1 = $allCategoryFields->$valID->min;
            $values2 = $allCategoryFields->$valID->max;
        }
    }

    $for = str_replace(' ', '_', $categoryField->name);
    echo '<div class="form-group "><label class="control-label"  for="form-' . $for . '" >' .  Yii::t('app',$categoryField->name)  . '</label>';
    echo '<div class="row"><div class="col-md-6">';
    echo Html::dropDownList("ProductField[allCategoryFields][$categoryField->id][min]", $values1, $fieldValues
        , [
            'multiple' => $categoryField->allow_multiple ? true : false,
            'class' => 'form-control',
            'id' => "form-{$for}",
            'prompt' => Yii::t('app', "From"),
            'options' => [
                null => ['disabled' => true],
            ],
        ]);
    echo '</div><div class="col-md-6">';

    echo Html::dropDownList("ProductField[allCategoryFields][$categoryField->id][max]", $values2, $fieldValues
        , [
            'multiple' => $categoryField->allow_multiple ? true : false,
            'class' => 'form-control',
            'id' => "form-{$for}",
            'prompt' => Yii::t('app', "To"),
            'options' => [
                null => ['disabled' => true],
            ],
        ]);
    echo '</div></div></div>';


} else {
    $fieldValues = ArrayHelper::map($categoryField->categoryFieldValues, 'id', 'value_' . Yii::$app->language);

    $values = null;
    $queryParams = json_decode($queryParams);
    $valID = $categoryField->id;
    if (isset( $queryParams->ProductField) &&
        isset($queryParams->ProductField->allCategoryFields) &&
        property_exists($queryParams->ProductField->allCategoryFields, $valID)) {
        $values = $queryParams->ProductField->allCategoryFields->$valID;
    }



    $for = str_replace(' ', '_', $categoryField->name);
    echo '<div class="form-group"><label class="control-label"  for="form-' . $for . '" >' . Yii::t('app',$categoryField->name) . '</label>';
    echo Html::dropDownList("ProductField[allCategoryFields][$categoryField->id]", $values, $fieldValues
        , [
            'multiple' => $categoryField->allow_multiple ? true : false,
            'prompt' => Yii::t('app', "Select $categoryField->name"),
            'class' => 'form-control',
            'id' => "form-{$for}",
        ]);
    echo '</div>';

}
