<?php

use kartik\field\FieldRange;
use kartik\file\FileInput;
use kartik\growl\Growl;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */

$cookiesFavourite = Yii::$app->request->cookies->getValue('favourite', []);
$favorite = intval(in_array($model->id, $cookiesFavourite));
$currency = Yii::$app->session->get('currency');

if(!Yii::$app->user->isGuest) {
    $user = yii::$app->user->identity;
    if ($user->social_users_favourite) {
        $favorite = intval(in_array($model->id, unserialize($user->social_users_favourite)));
    }
}

?>

<div class="product-content">
    <div class="col-md-4 col-sm-4 col-xs-6 product-child">
        <div class="product">
            <div class="">
                <a href="<?= Url::to(["category/view",'id' => $model->id ])?>">
                    <img class="img-thumbnail" src="<?= $model->productFirstImage ? $model->productFirstImage : '/images/nopicture.png';  ?>" alt="<?= $model->title ?>">
                </a>
                <?php  if ($model->rate_id && $model->price): ?>
                    <div class="item-price">
                    <span data-bind="text: Price">
                        <?= \common\models\Currency::showCurrency(ArrayHelper::getValue($model,'price'), ArrayHelper::getValue($model,'rate.name_'.Yii::$app->language)) ?>

                    </span>
                    </div>
                <?php endif; ?>
            </div>
            <a href="<?= Url::to(["category/view", 'id' => $model->id ])?>">
                <span class="slide-title"><?= $model->title ?></span>
            </a>
            <div class="favourite"  data-id="<?= $model->id; ?>" data-favourite="<?= $favorite ?>"><i title="<?= yii::t('app','Saved Ads')?>" class="fa fa-star<?= $favorite? '' : '-o'?>"></i></div>
        </div>
    </div>
</div>