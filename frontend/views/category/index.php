<?php

/* @var $this yii\web\View */

use common\models\Config;
use frontend\widgets\CategoryFieldWidget;
use frontend\widgets\GridListContentWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ListView;

$this->title = 'My Yii Application';
$slug = Yii::$app->request->get('slug');

?>

<?php if (!Yii::$app->request->get('ProductField')): ?>
    <?= CategoryFieldWidget::widget(['slug' => $slug]); ?>
<?php endif; ?>

<?= GridListContentWidget::widget(['slug' => $slug]); ?>

<div id="catlist">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => Config::getGridList(),
        'viewParams' => [
            'fullView' => true,
            'context' => 'main-page',
        ],
        'layout' => "<div class='item-all'>{items}</div>\n<div class='link-page'>{pager}</div>",
        "emptyText" => '<div class="col-md-12">' . Yii::t('app', 'No results found.') . '</div>',
    ]); ?>
</div>


