<?php

use common\models\Config;
use common\models\Geo;
use yii\helpers\ArrayHelper;

?>
<style>
    body {
        margin: 0;
    }

    #map {
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        position: absolute !important;
    }

    .infowindow-content {
        padding-right: 11px;
    }

    .infowindow-content .attributes span {
        font-weight: 500;
        margin-left: 4px;
    }

    .title-infoWindow {
        font-weight: bold;
    }

    .phones {
        padding-top: 10px;
    }

    .infoWindowDescription {
        padding-top: 10px;
    }

    .map_place {
        padding-top: 3px;
    }

    .infowindow-content {
        max-width: 240px;
    }

    .infoWindowImage, .infowindow-content {
        float: left;
    }

    .infowindow-image {
        max-width: 200px !important;
    }

    .infoprice span {
        padding-top: 4px;
        font-weight: bold;
    }
</style>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBKRItjGWcnbrLmsLBn9i-SPPAdiTKyMho"></script>



<div style="width: 100vw; height: 100vh; position: relative;">
    <div id="map"
</div>
</div>

<?php if ($type == 'announcement'): ?>
    <input type="hidden" class="addressLat" value="<?= ArrayHelper::getValue($city, 'name') ?>,Latvia">

    <input type="hidden" class="title"
           value="<?= Config::replaceTitle(ArrayHelper::getValue($product, 'title'), 30) ?>">
    <input type="hidden" class="phone_one" value="<?= ArrayHelper::getValue($product, 'productAddress.number_one') ?>">
    <input type="hidden" class="phone_two" value="<?= ArrayHelper::getValue($product, 'productAddress.number_two') ?>">
    <input type="hidden" class="description"
           value="<?= Config::replaceTitle(ArrayHelper::getValue($product, 'description'), 150) ?>">
    <img class="image"
         src="<?= ArrayHelper::getValue($product, 'productFirstImage') ? $product->productFirstImage : ''; ?>">
    <input type="hidden" class="price" value="<?= ArrayHelper::getValue($product, 'price'); ?>">
    <input type="hidden" class="rate" value="<?= ArrayHelper::getValue($product, 'rate.name_'.Yii::$app->language) ?>">
    <input type="hidden" class="map-place"
           value="<?= ArrayHelper::getValue($city, 'name') ?>">

    <input type="hidden" class="lat1"  value="<?= ArrayHelper::getValue($city, 'latitude') ?>">
    <input type="hidden" class="lang1"  value="<?= ArrayHelper::getValue($city, 'longitude') ?>">
<?php elseif ($type == 'user') : ?>
    <input type="hidden" class="addressLat" value="<?= ArrayHelper::getValue($city, 'name') ?>,Latvia">
    <input type="hidden" class="title"
           value="<?= ArrayHelper::getValue($user, 'first_name') . ' '. ArrayHelper::getValue($user, 'last_name')  ?>">
    <input type="hidden" class="phone_one" value="<?= ArrayHelper::getValue($user, 'phone') ?>">

    <input type="hidden" class="lat1"  value="<?= ArrayHelper::getValue($city, 'latitude') ?>">
    <input type="hidden" class="lang1"  value="<?= ArrayHelper::getValue($city, 'longitude') ?>">

    <?php if ($user->image): ?>
        <?php
        echo Config::getAvatarPhoto($user->image, 64, 'img-circle image');
    else: ?>
        <img alt="image" class="img-circle image" src=""/>
    <?php endif; ?>

    <input  type="hidden"  class="map-place"
           value="<?= ArrayHelper::getValue($city, 'name') ?>">
<?php elseif ($type == 'city') : ?>
    <input type="hidden" class="addressLat" value="<?= ArrayHelper::getValue($city, 'name') ?>,Latvia">
    <input type="hidden" class="map-place"
           value="<?= ArrayHelper::getValue($city, 'name') ?>">

    <input type="hidden" class="lat1"  value="<?= ArrayHelper::getValue($city, 'latitude') ?>">
    <input type="hidden" class="lang1"  value="<?= ArrayHelper::getValue($city, 'longitude') ?>">

<?php endif; ?>

<script>
    var price = ($('.price').val()) ? '<div class="infoprice"> Price - <span>' + $('.price').val() + ' ' + $('.rate').val() + '</span></div>' : '';
    var title = ($('.title').val()) ? '<div class="infowindow-name title-infoWindow"><span>' + $('.title').val() + '</span></div>' : '';
    var description = ($('.description').val()) ? '<div class="infoWindowDescription">' + $('.description').val() + '</div>' : '';
    var img = ($('.image').attr('src')) ? '<div class="infoWindowImage"><img class="infowindow-image" src="' + $('.image').attr('src') + '"> </div>' : '';
    var phone_one = ($('.phone_one').val()) ? '<div class="phone_one">Tel: ' + $('.phone_one').val() + '</div>' : '';
    var phone_two = ($('.phone_two').val()) ? '<div class="phone_two">Tel: ' + $('.phone_two').val() + '</div>' : '';
    var map_place = ($('.map-place').val()) ? '<div class="map_place"> ' + $('.map-place').val() + '</div>' : '';

    var addressLat =  $('.addressLat').val();
    var lat1 =  $('.lat1').val();
    var lang1 =  $('.lang1').val();

    $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?latlng='+lat1+','+lang1+'&sensor=false', null, function (data) {
        var p = data.results[0].geometry.location;

        var latlng = new google.maps.LatLng(p.lat, p.lng);

        var lat = p.lat;
        var lang = p.lng;
        var myLatLng = {lat: lat, lng: lang};
        // Create a map object and specify the DOM element for display.
        var map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 13
        });
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: name
        });
        var currCenter = map.getCenter();
        var contentString = '<div class="infowindow-content"> ' +
            title +
            map_place +
            '<div class="phones">' +
            phone_one +
            phone_two +
            '</div>' +
            price +
            description +
            '</div>' + img;

        marker.setMap(map);

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        google.maps.event.addDomListener(window, 'resize', function () {
            google.maps.event.trigger(map, "resize");
            map.setCenter(myLatLng);
            map.setZoom(13);
        });

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });
        // show map, open infoBox
        google.maps.event.addListenerOnce(map, 'tilesloaded', function () {
            infowindow.open(map, marker);
        });

        });


</script>
