<?php

/* @var $this yii\web\View */

use common\models\Config;
use frontend\widgets\GridListContentWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ListView;

$this->title = 'My Yii Application';
?>



<?= GridListContentWidget::widget(['slug' => '/category/preferred/']); ?>


<div id="catlist">
<?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => Config::getGridList(),
    'viewParams' => [
        'fullView' => true,
        'context' => 'main-page',
    ],
    'options' => [
        'tag' => 'div',
        'class' => 'list-wrapper',
        'id' => 'list-wrapper',
    ],
    'itemOptions' => [
       // 'options' => ['id' => 'preferred-item'],
        'tag' => 'div',
        'class' => 'each-item',
    ],

    'layout' => "<div class='item-all'>{items}</div>\n<div class='link-page'>{pager}</div>",
    "emptyText" =>'<div class="col-md-12">'.Yii::t('app','No results found.').'</div>',
    'summary' =>  '',
]);
?>
</div>

<?php
if(yii::$app->controller->action->id == 'preferred'){
    $cookiesFavourite = Config::getFavorites();
    $saved = yii::t('app','saved-ads');
    $this->registerJs('
        $(".each-item .product .favourite").each(function(){
            $(this).click(function(){
                    $(this).closest(".each-item").remove(); 
            });
  
        })
');
}
?>
