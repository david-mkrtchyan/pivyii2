<?php

/* @var $this yii\web\View */

use common\models\Config;
use frontend\widgets\GridListContentWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ListView;

$this->title = Yii::t('app', 'Search') . ' ' . $search;
?>


<div class="search">
    <div class="titler">
        <span><em>" «<b><?= $search ?></b>» "</em> </span>
    </div>
    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="70"
             aria-valuemin="0" aria-valuemax="100" style="width:10%">
            <span class="sr-only">70% Complete</span>
        </div>
    </div>
</div>


<?= GridListContentWidget::widget(['slug' => '/category/search/']); ?>


<div id="catlist">
    <?php
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => Config::getGridList(),
        'viewParams' => [
            'fullView' => true,
            'context' => 'main-page',
        ],
        'options' => [
            'tag' => 'div',
            'class' => 'list-wrapper',
            'id' => 'list-wrapper',
        ],
        'itemOptions' => [
            // 'options' => ['id' => 'preferred-item'],
            'tag' => 'div',
            'class' => 'each-item',
        ],

        'layout' => "<div class='item-all'>{items}</div>\n<div class='link-page'>{pager}</div>",
        "emptyText" => '<div class="col-md-12">' . Yii::t('app', 'No results found.') . '</div>',
        'summary' => '',
    ]);
    ?>
</div>

