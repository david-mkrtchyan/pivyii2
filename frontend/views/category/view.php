<?php

use common\models\Albom;
use common\models\CategoryField;
use common\models\Config;
use common\models\Geo;
use frontend\widgets\MessagesWidget;
use kartik\checkbox\CheckboxX;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $model common\models\Report */
/* @var $form yii\widgets\ActiveForm */

$lang = Yii::$app->language;
$currency = Yii::$app->session->get('currency');

$cookiesFavourite = Yii::$app->request->cookies->getValue('favourite', []);
$favorite = intval(in_array($model['id'], $cookiesFavourite));
$currency = Yii::$app->session->get('currency');
if (!Yii::$app->user->isGuest) {
    $user = yii::$app->user->identity;
    if ($user->social_users_favourite) {
        $favorite = intval(in_array($model['id'], unserialize($user->social_users_favourite)));
    }
}

$this->title = $model->title;

$this->registerMetaTag(['name' => 'description', 'content' => $model->description]);
$this->registerMetaTag(['property' => 'og:app_id', 'content' => '538872319602365']);
$this->registerMetaTag(['property' => 'og:admins', 'content' => '1020410409']);
$this->registerMetaTag(['property' => 'og:title', 'content' => $model->title]);
$this->registerMetaTag(['property' => 'og:description', 'content' => $model->description]);
$this->registerMetaTag(['property' => 'og:image', 'content' => Yii::$app->request->hostInfo . $model->productFirstImage]);

?>

<div class="row">
    <div class="col-md-8  sidenav">
        <div class="side-menu-page ">
            <section class="header-section clearfix">

                <div class="title"><h1> <?= yii::t('app', $model->title); ?></h1></div>

                <div id="exchange_wanted">
                <span id="other-date-cat-other-exchange_wanted"><?=
                    Yii::t('app', ArrayHelper::getValue($model, "category.categoryFieldsWish.0.value_$lang"));
                    ?></span>
                </div>

                <?php if ($model->rate_id && $model->price): ?>
                    <span id="other-date-cat" class=" other-date-page"><?php

                        echo \common\models\Currency::showCurrency($model->price, ArrayHelper::getValue($model, "rate.name_" . Yii::$app->language));

                        ?></span>

                <?php endif; ?>

                <?php if ($model->productAddress): ?>
                    <span
                            class="map-home"><?= Html::img(Yii::$app->request->baseUrl . '/images/map.png', ["width" => "100%", "class" => "img-responsive map-image"]); ?></span>
                    <span id="other-date-cat"
                          class=" other-date-page-region">
                                <?php if (ArrayHelper::getValue($model, 'productAddress.city_id')): ?>
                                    <a href="<?= Url::toRoute(['/category/get-map/', 'id' => $model->id, 'type' => 'announcement']) ?>"
                                       target="_blank"
                                       data-id='<?= $model->id ?>' class="get-map">
                                        <?= yii::t('app', 'Look at the map'); ?>
                                </a>
                                <?php endif; ?>
                        </span>
                    <!--Geo::getFullName(ArrayHelper::getValue($model,'productAddress.city_id'), 'en')-->
                <?php endif; ?>
            </section>
            <?php if ($model->albom): ?>
                <div id="jssor_1"
                     style="position:relative;margin:0 auto;top:0px;left:0px;width:800px; overflow:hidden;visibility:hidden;background-color:#24262e;"></div>

                <?php

                $this->registerJs("
                            $(document).ready(function() {
                $('#imageGallery').lightSlider({
                    gallery:true,
                    item:1,
                    thumbItem:9,
                    slideMargin:0,
                    enableDrag: true,
                    currentPagerPosition:'left',
                    });
                });
              ");
                ?>

                <ul id="imageGallery">
                    <?php
                    foreach ($model->albom as $key => $value) {
                        if (!empty($value) && $model->created_at) {
                            $image = ArrayHelper::getValue($value, 'image');
                            $fullUrl = Albom::getImagePath($model->created_at, $image);
                            if (file_exists(yii::getAlias('@frontend' . '/web/' . $fullUrl))) {
                                ?>
                                <li data-thumb="<?= $fullUrl ?>" data-src="<?= $fullUrl ?>">
                                    <img src="<?= $fullUrl ?>"/>

                                    <a href="<?= $fullUrl ?>" class="gallery-zoom">
                                        <i class="icon-zoom-in" id="gallery-image-<?= $key ?>"></i>
                                    </a>

                                </li>
                            <?php }
                        }
                    } ?>
                </ul>

                <ul id="lightgallery" style="display: none;">
                    <?php foreach ($model->albom as $key => $value) {
                        if (!empty($value) && $model->created_at) {
                            $image = ArrayHelper::getValue($value, 'image');
                            $fullUrl = Albom::getImagePath($model->created_at, $image);
                            if (file_exists(yii::getAlias('@frontend' . '/web/' . $fullUrl))) {
                                ?>
                                <li class="col-xs-6 col-sm-4 col-md-3" data-src="<?= $fullUrl ?>">
                                    <a href="">
                                        <img class="img-responsive gallery-image-<?= $key ?>"
                                             src="<?= $fullUrl ?>">
                                    </a>
                                </li>

                            <?php }
                        }
                        ?>
                    <?php } ?>
                </ul>
            <?php endif; ?>


            <?php if ($model->description): ?>
                <div class="table-description">
                    <?= $model->description; ?>
                </div>
            <?php endif; ?>

            <section id="category-fields">

                <table class="table">
                    <tbody>
                    <?php
                    $multipleList = [];
                    $multipleListUseDatabase = [];
                    if ($model->productFields):
                        foreach ($model->productFields as $productField) :
                            if ($categoryField = $productField->categoryField):
                                if ($categoryField->category_fields_wish_id == $model->category_fields_wish_id):
                                    if (($categoryField->name == 'Rate') || ($categoryField->name == 'Price')) {
                                        continue;
                                    }

                                    $categoryFieldValue = $productField->categoryFieldValue;

                                    if ($categoryField->type == CategoryField::typeInput &&
                                        $categoryField->use_database = CategoryField::useDatabaseYes &&
                                            $categoryField->db_name
                                    ): ?>
                                        <?php
                                        if ($categoryField->type == CategoryField::typeInput && $categoryField->allow_multiple == CategoryField::multipleYes):
                                            $table = Yii::$app->db->schema->getTableSchema("{{%$categoryField->db_name}}");
                                            if (!is_null($table)):
                                                $useMultipleDb = $model->getCategoryFieldsValueName($categoryField->id, $table->name);
                                                if ($useMultipleDb) {
                                                    $multipleListUseDatabase['name'] = Yii::t('app', $categoryField->name);
                                                    $multipleListUseDatabase['value'] = $useMultipleDb;
                                                }
                                            endif;
                                        else:
                                            $dataTable = \frontend\models\Config::tableNameFromCategoryFieldOne($categoryField->db_name, $productField->value);
                                            if ($dataTable): ?>
                                                <tr>
                                                    <th>
                                                    <?= Yii::t('app', $categoryField->name) ?></td>
                                                    <td><?= ArrayHelper::getValue($dataTable, 'name_' . $lang) ?></td>
                                                </tr>
                                            <?php endif; endif;
                                        ?>
                                    <?php else: ?>
                                        <?php if ($categoryFieldValue):
                                            if ($categoryField->allow_multiple == CategoryField::multipleYes):
                                                $multipleList[$categoryField->name][] = $categoryFieldValue['value_' . $lang]; ?>
                                            <?php else: ?>
                                                <tr>
                                                    <th>
                                                    <?= Yii::t('app', $categoryField->name) ?></td>
                                                    <td><?= $categoryFieldValue['value_' . $lang] ?></td>
                                                </tr>
                                            <?php endif;
                                            ?>

                                        <?php else: ?>
                                            <tr>
                                                <th>
                                                <?= Yii::t('app', $categoryField->name) ?></td>
                                                <td><?= Yii::t('app', $productField->value) ?></td>
                                            </tr>
                                        <?php endif;
                                    endif;
                                endif;
                            endif;
                        endforeach;
                    endif;
                    ?>

                    <?php
                    if ($multipleList && is_array($multipleList)):
                        foreach ($multipleList as $key => $value):
                            ?>
                            <tr>
                                <th>
                                <?= Yii::t('app', $key) ?></td>
                                <td><?= implode(', ', $value) ?></td>
                            </tr>
                        <?php
                        endforeach;
                    endif;
                    ?>

                    <?php
                    if ($multipleListUseDatabase && is_array($multipleListUseDatabase)):
                        ?>
                        <tr>
                            <th>
                            <?= $multipleListUseDatabase['name'] ?></td>
                            <td><?= implode(', ', $multipleListUseDatabase['value']) ?></td>
                        </tr>
                    <?php
                    endif;
                    ?>
                    </tbody>
                </table>
            </section>

            <?php if (!Yii::$app->user->isGuest): ?>
                <section class="message-announcement clearfix">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#private">
                                <?= Yii::t('app', 'Private Message') ?>
                            </a></li>
                        <li><a data-toggle="tab" href="#public">
                                <?= Yii::t('app', 'Public Message') ?>
                            </a></li>
                    </ul>

                    <div class="tab-content clearfix">
                        <div id="private" class="tab-pane fade in active">
                            <?php $form = ActiveForm::begin([
                                'id' => 'private_message',
                                'options' => [
                                    'class' => 'message_form'
                                ]
                            ]); ?>

                            <?= $form->field($productMessage, 'message')->textarea([
                                'id' => 'private_productmessage-message'
                            ])->label(false); ?>

                            <div class="form-group">
                                <?= Html::submitButton(yii::t('app', 'Send'), ['class' => 'submit-message btn btn-primary', 'name' => 'signup-button']) ?>
                            </div>

                            <div class="settings clearfix">
                                <div class="mdc-form-field">

                                    <label>
                                        <?php
                                        $checked = \Yii::$app->getRequest()->getCookies()->getValue('private');

                                        $checked = ($checked == 'true') ? CheckboxX::INPUT_CHECKBOX : CheckboxX::INPUT_TEXT;

                                        echo $form->field($productMessage, 'checkedPrivate')->widget(
                                            CheckboxX::classname(), [
                                                'initInputType' => $checked,
                                                'autoLabel' => true,
                                                'pluginOptions' => [
                                                    'threeState' => false,
                                                ]
                                            ]
                                        )->label(false);
                                        ?>
                                    </label>
                                </div>
                                <span class="help"><?= Yii::t('app', 'Line change') ?>
                                    : <kbd><?= Yii::t('app', 'Shift') ?></kbd> + <kbd><?= Yii::t('app', 'Enter') ?></kbd>
                            </span>
                            </div>

                            <?php
                            if (Yii::$app->user->identity->id == $model->user_id): ?>
                                <?= $form->field($productMessage, 'owner_chosen_user_id')->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map($userList, "user.id", function ($person, $defaultValue) {
                                        return ArrayHelper::getValue($person, 'user.first_name') . ' ' . ArrayHelper::getValue($person, 'user.last_name');
                                    }),
                                    'language' => Yii::$app->language,
                                    'options' => ['placeholder' => Yii::t('app', 'Select a user' . ' ...')],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ])->label(false); ?>

                            <?php endif; ?>


                            <?php ActiveForm::end(); ?>


                            <div class="private-messages">
                                <?php
                                if ($messagesPrivate):
                                    echo MessagesWidget::widget(['messages' => $messagesPrivate, 'type' => 'private']);
                                endif; ?>
                            </div>


                        </div>


                        <div id="public" class="tab-pane fade">
                            <?php $form = ActiveForm::begin([
                                'id' => 'public_message',
                                'options' => [
                                    'class' => 'message_form'
                                ]
                            ]); ?>

                            <?= $form->field($productMessage, 'message')->textarea([
                                'id' => 'public_productmessage-message'
                            ])->label(false); ?>

                            <div class="form-group">
                                <?= Html::submitButton(yii::t('app', 'Send'), ['class' => 'submit-message btn btn-primary', 'name' => 'signup-button']) ?>
                            </div>

                            <div class="settings clearfix">
                                <div class="mdc-form-field">

                                    <label>
                                        <?php
                                        $checked = \Yii::$app->getRequest()->getCookies()->getValue('public');
                                        $checked = ($checked == 'true') ? CheckboxX::INPUT_CHECKBOX : CheckboxX::INPUT_TEXT;
                                        echo $form->field($productMessage, 'checkedPublic')->widget(CheckboxX::classname(),
                                            [
                                                'initInputType' => $checked,
                                                'autoLabel' => true,
                                                'value' => 1,
                                                'pluginOptions' => [
                                                    'threeState' => false,
                                                ]
                                            ]
                                        )->label(false);
                                        ?>
                                    </label>
                                </div>
                                <span class="help"><?= Yii::t('app', 'Line change') ?>
                                    : <kbd><?= Yii::t('app', 'Shift') ?></kbd> + <kbd><?= Yii::t('app', 'Enter') ?></kbd>
                            </span>
                            </div>


                            <?php ActiveForm::end(); ?>

                            <div class="public-messages">
                                <?php
                                if ($messagesPublic):
                                    echo MessagesWidget::widget(['messages' => $messagesPublic, 'type' => 'public']);
                                endif; ?>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>

            <section id="fotter-section" class="date-views">
                <div class="line-thin">
                    <div class="catItemHeader line-thin-top">
                             <span class="catItemHitsBlock">

                                <span class="catItemHits text-left">
                                      <span class="glyphicon glyphicon-eye-open"></span><b>
                                          <?= $model->counter ? $model->counter : '0' ?>
                                      </b> <?= yii::t('app', 'Times') ?> </span>

                                <span class="post-meta  text-right">
                                    <li> <em>
                                            <span
                                                    class="date-create"><span
                                                        class="fa fa-clock-o pink-color"></span><?= yii::t('app', 'Date:'); ?> <?= date('Y-m-d', $model->created_at); ?> </span>
                                                         <span><?= yii::t('app', 'Renewed:'); ?> <?= date('Y-m-d', $model->updated_at); ?> </span>

                                    </em></li>
                                </span>
                              </span>
                        <div class="clr"></div>
                    </div>
                </div>
            </section>


        </div>


    </div>


    <div class="col-md-4">
        <div class="category-aside-group">
            <div class="category-aside-user clearfix">
                <ul class="nav nav-pills">
                    <li>
                        <!-- <i title="<? /*= yii::t('app', 'Նախընտրած') */ ?>"
                                    class="fa fa-star<? /*= $favorite ? '' : '-o' */ ?>"></i>-->
                        <a href="#" id="addToFavoritesLink" data-id="<?= $model->id ?>"
                           data-favourite="<?= $favorite ?>"
                           class="trackClicks" data-id><i
                                    class="svg-icon-25 svg-favorite"></i><?= Yii::t('app', 'Favorite') ?>
                        </a>
                    </li>
                    <li class="dropdown sharing-dropdown">
                        <a href="#" id="sharing" role="button" class="trackClicks"><i
                                    class="svg-icon-25 svg-share"></i><?= Yii::t('app', 'Share') ?>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a target="_blank"
                                   href="https://www.facebook.com/sharer/sharer.php?u=<?= Config::getSiteFullUrl() ?>">
                                    <i class="fa fa-facebook-f"></i>
                                    <?= Yii::t('app', 'Facebook') ?>
                                </a>
                            </li>
                            <li>
                                <a target="_blank"
                                   href="https://twitter.com/intent/tweet?url=<?= Config::getSiteFullUrl() ?>">
                                    <i class="fa fa-vk"></i>
                                    <?= Yii::t('app', 'Twetter') ?></a>
                            </li>
                            <li>
                                <a target="_blank"
                                   href="https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.deprecated=1&st.shareUrl=<?= Config::getSiteFullUrl() ?>"
                                >
                                    <i class="fa fa-odnoklassniki"></i>
                                    <?= Yii::t('app', 'OK') ?></a>
                            </li>
                        </ul>
                    </li>


                    <li>
                        <a href="#" id="listing-complaint"
                           class="trackClicks complain page_d"><i
                                    class="svg-icon-25 svg-report "></i><?= Yii::t('app', 'Report') ?>
                        </a>
                    </li>
                </ul>
            </div>

            <?php if ($user = $model->user): ?>
                <div class="user-data">
                    <div class="group-user-data">
                        <a href="<?= Url::toRoute(['/user/profile', 'id' => $user->id]); ?>">
                            <?php if ($user->image): ?>
                                <?php
                                echo Config::getAvatarPhoto($user->image, 48, 'img-circle');
                            else: ?>
                                <img alt="image" class="img-circle"
                                     src="<?= Yii::$app->request->baseUrl ?>/images/default_avatar.png"/>
                            <?php endif; ?>
                            <div class="name-user">
                                <?= ucfirst($user->first_name) . ' ' . ucfirst(substr($user->last_name, 0, 1)) . '.' ?>

                            </div>
                        </a>


                        <div class="address">
                            <?= ArrayHelper::getValue($model, 'productAddress.address') ?>
                            <div class="map-user">
                                <span class="map-home"><?= Html::img(Yii::$app->request->baseUrl . '/images/map.png', ["width" => "100%", "class" => "img-responsive map-image"]); ?></span>

                                <span id="other-date-cat"
                                      class=" other-date-page-region">
                                <?php if (ArrayHelper::getValue($user, 'city_id')): ?>
                                    <a href="<?= Url::toRoute(['/category/get-map/', 'id' => $user->id, 'type' => 'user']) ?>"
                                       target="_blank"
                                       data-id='<?= $model->id ?>' class="get-map">
                                        <?= yii::t('app', 'Look at the map'); ?>
                                </a>
                                <?php endif; ?>
                        </span></div>
                        </div>

                        <?php if (!Yii::$app->user->isGuest &&
                            $user->id !== Yii::$app->user->identity->id):
                            $userFollow = Config::checkUserFollow($user->id);
                            ?>
                            <div class="follow-user-div">
                                <a href="#" class="follow-user" data-id="<?= $user->id ?>">
                                    <span class="icon">
                                        <i class="fa fa-<?= ($userFollow) ? 'check' : 'plus' ?>" aria-hidden="true"></i>
                                    </span>
                                    <span class="text-follow">
                                        <?= ($userFollow) ? Yii::t('app', 'Follower') : Yii::t('app', 'Follow') ?>
                                    </span>
                                </a>
                            </div>
                        <?php endif; ?>

                        <?php if (($number_one = ArrayHelper::getValue($model, 'productAddress.number_one')) ||
                            ($number_two = ArrayHelper::getValue($model, 'productAddress.number_two'))
                        ): ?>
                            <div class="phone">
                                <?= Yii::t('app', 'Tel' . ':') ?>
                                <?= ArrayHelper::getValue($model, 'productAddress.number_one') ?><br/>
                                <?= Yii::t('app', 'Tel:') ?> <?= ArrayHelper::getValue($model, 'productAddress.number_two') ?>
                            </div>
                        <?php else: ?>
                            <div class="phone">
                                <?= Yii::t('app', 'Tel:') ?>
                                <?= ArrayHelper::getValue($user, 'phone') ?>
                            </div>
                        <?php endif; ?>

                        <div class="with-us">
                            <?= Yii::t('app', 'With us' . '`') ?>
                            <?= date('Y-m-d', $user->created_at) ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>


        <?= \frontend\widgets\BannerWidget::widget(['position' => 'Left', 'name' => 'html-code', 'banner_page_name' => yii::$app->controller->id . '/' . yii::$app->controller->action->id, 'device' => 'Desktop', 'width' => '290', 'height' => '250']); ?>

    </div>
</div>

<div class="row">
    <h3 class="similar-ads"><?= Yii::t('app', 'Similar ads') ?></h3>
    <?= \frontend\widgets\SameAnnouncementCategory::widget(['model' => $model]) ?>
</div>
<?php
Modal::begin([
    'header' => '<h3>' . Yii::t('app', 'Report this ad') . '</h3>',
    'id' => 'complain',
    'size' => 'modal-md',
]); ?>


<?php $form = ActiveForm::begin(); ?>

<?= $form->field($report, 'report_category_id')->radioList(
    $reportCategory
) . '<br/>'; ?>

<?php if (!Yii::$app->user->isGuest): ?>
    <?= $form->field($report, 'email')->textInput(['value' => yii::$app->user->identity->email]); ?>

<?php else: ?>
    <?= $form->field($report, 'email')->textInput(['autofocus' => true]) ?>
<?php endif; ?>

<?= $form->field($report, 'review')->textArea(['autofocus' => true, 'rows' => 4]) ?>

<?= $form->field($report, 'product_id')->hiddenInput(['value' => $report->product_id])->label(false); ?>

<div class="form-group">
    <?= Html::submitButton(yii::t('app', 'Send'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
</div>
<?php ActiveForm::end(); ?>

<?php Modal::end(); ?>


<?php
Modal::begin([
    'header' => '<h3>' . Yii::t('app', 'Edit message') . '</h3>',
    'id' => 'modal-edit-message',
    'size' => 'modal-md',
]); ?>
<textarea class="form-control edit-value-message" rows="6"> </textarea>
<div class="form-group">
    <?= Html::submitButton(yii::t('app', 'Edit'), ['class' => 'btn btn-primary edit-message-button']) ?>
</div>
<?php Modal::end(); ?>

<style>
    .post img {
        display: block;
        max-width: 100%;
    }

    .page-load-status {
        display: none; /* hidden by default */
        padding-top: 20px;
        border-top: 1px solid #DDD;
        text-align: center;
        color: #777;
    }
</style>


<?php $this->registerJs('
$(document).ready(function(){

//-------------------------------------//
// hack CodePen to load pens as pages

var nextPenSlugs = [
  "2",
  "4",
  "6",
];

function getPenPath() {
  var slug = nextPenSlugs[ this.loadCount ];
  if ( slug ) {
    return \'/category/view/?id=38&limit=\' + slug;
  }
}

 
//-------------------------------------//
// init Infinite Scroll

/*$(\'.post-comment_private\').infiniteScroll({
  path: getPenPath,
  append: \'.comment-content_private\',
  button: \'.view-more-button_private\',
  // using button, disable loading on scroll 
  scrollThreshold: false,
  history: false,
  status: \'.page-load-status_private\',
});

$(\'.post-comment_public\').infiniteScroll({
  path: getPenPath,
  append: \'.comment-content_public\',
  button: \'.view-more-button_public\',
  // using button, disable loading on scroll 
  scrollThreshold: false,
  history: false,
  status: \'.page-load-status_public\',
});*/


    $(document).on("click",".group-message-button a",function(e){
            var thisLink = $(this);
            var id = thisLink.attr("data-id");
            var dataType = thisLink.attr("data-type");
            var dataLike = thisLink.attr("data-like");
            var productId = thisLink.attr("data-product");
            var dataAction = thisLink.attr("data-action");
            var url = "/message/"+dataAction+"/";
       
            if(dataAction == "edit-message"){
                var editTextValue = thisLink.closest(".media-body").find(".text-muted-message").text();
                var str = editTextValue.replace(/\s/g, \'\');
                $(".edit-value-message").html(str);
                $("#modal-edit-message").modal("show");
                
                $(document).on("click",".edit-message-button",function(e){
                     var message = $(".edit-value-message").val();
                     var obj = {
                             id: id, url:url,message:message,type:dataType,productId:productId
                     };
                     sendMessages(obj);
                     $("#modal-edit-message").modal("hide");
                });
                   
            }else{
                var obj = {
                   id: id, url:url,type:dataType,like:dataLike,productId:productId
                };
            sendMessages(obj);
            }
            
     });
    
    $(document).on("click",".settings .cbx-label",function(e){
            var checked = $(this).closest(".form-group").find(".cbx-active").find(".cbx-icon").find(".glyphicon-ok").length;
            var checkedValue  = checked ? false : true ;
            var checkedValue  = checked ? false : true ;
            var type = $(this).closest("form").closest("div").attr("id");
            var obj = {
                  checkedValue:checkedValue,type:type
            };
              
          $.ajax({
              url: "/message/checked/",
              type: \'post\',              
              data: obj,   
              dataType: \'json\',
              success: function (data) {
                  console.log(data,"12312");
              }
           });
    });
    
    $(document).on("submit",".message_form",function(e){
         e.preventDefault();
         var message,type,chosen_user;
         var thisForm = $(this);
          var sendMessage = false;
          if(thisForm.find(".cbx-active").find(".cbx-icon").find(".glyphicon-ok").length)
          {
                     var sendMessage = true;
          }
                
         if(thisForm.attr("id") == "private_message"){
               var type = "private";
               var  message = $("#private_productmessage-message").val();
               var  chosen_user = $("#productmessage-owner_chosen_user_id").val();
         }else if(thisForm.attr("id") == "public_message"){
               var  type = "public";
               var  message = $("#public_productmessage-message").val();
         }
         
         if(type && message){
             var obj = {
              sendMessage: sendMessage, message: message,type : type, product_id:' . Yii::$app->request->get('id') . ', url:"/message/send-message/",chosen_user:chosen_user
              };
             sendMessages(obj)
         }
    });
    
    function sendMessages(obj){
          if(obj){
                   $.ajax({
                     url:  obj.url,
                     type: \'post\',              
                     data: obj,   
                     dataType: \'html\',
                     success: function (data) {
                         console.log(data,"12312")
                         if(data){
                             if(obj.type == "private"){
                                   /*if(obj.sendMessage == "newItem"){
                                        console.log(data);
                                       $(".private-messages .comment-content_private").prepend(data);
                                       $("#private_productmessage-message").val("");
                                       }*/
                                 
                                        $(".private-messages").html(data);
                                        $("#private_productmessage-message").val("");
                                   
                                  
                              }else if(obj.type == "public"){
                                  $(".public-messages").html(data);
                                  $("#public_productmessage-message").val("");
                              }
                         }
                     }
                 });
          }
    }
    
    $(\'.responsive-slider\').slick({
        dots: false,
        infinite: true,
        speed: 300,
        autoplay: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplaySpeed: 15000,
        responsive: [
            {breakpoint: 1440, settings: {slidesToShow: 4, slidesToScroll: 4, infinite: true, dots: false}},
            {breakpoint: 1366, settings: {slidesToShow: 4, slidesToScroll: 4, infinite: true, dots: false}},
            {breakpoint: 900, settings: {slidesToShow: 3, slidesToScroll: 3, infinite: true, dots: false}},
            {breakpoint: 600, settings: {slidesToShow: 2, slidesToScroll: 2, dots: true, arrows: false}},
            {breakpoint: 480, settings: {slidesToShow: 2, slidesToScroll: 2, dots: true, arrows: false}}
        ]
    });
    });
') ?>
