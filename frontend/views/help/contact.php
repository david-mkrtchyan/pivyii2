<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $contactForm \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->registerCss('.site-contact{    padding-top: 20px;}');
?>
<div class="site-contact">

    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($contactForm, 'name')->textInput(['autofocus' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($contactForm, 'subject') ?>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($contactForm, 'email') ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($contactForm, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div>{input}</div><div class="verifyCode-image">{image}</div>',
                    ]) ?>
                </div>
            </div>


            <?= $form->field($contactForm, 'body')->textarea(['rows' => 6]) ?>


            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
