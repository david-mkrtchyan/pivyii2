<div class="help-block-content">
    <?= $help->text ?>
    <?php if($help->slug == 'contact' || $help->slug == 'help'): ?>
    <?=  $this->render('contact',[
            'contactForm' => $contactForm
        ]); ?>
    <?php endif; ?>
</div>