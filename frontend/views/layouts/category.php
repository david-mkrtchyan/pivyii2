<?php

/* @var $this \yii\web\View */

/* @var $content string */

use common\models\Config;
use frontend\assets\MainAsset;
use frontend\widgets\AlertWidget;
use frontend\widgets\AsideCategoryWidget;
use frontend\widgets\AsideMenuWidget;
use frontend\widgets\CountryCategoryWidget;
use frontend\widgets\FacebookPluginWidget;
use frontend\widgets\HeaderWidget;
use frontend\widgets\FooterWidget;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;

MainAsset::register($this);
$app = Yii::$app;
$request = $app->request;
$baseUrl = $request->baseUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= Config::getMobileTypeText() ?>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>


    <link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>/slick/slick-theme.css">

    <?php $this->head() ?>
</head>
<body style="background:<?= Config::getSettingValue('background') ? Config::getSettingValue('background') : '#f8f8f8' ?>">
<?php $this->beginBody() ?>

<div class="container parent-container">
    <!-- Header -->
    <?= HeaderWidget::widget(); ?>
    <div id="content" class="clearfix">
        <?= Breadcrumbs::widget([
            'homeLink' => ['label' => Yii::t('app','Home'), 'url' => '/'],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= AlertWidget::widget() ?>

        <div class="col-md-3 sidenav">
            <div class='aside-sidenav aside-sidenav-search'>
                <?php if (Yii::$app->request->get('ProductField')): ?>
                    <?= AsideCategoryWidget::widget(['slug' => Yii::$app->request->get('slug')]) ?>
                <?php else: ?>
                    <div class="recent-search"> </div>
                <?php endif; ?>

                <?= CountryCategoryWidget::widget(['slug' => Yii::$app->request->get('slug')]) ?>
            </div>
            <div class='aside-facebook'>
                <?= FacebookPluginWidget::widget() ?>
            </div>
        </div>


        <div class="col-md-9 text-left">
            <?= $content ?>
        </div>
    </div>

    <!-- Footer -->
    <?= FooterWidget::widget(); ?>
</div>

<!-- Login modal -->
<?php //frontend\widgets\LoginModalWidget::widget(); ?>

<!-- Mainly scripts -->
<?php
$icon = '<i class="svg-icon-16 svg-list"></i>';
$showFewer = Yii::t('app', 'Show fewer');
$showShowAll = Yii::t('app', 'Show all tabs');
$showShowAllSearch = Yii::t('app', 'Show all search');
$showShowAllFewer = Yii::t('app', 'Show fewer');
$showAll = Yii::$app->request->get('showAll');

$this->registerJs("
  $(document).ready(function(){
  
       $(document).on('click','.show_hide_item',function (e) {
             var showAllGet = '$showAll'
             e.preventDefault();
             $(this).closest('.category-content').find('.hide').toggleClass('active');
             var spanText = $(this).find('span');
             var showAll = $(this).find('.showAll');
             spanText.text(spanText.text() == '$showFewer' ? '$showShowAll' : '$showFewer');  
             
             if(showAllGet !== 'true' ){
               showAll.val(showAll.val() == 'false' ? 'true' : 'false')
             }
           
            
            });
             
           $(document).on('click','.show_hide_item_search',function (e) {
             e.preventDefault();
             $(this).closest('.recent-search').find('.hide-search').toggleClass('active');
             var spanText = $(this).find('span');
             var showAll = $(this).find('.showAll');
             spanText.text(spanText.text() == '$showShowAllFewer' ? '$showShowAllSearch' : '$showShowAllFewer');  
             
             showAll.val(showAll.val() == 'false' ? 'true' : 'false')
           
            
           }); 
            
        }); 
  ")
?>
<script src="<?= $baseUrl ?>/js/jquery-3.1.1.min.js"></script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
