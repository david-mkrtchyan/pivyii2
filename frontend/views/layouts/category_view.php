<?php

/* @var $this \yii\web\View */

/* @var $content string */

use common\models\Config;
use frontend\assets\MainAsset;
use frontend\widgets\AlertWidget;
use frontend\widgets\HeaderWidget;
use frontend\widgets\FooterWidget;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

MainAsset::register($this);
$app = Yii::$app;
$request = $app->request;
$baseUrl = $request->baseUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= Config::getMobileTypeText() ?>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>/slick/slick-theme.css">
    <?php $this->head() ?>
</head>
<body style="background:<?= Config::getSettingValue('background') ? Config::getSettingValue('background') : '#f8f8f8' ?>">
<?php $this->beginBody() ?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1&appId=1395946733883770&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<div class="container parent-container">
    <!-- Header -->
    <?= HeaderWidget::widget(); ?>
    <div id="content" class="clearfix">
        <?= Breadcrumbs::widget([
            'homeLink' => ['label' => Yii::t('app','Home'), 'url' => '/'],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= AlertWidget::widget() ?>
        <?= $content ?>
    </div>

    <!-- Footer -->
    <?= FooterWidget::widget(); ?>
</div>

<!-- Login modal -->
<?php //frontend\widgets\LoginModalWidget::widget(); ?>

<!-- Mainly scripts -->
<script src="<?= $baseUrl ?>/js/jquery-3.1.1.min.js"></script>

<?php
$follower = Yii::t('app', 'Follower');
$follow = Yii::t('app', 'Follow');

$this->registerJs("
    $('.follow-user').click(function (e) {
        event.preventDefault();
        var e = $(this);
        var id = e.attr('data-id');

        $.ajax({
            url: '/site/add-user-favourite',
            type: 'post',
            data: {
                id: id
            },
            dataType: 'json',
            success: function (data) {
                if (data) {
                    if(data.added == 0){
                        e.find('.text-follow').text('".$follow."');
                        $('.follow-user .icon').html('<i class=\"fa fa-plus\" aria-hidden=\"true\"></i>');
                    }else if(data.added == 1){
                        $('.follow-user .icon').html('<i class=\"fa fa-check\" aria-hidden=\"true\"></i>');
                        e.find('.text-follow').text('".$follower."');
                    }
                }
            }
        });
    });
    
    $( document ).on('click',\".profile-add.dropdown-toggle.action-split\",function() {
        $( this ).closest('.share').toggleClass( \"open\");
    });



")?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
