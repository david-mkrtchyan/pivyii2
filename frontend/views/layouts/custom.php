<?php

/* @var $this \yii\web\View */

/* @var $content string */

use common\models\Config;
use frontend\assets\MainAsset;
use frontend\widgets\AlertWidget;
use frontend\widgets\AsideCategoryWidget;
use frontend\widgets\AsideMenuWidget;
use frontend\widgets\FacebookPluginWidget;
use frontend\widgets\HeaderWidget;
use frontend\widgets\FooterWidget;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;

MainAsset::register($this);
$app = Yii::$app;
$request = $app->request;
$baseUrl = $request->baseUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= Config::getMobileTypeText() ?>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>


    <link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>/slick/slick-theme.css">

    <?php $this->head() ?>
</head>
<body style="background:<?= Config::getSettingValue('background') ? Config::getSettingValue('background') : '#f8f8f8' ?>">
<?php $this->beginBody() ?>

<div class="container parent-container">
    <!-- Header -->
    <?= HeaderWidget::widget(); ?>
    <div id="content" class="clearfix">
        <?= Breadcrumbs::widget([
            'homeLink' => ['label' => Yii::t('app','Home'), 'url' => '/'],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= AlertWidget::widget() ?>

        <div class="col-md-3 sidenav">
            <div class='aside-sidenav'>
                <?= AsideMenuWidget::widget() ?>
            </div>
            <div class='aside-facebook'>
            <?php if (Yii::$app->user->isGuest): ?>
                <?= FacebookPluginWidget::widget() ?>
            <?php endif; ?>
            </div>
        </div>
        <div class="col-md-9 text-left">
            <?= $content ?>
        </div>
    </div>

    <!-- Footer -->
    <?= FooterWidget::widget(); ?>
</div>

<!-- Login modal -->
<?php //frontend\widgets\LoginModalWidget::widget(); ?>

<!-- Mainly scripts -->

<script src="<?= $baseUrl ?>/js/jquery-3.1.1.min.js"></script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
