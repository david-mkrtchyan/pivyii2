<?php

/* @var $this \yii\web\View */

/* @var $content string */

use frontend\assets\EmptyAsset;
use yii\helpers\Html;
use common\models\Config;
EmptyAsset::register($this);

$app = Yii::$app;
$request = $app->request;
$baseUrl = $request->baseUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= $app->language ?>">
<head>
    <meta charset="<?= $app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= Config::getMobileTypeText() ?>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="gray-bg">
<?php $this->beginBody() ?>

<?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
