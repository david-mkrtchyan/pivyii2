<?php

use common\models\Config;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $message common\models\ProductMessage */
/* @var $form yii\widgets\ActiveForm */


if ($message): ?>
    <article class="post-comment post-comment_<?= $type ?>">
        <div class="feed-element clearfix">
            <a href="<?= Url::toRoute(['/user/profile', 'id' => $message->user_id]); ?>"
               class="pull-left">
                <?= Config::getAvatarPhoto(ArrayHelper::getValue($message, 'user.image'), 48, 'img-circle') ?>
            </a>

            <div class="media-body ">
                <small class="pull-right"><?= Config::timeElapsedString($message->created_at) ?></small>
                <strong>
                    <a href="<?= Url::toRoute(['/user/profile', 'id' => $message->user_id]); ?>">
                        <?= ArrayHelper::getValue($message, 'user.first_name') . ' ' . ArrayHelper::getValue($message, 'user.last_name') ?>
                    </a>
                </strong>
                <?= Yii::t('app', 'posted message on') ?>
                <strong>
                    <?php if ($message->owner_chosen_user_id): ?>
                        <a href="<?= Url::toRoute(['/user/profile', 'id' => ArrayHelper::getValue($message, 'ownerChosenUser.id')]); ?>">
                            <?= ArrayHelper::getValue($message, 'ownerChosenUser.first_name') . ' ' . ArrayHelper::getValue($message, 'ownerChosenUser.last_name') ?>
                        </a>
                    <?php else: ?>
                        <a href="<?= Url::toRoute(['/user/profile', 'id' => ArrayHelper::getValue($message, 'product.user.id')]); ?>">
                            <?= ArrayHelper::getValue($message, 'product.user.first_name') . ' ' . ArrayHelper::getValue($message, 'product.user.last_name') ?>
                        </a>
                    <?php endif; ?>

                </strong>
                <br>
                <strong class="text-muted text-muted-message">
                    <?= ArrayHelper::getValue($message, 'message') ?>
                </strong>
                <br>
                <small class="text-muted"> <?= date("H:i a", $message->created_at); ?>
                    - <?= date('m.d.Y', $message->created_at); ?></small>
                <div class="pull-right group-message-button">
                    <a class="btn btn-xs btn-white like btn-primary" data-like="1"
                       data-type="<?= $message->type ?>" data-product="<?= $message->product_id ?>"
                       data-action="like" data-id="<?= $message->id ?>"><i
                                class="fa fa-thumbs-up"></i> <?= count($message->productMessageLike) ?> </a>
                    <a class="btn btn-xs btn-white like btn-primary" data-like="0"
                       data-type="<?= $message->type ?>" data-product="<?= $message->product_id ?>"
                       data-action="like" data-id="<?= $message->id ?>"><i
                                class="fa fa-thumbs-down"></i> <?= count($message->productMessageLikeDislike) ?>
                    </a>
                    <?php if (Yii::$app->user->identity->id == $message->user_id): ?>

                        <a class="btn btn-xs btn-primary message" data-type="<?= $message->type ?>"
                           data-product="<?= $message->product_id ?>" data-action="edit-message"
                           data-id="<?= $message->id ?>"><i class="fa fa-pencil"></i> </a>
                        <a class="btn btn-xs btn-primary btn-white delete" data-type="<?= $message->type ?>"
                           data-product="<?= $message->product_id ?>" data-action="delete"
                           data-id="<?= $message->id ?>"><i class="fa fa-trash"></i> </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </article>
<?php endif; ?>