<?php

/* @var $this yii\web\View */

use frontend\widgets\SliderWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = 'My Yii Application';
/** @var  \common\models\Product $products */
?>


<?php if ($products): ?>

    <div class="col-md-12" style="overflow: hidden;padding-bottom: 30px">
    <?= \frontend\widgets\BannerWidget::widget(['position' => 'Top','name'=>'banner_home_page' ,'banner_page_name'=> yii::$app->controller->id.'/'.yii::$app->controller->action->id,'device' => 'Desktop' , 'width' =>'680' ,'height' => '160']); ?>
    </div>

    <div class="group-sliders">

        <div class="group-regular">
            <h3><?= Yii::t('app','Latest Ads')?></h3>
            <section class="regular slider">
                <?php
                $productFirst = array_chunk(array_slice($products, 0, 24),2);
                if ($productFirst):
                    echo SliderWidget::widget(['data' => $productFirst]);
                endif; ?>
            </section>
        </div>

        <div class="group-regular2">
            <h3><?= Yii::t('app','Most Viewed')?></h3>
            <section class="regular2 slider">
                <?php
                $mostViewProduct = array_chunk(array_slice($mostViewProduct, 0, 24),2);
                if ($mostViewProduct):
                    echo SliderWidget::widget(['data' => $mostViewProduct]);
                endif; ?>
            </section>
        </div>

    </div>
<?php endif; ?>


<?php $this->registerJs('
     $(document).on(\'ready\', function() {
      
          $(\'.regular\').slick({
          dots: false,
          infinite: true,
          speed: 300,
          autoplay: true,
          slidesToShow: 3,  
          slidesToScroll: 3,
          autoplaySpeed: 15000,
          responsive: [
            { breakpoint: 1440,  settings: { slidesToShow: 3, slidesToScroll: 3, infinite: true,   dots: false   }},
            { breakpoint: 1366,  settings: { slidesToShow: 3, slidesToScroll: 3, infinite: true,   dots: false   }},
            { breakpoint: 900,  settings: { slidesToShow: 3, slidesToScroll: 3, infinite: true,   dots: false    }},
            { breakpoint: 600,   settings: { slidesToShow: 2, slidesToScroll: 2, dots: true,  arrows: false      }},
            { breakpoint: 480,   settings: { slidesToShow: 2, slidesToScroll: 2, dots: true,  arrows: false      }}     
        	]
        });
        $(\'.regular\').show();
      
   
        
        $(".regular2").slick({
            dots: false,
            arrows: true,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
            { breakpoint: 1440,  settings: { slidesToShow: 3, slidesToScroll: 3, infinite: true,   dots: false   }},
            { breakpoint: 1366,  settings: { slidesToShow: 3, slidesToScroll: 3, infinite: true,   dots: false   }},
            { breakpoint: 900,  settings: { slidesToShow: 3, slidesToScroll: 3, infinite: true,   dots: false    }},
            { breakpoint: 600,   settings: { slidesToShow: 2, slidesToScroll: 2, dots: true,  arrows: false      }},
            { breakpoint: 480,   settings: { slidesToShow: 2, slidesToScroll: 2, dots: true,  arrows: false      }}     
        	]
        });
         
    });
 ') ?>