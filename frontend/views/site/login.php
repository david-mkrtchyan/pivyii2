<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="site-login">
        <h1><?= Html::encode($this->title) ?></h1>
        <div class="group-user-field">
            <div class="exists-user">
                <?= Html::radio('user', 'true', ['label' => Yii::t('app', 'Existing User')]) ?>
            </div>

            <div class="new-user">
                <?= Html::radio('user', '', ['label' => Yii::t('app', 'New user'), 'class' => 'register-form']) ?>
            </div>
        </div>

        <p><?= Yii::t('app', 'Please fill out the following fields to login:') ?></p>

        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div style="color:#999;margin:1em 0">
                    <?= Yii::t('app', 'If you forgot your password you can') ?> <?= Html::a(Yii::t('app', 'reset it'), ['site/request-password-reset']) ?>.
                </div>


                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

<?php
$registerUrl = Url::toRoute(['site/signup']);
$this->registerJs('
$(document).ready(function () {
      $( document ).on(\'click\',".register-form",function() {
        window.location.href = \'' . $registerUrl . '\';
    })
        })
') ?>