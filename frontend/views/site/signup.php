<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\SignupForm */

use borales\extensions\phoneInput\PhoneInput;
use common\models\Config;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="group-user-field">
        <div class="exists-user">
            <?= Html::radio('user', '', ['label' => Yii::t('app', 'Existing User'),'class' => 'existing-form']) ?>
        </div>

        <div class="new-user">
            <?= Html::radio('user', 'true', ['label' => Yii::t('app', 'New user')]) ?>
        </div>
    </div>


    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'email') ?>
                </div>

                <div class="col-md-6">
                    <?= $form->field($model, 'adresse') ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'password')->passwordInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'repeat_password')->passwordInput() ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'first_name') ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'last_name') ?>
                </div>
            </div>


            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'birth_day')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Enter birth date ...'],
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'autoclose' => true
                        ]
                    ]); ?>
                </div>
            </div>


            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'city_id')->widget(Select2::classname(), [
                        'data' => Config::getRegionList(),
                        'options' => ['placeholder' => Yii::t('app','Select a city ...')],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'phone')
                        ->widget(PhoneInput::className(), [
                            'options' => ['class' => 'form-control'],
                            'jsOptions' => [
                                'preferredCountries' => ['LT'],
                                'nationalMode' => false
                            ]
                        ]);
                    ?>
                </div>

            </div>

            <div class="form-group">
                <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>


<?php
$registerUrl = Url::toRoute(['site/login']);
$this->registerJs('
$(document).ready(function () {
      $( document ).on(\'click\',".existing-form",function() {
        window.location.href = \'' . $registerUrl . '\';
    })
        })
') ?>