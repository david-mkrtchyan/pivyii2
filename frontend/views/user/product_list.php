<?php

use common\models\Config;
use kartik\field\FieldRange;
use kartik\file\FileInput;
use kartik\growl\Growl;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */

?>


<dl>
    <dt>
        <img src="<?= $model->productFirstImage ? $model->productFirstImage : '/images/nopicture.png'; ?>">
        <strong>
            <?= ArrayHelper::getValue($model, 'price') . ' ' . ArrayHelper::getValue($model, 'rate.name_'.Yii::$app->language) ?>
        </strong>
        <span class="title"> <a target="_blank" href="<?= Url::to(['/category/view','id' =>$model->id ]) ?>"> <?= Config::replaceTitle($model->title, '70') ?></a></span>
    </dt>
    <dd>
        <p class="profile-description">
            <?= Config::replaceTitle($model->description, '200') ?>
        </p>
    </dd>
</dl>
