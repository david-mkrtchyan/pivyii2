<?php

use common\models\Config;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $categories array */
/* @var $shops array */
/* @var $brands array */

$this->title = Yii::t('app','Ads');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-4 sidenav">
        <?php if ($user): ?>
            <div class="user-data profile-user-data" style="    background: white;">
                <div class="group-user-data">
                    <?php if ($user->image): ?>
                        <?php
                        echo Config::getAvatarPhoto($user->image, 48, 'img-circle');
                    else: ?>
                        <img alt="image" class="img-circle"
                             src="<?= Yii::$app->request->baseUrl ?>/images/default_avatar.png"/>
                    <?php endif; ?>
                    <div class="name-user">
                        <?= ucfirst($user->first_name) . ' ' . ucfirst(substr($user->last_name, 0, 1)) . '.' ?>
                    </div>


                    <div class="address">
                        <?= $user->adresse ?>
                        <div class="map-user">
                            <span class="map-home"><?= Html::img(Yii::$app->request->baseUrl . '/images/map.png', ["width" => "100%", "class" => "img-responsive map-image"]); ?></span>

                            <span id="other-date-cat"
                                  class=" other-date-page-region">
                                <?php if (ArrayHelper::getValue($user, 'city_id')): ?>
                                    <a href="<?= Url::toRoute(['/category/get-map/', 'id' => $user->id, 'type' => 'user']) ?>"
                                       target="_blank"
                                       data-id='<?= $user->id ?>' class="get-map">
                                        <?= yii::t('app', 'Look at the map'); ?>
                                </a>
                                <?php endif; ?>
                        </span></div>
                    </div>

                    <?php if (!Yii::$app->user->isGuest):
                        $userFollow = Config::checkUserFollow($user->id);
                        ?>
                        <div class="follow-user-div">
                            <a href="#" class="follow-user" data-id="<?= $user->id ?>">
                                    <span class="icon">
                                        <i class="fa fa-<?= ($userFollow) ? 'check' : 'plus' ?>" aria-hidden="true"></i>
                                    </span>
                                <span class="text-follow">
                                        <?= ($userFollow) ? Yii::t('app', 'Follower') : Yii::t('app', 'Follow') ?>
                                    </span>
                            </a>
                        </div>
                    <?php endif; ?>

                    <div class="phone">
                        <?= Yii::t('app', 'Tel:') ?>
                        <?= ArrayHelper::getValue($user, 'phone') ?>
                    </div>

                    <div class="with-us">
                        <?= Yii::t('app', 'With us`') ?>
                        <?= date('Y-m-d', $user->created_at) ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div id="adform-adbox-huh0hgcwrc"
             class="adform-adbox adform-adbox-static adform-adbox-static-T adform-adbox-static-I adform-domevents-hover-3wmsj2kepuo adform-domevents-hover-32msd5hvutk adform-domevents-click-99z6lng0v4"
             style="    margin-top: 18px;width: 290px; height: 250px; left: 0px; top: 0px;">
            <div style="position: relative; width: 100%; height: 100%; overflow: hidden;">
                <iframe width="300" height="250" src="javascript:'';"
                        srcdoc="<!DOCTYPE html><title>ad</title><base href='https://s2.adform.net/Banners/Elements/Files/428/4002052/bvpath_258/'><script>try{parent.AdformWinl44gb67ba0(window)}catch(ex){new Image().src='https://track.adform.net/jslog/?src=htmlcb&amp;msg='+encodeURIComponent(''+(ex.stack||ex))}</script><script src='https://s2.adform.net/Banners/Elements/Files/428/4002052/4002052.js?ADFassetID=4002052&amp;bv=258' charset='UTF-8'></script>"
                        allowtransparency="true" webkitallowfullscreen="" mozallowfullscreen=""
                        allowfullscreen=""
                        marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0"
                        scrolling="no"></iframe>
            </div>
            <div style="position: absolute; width: 100%; height: 100%; overflow: hidden; top: 0px; left: 0px; z-index: -1;">
                <a href="https://adclick.g.doubleclick.net/pcs/click?xai=AKAOjsu4yYYVMVfQ5TZam5EUvSjSENBxtEk0HQuKdxKkn0oETNxoTbu6YxCUSLyOgcKSO--hi7v4ACCYwI2lkvTyxgmtQpF56SpdgfEyg0p2HbBIz3FkjU8s9m3_vjaIMOMgCDEFSpTrSK1Ije-uSAGUeACHFgRkjEPKzD-ZY7yKqeiOG-yyijFMBQstbjbARhbF8gbbs15pXiLony5BeyJDQZRY8AL0FsHu3-uNSgJnhm3WI1BrJN1kKpaXSWwV9CYA5UtOgs0Q6A&amp;sai=AMfl-YQ4BeiSoCC7s2QMznn3GtE3VOx4x6cBT9HO_QBf-H_M3WdPAkEtMuLeHjjJ5o0fNVJ9_f4bKcNOhP0VFg8FCzaAHJOrKTQgoWFGFD0k6-8zy_yXnjH3SJ1grjIs&amp;sig=Cg0ArKJSzMCwGVeWqxJkEAE&amp;urlfix=1&amp;adurl=https://track.adform.net/C/?bn=22965829;adfibeg=0;cdata=IOy3hxW-8C_GJIyoiE9h2796NsOawyMOuf9qhOc7-w52_LWnYY-1E2Fh8XFAkx-0dGE_jH0cjmJCCimGjVLDOn9A0wbA14OqdMvVaVqwzKOvPCrSsv5_L0T8zZtcN9GrQeEimShqzcc1;;fc=1;CREFURL=https%3a%2f%2fwww.dba.dk"
                   style="width:1px;height:1px;display:block;border:0;"></a></div>
        </div>
        <iframe class="blog-content"
                src="https://guide.dba.dk/widget/article/?path=1/1/40520&amp;marketing_referrer=dba"
                scrolling="no"
                style="margin-top:20px;border: none; height: 250px; overflow: hidden; width: 290px;"></iframe>


    </div>
    <div class="col-md-8 text-left">
        <div class="profile-user-index">
            <h1><?= Html::encode($this->title) ?></h1>
            <div class="tab-content">
                <div id="catlist">
                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'itemView' => 'product_list',
                        'viewParams' => [
                            'fullView' => true,
                            'context' => 'main-page',
                        ],
                        'layout' => "<div class='item-all'>{items}</div>\n<div class='link-page'>{pager}</div>",
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>


