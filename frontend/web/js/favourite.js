$(function () {


    $("[data-toggle='tooltip']").tooltip();


    $(document).on('click','#addToFavoritesLink,.product .favourite,.slider-parent-block .favourite,.slider.responsive-slider .favourite,.favourite-profile',function (e) {

        var cart = $('.favorite-basket');
        var imgtodrag = $(this).find('.svg-favorite').eq(0);

        e.preventDefault();
        var e = $(this);
        var id = e.attr('data-id');
        var requestValue = e.attr('data-favourite');
        var isAdded = 0;
        if (parseInt(requestValue) == 0) {
            isAdded = 1;
        } else if (parseInt(requestValue) == 1) {
            isAdded = 0;
        }
        $.ajax({
            url: '/site/addfavourite',
            type: 'post', // performing a POST request
            data: {
                id: id,
                isAdded: isAdded
            },
            dataType: 'json',
            success: function (data) {
                if (data) {
                    e.attr("data-favourite", data.isAdded);

                    if (data.isAdded == 1) {
                        if (!e.hasClass('trackClicks')) {
                            e.find('i').addClass('fa-star');
                            e.find('i').removeClass('fa-star-o');
                        } else {
                            if (imgtodrag) {
                                var imgclone = imgtodrag.clone()
                                    .offset({
                                        top: imgtodrag.offset().top,
                                        left: imgtodrag.offset().left
                                    })
                                    .css({
                                        'opacity': '0.5',
                                        'position': 'absolute',
                                        'height': '150px',
                                        'width': '150px',
                                        'z-index': '100'
                                    })
                                    .appendTo($('body'))
                                    .animate({
                                        'top': cart.offset().top + 10,
                                        'left': cart.offset().left + 10,
                                        'width': 75,
                                        'height': 75
                                    }, 1000, 'easeInOutExpo');

                                setTimeout(function () {
                                    cart.effect("shake", {
                                        times: 2
                                    }, 200);
                                }, 1500);

                                imgclone.animate({
                                    'width': 0,
                                    'height': 0
                                }, function () {
                                    $(this).detach()
                                });
                            }
                        }
                        getFavoriteSum($sum = true);
                    } else if (data.isAdded == 0) {
                        if (!e.hasClass('trackClicks')) {
                            e.find('i').removeClass('fa-star');
                            e.find('i').addClass('fa-star-o');
                        } else {
                            getFavoriteSum($sum = false);
                        }
                    }
                }
            }
        });
    });
})


function getFavoriteSum($sum) {

    var attr1 = $('#saved-ads').attr('data-original-title');
    var attr = $('#saved-ads').attr('title');
    var attr2 = '';
    var titleAttr = '';

    if (typeof attr !== typeof undefined && attr !== false) {
        attr2 = attr;
        titleAttr = 'title';

    } else if (typeof attr1 !== typeof undefined && attr1 !== false) {
        attr2 = attr1;
        titleAttr = 'data-original-title';
    }


    var title = attr2.split(" ");

    var text = "";
    if ($sum == true) {
        if (title.length > 0) {
            if (typeof title[0] !== "undefined") {
                if (title[0] >= 0) {
                    for (var i = 0; i < title.length; i++) {
                        title[0] = parseInt(title[0]) + parseInt(1);
                        text += title[i] + " ";
                    }
                    $("#saved-ads").attr(titleAttr, text);
                    $("#saved-ads").removeClass('disabled');
                    $("#saved-ads").addClass('active');
                }
            }
        }
    } else if ($sum == false) {
        if (title.length > 0) {
            if (typeof title[0] !== "undefined") {
                if (title[0] > 1) {
                    for (var i = 0; i < title.length; i++) {
                        title[0] = parseInt(title[0]) - parseInt(1);
                        text += title[i] + " ";
                    }
                    $("#saved-ads").attr(titleAttr, text);
                } else {
                    $("#saved-ads").removeClass('active');
                    $("#saved-ads").addClass('disabled');
                    var path = window.location.pathname;
                    var page = path.split("/").pop();
                    if (page == "preferred") {
                        window.location.href = '/';
                    }
                }
            }
        }
    }
}


