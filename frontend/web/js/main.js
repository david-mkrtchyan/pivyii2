
$(document).ready(function(){


    $( ".mobile-search" ).click(function() {
        $('.mobile-autocomplate').toggleClass('active');
    });


    if($('#signupform-city_id').length){
        getGeoSelect2('#signupform-city_id');
    }

    if($('#productaddress-city_id').length){
        getGeoSelect2('#productaddress-city_id');
    }

    $( ".srach-category" ).click(function() {
        $('.country-list-select').toggleClass('active');
        if($('.country-list-select.active').length){
            $('.adv-search-device .select2-selection__rendered').trigger('click');
        }
    });



    function getGeoSelect2($id) {
        $($id).select2({
            ajax: {
                url: function () {
                    return '/geo/ajax-city/';
                },
                dataType: 'json',
                delay: 250,
                placeholder: "Please, enter Location...",
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 100) < data.total_count
                        }
                    };
                },
                cache: true
            },
            width: '',
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 2
        });
    }


    $('.catalog').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: false,
        disableLink: true,
        showCount: true,
        speed: 300
    });


    if(document.getElementById('lightgallery')) {
        $('#lightgallery').lightGallery();

        $('.gallery-zoom').click(function (e) {
            e.preventDefault();
            var zomId = $(this).find('.icon-zoom-in').attr('id');
            if ($('#lightgallery').find('.' + zomId)) {
                $('#lightgallery').find('.' + zomId).trigger('click');
            }
        })
    }

    $( document ).on('click',"#slide-nav .mobile-search",function() {
        $( ".mobile-search-two" ).toggle( "active", function() {
        });
    });

    if ($(window).width() > 767) {

        $('.top-header ul.nav li.dropdown').hover(function() {
            $(this).find('.dropdown-toggle').css({'background':'#015cc1','color':'white'});
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(400);
        }, function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(400);
            $(this).find('.dropdown-toggle').css({'background':'#006ce3','color':'black'});
        });
    }

    $('#imageGallery').hover(function() {
        $('#imageGallery .icon-zoom-in').css('opacity', '1');
    }, function() {
        $('#imageGallery .icon-zoom-in').css('opacity', '0');
    });



    $(function () {
        $('.phone').click(function () {
            $('#modal').modal('show');
        });
    });

    $(function () {
        $('.email').click(function () {
            $('#modalemail').modal('show');
        });
    });

    $(function () {
        $('.complain').click(function () {
            $('#modal').modal('hide');
            $('#complain').modal('show');
        });
    });


    $('.lslide').mouseover(function( ) {
        $('.lslide').find('a').css({'display':'block'});
    });

    $(".lslide").mouseover(function()
    {
        $(this).find('a').show();
    }).mouseout(function()
    {
        $(this).find('a').show();
    });




    $('.search_m_menu').click(function(){ $('.navbar-toggle').click(), $('.mobile-search').click()});


    //stick in the fixed 100% height behind the navbar but don't wrap it
    $('#slide-nav.navbar-inverse').after($('<div class="inverse" id="navbar-height-col"></div>'));
    $('#slide-nav.navbar-default').after($('<div id="navbar-height-col"></div>'));

    // Enter your ids or classes
    var toggler = '.navbar-toggle';
    var toggler_search = '.search_m_menu';
    var pagewrapper = '#page-content';
    var navigationwrapper = '.navbar-header';
    var menuwidth = '80%'; // the menu inside the slide menu itself
    var slidewidth = '80%';
    var menuneg = '-80%';
    var slideneg = '-80%';




    $("#slide-nav").on("click", toggler, function (e) {

        var selected = $(this).hasClass('slide-active');

        $('#slidemenu').stop().animate({
            left: selected ? menuneg : '0px'
        });

        $(pagewrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });

        $(navigationwrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });



        $(this).toggleClass('slide-active', !selected);
        $('#slidemenu').toggleClass('slide-active');

        $('#page-content, .navbar, body, .navbar-header').toggleClass('slide-active');
        $('html').toggleClass('main');

    });


    var selected = '#slidemenu, #page-content, body, .navbar, .navbar-header';


    $(window).on("resize", function () {
        if ($(window).width() > 767 && $('.navbar-toggle').is(':hidden')) {
            $(selected).removeClass('slide-active');
            $(selected).removeClass('main');
        }
    });



});
