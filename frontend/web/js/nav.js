$(document).ready(function () {
    $('.search_m_menu').click(function(){ $('.navbar-toggle').click(), $('.mobile-search').click()});


    //stick in the fixed 100% height behind the navbar but don't wrap it
    $('#slide-nav.navbar-inverse').after($('<div class="inverse" id="navbar-height-col"></div>'));
    $('#slide-nav.navbar-default').after($('<div id="navbar-height-col"></div>'));

    // Enter your ids or classes
    var toggler = '.navbar-toggle';
    var toggler_search = '.search_m_menu';
    var pagewrapper = '#page-content';
    var navigationwrapper = '.navbar-header';
    var menuwidth = '85%'; // the menu inside the slide menu itself
    var slidewidth = '85%';
    var menuneg = '-85%';
    var slideneg = '-85%';




    $("#slide-nav").on("click", toggler, function (e) {

        var selected = $(this).hasClass('slide-active');

        $('#slidemenu').stop().animate({
            left: selected ? menuneg : '0px'
        });

        $(pagewrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });

        $(navigationwrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });



        $(this).toggleClass('slide-active', !selected);
        $('#slidemenu').toggleClass('slide-active');

        $('#page-content, .navbar, body, .navbar-header').toggleClass('slide-active');
        $('html').toggleClass('main');

    });


    var selected = '#slidemenu, #page-content, body, .navbar, .navbar-header';


    $(window).on("resize", function () {

        if ($(window).width() > 767 && $('.navbar-toggle').is(':hidden')) {
            $(selected).removeClass('slide-active');
            $(selected).removeClass('main');
        }


    });


    /*$(".fa-search").click(function(){
        $("body").addClass("slide-active");
        $("html").addClass("main");
    });

    $("button.close").click(function(){
        $("body").removeClass("slide-active");
        $("html").removeClass("main");
    });*/



});
