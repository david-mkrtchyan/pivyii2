<?php

namespace frontend\widgets;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class AlertWidget extends \yii\bootstrap\Widget
{
    public static $class = [
        'success' => 'success',
        'info' => 'info',
        'warning' => 'warning',
        'danger' => 'danger',
        'errors' => 'danger',
    ];


    public function run()
    {

        $classList = [];
        if(Yii::$app->session->getAllFlashes()){
            foreach (Yii::$app->session->getAllFlashes() as $key => $message){
                if(ArrayHelper::getValue(self::$class,$key)){
                    $classList[$key] = $message;
                }
            }
        }

        return $this->render('alert',[
            'classes' => $classList
        ]);
    }

}