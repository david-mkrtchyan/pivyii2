<?php

namespace frontend\widgets;

use common\models\Category;
use common\models\Product;
use common\models\ProductField;
use frontend\models\ProductAddress;
use yii\web\NotFoundHttpException;

class AsideCategoryWidget extends \yii\bootstrap\Widget
{
    public $slug;

    public function run(){

        $category = Category::findOne(['slug' => htmlentities($this->slug)]);

        $country = ProductAddress::find()->groupBy('city_id')->all();

        if (!$category) throw new NotFoundHttpException();

        return $this->render('aside-category',[
            'country' => $country ,
            'children' => $category->childs,
        ]);
    }

}