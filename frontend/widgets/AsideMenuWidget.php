<?php

namespace frontend\widgets;

use common\models\Category;

class AsideMenuWidget extends \yii\bootstrap\Widget
{
    public function run(){

        $categories = Category::find()->where(['parent_id' => 0])->orderBy('rank asc')->all();

        return $this->render('aside', compact('categories'));
    }

}