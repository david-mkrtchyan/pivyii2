<?php


namespace frontend\widgets;

use common\models\BannerCountry;
use common\models\Config;
use common\models\GeoCity;
use common\models\GeoCountry;
use common\models\Users;
use Symfony\Component\Console\Tests\Helper\HelperSetTest;
use yii\base\Widget;
use Yii;
use common\models\Banner;
use common\models\BannerPages;
use common\models\BannerPlacement;
use common\models\Settings;
use yii\db\Expression;
use yii\helpers\ArrayHelper;


class BannerWidget extends Widget
{
    public $position;
    public $width;
    public $height;
    public $device;
    public $banner_page_name;
    public $page;
    public $name;

    public function init()
    {
        $this->page = BannerPages::find()->where(['name' => $this->banner_page_name])->one();
        if (!$this->page) {
            return false;
        }

        $this->position = $this->getposition()[$this->position];
        $this->device = $this->getBannerMobile()[$this->device];

        if ($this->position !== 0 && $this->position !== 1 && $this->position !== 2 && $this->position !== 3 && $this->position !== 4) {
            return false;
        }
        if ($this->device !== '0' && $this->device !== '1' && $this->device !== '2') {
            return false;
        }

    }

    public function run()
    {
        $page = ArrayHelper::getValue($this->page,'id');
        $bannerPlacement = BannerPlacement::find()->where(['status' => '1', 'position' => $this->position, 'is_mobile' => $this->device, 'banner_pages_id' => $page, 'width' => $this->width, 'height' => $this->height ])->one();

        if (!$bannerPlacement) return false;
        $banners = Banner::find()->where(['banner_page_id' => $this->page->id, 'banner_placement_id' => $bannerPlacement->id, 'is_active' => '1'])->all();
        if (!$banners) return false;
        $bannerAllowId = [];

        $countryId = $this->getCountryId($banners);
        if (!$countryId) return false;
        $geoBanners = Banner::find()->where(['id' => $countryId])->all();
        foreach ($geoBanners as $banner) {
            $bannerRules = $banner->bannerRules[0];
            $bannerStatSum = \common\models\BannerStat::find()->select('sum(shows_count) as shows_count')->where(['banner_id' => $banner->id])->asArray()->one();

            $limitEndDate = strtotime($bannerRules->limit_end_date);
            $dateNow = strtotime(date('d-m-Y'));
            $bannerLimitDays = strtotime('+' . $bannerRules->limit_days . ' day', $banner->updated_at);

            if (
                ($limitEndDate < 0 || ($dateNow <= $limitEndDate)) &&
                (($bannerRules->limit_days === null && $bannerRules->limit_days == 0) || ($bannerRules->limit_days && ($dateNow <= $bannerLimitDays))) &&
                ($bannerRules->limit_count=== null || ($bannerStatSum['shows_count'] < $bannerRules->limit_count))
            ) {
                $bannerAllowId[] = $banner->id;
            }
        }
        $banners = Banner::find()->where(["id" => $bannerAllowId])->all();

        //$banners = Banner::find()->where(["id" => $bannerAllowId])->orderBy(new Expression('rand()'))->limit(1)->one();

        return $this->render('banner', [
            'bannerPlacement' => $bannerPlacement,
            'banners' => $banners
        ]);
    }

    public function getCountryId($banners)
    {
        $countryId = [];
        $countryDefaultId = [];
        $id = [];

        $user = Config::getGeoCity();
        if(!$user) return;
        $first_key = key($user);


        foreach ($banners as $banner) {
            $banner->bannerRules[0]->limit_days;
            $city = GeoCity::find()->where(['geonameid' => $first_key])->one();
            if(!$city) return;
            $country = GeoCountry::find()->where(['fips_code'=> $city ])->one();
            if(!$country) return;
            $bannerCountry = BannerCountry::find()->where(['country_include_id' => $country->geonameId, 'banner_id' => $banner->id])->asArray()->all();
            $bannerCountryDefault = BannerCountry::find()->where(['banner_id' => $banner->id])->asArray()->all();
            if ($bannerCountry) {
                foreach ($bannerCountry as $key) {
                    if (in_array($country->geonameId, $key)) {
                        $countryId[$key['banner_id']] = $key['banner_id'];
                    }
                }
            }/*elseif($bannerCountryDefault){
                foreach ($bannerCountryDefault as $value) {
                    $countryDefaultId[$value['banner_id']] = $value['banner_id'];
                }
            }*/
            elseif (!$bannerCountryDefault) {
                $countryDefaultId[$banner->id] = $banner->id;
            }
        }
        if ($countryId) {
            $id = $countryId;
        } elseif ($countryDefaultId) {
            $id = $countryDefaultId;
        }
        return $id;
    }


    public function getposition()
    {
        return [
            'Left' => 0,
            'Right' => 1,
            'Bottom' => 2,
            'Top' => 3,
            'Middle' => 4,
        ];
    }

    public function getBannerMobile()
    {
        return [
            'Desktop' => '0',
            'Tablet' => '1',
            'Mobile' => '2',
        ];
    }
}
