<?php

namespace frontend\widgets;

use common\models\Category;
use yii\web\NotFoundHttpException;

class CategoryFieldWidget extends \yii\bootstrap\Widget
{
    public $slug;

    public function run(){

        $category = Category::findOne(['slug' => htmlentities($this->slug)]);

        if (!$category) throw new NotFoundHttpException();

        return $this->render('category-field-index',[
            'children' => $category->childs,
            'category' => $category,
        ]);
    }

}