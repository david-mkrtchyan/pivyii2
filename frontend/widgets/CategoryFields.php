<?php

namespace frontend\widgets;

class CategoryFields extends \yii\bootstrap\Widget
{
    public $model;
    public $fieldModel;
    public $id;
    public $queryParams;
    public $profile;

    public function run()
    {
        //var_dump($this->profile);
        $template = $this->profile ? 'profile-category-fields' : 'category-fields';

        return $this->render($template, [
            'model' => $this->model,
            'fieldModel' => $this->fieldModel,
            'id' => $this->id,
            'queryParams' => $this->queryParams,
        ]);
    }
}