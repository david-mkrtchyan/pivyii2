<?php

namespace frontend\widgets;

class ContentWidget extends \yii\bootstrap\Widget
{
    public $content;

    public function run(){
        $content = $this->content;
        return $this->render('content', compact('content'));
    }
}