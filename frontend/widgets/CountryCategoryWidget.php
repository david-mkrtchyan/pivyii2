<?php

namespace frontend\widgets;

use common\models\Config;
use frontend\models\ProductAddress;

class CountryCategoryWidget extends \yii\bootstrap\Widget
{
    public $slug;

    public function run(){
        $country = Config::getRegionList();

        return $this->render('country-category',[
            'country' => $country
        ]);
    }

}