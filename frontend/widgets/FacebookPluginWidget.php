<?php

namespace frontend\widgets;

class FacebookPluginWidget extends \yii\bootstrap\Widget
{
    public $content;

    public function run(){
        return $this->render('facebook-plugin');
    }
}