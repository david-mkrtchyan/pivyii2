<?php

namespace frontend\widgets;

use common\models\Contact;
use common\models\Help;
use frontend\models\ContactForm;
use Yii;

class FooterWidget extends \yii\bootstrap\Widget
{
    public function run(){

        $help = Help::find()->all();

        return $this->render('footer',[
            'help' => $help,
        ]);
    }
}