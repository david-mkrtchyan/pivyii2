<?php

namespace frontend\widgets;

class GridListContentWidget extends \yii\bootstrap\Widget
{
    public $slug;

    public function run(){
        return $this->render('grid-list',[
            'slug' => $this->slug,
        ]);
    }
}