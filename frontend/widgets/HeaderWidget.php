<?php

namespace frontend\widgets;

use common\models\Config;
use common\models\Geo;
use common\models\GeoCity;
use common\models\GeoCountry;
use common\models\Product;
use Yii;
use yii\helpers\ArrayHelper;

class HeaderWidget extends \yii\bootstrap\Widget
{
    public function run(){
        $currency = Config::getCurrencyDefault();

        $regionList =  Config::getRegionList();

        $cookiesFavourite = Config::getFavorites();
        $favoriteCount = 0;
        if ($cookiesFavourite) {
            $favoriteCount = Product::find()->andWhere(['id'=> $cookiesFavourite])->count();
        }

        return  $this->render('header',
            [
                'currency'=>$currency,
                'favoriteCount' => $favoriteCount,
                'getCountryRegionList' => $regionList,
            ]
        );
    }
}