<?php

namespace frontend\widgets;

use common\models\LoginForm;
use frontend\models\SignupForm;

class LoginModalWidget extends \yii\bootstrap\Widget
{
    public function run(){
        return $this->render('login-modal',[
            'loginForm' => new LoginForm(),
            'signupForm' => new SignupForm(),
        ]);
    }
}