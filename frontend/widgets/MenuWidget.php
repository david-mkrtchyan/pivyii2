<?php

namespace frontend\widgets;

use common\models\Category;

class MenuWidget extends \yii\bootstrap\Widget
{
    public function run(){
        $categories = Category::findAll(['parent_id' => 0]);
        return $this->render('menu', compact('categories'));
    }
}