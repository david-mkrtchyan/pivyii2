<?php

namespace frontend\widgets;

class MessagesWidget extends \yii\bootstrap\Widget
{
    public $messages;
    public $type;

    public function run(){

        return $this->render('product-message', [
            'messages' => $this->messages,
            'type' => $this->type,
        ]);
    }
}