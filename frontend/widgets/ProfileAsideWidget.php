<?php
/**
 * Created by PhpStorm.
 * User: Mkrtchyan
 * Date: 6/26/2018
 * Time: 6:56 AM
 */

namespace frontend\widgets;


use common\models\Product;
use common\models\UserFollow;
use Yii;

class ProfileAsideWidget extends \yii\bootstrap\Widget
{
    public function run(){
        $user = Yii::$app->user->identity;
        $countProduct = Product::find()->where(['user_id' => $user->id])->count();
        $countProduct = $countProduct ? $countProduct : 0;
        $favoriteCount = 0;
        if ($user->social_users_favourite) {
            $favorite =  unserialize($user->social_users_favourite);
            if($favorite){
                $favoriteCount = Product::find()->andWhere(['id'=> $favorite])->count();
            }
        }

        $sellersFollowCount = UserFollow::find()->where(['sender_id' => $user->id,'follow' => UserFollow::followYes])->count();

        return $this->render('profile-aside',[
            'countProduct' => $countProduct,
            'favoriteCount' => $favoriteCount,
            'sellersFollowCount' => $sellersFollowCount,
        ]);
    }
}

