<?php
/**
 * Created by PhpStorm.
 * User: Mkrtchyan
 * Date: 6/26/2018
 * Time: 6:56 AM
 */

namespace frontend\widgets;


use common\models\Category;
use common\models\CategoryField;
use common\models\Config;

class RecentSearchWidget extends \yii\bootstrap\Widget
{
    public $id;

    public function run(){

        $search = Config::addRecentSearch();
        $results = $category = [];

        if($this->id && isset($search[$this->id])){
            $results = $search[$this->id];
            $category = Category::find()->where(['id' => $this->id])->one();
        }

        return $this->render('recent-search',[
            'results' => $results,
            'id' => $this->id,
            'category' => $category,
        ]);
    }
}

