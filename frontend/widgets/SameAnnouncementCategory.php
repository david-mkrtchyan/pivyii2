<?php
/**
 * Created by PhpStorm.
 * User: Mkrtchyan
 * Date: 6/26/2018
 * Time: 6:56 AM
 */

namespace frontend\widgets;


use common\models\Product;

class SameAnnouncementCategory extends \yii\bootstrap\Widget
{
    public $model;

    public function run(){

        $modelSame = Product::find()->where(['category_id' => $this->model->category_id, 'status' => Product::statusActive])
            ->andWhere(['not', ['id' => $this->model->id]])
            ->orderBy('created_at desc')->limit(12)->all();

        return $this->render('same-announcement',[
            'modelSame' => $modelSame
        ]);
    }
}

