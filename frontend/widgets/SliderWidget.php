<?php

namespace frontend\widgets;

class SliderWidget extends \yii\bootstrap\Widget
{
    public $data;

    public function run(){

        return $this->render('slider', [
            'data' => $this->data
        ]);
    }
}
