<?php if ($classes && is_array($classes)): ?>
    <?php foreach ($classes as $key => $value): ?>
        <div class="alert alert-<?= $key ?>">
            <?= $value; ?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
