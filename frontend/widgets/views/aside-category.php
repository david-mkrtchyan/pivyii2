<?php

use common\models\Geo;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$city_id = Yii::$app->request->get('city_id');

?>

<!--<h1 style="    text-align: center;
    margin-top: 300px;font-size: 50px">Puslapis ruošiamas</h1>-->
<div class="category-aside">

    <div class="last-field-category cat-sidebar cat-sidebar-left aside-left-category">
        <div class="recent-search"> </div>
        <?php
        $form = ActiveForm::begin([
                'method' => 'get',
                'action' => '/category/' . Yii::$app->request->get('slug')
            ]
        ); ?>
        <div class="hidden-sm hidden-xs search-block">
            <button class="search-animate clear-default" style="top: 0px;">
                <span class="search-icon"><i class="fa fa-search fa-2x"></i></span>
                <span class="btn btn-success search-btn" style="width: 0px;"><?= Yii::t('app', 'Search') ?></span>
            </button>
        </div>
        <div class="form-group">
            <label class="control-label" for="form-Color"><?= Yii::t('app', 'Categories') ?></label>
            <?= Html::dropDownList('category_wish', Yii::$app->request->get('category_wish'),
                ArrayHelper::map($children, 'id', 'name_' . Yii::$app->language), [
                    'class' => 'form-control',
                    'id' => 'categories',
                ]) ?>
        </div>
        <div class="category-content"></div>

        <input type="text" class="form-control" name="city_id" value="<?= $city_id ?>" style="display:none;">

        <?php ActiveForm::end(); ?>

    </div>
</div>


<?php
$queryParams = json_encode(Yii::$app->request->queryParams);
$script = <<< JS
 $(document).ready(function(){ 
     $(document).on('change','#categories',function(e){
  
        var catId = $(this).val();
        var queryParams = $queryParams;
        $.ajax({
              url: '/category/get-field/',
              type: 'POST',
              data: {
                  'catId' : catId,
                  'queryParams' : queryParams,
              },
              success: function(data) {
                       if(data){
                           var data = JSON.parse(data);
                           $('.category-content').html(data.data); 
                           $('.recent-search').html(data.searchList); 
                       }else{
                              $('.category-content').html(data); 
                       }
                   
                      if($('.showAll').length && $('.showAll').val() == 'true'){
                              $('.show_hide_item').trigger('click');
                      }
              },
         }); 
    });
     
    
    $(document).on('change','#category_fields_wish',function(e){
  
        var id = $(this).val();
       
        var catId = $('#categories').val();
        var queryParams = $queryParams;
        $.ajax({
              url: '/category/get-field/',
              type: 'POST',
              data: {
                  'id' : id,
                  'catId' : catId,
                  'queryParams' : queryParams,
              },
              success: function(data) {
                    
                      if(data){
                           var data = JSON.parse(data);
                           $('.category-content').html(data.data); 
                           $('.recent-search').html(data.searchList); 
                       }else{
                              $('.category-content').html(data); 
                       }
                    
                      $('.content-category-field').show(); 
                      if($('.category-content').html()){
                          $('.category-content').addClass('content-added');
                      }
                      if($('.showAll').length && $('.showAll').val() == 'true'){
                              $('.show_hide_item').trigger('click');
                      }
              },
         }); 
    });
   
   $(document).on('click','.location',function(e){
       e.preventDefault();
       var thisVal = $(this);
     if(history.pushState) {
         var url=window.location.href,
    separator = (url.indexOf("?")===-1)?"?":"&",
    newParam=separator + "cityCode_id="+$(this).attr("data-location");
    var newUrl=url.replace(newParam,"");
    newUrl+=newParam;
    window.location.href = newUrl;
     
        }
  });
    
   
   /*$(document).on('change',"input[type='checkbox']",function(e){
         $(this).closest('form').trigger('submit');
   })*/
    
  
   if( $( "#categories" ).length){
      $( "#categories" ).trigger( "change" ); 
   }
    
    $(window).on("scroll", function () {
        var windowScroll = $(window).scrollTop();
        var sidebarHeight = $(".cat-sidebar").height();

        if (windowScroll < sidebarHeight) {
            setTimeout(function () {
                $('.search-animate').animate({
                    top: windowScroll + 'px'
                }, 1, 'easeInOutCubic');
            }, 150);
        }
    });
    $(".search-animate").on("mouseenter", function () {
        $(".search-btn").animate({
            width: "100px"
        }, 200, "swing");
    });
    $(".search-animate").on("mouseleave", function () {
        $(".search-btn").animate({
            width: 0
        }, 200, "swing");
    });
    
    
     $(document).on('change',"#form-Brand",function(){
        var id = $(this).val(); 
        $.ajax({
             type: "post",
             url:  "/profile/category/get-model-list",
             data: {
                id: id
             },
             dataType: "html",
             success: function(data) {
                $("#form-Model").html(data);
             },
         });
    })
        
}); 
JS;
$this->registerJs($script);
?>

