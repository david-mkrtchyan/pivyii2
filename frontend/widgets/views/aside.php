<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \common\models\Category $categories */
?>


<aside class="sidebar sidebar-left aside-sidenav-menu">
    <nav class="menuContainer" data-menu="hover">
        <p class="muted categories-title"></p>
        <ul class="taxonomy-navigation">
            <?php foreach ($categories as $category): ?>
            <li class="single item-show">
                <a href="<?= Url::to(['/category/'.$category->slug])?>" title="<?= $category['name_'.Yii::$app->language] ?>"  class="trackClicks">
                    <?= \common\models\Config::getIcon($category->icon,'icon-sidebar circle'); ?>
                    <?= $category['name_'.Yii::$app->language] ?></a>
            </li>
            <?php endforeach; ?>
        </ul>
    </nav>
</aside>