<?php
/**
 * Created by PhpStorm.
 * User: Zakar
 * Date: 7/11/2017
 * Time: 12:52 PM
 */
?>

<?php if ($banners && $bannerPlacement): ?>
    <?php foreach ($banners as $banner): ?>
        <div class="banner-widget custom-class"
             style="width:<?= $bannerPlacement->width; ?>px;height: <?= $bannerPlacement->height; ?>px;overflow: hidden">
            <div class="banner-content">

                <?php if ($banner->bannerRules[0]->file_type && $banner->bannerRules[0]->file_type == 'image') { ?>
                    <a href="<?= $banner->destination_url ?>" class="banner-url-widget" data-id="<?= $banner->id; ?>">
                        <img class="banner-image" src="<?php echo '/banners/' . $banner->content ?>">
                    </a>
                    <div class="close-banner close">
                        <button class="btn  close-button close-button-widget"></button>
                    </div>
                    <!--<div class="banner-layout">
                        Скрыть рекламу:
                    </div>-->
                <?php } ?>

                <?php
                if ($banner->bannerRules[0]->file_type && $banner->bannerRules[0]->file_type == 'video') { ?>
                    <a href=" <?= $banner->destination_url ?>" class="banner-url-widget" data-id="<?= $banner->id; ?>">
                        <video width="<?= $bannerPlacement->width; ?>" height="<?= $bannerPlacement->height; ?>"
                               autoplay> <!-- controls-->
                            <source src="<?php echo '/banners/' . $banner->content ?>"
                                    type="<?= $banner->file_extension_type; ?>">
                        </video>
                    </a>
                    <div class="close-banner close">
                        <button class="close-button close-button-widget"></button>
                    </div>
                <?php } ?>

                <?php
                if ($banner->bannerRules[0]->file_type && $banner->bannerRules[0]->file_type == 'flash') { ?>
                    <a href="<?= $banner->destination_url ?>" class="banner-url-widget" data-id="<?= $banner->id; ?>">
                        <object width="<?= $bannerPlacement->width; ?>" height="<?= $bannerPlacement->height; ?>">
                            <param name="movie" value="<?php echo '/banners/' . $banner->content ?>">
                            <embed src="<?php echo '/banners/' . $banner->content ?>"
                                   width="<?= $bannerPlacement->width; ?>" height="<?= $bannerPlacement->height; ?>">
                            </embed>
                        </object>
                    </a>
                    <div class="close-banner close">
                        <button class="close-button close-button-widget"></button>
                    </div>
                <?php } ?>

                <?php
                if ($banner->bannerRules[0]->file_type && $banner->bannerRules[0]->file_type == 'code') { ?>
                    <a href="<?= $banner->destination_url ?>" class="banner-url-widget" data-id="<?= $banner->id; ?>">
                        <?php echo $banner->content; ?>
                    </a>
                    <div class="close-banner close">
                        <button class="close-button close-button-widget"></button>
                    </div>
                <?php } ?>


            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>