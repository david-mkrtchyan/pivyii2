<?php

use common\models\Category;
use common\models\CategoryField;
use common\models\Config;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;


$modelCategoryFields = $model->categoryFields;
$category_id = $model->category_id;

$controller = Yii::$app->controller->id;
$class = $showPosition = null;
$showTabs = false;
$firstShow = $secondShow = $cityCode_id = $imageChecked = null;

$queryParamsData = json_decode($queryParams);

if (isset($queryParamsData->cityCode_id) && isset($queryParams->cityCode_id) &&
    property_exists($queryParamsData, 'cityCode_id')) {
    $cityCode_id = $queryParams->cityCode_id;
}

$showAll = false;
if (property_exists($queryParamsData, 'showAll')) {
    $showAll = \yii\helpers\ArrayHelper::getValue($queryParamsData, 'showAll');
}

if (property_exists($queryParamsData, 'category_fields_wish_id') && !$id) {
    $id =  \yii\helpers\ArrayHelper::getValue($queryParamsData, 'category_fields_wish_id');
}

if (property_exists($queryParamsData, 'imageChecked')) {
    $imageChecked = true;
}

if ($modelCategoryFields):

    if (isset($id) && !is_null($id)) {
        $modelCategoryFields = $model->getCategoryFields($id);
    }
    $count = 2; ?>

    <div class="<?= "show  field-alert" ?>  clearfix">
        <div class="form-group">
            <label class="control-label" for="form-Price"><?= Yii::t('app', 'Wish') ?></label>
            <div class="row">
                <div class="col-md-12">
                    <?= Html::dropDownList('category_fields_wish_id', [$id => $id],
                        ArrayHelper::map(ArrayHelper::getValue(Category::findOne($category_id),'categoryFieldsWish'),'id','value_'.Yii::$app->language), [
                            'class' => 'form-control',
                            'id' => 'category_fields_wish',
                        ])
                    ?>
                </div>
            </div>
        </div>
    </div>


    <div class="<?= "show  field-alert" ?>  clearfix">
        <div class="form-group">
            <label class="control-label" for="form-Price"><?= Yii::t('app', 'Location') ?></label>
            <div class="row">
                <div class="col-md-12">
                    <?= Html::dropDownList('cityCode_id', $cityCode_id,
                        ['' => Yii::t('app', 'Select Location')] + Config::getRegionList(), [
                            'class' => 'form-control',
                            'id' => 'categories',
                        ])
                    ?>
                </div>
            </div>
        </div>
    </div>

    <?php
    foreach ($modelCategoryFields as $categoryField) :

        if ($categoryField->type == CategoryField::typeSelect) {
            $showPosition = 'select';
            $content = $this->render('/category-field/field-ajax/select', ['categoryField' => $categoryField, 'model' => $model, 'fieldModel' => $fieldModel, 'queryParams' => $queryParams]);
        } elseif ($categoryField->type == CategoryField::typeCheckbox) {
            $content = $this->render('/category-field/field-ajax/checkbox', ['categoryField' => $categoryField, 'model' => $model, 'fieldModel' => $fieldModel, 'queryParams' => $queryParams]);
            $showPosition = 'checkbox';
        } elseif ($categoryField->type == CategoryField::typeRadio) {
            $content = $this->render('/category-field/field-ajax/radio', ['categoryField' => $categoryField, 'model' => $model, 'fieldModel' => $fieldModel, 'queryParams' => $queryParams]);
        } elseif ($categoryField->type == CategoryField::typeInput) {
            $showPosition = 'radio';
            $content = $this->render('/category-field/field-ajax/input', ['categoryField' => $categoryField, 'model' => $model, 'fieldModel' => $fieldModel, 'queryParams' => $queryParams]);
        } elseif ($categoryField->type == CategoryField::typeTextArea) {
            $showPosition = 'textArea';
            $content = $this->render('/category-field/field-ajax/textArea', ['categoryField' => $categoryField, 'model' => $model, 'fieldModel' => $fieldModel, 'queryParams' => $queryParams]);
        }

        if ($content) {
            if ($controller == 'category') {
                $showTabs = ($count > 4) ? true : false;
                $class = ($count > 4) ? "hide  field-alert" : "show  field-alert";
                if ($showPosition == 'checkbox') {
                    $class = "hide  field-alert";
                    $secondShow .= '<div class="' . $class . ' clearfix">' . $content . '</div>';
                } else {
                    $firstShow .= '<div class="' . $class . ' clearfix">' . $content . '</div>';
                    $count++;
                }
            }
        }

        ?>

    <?php endforeach; ?>


    <?PHP
    echo $firstShow;
    ?>

    <div class="<?= "hide  field-alert" ?> field-image  clearfix">
        <div class="form-group">
            <label class="control-label" for="form-Price"><?= Yii::t('app', 'Ads with photos') ?></label>
            <div class="row imageChecked-checkbox">
                <div class="col-md-12">
                    <?= Html::checkbox('imageChecked', $imageChecked,
                        [
                            'id' => 'imageChecked'
                        ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

    <?= $secondShow ?>


    <div class="group-button-category clearfix">

        <div class="form-group search-button">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn  btn-primary']) ?>
        </div>


        <?php   if ($controller == 'category' && $showTabs == true) {  ?>

            <li id="section-toggle" class="single">
                <a href="#" class="show_hide_item">
                    <i class="svg-icon-16 svg-list"></i>
                    <span data-expanded-text="Vis færre"><?= Yii::t('app', 'Show all tabs') ?></span>
                    <input type="text" class="showAll" name="showAll"
                           value="<?= ($showAll == 'true') ? "$showAll" : 'false'; ?>" style="display:none;">
                    <!--<i class="svg-arrow"></i>-->
                </a>
            </li>

        <?php } ?>
    </div>
<?php endif; ?>
