<?php

use common\models\Geo;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$city_id = Yii::$app->request->get('cityCode_id');
?>

<?php if ($country): ?>
    <div class="category-aside country-aside">
        <div class="last-country-category">
            <p class="title-country-category title-list">
                <?= Yii::t('app', 'Where is the product?') ?>
            </p>
            <ul class="list-country-list">
                <?php foreach ($country as $key=>$value):  ?>
                    <li class="<?= ($city_id == $key) ? 'active' : null ?>">
                        <div class="group-geo-map">
                            <a href="<?= Url::toRoute(['/category/get-map/', 'id' =>$key, 'type' => 'city']) ?>"
                               target="_blank"
                               data-id='<?= $key ?>' class="get-map">
                            <span class="map-home <?= ($city_id == $key) ? 'active' : null ?>">
                               <i class="material-icons">place</i>
                            </span>
                            </a>

                            <a href="<?= Url::to(['/category/index', 'id' => $key]) ?>" class="location"
                               data-location="<?= $key ?>">
                                </span><?= $value; ?></span>
                            </a>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>

<?php endif; ?>