<?php

use common\models\Config;
use common\models\MobileDetect;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="mobile-version" style="">
    <div class="list-grid clearfix">
        <div class="mobile-view">
            <a href="<?= Url::toRoute(['/?typeResponse=mobile']) ?>" class="">
                <?= Yii::t('app', 'Mobile Version') ?>
            </a>
            <a href="<?= Url::toRoute(['/?typeResponse=desktop']) ?>" class="">
                <?= Yii::t('app', 'Desktop Version') ?>
            </a>
        </div>
    </div>
</div>

<footer class="container-fluid text-center">
    <div class="row header">
        <div class="col-lg-4 col-md-4 col-sm-4  col-xs-12 text-left">
            <?= Yii::t('app', '© '.date('Y').' Piv.lt') ?>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8  col-xs-12 text-left">
            <ul class="nav " style="    float: right;">
                <?php if ($help): ?>
                    <?php foreach ($help as $value): ?>
                        <li class="footer-title"><strong>
                                <a href="<?= Url::toRoute(['/help/'.$value->slug]) ?>">
                                    <?= Yii::t('app', $value['page_name_'.Yii::$app->language]) ?>
                                </a>
                            </strong>
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>

    </div>
</footer>

<?php
        $detect = new MobileDetect();
        if ($detect->isMobile() || $detect->isTablet()):
            $this->registerCss('.mobile-version{display:block}');
        ?>
 <?php endif; ?>