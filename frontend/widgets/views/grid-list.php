<?php

use common\models\Config;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

if($searchQ = Yii::$app->request->get('q')){
    $searchQ = '&q='.Yii::$app->request->get('q');
}
$showType = Yii::$app->session->get('showType');
$list = ArrayHelper::getValue(Config::getShowType(), 'list');
$grid = ArrayHelper::getValue(Config::getShowType(), 'grid');
?>
<div class="list-grid clearfix">
    <div class="grid-view list-view">
        <a href="<?= Url::to("$slug?showType=".$list.$searchQ) ?>"
           class="<?= $showType == $list ? 'active' : null ?>">
            <span class="svg-icon-16 svg-gallery pull-left" title="Vis annoncer som galleri"></span>
            <?= Yii::t('app','List View')?>
        </a>
        <a href="<?= Url::to("$slug?showType=".$grid.$searchQ) ?>"
           class="<?= $showType == $grid ? 'active' : null ?>">
            <span class="svg-icon-16 svg-list" title="Vis annoncer som liste"></span>
            <?= Yii::t('app','Grid View')?>
        </a>
    </div>
</div>