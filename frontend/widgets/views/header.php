<?php

use common\models\Config;
use frontend\widgets\AsideMenuWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\widgets\ActiveForm;

$sessionLang = null;
if (isset($_SESSION['language']) && $_SESSION['language']) {
    $sessionLang = ArrayHelper::getValue(Yii::$app->params['languages_long'], $_SESSION['language']);
}
$currencyName = Yii::$app->session->get('currency');
$cookiesFavourite = null;

$disabledClass = ($favoriteCount == 0) ? 'disabled' : '';

?>


<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1&appId=1395946733883770&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>


<nav class="navbar navbar-expand-lg navbar-light bg-light navbar-inverse header-nav">
    <div class="row header-content">

        <div class="col-md-3">
            <div class="navbar-header">
                <button class="navbar-toggler navbar-toggler-content" type="button" data-toggle="collapse"
                        data-target="#navbarNavDropdown"
                        aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="logo" href="/">  <?= Html::img('/images/logo2.png') ?></a>
            </div>
        </div>


        <div class="col-md-9" id="navbarNavDropdown">
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav navbar-right nav social-buttons">
                    <li class="<?php // Config::getLangActive('lt') ?>">
                        <a href="<?= Url::to(['/site/index', 'language' => 'lt']) ?>"><i
                                    class="flag flag-18 flag-18-lt"></i>
                        </a>
                    </li>

                    <li class="<?php //  Config::getLangActive('en') ?>">
                        <a href="<?= Url::to(['/site/index', 'language' => 'en']) ?>"><i
                                    class="flag flag-18 flag-18-en"></i>
                        </a>
                    </li>
                    <li class="<?php // Config::getLangActive('ru') ?>">
                        <a href="<?= Url::to(['/site/index', 'language' => 'ru']) ?>"><i
                                    class="flag flag-18 flag-18-ru"></i>
                        </a>
                    </li>


                    <li class="nohover dropdown">
                        <a id="saved-ads" class="<?= $disabledClass ?>"
                           data-toggle="tooltip" data-placement="bottom"
                           data-original-title="<?= $favoriteCount . ' ' . yii::t('app', 'saved-ads') ?>"
                           href="<?= Yii::$app->user->isGuest ? Url::to('/category/preferred/') : Url::to('/profile/preferred') ?>"
                           title="<?= $favoriteCount . ' ' . yii::t('app', 'Preferred') ?>">
                            <img class="favorite-basket" src="/images/star.png" alt="/images/star.png">
                        </a>
                    </li>

                    <li class="nohover dropdown">
                        <a data-toggle="tooltip" data-placement="bottom"
                           data-original-title=""
                           href="<?= Url::to('/category/last-viewed/') ?>"
                           title="<?= yii::t('app', 'Last Watched Announcement') ?>">
                            <img class="last-viewed" src="/images/1-06-512.png" alt="/images/star.png">
                        </a>
                    </li>
                </ul>
            </div>
            <div class="last-group">
                <div class="col-md-6 search-block">
                    <div class="collapse navbar-collapse header-block">
                        <div class="form-group">
                            <?php
                            $form = ActiveForm::begin([
                                'method' => 'GET',
                                'action' => '/category/search/',
                                'options' => [
                                    'class' => 'topSearchForm'
                                ]
                            ]); ?>
                            <div class="input-group adv-search-device" id="adv-search">
                                <?= AutoComplete::widget([
                                    'name' => 'q',
                                    'id' => 'search-term',
                                    'options' => [
                                        'type' => 'search',
                                        'placeholder' => yii::t('app', 'Search'),
                                        'class' => 'form-control form-search-input',
                                        'title' => yii::t('app', 'Search'),
                                    ],

                                ]) ?>

                                <?php
                                if ($getCountryRegionList): ?>
                                    <div class="country-list-select">
                                        <?= Select2::widget([
                                            'name' => 'city',
                                            'theme' => Select2::THEME_BOOTSTRAP,
                                            'options' => [
                                                'class' => 'country-list-select2',
                                                'placeholder' => Yii::t('app', 'All cities'),
                                                'multiple' => true,
                                            ],
                                            'pluginOptions' => [
                                                'allowClear' => true,
                                            ],
                                            'data' => $getCountryRegionList
                                        ])
                                        ?>
                                    </div>
                                <?php endif; ?>

                                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>

                                <!--<input type="text" class="form-control form-search-input"
                                       placeholder="Search"/>-->
                                <div class="input-group-btn">
                                    <a class="srach-category" href="#">
                                        <span><?= Yii::t('app', 'All cities') ?></span>
                                        <b role="presentation"></b></a>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 header-created">
                    <div id="syi-create" class="dropdown-call-to-action ">
                        <a href="<?= Url::to('/profile/category/add') ?>" title="" data-ga-act="PostAdBegin"
                           data-ga-lbl="header"
                           id="syi-btn" class="btn btn-large btn-call-to-action trackClicks">
                            <?= Yii::t('app', 'Create ad') ?>
                        </a>
                    </div>
                </div>

                <div class="col-md-2">
                    <!-- Example split default button -->
                    <?php

                    if (Yii::$app->user->isGuest): ?>
                        <div class="btn-group" style="float: right;">
                            <a href="<?= Url::to(['/site/login']) ?>"
                               class="btn btn-default action-btn action-btn-login">
                                <i class="fa fa-sign-in hidden-xs"></i> <?= Yii::t('app','Log in') ?> <span
                                        class="hidden-lg hidden-md hidden-sm">/ <?= Yii::t('app', 'Create Account') ?></span>
                                <span class="txt_blue visible-xs"><strong>
                                        <?= Yii::t('app', 'NEW PROFILE') ?>
                                        ,</strong> <?= Yii::t('app', 'CHECK IT OUT') ?>!</span>
                            </a>
                        </div>
                    <?php else: ?>
                        <div class="btn-group share" style="float: right;">
                            <a href="<?= Url::toRoute(['/profile/product']) ?>"
                               class="btn btn-default action-btn my-profile-link">
                                <?= Yii::t('app', 'My Profile') ?></a>
                            <button type="button"
                                    class="profile-add btn btn-default dropdown-toggle dropdown-toggle-split action-split"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="<?= Url::toRoute(['/profile/product']) ?>">
                                    <?= Yii::t('app', 'My Ads') ?></a>
                                <a class="dropdown-item" href="<?= Url::toRoute(['/profile/product/preferred/']) ?>">
                                    <?= Yii::t('app', 'Favorites') ?>
                                </a>
                                <a class="dropdown-item" href="<?= Url::toRoute(['/profile/user/follow/']) ?>">
                                    <?= Yii::t('app', 'Sellers I follow') ?>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?= Url::toRoute(['/profile/user/update']) ?>">
                                    <?= Yii::t('app', 'My User Information') ?>
                                </a>
                                <a class="dropdown-item" href="<?= Url::toRoute(['/profile/user/change-password']) ?>">
                                    <?= Yii::t('app', 'Change Password') ?>
                                </a>

                                <a class="dropdown-item" href="<?= Url::toRoute(['/profile/message/index']) ?>">
                                    <?= Yii::t('app', 'Messages') ?>
                                </a>

                                <?php
                                $user = Yii::$app->user->identity;
                                echo Html::beginForm(['/site/logout'], 'post')
                                    . Html::submitButton(
                                        '  <i class="fa fa-sign-out"></i> ' . Yii::t('app', 'Logout') . ' (' . $user->first_name . ' ' . $user->last_name . ')',
                                        ['class' => 'btn btn-link logout']
                                    )
                                    . Html::endForm(); ?>

                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</nav>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="slide-nav">
    <div class="container">
        <div class="">

            <div class="navbar-header">
                <div class="col-xs-1 p0 visible-xs">
                    <a class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <i class="fa fa-chevron-left"></i>
                    </a>
                </div>

                <a class="navbar-brand" href="/"><img id="navbar-logo2" src="/images/logo2.png" alt="Logo"></a>
                <a data-toggle="modal" data-target="#myModal"
                   class="hidden-sm hidden-md hidden-lg visible-xs mobile-search"><i class="fa fa-search" aria-hidden="true"></i></a>
            </div>


            <div class="mobile-autocomplate">
                <?php
                $form = ActiveForm::begin([
                    'method' => 'GET',
                    'action' => '/category/search/',
                    'options' => [
                        'class' => 'topSearchForm'
                    ]
                ]); ?>
                <div class="input-group" id="adv-search">
                    <?= AutoComplete::widget([
                        'name' => 'q',
                        'id' => 'search-term-mobile',
                        'options' => [
                            'type' => 'search',
                            'placeholder' => yii::t('app', 'Search'),
                            'class' => 'form-control form-search-input form-search-input-mobile',
                            'title' => yii::t('app', 'Search'),
                        ],

                    ]) ?>

                    <?php
                    if ($getCountryRegionList): ?>
                        <div class="country-list-select">
                            <?= Select2::widget([
                                'name' => 'city',
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'class' => 'country-list-select2',
                                    'placeholder' => Yii::t('app', 'All cities'),
                                    'multiple' => true,
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                                'data' => $getCountryRegionList
                            ])
                            ?>
                        </div>
                    <?php endif; ?>


                    <!--<input type="text" class="form-control form-search-input"
                           placeholder="Search"/>-->
                    <div class="input-group-btn">
                        <a class="srach-category" href="#">
                            <span><?= Yii::t('app', 'All cities') ?></span>
                            <b role="presentation"></b></a>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>



            <div id="slidemenu">
                <div class="visible-xs logo-mobile">
                  <!--  <a href="index.php"><img src="/images/logo1223.png" alt="Logo"></a>-->
                </div>
                <ul class="navbar-nav navbar-right nav myprofile">
                    <li class="signin">
                        <?= Html::a(Yii::t('app', 'Create ad'), ['/profile/category/add'], ['class' => 'added mobile ']) ?>
                    </li>
                    <li class="signin">
                        <?php if (yii::$app->user->isGuest): ?>
                            <?= Html::a(Yii::t('app', 'Login'), ['site/login'], ['class' => 'login mobile']) ?>
                            <span class="span-left-right">|</span>
                            <?= Html::a(yii::t('app', 'Register'), ['site/signup'], ['class' => 'signup mobile']) ?>
                        <?php else: ?>
                            <?= Html::beginForm(['/site/logout'], 'post')
                            . Html::submitButton(
                                Yii::t('app', 'Logout') . ' (' . Yii::$app->user->identity->first_name . ' ' . Yii::$app->user->identity->last_name . ')',
                                ['class' => 'btn btn-link logout']
                            )
                            . Html::endForm(); ?>
                        <?php endif; ?>                    </li>
                </ul>
                <ul id="nav" class="navbar-nav navbar-right nav">
                    <li>
                        <a id="saved-ads" class="<?= $disabledClass ?>" href="<?= Url::to(['/category/preferred']) ?>"
                           data-toggle="tooltip" data-placement="bottom"
                           title="<?= $favoriteCount . ' ' . yii::t('app', 'Preferred') ?>">
                            <img src="/images/star.png" alt="/images/star.png">
                            <span class="clearfix"><?= yii::t('app', 'Preferred') ?></span>
                        </a>
                    </li>

                    <li class="nohover dropdown">
                        <a data-toggle="tooltip" data-placement="bottom" class="last-viewed"
                           data-original-title=""
                           href="<?= Url::to('/category/last-viewed/') ?>"
                           title="<?= yii::t('app', 'Last Watched') ?>">
                            <i class="fa fa-eye"></i>
                            <span class="clearfix"><?= yii::t('app', 'Last Watched') ?></span>
                        </a>
                    </li>

                    <li class="visible-xs nav-header">Setting</li>
                    <li class="nohover dropdown">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown"><i
                                    class="flag flag-18 flag-18-<?= Yii::$app->language ?>"></i> <span
                                    class="current_language visible-xs-inline-block">
                                <?= $sessionLang ? $sessionLang : ArrayHelper::getValue(Yii::$app->params['languages_long'], Yii::$app->language) ?>
                            </span></span>
                        </a>
                        <ul id="w9" class="row dropdown-menu">
                            <li class="<?= Config::getLangActive('en') ?>"><a
                                        href="<?= Url::to(['/site/index', 'language' => 'en']) ?>"><i
                                            class="flag flag-18 flag-18-en"></i> <?= Yii::$app->params['languages_long']['en'] ?>
                                </a></li>
                            <li class="<?= Config::getLangActive('ru') ?>"><a
                                        href="<?= Url::to(['/site/index', 'language' => 'ru']) ?>"><i
                                            class="flag flag-18 flag-18-ru"></i> <?= Yii::$app->params['languages_long']['ru'] ?>
                                </a></li>

                            <li class="<?= Config::getLangActive('lt') ?>"><a
                                        href="<?= Url::to(['/site/index', 'language' => 'lt']) ?>"><i
                                            class="flag flag-18 flag-18-lt"></i> <?= Yii::$app->params['languages_long']['lt'] ?>
                                </a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-left">
                    <li class="visible-xs nav-header">Web Page</li>

                    <div class="aside-sidenav-mobile">
                    <?= AsideMenuWidget::widget() ?>
                    </div>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php
$beginContent = Yii::t('app', 'Learn more about');
$this->registerJs('

    $(\'.search_m_menu\').click(function(){ $(\'.navbar-toggle\').click(), $(\'.mobile-search\').click()});


    //stick in the fixed 100% height behind the navbar but don\'t wrap it
    $(\'#slide-nav.navbar-inverse\').after($(\'<div class="inverse" id="navbar-height-col"></div>\'));
    $(\'#slide-nav.navbar-default\').after($(\'<div id="navbar-height-col"></div>\'));

    // Enter your ids or classes
    var toggler = \'.navbar-toggle\';
    var toggler_search = \'.search_m_menu\';
    var pagewrapper = \'#page-content\';
    var navigationwrapper = \'.navbar-header\';
    var menuwidth = \'80%\'; // the menu inside the slide menu itself
    var slidewidth = \'80%\';
    var menuneg = \'-80%\';
    var slideneg = \'-80%\';




    $("#slide-nav").on("click", toggler, function (e) {

        var selected = $(this).hasClass(\'slide-active\');

        $(\'#slidemenu\').stop().animate({
            left: selected ? menuneg : \'0px\'
        });

        $(pagewrapper).stop().animate({
            left: selected ? \'0px\' : slidewidth
        });

        $(navigationwrapper).stop().animate({
            left: selected ? \'0px\' : slidewidth
        });



        $(this).toggleClass(\'slide-active\', !selected);
        $(\'#slidemenu\').toggleClass(\'slide-active\');

        $(\'#page-content, .navbar, body, .navbar-header\').toggleClass(\'slide-active\');
        $(\'html\').toggleClass(\'main\');

    });


    var selected = \'#slidemenu, #page-content, body, .navbar, .navbar-header\';


    $(window).on("resize", function () {

        if ($(window).width() > 767 && $(\'.navbar-toggle\').is(\':hidden\')) {
            $(selected).removeClass(\'slide-active\');
            $(selected).removeClass(\'main\');
        }


    });


    /*$(".fa-search").click(function(){
        $("body").addClass("slide-active");
        $("html").addClass("main");
    });

    $("button.close").click(function(){
        $("body").removeClass("slide-active");
        $("html").removeClass("main");
    });*/


  jQuery(\'.topSearchForm\').keydown(function(event) { 
        if (event.keyCode == 13) {
           
           $( ".topSearchForm" ).submit()
        }
    });

function stripHTML(dirtyString) {
  var container = document.createElement(\'div\');
  var text = document.createTextNode(dirtyString);
  container.appendChild(text);
  return container.innerHTML; // innerHTML will be a xss safe string
}

 function autocomplate(id){
 $(id).autocomplete({
   html:true,
   source: function (request, response) {
       var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
       var slug = $(".country-list-select2").select2("val"); 
       $.ajax({
           url: "/category/search/?city="+slug,
           method: "get",
           data: { "term": request.term},
           dataType: "json",
           success: function (data) {
               
                     response( $.map( data, function(item) {
                     return {
                               category: item.category,
                                count: item.count,
                                label: item.title,
                                value: item.title,
                                category_id: item.category_id,
                                id: item.id,
                                
                            }
                     }));
                    
                    
               slugUrl = \'\';
               if(slug){    slugUrl = \'=\'+slug;    }
               $categoryText = stripHTML($( ".select-category option:selected" ).text());
               $categoryVal = stripHTML($( ".select-country option:selected" ).val());
           
                var searchTerm = $(id).val();
                
               $(".ui-autocomplete").append(\'<a href="/category/search/?q=\'+searchTerm+\'">\' +
                \'<li class="calltoactionV2 selected"><span class="simpleSearchSeeMoreRowRedesignIcon"></span>\' +
                \'<span class="text">' . $beginContent . ' «<b>\'+searchTerm+\'</b>» </span></a></li></a>\');
    
           }
       });

   },
 
    select: function(value, data){
          
    },
    
     
}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
   var valQ = $(id).val();
  return $( "<li>" )
    .data( "ui-autocomplete-item", item )
    .append( \'<a href="/category/search/?q=\'+valQ+\'&searchId=\'+ item.category_id+\'" ><span class="label-item">\' + item.label + "</span><span class=\'label-count\'>" + item.count + \'</span><br><span class="label-category">\'+ item.category + \'</a>\' )
    .appendTo( ul );
};

 }
 
 autocomplate("#search-term");
 autocomplate("#search-term-mobile");






');
?>


<!--  <li class="nav-header">
                <a class="btn btn-social-icon btn-facebook" target="_blank"
                   href="https://www.facebook.com/search?q=piv.lt"><span
                            class="fa fa-facebook"></span></a>
            </li>

            <li class="nav-header">
                <a class="btn btn-social-icon btn-google" target="_blank" href="https://plus.google.com/+dbadk"><span
                            class="fa fa-google"></span></a>
            </li>

            <li class="nav-header">
                <a class="btn btn-social-icon btn-twitter" target="_blank" href="https://twitter.com/piv"><span
                            class="fa fa-twitter"></span></a>
            </li>
-->