<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<div class="modal fade avilable-Enquiry-pop" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">

            <div class="modal-body sign-in-block active">
                <div id="registrationForm" class="form-horizontal user_login_div">

                    <div class="regSocial">

                        <div class="social_login">
                            <a data-eauth-service="google" href="/site/e-auth?service=google">
                                <div class="btn btn-block btn-large btn-google">
                                    <span class="icon-container"><i class="fa fa-google-plus"></i></span>
                                    <span class="text-container">Sign in with Google Plus</span>
                                </div>
                            </a>
                        </div>

                        <div class="social_login">
                            <a data-eauth-service="facebook" href="/site/e-auth?service=facebook">
                                <div class="btn btn-block btn-large btn-facebook">
                                    <span class="icon-container"><i class="fa fa-facebook"></i></span>
                                    <span class="text-container">Sign in with Facebook</span>
                                </div>
                            </a>
                        </div>

                    </div>
                    <div class="or"><span>or</span></div>
                    <div class="signup-or-separator">
                        <span class="h6 signup-or-separator--text">Sign in with Email</span>
                    </div>
                    <p class="bg-danger login_errors"></p>
                    <div class="row">
                        <div class="col-md-12">


                            <?php
                            $form = ActiveForm::begin([
                                'enableClientValidation' => true,
                                'enableAjaxValidation' => true,
                                'id' => 'login-form',
                                'action' => Url::to(['/login']),
                            ]); ?>

                            <?= $form->field($loginForm, 'email', [
                                'template' => '<div class="col-md-12">{input}{error}{hint}</div>',

                            ])->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Email Address')]) ?>

                            <?= $form->field($loginForm, 'password', [
                                'template' => '<div class="col-md-12">{input}{error}{hint}</div>',

                            ])->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Password')]) ?>


                            <div class="pull-left">
                                <div class="field-loginform-rememberme">
                                    <label>
                                        <?= $form->field($loginForm, 'rememberMe', [
                                            'template' => '{input}',
                                            'options' => [
                                                'tag' => false,
                                            ],
                                        ])->checkbox(['autofocus' => true], false) ?>
                                        <?= Yii::t('app', 'Remember Me') ?>
                                    </label>
                                </div>
                            </div>

                            <div class="pull-right">
                                <?= Html::a('Forgot password?', ['site/request-password-reset']) ?>
                            </div>

                            <div class="text-center">
                                <?= Html::submitButton('Sign in', ['id' => 'login-btn', 'class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>

                            <hr>
                            <span class="text-center">Don't have an account?  <span
                                        class="signup_span sign_up">Sign up </span> </span>
                        </div>
                    </div>


                </div>
            </div>

            <div class="modal-body sign-up-block">
                <div id="registrationForm" class="form-horizontal user_login_div">

                    <div class="regSocial">

                        <div class="social_login">
                            <a data-eauth-service="google" href="">
                                <div class="btn btn-block btn-large btn-google">
                                    <span class="icon-container"><i class="fa fa-google-plus"></i></span>
                                    <span class="text-container">Sign up with Google Plus</span>
                                </div>
                            </a>
                        </div>

                        <div class="social_login">
                            <a data-eauth-service="facebook" href="">
                                <div class="btn btn-block btn-large btn-facebook">
                                    <span class="icon-container"><i class="fa fa-facebook"></i></span>
                                    <span class="text-container">Sign up with Facebook</span>
                                </div>
                            </a>
                        </div>

                    </div>
                    <div class="or"><span>or</span></div>
                    <div class="signup-or-separator">
                        <span class="h6 signup-or-separator--text">Sign up with Email</span>
                    </div>
                    <p class="bg-danger login_errors"></p>
                    <div class="row">
                        <div class="col-md-12">

                            <?php $form = ActiveForm::begin([
                                'enableClientValidation' => true,
                                'enableAjaxValidation' => true,
                                'id' => 'form-signup',
                                'action' => Url::to(['/signup']),
                            ]); ?>

                            <?= $form->field($signupForm, 'email', [
                                'template' => '<div class="col-md-12">{input}{error}{hint}</div>',

                            ])->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Email Address')]) ?>

                            <?= $form->field($signupForm, 'password', [
                                'template' => '<div class="col-md-12">{input}{error}{hint}</div>',

                            ])->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Password')]) ?>


                            <?= $form->field($signupForm, 'first_name', [
                                'template' => '<div class="col-md-12">{input}{error}{hint}</div>',

                            ])->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'First Name')]) ?>

                            <?= $form->field($signupForm, 'last_name', [
                                'template' => '<div class="col-md-12">{input}{error}{hint}</div>',

                            ])->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Last Name')]) ?>

                            <?= $form->field($signupForm, 'birth_day', [
                                'template' => '<div class="col-md-12">{input}{error}{hint}</div>',

                            ])->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Birth Day')]) ?>

                            <?= $form->field($signupForm, 'adresse', [
                                'template' => '<div class="col-md-12">{input}{error}{hint}</div>',

                            ])->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Adresse')]) ?>


                            <?= $form->field($signupForm, 'city_id', [
                                'template' => '<div class="col-md-12">{input}{error}{hint}</div>',

                            ])->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'City Name')]) ?>

                            <?= $form->field($signupForm, 'phone', [
                                'template' => '<div class="col-md-12">{input}{error}{hint}</div>',

                            ])->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Phone')]) ?>



                            <div class="text-center">
                                <?= Html::submitButton('Signup', ['class' => 'btn btn-primary btn-block', 'name' => 'signup-button']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>

                            <hr>
                            <span class="text-center">Don't have an account?  <span
                                        class="signup_span sign_in">Sign in </span> </span>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
