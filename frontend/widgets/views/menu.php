<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
/** @var \common\models\Category $categories */
?>
<ul id="tree3">
    <?php foreach ($categories as $category): ?>
        <li class="branch">
            <i class="indicator glyphicon glyphicon-chevron-down"></i>
            <?=  Html::a(ArrayHelper::getValue($category, "name_".Yii::$app->language), '') ?>
        </li>
    <?php endforeach; ?>
</ul>


