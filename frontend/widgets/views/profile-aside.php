<?php

use yii\helpers\Url;

$user = Yii::$app->user->identity;
?>


<aside class="sidebar aside-sidenav sidebar-left">

    <div class="mydba-menu">
        <ul>
            <li>
                <a href="<?= Url::toRoute(['/profile/product/index'])?>">
                    <?= Yii::t('app','My ads')?>
                    <small>( <?= $countProduct ?> )</small>
                </a>
            </li>

            <li>
                <a href="<?= Url::toRoute(['/profile/product/preferred/'])?>">
                    <?= Yii::t('app','Favorites')?>
                    <small>( <?= $favoriteCount ?> )</small>
                </a>
            </li>

            <li>
                <a href="<?= Url::toRoute(['/profile/user/follow/'])?>">
                    <?= Yii::t('app','Sellers I follow')?>
                    <small>( <?= $sellersFollowCount ?> )</small>
                </a>
            </li>

            <li>
                <a href="<?= Url::toRoute(['/profile/user/update'])?>">
                    <?= Yii::t('app','My User Information')?>
                </a>
            </li>

            <li>
                <a href="<?= Url::toRoute(['/profile/user/change-password'])?>">
                    <?= Yii::t('app','Change Password')?>
                </a>
            </li>

            <li>
                <a href="<?= Url::toRoute(['/profile/message/index'])?>">
                    <?= Yii::t('app','Messages')?>
                </a>
            </li>
        </ul>
    </div>
</aside>