<?php

use common\models\CategoryField;


$modelCategoryFields = $model->categoryFields;
if (isset($id) && !is_null($id)) {
    $modelCategoryFields = $model->getCategoryFields($id);
}

foreach ($modelCategoryFields as $categoryField) :
    if ($categoryField->type == CategoryField::typeSelect) {
        echo $this->render('/category-field/field-ajax/select', ['categoryField' => $categoryField, 'model' => $model, 'fieldModel' => $fieldModel]);
    } elseif ($categoryField->type == CategoryField::typeCheckbox) {
        echo $this->render('/category-field/field-ajax/checkbox', ['categoryField' => $categoryField, 'model' => $model, 'fieldModel' => $fieldModel]);
    } elseif ($categoryField->type == CategoryField::typeRadio) {
        echo $this->render('/category-field/field-ajax/radio', ['categoryField' => $categoryField, 'model' => $model, 'fieldModel' => $fieldModel]);
    } elseif ($categoryField->type == CategoryField::typeInput) {
        echo $this->render('/category-field/field-ajax/input', ['categoryField' => $categoryField, 'model' => $model, 'fieldModel' => $fieldModel]);
    } elseif ($categoryField->type == CategoryField::typeTextArea) {
        echo $this->render('/category-field/field-ajax/textArea', ['categoryField' => $categoryField, 'model' => $model, 'fieldModel' => $fieldModel]);
    }
endforeach;
?>
