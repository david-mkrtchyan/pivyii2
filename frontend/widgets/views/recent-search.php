<?php

use common\models\Config;

$lang = Yii::$app->language;
$count = 0;
?>
<div class="your_recent_searches">
    <?= Yii::t('app', 'Your recent searches') ?>
</div>
<div class="search-recent-result">
    <?php if ($results && $id && $category): ?>
        <?php foreach ($results as $key => $result): ?>
            <?php if ($result): ?>
                <?php foreach ($result as $key2 => $value):
                    $class = 'show-search';
                    if ($count > 3) {
                        $class = 'hide-search';
                    }
                    ?>
                    <div class="search-link-div <?= $class ?>">
                        <a class="url-result-field"
                           href="?category_wish=<?= $id ?>&ProductField%5BallCategoryFields%5D%5B<?= $key ?>%5D=<?= $value ?>">
                            <?= '<i class="fa fa-search" aria-hidden="true"></i>' . $category['name_' . $lang] ?>
                            <?php if ($categoryField = \common\models\CategoryField::find()->where(['id' => intval($key)])->one()): ?>
                                <?php if ($categoryField->use_database && $categoryField->db_name): ?>
                                    <?php
                                    $table = Yii::$app->db->schema->getTableSchema("{{%$categoryField->db_name}}");

                                    if (!is_null($table)) {
                                        $value = intval($value);
                                        $post = Yii::$app->db->createCommand("SELECT *FROM $table->name
                                WHERE id = $value
                                 ")->queryOne();
                                        if ($post) {
                                            echo '<span class="span-result-field ' . $class . '">( ' . Config::replaceTitle($post['name_' . $lang], 25) . ' )</span>';
                                            $count++;
                                        }
                                    }
                                    ?>
                                <?php else: ?>
                                    <?php if ($categoryFieldValue = \common\models\CategoryFieldValue::find()->where(['id' => intval($value)])->one()): ?>
                                        <?= '<span  class="span-result-field ' . $class . '">( ' . Config::replaceTitle($categoryFieldValue['value_' . $lang], 25) . ' )</span>' ?>
                                        <?php $count++; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </a>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if ($count > 3): ?>
        <li id="section-toggle" class="single">
            <a href="#" class="show_hide_item_search">
                <i class="fa fa-search" aria-hidden="true"></i>
                <span data-expanded-text="Vis færre"><?= Yii::t('app', 'Show all search') ?></span>
                <input type="text" class="showAll" name="showAll" value="false" style="display:none;">
                <!--<i class="svg-arrow"></i>-->
            </a>
        </li>
    <?php endif; ?>
</div>
