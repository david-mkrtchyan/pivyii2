<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$cookiesFavourite = Yii::$app->request->cookies->getValue('favourite', []);
$currency = Yii::$app->session->get('currency');
$lang = Yii::$app->language;
?>
<?php if ($modelSame): ?>
    <div class="regular slider responsive-slider same-announcement">
        <?php foreach ($modelSame as $key => $value):
            $favorite = intval(in_array($value->id, $cookiesFavourite));
            if (!Yii::$app->user->isGuest) {
                $user = yii::$app->user->identity;
                if ($user->social_users_favourite) {
                    $favorite = intval(in_array($value->id, unserialize($user->social_users_favourite)));
                }
            }
            ?>
            <div>
                <div class="slides">
                    <div class="favourite" data-id="<?= $value->id; ?>" data-favourite="<?= $favorite ?>"><i
                                title="<?= yii::t('app', 'Saved Ads') ?>"
                                class="fa fa-star<?= $favorite ? '' : '-o' ?>"></i>
                    </div>
                    <a href="<?= Url::to(['/category/view', 'id' => $value->id]) ?>" tabindex="0">
                        <div class="slide-image">
                            <?php $img = $value->productFirstImage ? $value->productFirstImage : '/images/nopicture.png'; ?>
                            <img src="<?= $img; ?>" data-hover-img="<?= $img; ?>" class="flipimg">
                        </div>
                        <span class="slide-title"><?= $value->title ?></span>
                        <span class="slide-price">
                                <?php
                                if ($value->rate_id && $value->price): ?>
                                 <span class="other-date-cat" class=" other-date-page"><?php
                                echo \common\models\Currency::showCurrency($value->price, $value->rate['name_'.Yii::$app->language]);

                                endif; ?></span>


                        <span class="other-date-cat-other"><?=
                            Yii::t('app', ArrayHelper::getValue($value, "category.categoryFieldsWish.0.value_$lang"));
                            ?></span>
                    </span>
                    </a>
                </div>
            </div>

        <?php endforeach; ?>

    </div>
<?php endif; ?>