<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */

if ($data): ?>
    <?php foreach ($data as $val): ?>
        <div class="slider-parent-block">
            <?php foreach ($val as $value):
                if (!ArrayHelper::getValue($value, 'id')) {
                    return;
                }
                $cookiesFavourite = Yii::$app->request->cookies->getValue('favourite', []);
                $favorite = intval(in_array($value->id, $cookiesFavourite));
                $currency = Yii::$app->session->get('currency');

                if (!Yii::$app->user->isGuest) {
                    $user = yii::$app->user->identity;
                    if ($user->social_users_favourite) {
                        $favorite = intval(in_array($value->id, unserialize($user->social_users_favourite)));
                    }
                }
                ?>

                <div class="slider-item">
                    <div class="slider-item-other">
                        <div class="item-image">
                            <a href="<?= Url::to(["category/view", 'id' => $value->id]) ?>">
                                <img class="img-thumbnail" src="<?= $value->productFirstImage ? $value->productFirstImage : '/images/nopicture.png'; ?>">
                            </a>
                        </div>

                        <?php  if ($value->rate_id && $value->price): ?>
                        <div class="item-price">
                                        <span data-bind="text: Price">
  <?= \common\models\Currency::showCurrency(ArrayHelper::getValue($value, 'price'), ArrayHelper::getValue($value, 'rate.name_'.Yii::$app->language)) ?>

                                        </span>
                        </div>
                        <?php endif;?>

                        <div class="item-title">
                            <a href="<?= Url::to(["category/view", 'id' => $value->id]) ?>">
                                <span class="slide-title"><?= ArrayHelper::getValue($value, 'title') ?></span>
                            </a>
                        </div>

                        <div class="favourite" data-id="<?= $value->id; ?>" data-favourite="<?= $favorite ?>"><i
                                    title="<?= yii::t('app', 'Saved Ads') ?>"
                                    class="fa fa-star<?= $favorite ? '' : '-o' ?>"></i>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
